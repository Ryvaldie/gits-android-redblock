# RedBlock Application

## Persyaratan

 - [Android Studio 3.5 (recommended)](https://developer.android.com/studio/?gclid=CjwKCAiA_MPuBRB5EiwAHTTvMZc6KyJpu4OWqExIbeZ3QEF6WOjrkLVx2_IrYqKURPdux_8fc4bpjRoCf0QQAvD_BwE)..
 - Emulator / device dengan android version 4.1 (Jelly Bean) ke atas .

## Setup

 - Clone atau download redblok repository.
 - Buka project dengan `Android Studio`.
 - Pastikan emulator/device suadah dikonfigurasi.
 - Jalankan projek
 

# Build APK

## Build variant

   Terdapat tiga build variant pada projek ini, development, staging, dan production.

## Konfigurasi Build Variant

 - Buka `build.gradle` (app module).
 - Lihat blok `productFlavors`.
 - Konfigurasi setiap variant.
 

## Build APK dengan build variant

 - Klik `Build Variants` di bawah sebelah kiri.
 - Pilih salah satu `Active Build Variant`.
 - Pillih menu `Build` dan klik menu `Build Bundle(s) / APK(s)`.
 - Kemudian pilih `Build APK(s)`
    

# Flow Aplikasi

## Login

   Menampilkan halaman login, setelah login sukses akan nembak API getZPotensi dan akan diredirect ke halaman home apabila hit api getZpotensi mendapatkan response sukses

   - `getApiService -> login` digunakan untuk login 
   - `getApiService -> getZPotensi` digunakan untuk menghitung potensi di problem detail
    
## Home

   Terdapat empat menu yang terletak di button navigasi ( Dashboard, Cek Masalah, Upload Data dan Pengaturan )
    
   - `getApiService -> getNotificationList` digunakan untuk ngambil data list notification
    
## Dashboard

   Halaman untuk menampilkan dashboard list data color progress dan list data action progress
    
   - `getApiService -> getProgressBlock` digunakan untuk mengambil data list progress blok
   - `getApiService -> getProgressAction` digunakan untuk mengambil data list action progress
   
## Problem Chcek 

  Halaman untuk menampilkan list data problem yang dapat di filter berdasarkan color problemnya
  
   - `getApiService -> getProblemCheck` untuk mengambil data list problem check
  
## Update Masalah

   Halaman untuk menampilkan detail masalah, list data dan history masalahnya
   
   - `getApiService -> getProblapiemTarget` digunakan untuk mengambil data detail problem
   - `getApiService -> getProblem` digunakan untuk mengambil data problem
   - `getApiService -> getProblemHistory` digunakan untuk mengambil data history problem
   - `getApiService -> updateProblemTarget` digunakan untuk mengirim data terget masalah
   - `getApiService -> postProblem` digunakan untuk mengrim data masalah baru 
   - `getApiService -> updateProblem` digunakan untuk mengirim data update masalah
   - `getImangeService -> uploadImages` digunakan untuk upload image, file image yang dikirim bentuknya Multipart/file
  
## Detail Klasifikasi Masalah

   Halaman untuk create masalah baru atau edit masalah, data yang sudah dibuat atau diedit akan ditampilkan sebagai data draft dan ditampilkan dihalaman Masalah
   
## Upload data

   Halamn untuk Sinkron data antara data masalah dibackend dan mobile, menampilkan data masalah yang memiliki draf yang belum disimpan atau dikirim ke server, untuk api yang digunakan ketika simpan data sama seperti di halaman update masalah

   - `getApiService -> getAllData` digunakan untuk mengambil data masalah terbaru yang ada dibackend dan menyimpanya ke local data dimobile
   - `getApiService -> getZPotensi` digunakan untuk menghitung potensi di problem detail
   
# Pengatutan

   - `Petunjuk penggunaan` Menampilkan halan user guide
   - `Hapus Data Lama` Button untuk menghapus image yang disimpan di folder redblock dengan rentan waktuk 31 hari dari tanggal hari ini
   - `Keluar` Button untuk keluar dari aplikasi 
   
# Service

   - `FirebaseService` Digunakan untuk push notification 
   - `LocationUpdateService` Digunakan untuk getData Location terbaru


