package redblock.SSC.gits

import android.app.Application
import android.content.Context
import android.support.v7.app.AppCompatDelegate
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import io.fabric.sdk.android.Fabric
import redblock.co.gits.gitsdriver.utils.GitsHelper

/**
 * Created by irfanirawansukirman on 26/01/18.
 */
class GitsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())

        instance = this

        // Debug
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(GitsHelper.SystemLocale.onAttach(base, "en"))
    }

    companion object {
        lateinit var instance: GitsApplication

        fun getContext(): Context = instance.applicationContext
    }

}