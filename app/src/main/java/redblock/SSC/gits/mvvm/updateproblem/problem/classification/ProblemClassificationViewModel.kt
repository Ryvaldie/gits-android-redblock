package redblock.SSC.gits.mvvm.updateproblem.problem.classification

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import android.graphics.BitmapFactory
import id.zelory.compressor.Compressor
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import redblock.SSC.gits.data.model.ImageItemModel
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.data.param.ParamUpdateProblem
import redblock.SSC.gits.data.param.ProblemActionParam
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.SingleLiveEvent
import java.io.File


class ProblemClassificationViewModel(val context: Application, var repository: GitsRepository) : AndroidViewModel(context) {

    val edittableField = ObservableField(false)
    val successPost = SingleLiveEvent<Void>()
    val failPost = SingleLiveEvent<Void>()
    val showPostProgressEvent = ObservableField(false)
    var messageFail = ""
    val updatePrimaryProblem = SingleLiveEvent<Int>()
    val progressOverall = ObservableField<String>()
    val areaMasalahUpdate = ObservableField<String>()

    fun postUploadImages(paramCreateProblem: ParamCreateProblem) {
        val problemTemp = ArrayList<ProblemActionParam>()
        problemTemp.addAll(paramCreateProblem.problem_action ?: arrayListOf())
        val parts = ArrayList<MultipartBody.Part>()
        val problemIndex = ArrayList<Int>()
        val fileIndex = HashMap<Int, ArrayList<Int>>()
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = false
        for ((pIndex, problemAction) in problemTemp.withIndex()) {
            val indexes = ArrayList<Int>()
            var tempIndex = 0
            for ((index, file) in problemAction.files?.withIndex() ?: arrayListOf()) {
                if (file?.link?.contains("/") == true) {
                    if (!problemIndex.contains(pIndex)) {
                        problemIndex.add(pIndex)
                        tempIndex = pIndex
                    }
                    indexes.add(index)
                    val bitmapTemp = BitmapFactory.decodeFile(file.link, options)
                    if (bitmapTemp != null && bitmapTemp.width > 0 && bitmapTemp.height > 0) {
                        val filePath = Compressor.getDefault(context).compressToFile(File(file.link))
                        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), filePath)
                        parts.add(MultipartBody.Part.createFormData("upload", filePath.name, requestBody))
                    }
                }
            }
            if (!fileIndex.containsKey(tempIndex)) {
                fileIndex[tempIndex] = indexes
            }
        }

        if (parts.isEmpty()) {
            postProblem(paramCreateProblem)
        } else {
            repository.localDataSource.uploadImages(parts, object : GitsDataSource.GetUploadedImages {
                override fun onShowProgressDialog() {

                }

                override fun onHideProgressDialog() {

                }

                override fun onSuccess(data: List<String>) {
                    var count = 0
                    for (pIndex in problemIndex) {
                        val filesTemp = ArrayList<ImageItemModel?>()
                        filesTemp.addAll(problemTemp[pIndex].files ?: arrayListOf())
                        if (filesTemp.isNotEmpty()) {
                            for (index in fileIndex[pIndex] ?: arrayListOf()) {
                                if (count < data.size) {
                                    filesTemp[index]?.link = data[count]
                                    count++
                                }
                            }
                            paramCreateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        } else {
                            paramCreateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        }
                    }
                    postProblem(paramCreateProblem)
                }

                override fun onFinish() {

                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    showPostProgressEvent.set(false)
                    messageFail = errorMessage ?: ""
                    failPost.call()
                }

            })
        }
    }

    fun postProblem(paramCreateProblem: ParamCreateProblem) {
        repository.localDataSource.postProblem("", paramCreateProblem, object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: Any?) {
                showPostProgressEvent.set(false)
                successPost.call()
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                showPostProgressEvent.set(false)
                messageFail = errorMessage ?: ""
                failPost.call()
            }

        })
    }

    fun updateUploadImages(id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem) {
        val problemTemp = ArrayList<ProblemActionParam>()
        problemTemp.addAll(paramUpdateProblem.problem_action ?: arrayListOf())
        val parts = ArrayList<MultipartBody.Part>()
        val problemIndex = ArrayList<Int>()
        val fileIndex = HashMap<Int, ArrayList<Int>>()
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = false
        for ((pIndex, problemAction) in problemTemp.withIndex()) {
            val indexes = ArrayList<Int>()
            var tempIndex = 0
            for ((index, file) in problemAction.files?.withIndex() ?: arrayListOf()) {
                if (file?.link?.contains("/") == true) {
                    if (!problemIndex.contains(pIndex)) {
                        problemIndex.add(pIndex)
                        tempIndex = pIndex
                    }
                    indexes.add(index)
                    val bitmapTemp = BitmapFactory.decodeFile(file.link, options)
                    if (bitmapTemp != null && bitmapTemp.width > 0 && bitmapTemp.height > 0) {
                        val filePath = Compressor.getDefault(context).compressToFile(File(file.link))
                        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), filePath)
                        parts.add(MultipartBody.Part.createFormData("upload", filePath.name, requestBody))
                    }
                }
            }
            if (!fileIndex.containsKey(tempIndex)) {
                fileIndex[tempIndex] = indexes
            }
        }

        if (parts.isEmpty()) {
            updateProblem(id, targetId, paramUpdateProblem)
        } else {
            repository.localDataSource.uploadImages(parts, object : GitsDataSource.GetUploadedImages {
                override fun onShowProgressDialog() {

                }

                override fun onHideProgressDialog() {

                }

                override fun onSuccess(data: List<String>) {
                    var count = 0
                    for (pIndex in problemIndex) {
                        val filesTemp = ArrayList<ImageItemModel?>()
                        filesTemp.addAll(problemTemp[pIndex].files ?: arrayListOf())
                        if (filesTemp.isNotEmpty()) {
                            for (index in fileIndex[pIndex] ?: arrayListOf()) {
                                if (count < data.size) {
                                    filesTemp[index]?.link = data[count]
                                    count++
                                }
                            }
                            paramUpdateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        } else {
                            paramUpdateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        }
                    }
                    updateProblem(id, targetId, paramUpdateProblem)
                }

                override fun onFinish() {

                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    showPostProgressEvent.set(false)
                    messageFail = errorMessage ?: ""
                    failPost.call()
                }

            })
        }
    }

    fun updateProblem(id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem) {
        repository.localDataSource.updateProblem("", id, targetId, paramUpdateProblem, object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {
                showPostProgressEvent.set(false)
            }

            override fun onSuccess(data: Any?) {
                successPost.call()
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                messageFail = errorMessage ?: ""
                failPost.call()
            }

        })
    }

    fun editMode(boolean: Boolean) {
        edittableField.set(boolean)
    }

    fun getEditMode(): Boolean {
        return edittableField.get() ?: false
    }

}