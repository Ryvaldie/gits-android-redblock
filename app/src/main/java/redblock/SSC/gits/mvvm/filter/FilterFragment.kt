package redblock.SSC.gits.mvvm.filter


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_filter.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.FilterBlockModel
import redblock.SSC.gits.data.model.FilterModel
import redblock.SSC.gits.databinding.FragmentFilterBinding


class FilterFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentFilterBinding
    lateinit var mViewModel: FilterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentFilterBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as FilterActivity).obtainFilterViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : FilterUserActionListener {

            override fun onClickCheckBoxBlock(isChecked: Boolean, name: String) {
                if (isChecked) {
                    check_all_block.isChecked = false
                    if (mViewModel.blockCheckList.isNotEmpty()) {
                        mViewModel.blockCheckList += ",$name"
                    } else {
                        mViewModel.blockCheckList += name
                    }
                } else {
                    mViewModel.blockCheckList = mViewModel.blockCheckList.replace(",$name", "")
                            .replace(name, "")
                }
            }

            override fun onClickFrom(editText: EditText) {
                dialogDatePicker(editText, "")
            }

            override fun onClickTill(editText: EditText) {
                dialogDatePicker(editText, "")
            }

            override fun onClickWeekly() {
                mViewModel.weeklyField.set(true)
                mViewModel.monthlyField.set(false)
                mViewModel.yearlyField.set(false)
                mViewModel.customField.set(false)
            }

            override fun onClickMonthly() {
                mViewModel.monthlyField.set(true)
                mViewModel.weeklyField.set(false)
                mViewModel.yearlyField.set(false)
                mViewModel.customField.set(false)
            }

            override fun onClickYearly() {
                mViewModel.yearlyField.set(true)
                mViewModel.weeklyField.set(false)
                mViewModel.monthlyField.set(false)
                mViewModel.customField.set(false)
                mViewModel.startTime = ""
                mViewModel.endTime = ""
            }

            override fun onClickCustom() {
                mViewModel.customField.set(true)
                mViewModel.weeklyField.set(false)
                mViewModel.monthlyField.set(false)
                mViewModel.yearlyField.set(false)
            }

        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.start()

        from.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.startTime = from.text.toString()
//                mViewModel.endTime = addRunningMonth(mViewModel.startTime)
//                till.setText(mViewModel.endTime)
                validateMonthFilter(till, from.text.toString(), till.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        till.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.endTime = till.text.toString()
                validateMonthFilter(till, from.text.toString(), till.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        check_all_block.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mViewModel.blockCheckList = ""
                mViewModel.clearCheck()
            }
        }

        mViewModel.succesGetBlockEvent.observe(this, Observer {
            val filterJson = sharedPreferences.getString((activity as FilterActivity).type,
                    Gson().toJson(FilterModel(mViewModel.repository.localDataSource.getUserDBDao()
                            .getDataById(0)?.userConfig?.division ?: "")))
            val filter = Gson().fromJson(filterJson, FilterModel::class.java)

            when (filter.timeStat) {
                "custom" -> {
                    mViewModel.customField.set(true)
                    mViewModel.weeklyField.set(false)
                    mViewModel.monthlyField.set(false)
                    mViewModel.yearlyField.set(false)
                    if (filter.startTime.isNotEmpty() && filter.endTime.isNotEmpty()) {
                        from.setText(filter.startTime)
                        till.setText(filter.endTime)
                    }
                }
                "monthly" -> {
                    mViewModel.customField.set(false)
                    mViewModel.weeklyField.set(false)
                    mViewModel.monthlyField.set(true)
                    mViewModel.yearlyField.set(false)
                }
                else -> {
                    mViewModel.customField.set(false)
                    mViewModel.weeklyField.set(false)
                    mViewModel.monthlyField.set(false)
                    mViewModel.yearlyField.set(true)
                }
            }

            if (filter.block.isEmpty()) {
                check_all_block.isChecked = true
            } else {
                check_all_block.isChecked = false
                val tempBlock = ArrayList<FilterBlockModel>()
                tempBlock.addAll(mViewModel.blockList)
                mViewModel.blockList.clear()
                for (block in tempBlock) {
                    if (filter.block.contains(block.block ?: "", true)) {
                        mViewModel.blockCheckList += block.block + ","
                        mViewModel.blockList.add(FilterBlockModel(block.block, true))
                    } else {
                        mViewModel.blockCheckList = mViewModel.blockCheckList.replace(",${block.block}", "")
                                .replace(block.block ?: "", "")
                        mViewModel.blockList.add(FilterBlockModel(block.block, false))
                    }
                }
                mViewModel.blockCheckList = mViewModel.blockCheckList.dropLast(1)
            }

        })

    }

    private fun addRunningMonth(date: String): String {
        return if (date.contains("/")) {
            val fdate = date.split("/")
            val tmonth = fdate[0].toInt()
            val tyear = fdate[1].toInt() + 1
            "$tmonth/$tyear"
        } else {
            ""
        }
    }

    companion object {
        fun newInstance() = FilterFragment().apply {

        }

    }

}
