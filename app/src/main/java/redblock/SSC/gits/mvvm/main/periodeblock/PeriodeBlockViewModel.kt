package redblock.SSC.gits.mvvm.main.periodeblock;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.preference.PreferenceManager
import com.google.gson.Gson
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.mvvm.filter.FilterActivity
import redblock.SSC.gits.util.DateHelper


class PeriodeBlockViewModel(private val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val chartItemList = ObservableArrayList<Item>()
    val actionProgressItemList = ObservableArrayList<ProgressActionModel>()
    val userName = ObservableField<String>()
    val userDivision = ObservableField<String>()
    val onShowLoadEvent = ObservableField<Boolean>()
    var filter = FilterModel()
    private val tempDataBlock = ArrayList<Item>()
    private val tempDataAction = ArrayList<ProgressActionModel>()
    val isDetail = false

    fun start() {
        val filterJson = PreferenceManager.getDefaultSharedPreferences(context).getString(FilterActivity.FILTER_DASHBOARD,
                Gson().toJson(FilterModel(repository.localDataSource.getUserDBDao()
                        .getDataById(0)?.userConfig?.division ?: "")))
        filter = Gson().fromJson(filterJson, FilterModel::class.java)
        val data = getUserCredential()
        getProgressBlock(data)
        getProgressAction(data)
    }

    fun search(key: String) {
        tempDataBlock.apply {
            if (isEmpty()) {
                addAll(chartItemList)
            }
        }
        tempDataAction.apply {
            if (isEmpty()) {
                addAll(actionProgressItemList)
            }
        }
        chartItemList.apply {
            clear()
            addAll(tempDataBlock)
        }
        actionProgressItemList.apply {
            clear()
            addAll(tempDataAction)
        }
        if (key.isNotEmpty()) {
            for (dataBlock in tempDataBlock) {
                if (dataBlock.chartTitle?.contains(key, true) == false) {
                    chartItemList.remove(dataBlock)
                }
            }
            for (dataAction in tempDataAction) {
                if (dataAction.block?.contains(key, true) == false) {
                    actionProgressItemList.remove(dataAction)
                }
            }
        }
    }

    private fun getUserCredential(): UserConfigModel {
        val data = repository.localDataSource.getUserDBDao().getDataById(0)?.userConfig
                ?: UserConfigModel()
        val name = data.nik
        val division = data.division
        userName.set(name)
        userDivision.set(division)
        return data
    }

    fun setStateShowCase(state: Boolean) {
        repository.saveStateShowCase(state)
    }

    fun getStateShowCase() = repository.getStateShowCase()

    private fun getProgressBlock(data: UserConfigModel) {
        repository.getProgressBlock("", data.division, data.estate
                ?: "", DateHelper.getStartDate(), DateHelper.getEndDate(),
                object : GitsDataSource.GetProgressBlock {
                    override fun onShowProgressDialog() {
                        onShowLoadEvent.set(true)
                    }

                    override fun onHideProgressDialog() {
                        onShowLoadEvent.set(false)
                    }

                    override fun onSuccess(data: List<ProgressBlockModel?>?) {
                        chartItemList.clear()
                        val applyFilter = filter.block.isNotEmpty()
                        for (tempData in data ?: arrayListOf()) {
                            if (tempData != null) {
                                val color = ArrayList<Int>()
                                val date = ArrayList<String>()
                                val blockName = tempData.block ?: ""

                                for (tempProgress in tempData.colors ?: arrayListOf()) {
                                    date.add(DateHelper.getCharMonth(tempProgress?.month) ?: "")
                                    val colorId = when {
                                        tempProgress?.color?.toLowerCase() == "red" -> 0
                                        tempProgress?.color?.toLowerCase() == "orange" -> 1
                                        else -> 2
                                    }
                                    color.add(colorId)
                                }

                                if (applyFilter) {
                                    if (filter.block.split(",").contains(blockName)) {
                                        chartItemList.add(Item(blockName, color, date))
                                    }
                                } else {
                                    chartItemList.add(Item(blockName, color, date))
                                }
                            }
                        }
                    }

                    override fun onFinish() {

                    }

                    override fun onFailed(statusCode: Int, errorMessage: String?) {

                    }

                })
    }

    private fun getProgressAction(data: UserConfigModel) {
        val block = if (filter.block.isEmpty()) {
            null
        } else {
            filter.block
        }
        repository.getProgressAction("", data.division, data.estate ?: "", block
                , DateHelper.getStartDate(), DateHelper.getEndDate()
                , object : GitsDataSource.GetProgressAction {
            override fun onShowProgressDialog() {
                onShowLoadEvent.set(true)
            }

            override fun onHideProgressDialog() {
                onShowLoadEvent.set(false)
            }

            override fun onSuccess(data: List<ProgressActionModel>?) {
                actionProgressItemList.clear()
                val applyFilter = filter.block.isNotEmpty()
                for (tempData in data ?: arrayListOf()) {
                    if (tempData != null) {
                        if (applyFilter) {
                            if (filter.block.split(",").contains(tempData.block)) {
                                actionProgressItemList.add(tempData)
                            }
                        } else {
                            actionProgressItemList.add(tempData)
                        }
                    }
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {

            }

        })
    }

}