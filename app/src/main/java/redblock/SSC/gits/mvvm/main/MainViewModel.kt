package redblock.SSC.gits.mvvm.main

import android.app.Application
import com.google.firebase.iid.FirebaseInstanceId
import redblock.SSC.gits.data.model.TokenModel
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.SingleLiveEvent
import redblock.co.gits.gitsbase.BaseViewModel

/**
 * Created by irfanirawansukirman on 26/01/18.
 */
class MainViewModel(context: Application, val repository: GitsRepository) :
        BaseViewModel(context) {

    val clearMessageEvent = SingleLiveEvent<Void>()

    fun clear() {
        clearMessageEvent.call()
    }

    fun registerToken() {
        val token = TokenModel(
                FirebaseInstanceId.getInstance().token
        )
        repository.registerToken(repository.getToken(), token, object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {
                //Left empty
            }

            override fun onHideProgressDialog() {
                //Left empty
            }

            override fun onSuccess(data: Any?) {
                //Left empty
            }

            override fun onFinish() {
                //Left empty
            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                //Left empty
            }

        })
    }

    fun getNotifCount(): Int {
        return repository.getNotifCount()
    }

    fun saveNotifCount(count: Int) {
        repository.saveNotifCount(count)
    }

}