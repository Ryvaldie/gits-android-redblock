package redblock.SSC.gits.mvvm.updateproblem.problem.classification;

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.ObservableArrayList
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.toolbar.*
import permissions.dispatcher.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.data.model.local.HideDropDownModel
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.mvvm.service.LocationUpdateService
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction.ProblemActionFragment
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction.ProblemActionViewPagerAdapter
import redblock.SSC.gits.util.*


@RuntimePermissions
class ProblemClassificationActivity : BaseActivity() {

    private lateinit var back: ImageView
    private lateinit var title: TextView
    private lateinit var mainTitle: RelativeLayout
    private lateinit var notification: ImageView
    private lateinit var filter: ImageView
    private lateinit var save: TextView
    private lateinit var indicator: ImageView
    var typeAction = 1
    var data: ItemProblemModel = ItemProblemModel()
    var dataPost: Content = Content()
    var id = 1
    var problemArea = SingleLiveEvent<String>()
    var latLng = ""
    var primaryProblem = false
    var mAdapter: ProblemActionViewPagerAdapter? = null
    val hideDropDownModel = ObservableArrayList<HideDropDownModel>()
    var fragmentAction = ArrayList<ProblemActionFragment>()
    var isLocked = SingleLiveEvent<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_problem_classification)

        registerReceiver(connectionReceiver, IntentFilter(CONNECTIVITY_CHANGE))

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        back = toolbar.findViewById(R.id.btn_back)
        title = toolbar.findViewById(R.id.txt_toolbar_title)
        mainTitle = toolbar.findViewById(R.id.main_title)
        notification = toolbar.findViewById(R.id.notification)
        filter = toolbar.findViewById(R.id.filter)
        save = toolbar.findViewById(R.id.btn_save)
        indicator = toolbar.findViewById(R.id.indicator)
        initExtras()
        setupMainToolbar()
        setupFragment()

        ProblemClassificationActivityPermissionsDispatcher.requestLocationWithCheck(this)

        registerReceiver(locationReceiver, IntentFilter(LocationUpdateService.LOCATION_UPDATE))
    }

    fun setHideClasifikasi() {
        for(i in fragmentAction){
            hideDropDownModel.add(HideDropDownModel(i.mViewModel.stringHideDropDownModel))
        }
    }

    private fun initExtras() {
        typeAction = intent.getIntExtra("type", 1)
        if (intent.hasExtra("id")) {
            id = intent.getIntExtra("id", 1)
        }
        if (intent.hasExtra("data")) {
            val stringData = intent.getStringExtra("data")
            data = Gson().fromJson(stringData, ItemProblemModel::class.java)
        }
        if (intent.hasExtra("dataPost")) {
            val stringData = intent.getStringExtra("dataPost")
            dataPost = Gson().fromJson(stringData, Content::class.java)
        }
        if (intent.hasExtra("dataDraft")) {
            val stringData = intent.getStringExtra("dataDraft")
            val paramCreateProblem = Gson().fromJson(stringData, ParamCreateProblem::class.java)

            val problemAction = ArrayList<ItemProblemModel.ProblemAction>()
            //Reassigned problem action value
            paramCreateProblem.problem_action?.map {
                problemAction.add(
                        ItemProblemModel.ProblemAction(
                                PIC = it.PIC,
                                action_plan = it.action_plan,
                                cost = it.cost,
                                end_date = it.end_date,
                                finished = it.finished,
                                problem_detail = it.problem_detail,
                                problem_scale = it.problem_scale,
                                progress = it.progress,
                                start_date = it.start_date,
                                stat_cost = it.stat_cost,
                                type_unit_id = it.type_unit_id,
                                files = it.files,
                                problem_type_id = it.problem_type_id,
                                pokok = it.pokok,
                                kalibrasi = it.kalibrasi,
                                is_primary = it.is_primary,
                                is_locked = it.is_locked
                        )
                )
            }
            data.problem_detail?.problem_action = problemAction
        }
    }

    private fun setupMainToolbar() {
        toolbar.findViewById<ImageView>(R.id.indicator).gone()
        back.visible()
        back.setOnClickListener {
            onBackPressed()
        }
        title.visible()
        title.text = getString(R.string.text_problem_classification) + " - ${dataPost.block}"
        mainTitle.gone()
        notification.gone()
        filter.gone()
        save.gone()
        indicator.visible()
    }

    fun obtainProblemClassificationViewModel(): ProblemClassificationViewModel = obtainViewModel(ProblemClassificationViewModel::class.java)

    private val locationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            latLng = intent?.getStringExtra(LocationUpdateService.LOCATION_UPDATE) ?: ""
        }

    }

    private val connectionReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getBooleanExtra("STATUS", false) == true) {
                indicator.setImageResource(R.drawable.ic_online_indicator)
            } else {
                indicator.setImageResource(R.drawable.ic_offline_indicator)
            }
        }

    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        replaceFragmentInActivity(ProblemClassificationFragment.newInstance(), R.id.frame_main_content)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.findFragmentById(R.id.frame_main_content)?.onActivityResult(requestCode, resultCode, data)
    }

    @NeedsPermission(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun requestLocation() {
        startService(Intent(this, LocationUpdateService::class.java)
                .putExtra(LocationUpdateService.REQUEST_LOCATION, true))
    }

    @OnShowRationale(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun showRationaleForLocation(request: PermissionRequest) {
        showRationaleDialog(getString(R.string.text_storage_permission), request)
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    internal fun showDeniedForLocation() {
        toast(getString(R.string.text_storage_permission_denied))
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    internal fun showNeverAskForLocation() {
        showNeverAskAgainDialog(getString(R.string.text_open_setting_location_permission))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        ProblemClassificationActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    override fun onBackPressed() {
        if (sharedPreferences.getBoolean("ISEDIT", false)) {
            dialogBack()
        } else {
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(locationReceiver)
        unregisterReceiver(connectionReceiver)
    }

    companion object {

        fun startActivity(context: Activity, typeAction: Int, id: Int, dataGson: String, dataPost: String) {
            val intent = Intent(context, ProblemClassificationActivity::class.java)
            intent.putExtra("type", typeAction)
            intent.putExtra("id", id)
            intent.putExtra("data", dataGson)
            intent.putExtra("dataPost", dataPost)
            context.startActivityForResult(intent, CODE_PROBLEM_CLASSIFICATION)
        }

        fun startActivity(context: Activity, typeAction: Int, id: Int, dataPost: String) {
            val intent = Intent(context, ProblemClassificationActivity::class.java)
            intent.putExtra("type", typeAction)
            intent.putExtra("id", id)
            intent.putExtra("dataPost", dataPost)
            context.startActivityForResult(intent, CODE_PROBLEM_CLASSIFICATION)
        }

        fun startActivity(context: Activity, typeAction: Int, id: Int, dataGson: String, dataPost: String, dataDraft: String) {
            val intent = Intent(context, ProblemClassificationActivity::class.java)
            intent.putExtra("type", typeAction)
            intent.putExtra("id", id)
            intent.putExtra("data", dataGson)
            intent.putExtra("dataPost", dataPost)
            intent.putExtra("dataDraft", dataDraft)
            context.startActivityForResult(intent, CODE_PROBLEM_CLASSIFICATION)
        }

    }
}
