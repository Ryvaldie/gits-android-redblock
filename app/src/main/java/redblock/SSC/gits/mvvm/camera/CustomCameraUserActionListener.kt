package redblock.SSC.gits.mvvm.camera;


interface CustomCameraUserActionListener {

    fun onClickBack()

    fun onClickCapture()

    fun onClickDone()

    fun onClickCancel()

}