package redblock.SSC.gits.mvvm.userconfig


import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_user_config.*
import permissions.dispatcher.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.databinding.FragmentUserConfigBinding
import redblock.SSC.gits.mvvm.backgroundtask.AlarmReceiver
import redblock.SSC.gits.mvvm.main.MainActivity
import java.lang.NumberFormatException
import java.util.*

@RuntimePermissions
class UserConfigFragment : BaseFragment(), UserConfigUserActionListener {

    lateinit var mViewDataBinding: FragmentUserConfigBinding
    lateinit var mViewModel: UserConfigViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentUserConfigBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as UserConfigActivity).obtainUserConfigViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = this@UserConfigFragment

        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userConfig = mViewModel.repository.localDataSource.getUserDBDao().getDataById(0)

        if (userConfig != null) {
            mViewModel.divisiField.set(userConfig.userConfig?.division)
            mViewModel.reminderField.set(userConfig.userConfig?.reminder)
        }

        mViewModel.successEvent.observe(this, Observer {
            if ((activity as UserConfigActivity).editMode) {
                Handler().postDelayed({
                    MainActivity.startActivity(requireContext())
                    requireActivity().finishAffinity()
                }, 100)
            } else {
                mViewModel.getAllData(mViewModel.divisiField.get()?:"")
            }
        })

        mViewModel.successGetAllDataEvent.observe(this, Observer {
            if (it == true) {
                createAlarm(mViewModel.reminderField.get()?:"1")
                Handler().postDelayed({
                    MainActivity.startActivity(requireContext())
                    requireActivity().finishAffinity()
                }, 100)
            } else {
                toast("Error getting all data")
            }
        })

    }

    override fun onClickSave() {
        if (isValid()) {
            UserConfigFragmentPermissionsDispatcher.saveWithCheck(this)
        }
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun save() {
        mViewModel.save()
    }

    private fun isValid(): Boolean {
        return when {
            mViewModel.divisiField.get().isNullOrEmpty() -> {
                setDivisi.error = getString(R.string.text_must_be_filled)
                false
            }
            else -> true
        }
    }

    private fun createAlarm(reminder: String) {
        val alarmManager = requireContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val receiverIntent = Intent(context, AlarmReceiver::class.java)
        val local = mViewModel.repository.localDataSource
        val problemDetail = local.getDraftProblemTargetDao().getAllData()
        val problemList = local.getDraftProblemDBDao().getAllData()
        val notify = (problemDetail != null && problemDetail.isNotEmpty()) || (problemList != null && problemList.isNotEmpty())
        receiverIntent.putExtra("NOTIFY",  notify)
        val alarmIntent = receiverIntent.let { intent ->
            PendingIntent.getBroadcast(context, 123, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }
        val calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.DAY_OF_MONTH, 8)
            set(Calendar.HOUR_OF_DAY, 18)
            set(Calendar.MINUTE, 3)
        }

        val repeat = try {
            reminder.toInt()
        } catch (ex: NumberFormatException) {
            1
        }

        alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY * 7,
                alarmIntent
        )

    }

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showRationaleForWriteStorage(request: PermissionRequest) {
        showRationaleDialog(getString(R.string.text_storage_permission), request)
    }

    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    internal fun showDeniedForWriteStorage() {
        toast(getString(R.string.text_storage_permission_denied))
    }

    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    internal fun showNeverAskForStorage() {
        showNeverAskAgainDialog(getString(R.string.text_open_setting_storage_permission))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        UserConfigFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    companion object {
        fun newInstance() = UserConfigFragment().apply {

        }

    }

}
