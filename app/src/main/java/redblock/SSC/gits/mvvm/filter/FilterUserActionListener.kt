package redblock.SSC.gits.mvvm.filter;

import android.widget.EditText


interface FilterUserActionListener {

    fun onClickWeekly()

    fun onClickMonthly()

    fun onClickYearly()

    fun onClickCustom()

    fun onClickFrom(editText: EditText)

    fun onClickTill(editText: EditText)

    fun onClickCheckBoxBlock(isChecked: Boolean, name: String)

}