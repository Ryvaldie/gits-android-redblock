package redblock.SSC.gits.mvvm.problemcheck;

import android.databinding.BindingAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import redblock.SSC.gits.data.model.Content

/**
 * @author radhikayusuf.
 */

object ProblemCheckBindings {

    @BindingAdapter("app:problemCheckItems", "app:problemListener", "app:pagingProblem")
    @JvmStatic
    fun setupListProblem(recyclerView: RecyclerView, list: List<Content>, listener: ProblemCheckUserActionListener, page: Int) {
        recyclerView.apply {
            if (adapter == null) {
                adapter = ProblemCheckItemAdapter(ArrayList(0), listener)
                layoutManager = LinearLayoutManager(context)
            }
            (adapter as ProblemCheckItemAdapter).replaceList(list, page)
        }
    }

}