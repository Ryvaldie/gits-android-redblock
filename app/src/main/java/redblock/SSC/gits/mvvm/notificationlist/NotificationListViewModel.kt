package redblock.SSC.gits.mvvm.notificationlist;

import android.app.Application
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableList
import redblock.SSC.gits.data.model.ContentNotification
import redblock.SSC.gits.data.model.NotificationListModel
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.co.gits.gitsbase.BaseViewModel


class NotificationListViewModel(context: Application, private val gitsRepository: GitsRepository) : BaseViewModel(context) {

    val notificationListData: ObservableList<ContentNotification> = ObservableArrayList()
    val eventShowLoading = ObservableBoolean(false)

    fun start() {
        loadData()
    }

    private fun loadData() {
        gitsRepository.getNotificationList("", object : GitsDataSource.GetNotificationListCallback {
            override fun onShowProgressDialog() {
                eventShowLoading.set(true)
            }

            override fun onHideProgressDialog() {
                eventShowLoading.set(false)
            }

            override fun onSuccess(data: NotificationListModel) {
                notificationListData.apply {
                    clear()
                    addAll(data.content ?: listOf())
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                eventGlobalMessage.value = errorMessage
            }

        })
    }

}