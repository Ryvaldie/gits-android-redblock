package redblock.SSC.gits.mvvm.updateproblem.problem.classification;

import android.widget.EditText
import android.widget.TextView
import redblock.SSC.gits.data.model.DropDownModel
import redblock.SSC.gits.data.model.PicModel
import redblock.SSC.gits.data.model.UnitTypeModel


interface ProblemClassificationUserActionListener {

    fun onClickSave()

}