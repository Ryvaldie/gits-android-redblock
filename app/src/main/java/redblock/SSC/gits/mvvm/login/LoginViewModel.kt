package redblock.SSC.gits.mvvm.login;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.SharedPreferences
import android.databinding.ObservableField
import android.os.Environment
import android.os.Handler
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.Func
import redblock.SSC.gits.R
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.model.local.ProblemListDBModel
import redblock.SSC.gits.data.model.local.UserDBModel
import redblock.SSC.gits.data.param.ParamLogin
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.FILTER_DASHBOARD
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.FILTER_PROBLEM_CHECK
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.FILTER_UPLOAD_DATA
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.SingleLiveEvent
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class LoginViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val usernameField = ObservableField<String>()
    val passwordField = ObservableField<String>()
    val onShowLoadEvent = ObservableField(false)
    val loginSuccessEvent = SingleLiveEvent<String>()
    val showToastEvent = SingleLiveEvent<String>()
    val showDialogNoConnection = SingleLiveEvent<Void>()
    val imeiItems = SingleLiveEvent<List<ImeiModel>>()
    val imeiNotListed = SingleLiveEvent<Void>()

    lateinit var data: AllDataModel
    private lateinit var fetchConfiguration: FetchConfiguration
    private lateinit var fetch: Fetch
    private var token = ""

    lateinit var sharedPreferences: SharedPreferences

    fun getListImei() {
        repository.getListImei("", object : GitsDataSource.GetListImeiCallback {
            override fun onShowProgressDialog() {
                onShowLoadEvent.set(true)
            }

            override fun onHideProgressDialog() {
                onShowLoadEvent.set(false)
            }

            override fun onSuccess(data: List<ImeiModel>?) {
                imeiItems.value = data
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                imeiNotListed.call()
            }

        })
    }

    fun login() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

        val paramLogin = ParamLogin(
                usernameField.get() ?: "",
                GitsHelper.Func.getSHA1(passwordField.get() ?: "")
        )
        repository.login(paramLogin, object : GitsDataSource.GetLogin {
            override fun onShowProgressDialog() {
                onShowLoadEvent.set(true)
            }

            override fun onHideProgressDialog() {
                onShowLoadEvent.set(false)
            }

            override fun onSuccess(data: LoginModel) {
                token = data.token
                save(data)
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                onShowLoadEvent.set(false)
                if (statusCode == 501) {
                    showDialogNoConnection.call()
                } else {
                    showToastEvent.value = errorMessage
                }
            }

        })
    }

    fun getToken(): String {
        return repository.getToken()
    }

    fun getUserConfig(): UserConfigModel? {
        return repository.localDataSource.getUserDBDao().getDataById(0)?.userConfig
    }

    private fun saveUserConfig(userDBModel: UserDBModel) {
        repository.localDataSource.getUserDBDao().insertData(userDBModel)
    }

    fun save(data: LoginModel) {

        var divisi = ""
        if( data != null) {
            val tempDBModel = repository.localDataSource.getUserDBDao().getDataById(0)
            divisi = tempDBModel?.userConfig?.division ?: ""
        } else {
            val tempDBModel = repository.localDataSource.getUserDBDao().getDataById(0) ?: UserDBModel()
            tempDBModel.let {
                it.userConfig?.apply {
                    this.nik = data.user?.nik ?: ""
                    this.division = data.user?.division ?: ""
                    this.estate = data.user?.estate
                    this.region = data.user?.region
                    this.psm = data.user?.psm
                }
            }
            saveUserConfig(tempDBModel)
            divisi = tempDBModel.userConfig?.division ?: ""
        }

        var filter = Gson().fromJson(sharedPreferences.getString(FILTER_DASHBOARD,
                Gson().toJson(FilterModel(div = data.user?.division
                        ?: ""))), FilterModel::class.java)
        var filterModel = FilterModel(
                divisi,
                filter.timeStat, filter.block
                , sharedPreferences.getString("COLOR", "") ?: "",
                filter.endTime, filter.startTime
        )

        sharedPreferences.edit()
                .putString(FILTER_DASHBOARD, Gson().toJson(filterModel)).apply()

        filter = Gson().fromJson(sharedPreferences.getString(FILTER_PROBLEM_CHECK,
                Gson().toJson(FilterModel(div = data.user?.division
                        ?: ""))), FilterModel::class.java)

        filterModel = FilterModel(
                divisi,
                filter.timeStat, filter.block
                , sharedPreferences.getString("COLOR", "") ?: "",
                filter.endTime, filter.startTime
        )

        sharedPreferences.edit()
                .putString(FILTER_PROBLEM_CHECK, Gson().toJson(filterModel)).apply()

        filter = Gson().fromJson(sharedPreferences.getString(FILTER_UPLOAD_DATA,
                Gson().toJson(FilterModel(div = data.user?.division
                        ?: ""))), FilterModel::class.java)

        filterModel = FilterModel(
                divisi,
                filter.timeStat, filter.block
                , sharedPreferences.getString("COLOR", "") ?: "",
                filter.endTime, filter.startTime
        )

        sharedPreferences.edit()
                .putString(FILTER_UPLOAD_DATA, Gson().toJson(filterModel)).apply()

        getAllData(data)
    }

    fun getAllData(data: LoginModel) {
        repository.getAllData(token, data.user?.division ?: "", data.user?.psm
                ?: "", data.user?.region
                ?: "", true, object : GitsDataSource.GetAllData {
            override fun onShowProgressDialog() {
                onShowLoadEvent.set(true)
            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: AllDataModel) {
                createFilePath(data)
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                saveToken("")
                onShowLoadEvent.set(false)
                showToastEvent.value = errorMessage
            }

        })
    }

    fun createFilePath(data: AllDataModel) {
        fetchConfiguration = FetchConfiguration.Builder(context)
                .setDownloadConcurrentLimit(50)
                .enableFileExistChecks(false)
                .enableHashCheck(false)
                .build()
        fetch = Fetch.getInstance(fetchConfiguration)
        this.data = data
        try {
            val dir = File(Environment.getExternalStorageDirectory().path + "/RedBlock")
            if (!dir.exists()) {
                dir.mkdir()
            } else {
                deleteRecursive(dir)
                dir.mkdir()
            }

            val requests = ArrayList<Request>()
            for ((index1, content) in data.problem_check.withIndex()) {
                val itemProblemModel = content.problem_target?.problem ?: arrayListOf()
                for ((index2, item) in itemProblemModel.withIndex()) {
                    val problemAction = item.problem_detail?.problem_action ?: arrayListOf()
                    for ((index3, action) in problemAction.withIndex()) {
                        if (action != null) {
                            for ((index4, file) in (action.files ?: arrayListOf()).withIndex()) {
                                val fileName = Environment.getExternalStorageDirectory().path + "/RedBlock/" +
                                        "${file?.created_at
                                                ?: SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(Date())}$index1$index2$index3$index4.jpeg"
                                val request = Request(GitsHelper.Const.BASE_IMAGE_URL + (file?.link
                                        ?: ""), fileName)
                                request.priority = Priority.HIGH
                                request.networkType = NetworkType.ALL
                                request.addHeader("INDEX", "$index1,$index2,$index3,$index4")
                                requests.add(request)
                            }
                        }
                    }
                }
            }

            fetch.cancelAll()
            fetch.removeAll()
            fetch.enqueue(requests)

            fetch.getDownloads(Func {
                if (it.isNotEmpty()) {
                    if (!fetch.hasActiveDownloads) {
                        showToastEvent.value = context.getString(R.string.text_mengunduh_asset)
                        for (d in it) {
                            val indexes = d.headers["INDEX"] ?: ""
                            if (indexes.contains(",")) {
                                val index1 = indexes.split(",")[0].toInt()
                                val index2 = indexes.split(",")[1].toInt()
                                val index3 = indexes.split(",")[2].toInt()
                                val index4 = indexes.split(",")[3].toInt()
                                data.problem_check[index1].problem_target?.problem?.get(index2)?.problem_detail?.problem_action?.get(index3)?.files?.get(index4)?.link = d.file
                            }
                        }
                        if (it.isNotEmpty()) {
                            for (problemCheck in data.problem_check) {
                                repository.localDataSource.getProblemListDBDao().insertData(
                                        ProblemListDBModel(
                                                problemCheck.problem_target?.id,
                                                DateHelper.getTimeStamp(),
                                                problemCheck.problem_target?.problem
                                        )
                                )
                            }
                        }
                        fetch.close()
                        onShowLoadEvent.set(false)
                        loginSuccessEvent.value = token
                    }
                } else {
                    onShowLoadEvent.set(false)
                    loginSuccessEvent.value = token
                }
            })

        } catch (e: IOException) {
            saveToken("")
            onShowLoadEvent.set(false)
            showToastEvent.value = e.localizedMessage
        }
    }

    private fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory) {
            for (child in fileOrDirectory.listFiles()) {
                deleteRecursive(child)
            }
        }

        fileOrDirectory.delete()
    }

    fun saveToken(token: String) {
        repository.saveToken(token)
    }

}