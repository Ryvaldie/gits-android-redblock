package redblock.SSC.gits.mvvm.main

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import redblock.SSC.gits.mvvm.main.allblock.AllBlockFragment
import redblock.SSC.gits.mvvm.main.periodeblock.PeriodeBlockFragment

class MainViewPagerAdapter (var context: Context, private var mFragmentTitleList: List<String>, var fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return AllBlockFragment.newInstance(context)
        /*if (position == 0) {
            PeriodeBlockFragment.newInstance()
        } else {
            AllBlockFragment.newInstance(context)
        }*/
    }

    override fun getCount(): Int {
        return mFragmentTitleList.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return mFragmentTitleList[position]
    }

}
