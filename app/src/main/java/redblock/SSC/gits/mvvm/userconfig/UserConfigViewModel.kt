package redblock.SSC.gits.mvvm.userconfig;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import android.os.Environment
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.DownloadBlock
import com.tonyodev.fetch2core.Func
import redblock.SSC.gits.data.model.AllDataModel
import redblock.SSC.gits.data.model.FilterModel
import redblock.SSC.gits.data.model.UserConfigModel
import redblock.SSC.gits.data.model.local.ProblemListDBModel
import redblock.SSC.gits.data.model.local.UserDBModel
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.SingleLiveEvent
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class UserConfigViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context), FetchListener {

    val divisiField = ObservableField<String>()
    val reminderField = ObservableField("1")
    val successEvent = SingleLiveEvent<Void>()
    val successGetAllDataEvent = SingleLiveEvent<Boolean>()
    val showLoadEvent = ObservableField(false)
    lateinit var data: AllDataModel
    private val fetchConfiguration = FetchConfiguration.Builder(context)
            .setDownloadConcurrentLimit(15)
            .enableFileExistChecks(false)
            .build()
    private val fetch = Fetch.getInstance(fetchConfiguration)
    var fileSize = 0

    fun save() {
        val tempDBModel = repository.localDataSource.getUserDBDao().getDataById(0)
        repository.localDataSource.getUserDBDao().insertData(
                UserDBModel(0, DateHelper.getTimeStamp(),
                        UserConfigModel(
                                tempDBModel?.userConfig?.nik ?: "",
                                tempDBModel?.userConfig?.password ?: "",
                                divisiField.get() ?: "",
                                reminderField.get() ?: ""
                        )
                )
        )
        val filterJson = PreferenceManager.getDefaultSharedPreferences(context).getString("FILTER",
                Gson().toJson(FilterModel("div1", "yearly", "", "", "", "")))
        val filter = Gson().fromJson(filterJson, FilterModel::class.java)
        val filterModel = FilterModel(
                divisiField.get() ?: "",
                filter.timeStat, filter.block
                , PreferenceManager.getDefaultSharedPreferences(context)
                .getString("COLOR", "") ?: "",
                filter.endTime, filter.startTime
        )
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putString("FILTER", Gson().toJson(filterModel)).apply()
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putBoolean("REFRESH", true).apply()
        successEvent.call()
    }

    fun getAllData(div: String) {
        val data = repository.localDataSource.getUserDBDao().getAllData()?.filter {
            it.dateUpdated != null
        }
        val psm = data?.get(data.lastIndex)?.userConfig?.psm ?: ""
        val region = data?.get(data.lastIndex)?.userConfig?.region ?: ""
        repository.getAllData("", div, psm, region, true, object : GitsDataSource.GetAllData {
            override fun onShowProgressDialog() {
                showLoadEvent.set(true)
            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: AllDataModel) {
                createFilePath(data)
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                successGetAllDataEvent.value = false
            }

        })
    }

    fun createFilePath(data: AllDataModel) {
        this.data = data
        try {
//            val dir = Environment.getExternalStorageDirectory().path + "/RedBlock"
            for ((index1, content) in data.problem_check.withIndex()) {
                val itemProblemModel = content.problem_target?.problem ?: arrayListOf()
                for ((index2, item) in itemProblemModel.withIndex()) {
                    val problemAction = item.problem_detail?.problem_action ?: arrayListOf()
                    for ((index3, action) in problemAction.withIndex()) {
                        if (action != null) {
                            fileSize = 0
                            for ((index4, file) in (action.files ?: arrayListOf()).withIndex()) {
                                val fileName = Environment.getExternalStorageDirectory().path + "/RedBlock/" +
                                        "${file?.created_at?: SimpleDateFormat("ddMMyyyyHHmm", Locale.getDefault()).format(Date())}$index1$index2$index3$index4.jpeg"
                                val request = Request(file?.link ?: "", fileName)
                                fetch.remove(request.id)
                                request.addHeader("INDEX", "$index1,$index2,$index3,$index4")
                                fetch.enqueue(request, Func {
                                    fileSize += 1
                                }, Func {
                                    //Left empty
                                })
                            }
                        }
                    }
                }
            }
            if (fileSize > 0) {
                fetch.addListener(this)
            } else {
                showLoadEvent.set(false)
                successGetAllDataEvent.value = true
            }
        } catch (e: IOException) {
            showLoadEvent.set(false)
            successGetAllDataEvent.value = false
        }
    }

    override fun onAdded(download: Download) {

    }

    override fun onCancelled(download: Download) {

    }

    override fun onCompleted(download: Download) {
        if (::data.isInitialized) {
            fetch.getDownloadsWithStatus(Status.COMPLETED, Func {
                if (it.size >= fileSize) {
                    for (d in it) {
                        val indexes = d.headers["INDEX"] ?: ""
                        if (indexes.contains(",")) {
                            val index1 = indexes.split(",")[0].toInt()
                            val index2 = indexes.split(",")[1].toInt()
                            val index3 = indexes.split(",")[2].toInt()
                            val index4 = indexes.split(",")[4].toInt()
                            data.problem_check[index1].problem_target?.problem?.get(index2)?.problem_detail?.problem_action?.get(index3)?.files?.get(index4)?.link = d.fileUri.path
                        }
                    }
                    if (it.isNotEmpty()) {
                        for (problemCheck in data.problem_check) {
                            if (problemCheck.problem_target != null) {
                                if (problemCheck.problem_target.problem != null) {
                                    repository.localDataSource.getProblemListDBDao().insertData(
                                            ProblemListDBModel(
                                                    problemCheck.problem_target.id,
                                                    DateHelper.getTimeStamp(),
                                                    problemCheck.problem_target.problem
                                            )
                                    )
                                }
                            }
                        }
                    }
                    showLoadEvent.set(false)
                    successGetAllDataEvent.value = true
                }
            })
        }
    }

    override fun onDeleted(download: Download) {
        //Left empty
    }

    override fun onDownloadBlockUpdated(download: Download, downloadBlock: DownloadBlock, totalBlocks: Int) {
        //Left empty
    }

    override fun onError(download: Download, error: Error, throwable: Throwable?) {
        //Left empty
    }

    override fun onPaused(download: Download) {

    }

    override fun onProgress(download: Download, etaInMilliSeconds: Long, downloadedBytesPerSecond: Long) {

    }

    override fun onQueued(download: Download, waitingOnNetwork: Boolean) {
        //Left empty
    }

    override fun onRemoved(download: Download) {
        //Left empty
    }

    override fun onResumed(download: Download) {
        //Left empty
    }

    override fun onStarted(download: Download, downloadBlocks: List<DownloadBlock>, totalBlocks: Int) {
        //Left empty
    }

    override fun onWaitingNetwork(download: Download) {
        //Left empty
    }

}
