package redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction


import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.esafirm.imagepicker.features.ImagePicker
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_problem_action.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.model.local.HideDropDownModel
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.databinding.FragmentProblemActionBinding
import redblock.SSC.gits.mvvm.camera.CustomCameraActivity
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.ProblemClassificationActivity
import redblock.SSC.gits.util.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class ProblemActionFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentProblemActionBinding
    lateinit var mViewModel: ProblemActionViewModel
    private var isAttached = false
    var setCheck = false
    var firstOpenStartDate = 0
    var firstOpenEndDate = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentProblemActionBinding.inflate(inflater, container, false)
        isAttached = true
        mViewModel = obtainProblemActionViewModel(this)

        mViewDataBinding.mViewModel = mViewModel.apply {
            reloadImageEvent.observe(this@ProblemActionFragment, Observer {
                val files = ArrayList<ImageItemModel>()
                files.addAll(mViewModel.imageBeforeArray)
                files.addAll(mViewModel.imageAfterArray)
                problemAction.files = files
            })

            imagePreviewEvent.observe(this@ProblemActionFragment, Observer {
                if (it != null) {
                    dialogPreviewImage(it)
                }
            })

            updateLocation.observe(this@ProblemActionFragment, Observer {
                if (location.text.isNullOrEmpty()) {
                    latLangValue.set((activity as ProblemClassificationActivity).latLng)
                    (activity as ProblemClassificationActivity).latLng = ""
                    location.setText(latLangValue.get())
                }
            })

        }

        mViewDataBinding.mListener = object : ProblemActionUserActionListener {

            override fun onClickProblemClassification(editText: EditText, editTextId: EditText, textView: TextView, array: ArrayList<DropDownModel>) {
                dropdownViewClassification((activity as ProblemClassificationActivity).hideDropDownModel,editText, editTextId, textView, array)
                setEdit()
            }

            override fun onClickCekLocation() {

            }

            override fun onClickProblemUnit(editText: EditText, editTextId: EditText, array: ArrayList<UnitTypeModel>) {
                dropdownViewUnit(editText, editTextId, array)
            }

            override fun onClickStartDate(editText: EditText) {
                dialogDatePicker(editText, "")
            }

            override fun onClickCompletionDate(editText: EditText) {
                dialogDatePicker(editText, "")
            }

            override fun onClickPIC(editText: EditText, array: ArrayList<PicModel>) {
                dropdownViewPic(editText, array)
            }

            override fun onClickCameraAfter() {
                dialogCamera(IMAGE_AFTER)
            }

            override fun onClickCameraBefore() {
                dialogCamera(IMAGE_BEFORE)
            }

            override fun onClickReloadUnit() {
                getProblemUnit()
            }

        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.problemAction = if (arguments?.containsKey("PROBLEM_ACTION") == true) {
            mViewModel.editMode(
                    when ((activity as ProblemClassificationActivity).data.problem_detail?.stat?.toLowerCase(Locale.getDefault())) {
                        "approved", "waiting", "revision", "close", "waiting for approval" -> true
                        else -> false
                    }
            )
            mViewModel.editModeKemajuan(
                    when ((activity as ProblemClassificationActivity).data.problem_detail?.stat?.toLowerCase(Locale.getDefault())) {
                        "approved", "waiting", "revision", "close", "waiting for approval" -> true
                        else -> false
                    }
            )
            mViewModel.addAble.set(false)
            Gson().fromJson(arguments?.getString("PROBLEM_ACTION"), ItemProblemModel.ProblemAction::class.java)
        } else {
            mViewModel.addAble.set(true)
            mViewModel.editMode(false)
            mViewModel.editModeKemajuan(false)
            ItemProblemModel.ProblemAction()
        }
        mViewModel.getProblemType()
        if(mViewModel.problemAction.files?.size != 0){
            mViewModel.fileImage.set(true)
        } else {
            mViewModel.fileImage.set(false)
        }

        if(!mViewModel.getEditMode() && mViewModel.problemAction.is_locked == 1) {
            (activity as ProblemClassificationActivity).isLocked.value = 1
            mViewModel.isLocked.set(true)
            mViewModel.editMode(true)
            mViewModel.editModeKemajuan(false)
        }

        editTextPic.disableCopyPaste()
        problemClassification.disableCopyPaste()
        et_biaya.disableCopyPaste()
        et_ket_biaya.disableCopyPaste()
        etUnit.disableCopyPaste()
        startDate.disableCopyPaste()
        endDate.disableCopyPaste()

        if (mViewModel.problemAction == ItemProblemModel.ProblemAction()) {
            setValueField(mViewModel.problemAction)
            setObserver()
            titleLuasSkala.text = getString(R.string.text_area_scale)
            titleKemajuan.text = getString(R.string.text_progress)
            getProblemUnit()
            getPic()
        } else {
            setObserver()
            titleLuasSkala.text = getString(R.string.text_area_scale)
            titleKemajuan.text = getString(R.string.text_progress)
            getProblemUnit()
            getPic()
            setValueField(mViewModel.problemAction)
        }

        Handler().postDelayed({
            setCheck = true
        }, 500)

        var positionAdapter = (activity as ProblemClassificationActivity).mAdapter?.currentPosition
        if(positionAdapter == 0)primaryProblem.isChecked = true
        else {
            primaryProblem.isChecked = mViewModel.problemAction.is_primary == 1
        }

    }

    private fun setUnitField(data: ItemProblemModel.ProblemAction) {
        problemUnitId.setText(data.type_unit_id.toString())
        for (unit in mViewModel.problemUnitArray) {
            if (unit.id == data.type_unit_id) {
                etUnit.setText(unit.unit ?: "")
                break
            }
        }
    }

    private fun setValueField(data: ItemProblemModel.ProblemAction) {
        primaryProblem.isChecked = data.is_primary == 1
        if (data.is_primary == 1) {
            (activity as ProblemClassificationActivity).primaryProblem = true
        }
        et_luas_skala.setText(data.problem_scale.toString())
        val kemajuan = if (data.progress.isNullOrEmpty()) {
            "0"
        } else {
            data.progress
        }
        mViewModel.mKemajuan.set(kemajuan)
        et_kemajuan.setText(kemajuan)
        if(!data.finished.isNullOrEmpty()) {
            et_selesai.setText(String.format("%.2f", data.finished?.toDouble()?.times(100)))
        }
        mViewModel.kalibrasiValue.set(if (data.progress.isNullOrEmpty()) {
            ""
        } else {
            data.kalibrasi
        })
        mViewModel.jumlahSatuan.set(if (data.progress.isNullOrEmpty()) {
            ""
        } else {
            if (data.pokok.isNullOrEmpty()) {
                data.pokok
            } else {
                (data.pokok?.toFloat() ?: 0f).roundToInt().toString()
            }
        })
        val startDateText = if (data.start_date.isNullOrEmpty() || data.start_date == getString(R.string.text_select_month)) {
            getString(R.string.text_select_month)
        } else {
            DateHelper.dateTimeProblem(data.start_date) ?: data.start_date
        }
        startDate.setText(startDateText)
        val endDateText = if (data.end_date.isNullOrEmpty() || data.end_date == getString(R.string.text_select_month)) {
            getString(R.string.text_select_month)
        } else {
            DateHelper.dateTimeProblem(data.end_date) ?: data.end_date
        }
        endDate.setText(endDateText)
        val statCost = if (data.stat_cost.isNullOrEmpty()) {
            getString(R.string.sesuai_anggaran)
        } else {
            data.stat_cost
        }
        et_ket_biaya.setText(statCost)
        et_biaya.setText(DecimalFormatter.rupiahString(data.cost ?: ""))
        editTextPic.setText(data.PIC ?: "")
        et_rincian_masalah.setText(data.problem_detail ?: "")
        et_rinsian_aksi.setText(data.action_plan ?: "")
        val imageAfterArray: ArrayList<ImageItemModel> = arrayListOf()
        val imageBeforeArray: ArrayList<ImageItemModel> = arrayListOf()
        for (image in data.files ?: arrayListOf()) {
            if (image != null && imageBeforeArray.size < 4 && imageAfterArray.size < 4) {
                if (image.section == "before") {
                    imageBeforeArray.add(image)
                } else {
                    imageAfterArray.add(image)
                }
            }
        }

/*
        Handle jika ada image after, hapus image before dan image after menjadi image before
*/
        if (imageAfterArray.isNotEmpty()) {
            imageBeforeArray.apply {
                clear()
                addAll(imageAfterArray)
            }
            imageAfterArray.clear()
        }

        mViewModel.imageAfterArray.apply {
            clear()
            addAll(imageAfterArray)
        }
        recyclerViewAfter.adapter?.notifyDataSetChanged()
        mViewModel.imageBeforeArray.apply {
            clear()
            addAll(imageBeforeArray)
        }
        recyclerViewBefore.adapter?.notifyDataSetChanged()

        mViewModel.setFirstView((activity as ProblemClassificationActivity).typeAction == TYPE_POST || data == ItemProblemModel.ProblemAction())
    }

    private fun setObserver() {
        problemClassificationId.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (problemClassificationId.text.toString() != "0") {
                    mViewModel.problemAction.problem_type_id = problemClassificationId.text.toString()
                }
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        problemClassification.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(!s.toString().equals("")){
                    mViewModel.stringHideDropDownModel = s.toString()
                    (activity as ProblemClassificationActivity).hideDropDownModel.clear()
                    (activity as ProblemClassificationActivity).setHideClasifikasi()
                }
            }

        })


        primaryProblem.setOnTouchListener { _, event -> event?.actionMasked == MotionEvent.ACTION_MOVE; }

        primaryProblem.setOnClickListener {
            setEdit()
            if (primaryProblem.isChecked) {
                if ((activity as ProblemClassificationActivity).primaryProblem) {
                    dialogPrimaryProblem()
                } else {
                    (activity as ProblemClassificationActivity).primaryProblem = true
                    mViewModel.problemAction.is_primary = if (primaryProblem.isChecked) {
                        1
                    } else {
                        0
                    }
                }
            } else {
                mViewModel.problemAction.is_primary = if (primaryProblem.isChecked) {
                    (activity as ProblemClassificationActivity).primaryProblem = true
                    1
                } else {
                    0
                }
            }
        }

        etUnit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                for (unit in mViewModel.problemUnitArray) {
                    if (unit.unit?.contains(etUnit.text.toString()) == true) {
                        mViewModel.problemAction.type_unit_id = unit.id
                        mViewModel.problemAction.type_unit = UnitTypeModel(
                                id = mViewModel.problemAction.type_unit_id,
                                unit = unit.unit
                        )
                        break
                    }
                }

                mViewModel.calibrationMode.set(false)
                mViewModel.satuanMode.set(false)
                val unit = etUnit.text.toString().toLowerCase()

                mViewModel.areaMasalahUpdate.value = et_luas_skala.text.toString()

                val title = when {
                    unit.contains("ha") -> getString(R.string.text_area_scale)
                    unit.contains("m³") -> {
                        mViewModel.calibrationMode.set(false)
                        mViewModel.satuanMode.set(true)
                        if(mViewModel.getEditMode()) {
                            mViewModel.editSatuan.set(true)
                            mViewModel.edittableFieldSatuanMeter.set(true)
                        } else {
                            mViewModel.editSatuan.set(false)
                            mViewModel.edittableFieldSatuanMeter.set(false)
                        }
                        mViewModel.titleSatuan.set(resources.getString(R.string.text_volume))
                        getString(R.string.text_luas)
                    }
                    unit.contains("kg") -> {
                        mViewModel.calibrationMode.set(true)
                        mViewModel.satuanMode.set(true)
                        mViewModel.editSatuan.set(true)
                        mViewModel.edittableFieldSatuanMeter.set(true)
                        mViewModel.titleSatuan.set(etUnit.text.toString())
                        getString(R.string.text_luas)
                    }
                    unit.contains("m") -> {
                        mViewModel.calibrationMode.set(false)
                        mViewModel.satuanMode.set(true)
                        if(mViewModel.getEditMode()) {
                            mViewModel.editSatuan.set(true)
                            mViewModel.edittableFieldSatuanMeter.set(true)
                        } else {
                            mViewModel.editSatuan.set(false)
                            mViewModel.edittableFieldSatuanMeter.set(false)
                        }
                        mViewModel.titleSatuan.set(resources.getString(R.string.text_panjang))
                        getString(R.string.text_luas)
                    }
                    else -> {
                        mViewModel.calibrationMode.set(true)
                        mViewModel.satuanMode.set(true)
                        mViewModel.editSatuan.set(true)
                        mViewModel.edittableFieldSatuanMeter.set(true)
                        mViewModel.titleSatuan.set(etUnit.text.toString())
                        getString(R.string.text_luas)
                    }
                }
                titleLuasSkala.text = title
                titleKemajuan.text = getString(R.string.text_progress)

                count()

/*
                 Untuk mengecek pengeditan value
*/
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        et_luas_skala.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.problem_scale = et_luas_skala.text.toString()
                val luasSkala = et_luas_skala.text.toString()
                val kemajuan = et_kemajuan.text.toString()
                val jumlahSatuan = etJumlahSatuan.text.toString()
                val unit = etUnit.text.toString()
                    mViewModel.areaMasalahUpdate.value = luasSkala
                (activity as ProblemClassificationActivity).problemArea.value = luasSkala
                val masalahArea = (activity as ProblemClassificationActivity).problemArea.value
                        ?: ""
                if (masalahArea.isNotEmpty() && luasSkala.isNotEmpty() && kemajuan.isNotEmpty()) {
                    try {
                        if (luasSkala.toFloat() > 0) {
                            val unit = etUnit.text.toString().toLowerCase()
                            var selesai = 0F
                            if(unit.equals("pokok", ignoreCase = true) ||
                                    unit.equals("kg", ignoreCase = true)){
                                selesai = ((kemajuan.toFloat() / jumlahSatuan.toFloat()) * 100)
                                et_selesai.setText(String.format("%.2f", selesai))
                            } else {
                                selesai = ((kemajuan.toFloat() / luasSkala.toFloat()) * 100)
                                et_selesai.setText(String.format("%.2f", selesai))
                            }
                            val finished = selesai / 100
                            if(kemajuan.equals("0", true) != true ) {
                                mViewModel.problemAction.finished = finished.toString()
                            }
                            count()
                        } else {
                            mViewModel.clearJumlahSatuan()
                        }
                    } catch (ex: NumberFormatException) {
                        mViewModel.clearJumlahSatuan()
                        et_selesai.setText("0")
                    }
                } else {
                    mViewModel.clearJumlahSatuan()
                    et_selesai.setText("0")
                }
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

/*
        Nilai kemajuan tidak boleh lebih besar dari luas poko dan kemajuan tidak boleh kecil dari data laman (pre_prosess)
*/
        et_kemajuan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.progress = et_kemajuan.text.toString()
                val luasSkala = et_luas_skala.text.toString()
                val pokok = etJumlahSatuan.text.toString()
                val kemajuan = et_kemajuan.text.toString()
                val jumlahSatuan = etJumlahSatuan.text.toString()
                val unit = etUnit.text.toString().toLowerCase()
                val oldDataKemajuan = mViewModel.problemAction.pre_progress ?: 0.0F
                if (kemajuan.isNotEmpty() && luasSkala.isNotEmpty()) {
                    try {
                        if(!unit.equals("pokok", ignoreCase = true) &&
                                !unit.equals("kg", ignoreCase = true)){
                            if (kemajuan.toFloat() > luasSkala.toFloat()) {
                                et_kemajuan.error = getString(R.string.text_kemajuan_lebih_besar_masalah)
                                et_kemajuan.removeTextChangedListener(this)
                                et_selesai.setText("0")
                                et_kemajuan.setText("")
                                et_kemajuan.addTextChangedListener(this)
                            } else if (mViewModel.problemAction.pre_progress != 0.0F
                                    && kemajuan.toFloat() < oldDataKemajuan){
                                et_kemajuan.error = getString(R.string.text_kemajuan_lebih_besar_data_lama)
                                et_kemajuan.removeTextChangedListener(this)
                                et_selesai.setText("0")
                                mViewModel.problemAction.progress = ""
                                et_kemajuan.addTextChangedListener(this)
                            } else if (luasSkala.toFloat() > 0) {
                                val unit = etUnit.text.toString().toLowerCase()
                                var selesai = 0F
                                if(unit.equals("pokok", ignoreCase = true) ||
                                        unit.equals("kg", ignoreCase = true)){
                                    selesai = ((kemajuan.toFloat() / jumlahSatuan.toFloat()) * 100)
                                    et_selesai.setText(String.format("%.2f", selesai))
                                } else {
                                    selesai = ((kemajuan.toFloat() / luasSkala.toFloat()) * 100)
                                    et_selesai.setText(String.format("%.2f", selesai))
                                }
                                val finished = selesai / 100
                                if(unit.equals("", true) != true) {
                                    mViewModel.problemAction.finished = finished.toString()
                                }
                            }

                        } else {
                            if (kemajuan.toFloat() > pokok.toFloat()) {
                                et_kemajuan.error = getString(R.string.text_kemajuan_lebih_besar_masalah_pokok)
                                et_kemajuan.removeTextChangedListener(this)
                                et_selesai.setText("0")
                                et_kemajuan.setText("")
                                et_kemajuan.addTextChangedListener(this)
                            } else if (luasSkala.toFloat() > 0) {
                                val unit = etUnit.text.toString().toLowerCase()
                                var selesai = 0F
                                if(unit.equals("pokok", ignoreCase = true) ||
                                        unit.equals("kg", ignoreCase = true)){
                                    selesai = ((kemajuan.toFloat() / jumlahSatuan.toFloat()) * 100)
                                    et_selesai.setText(String.format("%.2f", selesai))

                                    if(mViewModel.problemAction.pre_progress != 0.0F
                                            && kemajuan.toFloat() < oldDataKemajuan){
                                        et_kemajuan.error = getString(R.string.text_kemajuan_lebih_besar_data_lama)
                                        et_kemajuan.removeTextChangedListener(this)
                                        et_selesai.setText("0")
                                        mViewModel.problemAction.progress = ""
                                        et_kemajuan.addTextChangedListener(this)
                                    }
                                } else {
                                    selesai = ((kemajuan.toFloat() / luasSkala.toFloat()) * 100)
                                    et_selesai.setText(String.format("%.2f", selesai))
                                }

                                val finished = selesai / 100
                                mViewModel.problemAction.finished = finished.toString()
                            }
                        }
                    } catch (ex: NumberFormatException) {

                    }
                } else {
                    et_selesai.setText("0")
                }
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        et_selesai.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                count()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        startDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.start_date = startDate.text.toString()
                if(firstOpenStartDate == 0) firstOpenStartDate = 1
                else dateValidation(startDate, endDate)
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        endDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.end_date = endDate.text.toString()
                if(firstOpenEndDate == 0) firstOpenEndDate = 1
                else dateValidation(startDate, endDate)
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        et_ket_biaya.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.stat_cost = et_ket_biaya.text.toString()
                if (et_ket_biaya.text.toString() == getString(R.string.sesuai_anggaran)) {
                    titleBiaya.gone()
                    et_biaya.gone()
                    mViewModel.problemAction.cost = ""
                } else {
                    titleBiaya.visible()
                    et_biaya.visible()
                }
                mViewModel.onBudgetField.set(et_ket_biaya.text.toString() == getString(R.string.tidak_sesuai_anggaran))
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        et_biaya.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.cost = et_biaya.text.toString().replace("Rp", "").replace(".", "").replace(" ", "")
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                et_biaya.removeTextChangedListener(this)
                et_biaya.setText(DecimalFormatter.rupiahString(et_biaya.text.toString()))
                et_biaya.setSelection(et_biaya.text.length)
                et_biaya.addTextChangedListener(this)
            }

        })

        editTextPic.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.PIC = editTextPic.text.toString()
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        et_rincian_masalah.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.problem_detail = et_rincian_masalah.text.toString()
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        et_rinsian_aksi.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.action_plan = et_rinsian_aksi.text.toString()
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        et_ket_biaya.setOnClickListener {
            dropdownView(et_ket_biaya, arrayListOf(getString(R.string.sesuai_anggaran), getString(R.string.tidak_sesuai_anggaran)))
        }

        etKalibrasi.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (etKalibrasi.text.toString().isNotEmpty()) {
                    val kalibrasi = etKalibrasi.text.toString().toFloat()
                    val kemajuan = et_kemajuan.text.toString()
                    if (kalibrasi.toInt() in 1..200) {
                        etKalibrasi.error = null
                        mViewModel.problemAction.kalibrasi = etKalibrasi.text.toString()
                        count()
                        var jumlahSatuan = mViewModel.jumlahSatuan.get().toString()
                        val unit = etUnit.text.toString().toLowerCase()
                        if(unit.equals("pokok", ignoreCase = true) ||
                                unit.equals("kg", ignoreCase = true)){
                            try {
                                val selesai = ((kemajuan.toFloat() / jumlahSatuan.toFloat()) * 100)
                                et_selesai.setText(String.format("%.2f", selesai))
                            }catch (ex: NumberFormatException ){
                                // Do nothing
                            }
                        }
                    } else {
                        etKalibrasi.removeTextChangedListener(this)
                        etKalibrasi.setText("")
                        etKalibrasi.error = "masukan nilai dari 1-200"
                        etKalibrasi.addTextChangedListener(this)
                        mViewModel.clearJumlahSatuan()
                    }
                }
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        (activity as ProblemClassificationActivity).problemArea.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                mViewModel.clearJumlahSatuan()
            } else {
                count()
            }
            if (setCheck) {
                setEdit()
            }
        })

        etJumlahSatuan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mViewModel.problemAction.pokok = etJumlahSatuan.text.toString()
                if (setCheck) {
                    setEdit()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        mViewModel.imageAfterEvent.observe(this, Observer {
            dialogCamera(IMAGE_AFTER)
        })

        mViewModel.imageBeforeEvent.observe(this, Observer {
            dialogCamera(IMAGE_BEFORE)
        })

        mViewModel.setProblemUnit.observe(this, Observer {
            setUnitField(mViewModel.problemAction)
        })

        mViewModel.setProblemType.observe(this, Observer {
            var id = 0
            var title = ""
            var description = ""
            if (it != null) {
                for (problem in it) {
                    if (problem.id == mViewModel.problemAction.problem_type_id?.toInt()) {
                        id = problem.id
                        title = problem.title
                        description = problem.description
                        break
                    }
                }
            }

            problemClassificationId.setText(id.toString())
            problemClassification.setText(title)
            problemClassificationDescription.text = description
        })

        mViewModel.progressOverall.observe(this, Observer {
            //Left empty
        })

        mViewModel.areaMasalahUpdate.observe(this, Observer {
            //Left empty
        })

    }

    private fun count() {
        val luasSkala = et_luas_skala.text.toString()
        val masalahArea = (activity as ProblemClassificationActivity).problemArea.value
                ?: ""
        val kemajuan = et_kemajuan.text.toString()
        val unit = etUnit.text.toString().toLowerCase(Locale.getDefault())
        mViewModel.countProgressOverall(unit, masalahArea, luasSkala, kemajuan, et_selesai.text.toString())
    }

    private fun setEdit() {
        sharedPreferences.edit().putBoolean("ISEDIT", true).apply()
    }

    private fun getProblemUnit() {
        mViewModel.errorProblemUnit.value = false
        mViewModel.getUnitType()
    }

    private fun getPic() {
        mViewModel.errorProblemPic.value = false
        mViewModel.getPic()
    }

    private fun dialogCamera(type: String) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_camera)
        dialog.setCanceledOnTouchOutside(false)
        val buttonCamera = dialog.findViewById<Button>(R.id.btn_dialog_camera)
        buttonCamera.setOnClickListener {
            dialog.dismiss()
            if (type == IMAGE_AFTER) {
                CustomCameraActivity.startActivity(requireActivity(), IMAGE_AFTER)
            } else {
                CustomCameraActivity.startActivity(requireActivity(), IMAGE_BEFORE)
            }
        }
        val buttonGallery = dialog.findViewById<Button>(R.id.btn_dialog_gallery)
        buttonGallery.setOnClickListener {
            dialog.dismiss()
            ImagePicker.create(this)
                    .toolbarImageTitle(getString(R.string.text_select_photo))
                    .showCamera(false)
                    .theme(R.style.AppTheme)
                    .start()
        }

        dialog.show()
    }

    private fun dialogPrimaryProblem() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_back)
        dialog.setCanceledOnTouchOutside(false)

        val subject = dialog.findViewById<TextView>(R.id.subject)
        subject.text = getString(R.string.text_change_primary_problem)
        dialog.findViewById<TextView>(R.id.subTitle).gone()

        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            (activity as ProblemClassificationActivity).primaryProblem = true
            primaryProblem.isChecked = true
            mViewModel.problemAction.is_primary = 1
            (activity as ProblemClassificationActivity).obtainProblemClassificationViewModel()
                    .updatePrimaryProblem.value = (activity as ProblemClassificationActivity).mAdapter?.position
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.setOnClickListener {
            primaryProblem.isChecked = false
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == IMAGE_RESULT && resultCode == Activity.RESULT_OK && ::mViewModel.isInitialized) {
            //Get location
            (activity as ProblemClassificationActivity).requestLocation()
            mViewModel.waitForLocationUpdate()

            setEdit()
            val type = data?.getStringExtra("TYPE")
            val link = data?.getStringExtra("LINK")

            if (type == IMAGE_AFTER) {
                mViewModel.imageAfterArray.add(
                        ImageItemModel(
                                created_at = DateHelper.getPhotoTimeStamp(),
                                link = link,
                                section = "after"
                        )
                )
                recyclerViewAfter.adapter?.notifyItemInserted(mViewModel.imageAfterArray.lastIndex)
            }
            if (type == IMAGE_BEFORE) {
                mViewModel.imageBeforeArray.add(
                        ImageItemModel(
                                created_at = DateHelper.getPhotoTimeStamp(),
                                link = link,
                                section = "before"
                        )
                )
                recyclerViewBefore.adapter?.notifyItemInserted(mViewModel.imageBeforeArray.lastIndex)
            }

            val files = ArrayList<ImageItemModel>()
            files.addAll(mViewModel.imageBeforeArray)
            files.addAll(mViewModel.imageAfterArray)
            mViewModel.problemAction.files = files

        }

    }

    private fun obtainProblemActionViewModel(fragment: ProblemActionFragment): ProblemActionViewModel {
        return ViewModelProviders.of(this, ActionViewModelFactory.getInstance(fragment))
                .get(ProblemActionViewModel::class.java)
    }

    override fun onStop() {
        super.onStop()
        isAttached = false
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && isAttached) {
            mViewModel = obtainProblemActionViewModel(this)
        }
    }

    companion object {

        const val IMAGE_RESULT = 120

        const val IMAGE_BEFORE = "IMAGE_BEFORE"

        const val IMAGE_AFTER = "IMAGE_AFTER"

        fun newInstance(problemAction: String) = ProblemActionFragment().putArgs {
            putString("PROBLEM_ACTION", problemAction)
        }

        fun newInstance() = ProblemActionFragment().apply {

        }

    }

}

class ActionViewModelFactory private constructor(
        private val gitsRepository: GitsRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) = with(modelClass) {
        when {
            isAssignableFrom(ProblemActionViewModel::class.java) ->
                ProblemActionViewModel(gitsRepository)
            else ->
                throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
        }
    } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: ActionViewModelFactory? = null

        fun getInstance(fragment: ProblemActionFragment) =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ActionViewModelFactory(
                            Injection.provideGitsRepository(fragment.requireContext()))
                            .also { INSTANCE = it }
                }
    }

}
