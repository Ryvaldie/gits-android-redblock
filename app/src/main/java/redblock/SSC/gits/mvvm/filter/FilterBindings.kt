package redblock.SSC.gits.mvvm.filter

import android.databinding.BindingAdapter
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import redblock.SSC.gits.data.model.FilterBlockModel

/**
 * @author radhikayusuf.
 */

object FilterBindings {

    @BindingAdapter("app:filterCheckItems", "app:filterListener", "app:pagingFilter")
    @JvmStatic
    fun setupListProblem(recyclerView: RecyclerView, list: List<FilterBlockModel>, listener: FilterUserActionListener, page: Int) {
        recyclerView.apply {
            adapter = FilterItemAdapter(ArrayList(0), listener)
            layoutManager = GridLayoutManager(context, 4)
            (adapter as FilterItemAdapter).replaceList(list, page)
        }
    }
}