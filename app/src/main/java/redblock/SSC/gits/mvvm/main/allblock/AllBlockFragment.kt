package redblock.SSC.gits.mvvm.main.allblock


import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_all_block.*
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.ProgressActionModel
import redblock.SSC.gits.databinding.FragmentAllBlockBinding
import redblock.SSC.gits.mvvm.detailprogressaction.DetailProgressActionActivity
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.REFRESH_DASHBOARD
import redblock.SSC.gits.mvvm.main.MainActivity


class AllBlockFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentAllBlockBinding
    lateinit var mViewModel: AllBlockViewModel
    var count = 0
    val isFirstInstall = "isFirstInstall"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentAllBlockBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as MainActivity).obtainAllBlockViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

            viewLoadListener.observe(this@AllBlockFragment, Observer {

                if(sharedPreferences.getBoolean(isFirstInstall, true)){
                    Handler().postDelayed({
                        if (activity != null) {
                            (activity as MainActivity).setupShowCaseDashboard()
                        }
                    }, 1000)
                    sharedPreferences.edit().putBoolean(isFirstInstall, false).commit()
                }
//                if (it == true) {
//                    count++
//                }
//                if (count == 2) {
//                    Handler().postDelayed({
//                        if (activity != null) {
//                            (activity as MainActivity).setupShowCaseDashboard()
//                        }
//                    }, 1000)
//                }
            })

        }

        mViewDataBinding.mListener = object : AllBlockUserActionListener {
            override fun onActionChartClick(progressActionModel: ProgressActionModel) {
                DetailProgressActionActivity.startActivity(requireContext(), progressActionModel)
            }

        }

        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.start()

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            (activity as MainActivity).obtainMainViewModel().clear()
            mViewModel.start()
        }

    }

    override fun onResume() {
        super.onResume()
        if (sharedPreferences.getBoolean(REFRESH_DASHBOARD, false)) {
            if (mViewDataBinding.mViewModel != null) {
                mViewModel.start()
                sharedPreferences.edit().putBoolean(REFRESH_DASHBOARD, false).apply()
            }
        }
    }

    companion object {
        fun newInstance(context: Context) = AllBlockFragment().apply {

        }

    }

}
