package redblock.SSC.gits.mvvm.notificationlist;

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_user_guide.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.util.obtainViewModel
import redblock.SSC.gits.util.replaceFragmentInActivity


class NotificationListActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_list)
        toolbar.setNavigationOnClickListener { super.onBackPressed() }

        setupFragment()
        registerReceiver(notificationReceiver, IntentFilter(NOTIFICATION))
    }

    fun obtainViewModel(): NotificationListViewModel = obtainViewModel(NotificationListViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        replaceFragmentInActivity(NotificationListFragment.newInstance(), R.id.frame_main_content)
    }

    private val notificationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getBooleanExtra("NOTIF", false) == true) {
                obtainViewModel().start()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(notificationReceiver)
    }

    override fun onResume() {
        super.onResume()

        //Clear badge count
        sendBroadcast(Intent(NOTIFICATION).putExtra("NOTIF", false))
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
    }

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, NotificationListActivity::class.java))
        }
    }
}
