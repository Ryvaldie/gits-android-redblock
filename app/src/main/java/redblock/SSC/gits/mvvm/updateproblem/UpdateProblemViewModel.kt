package redblock.SSC.gits.mvvm.updateproblem;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import redblock.SSC.gits.data.source.GitsRepository


class UpdateProblemViewModel(context: Application, private val repository: GitsRepository) : AndroidViewModel(context) {

    val showUpdateLoading = ObservableField(false)
    val hideButtonEvent = ObservableField(true)

}