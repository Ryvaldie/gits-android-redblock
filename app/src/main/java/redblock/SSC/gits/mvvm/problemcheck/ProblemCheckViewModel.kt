package redblock.SSC.gits.mvvm.problemcheck;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.os.Environment
import android.os.Handler
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.Func
import redblock.SSC.gits.data.model.AllDataModel
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.data.model.FilterModel
import redblock.SSC.gits.data.model.ProblemCheckModel
import redblock.SSC.gits.data.model.local.ProblemListDBModel
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.FILTER_PROBLEM_CHECK
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.SingleLiveEvent
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ProblemCheckViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val problemItems = ObservableArrayList<Content>()
    val showLoading = ObservableBoolean()
    val paging: Int? = 0
    val refreshList = SingleLiveEvent<ArrayList<Content>>()
    var searchData = ""

    init {
        getProblemCheck()
    }

    fun start() {
        getProblemCheck()
    }

    fun search(key: String) {
        if (key.isNotEmpty()) {
            refreshList.value = ArrayList(problemItems.filter {
                it.block.contains(key, true)
            })
        } else {
            refreshList.value = ArrayList(problemItems)
        }
    }

    private fun getProblemCheck() {
        val filterJson = PreferenceManager.getDefaultSharedPreferences(context).getString(FILTER_PROBLEM_CHECK,
                Gson().toJson(FilterModel(repository.localDataSource.getUserDBDao()
                        .getDataById(0)?.userConfig?.division ?: "")))
        val filter = Gson().fromJson(filterJson, FilterModel::class.java)
        val data = repository.localDataSource.getUserDBDao().getAllData()?.filter {
            it.dateUpdated != null
        }
        val psm = data?.get(data.lastIndex)?.userConfig?.psm ?: ""
        val region = data?.get(data.lastIndex)?.userConfig?.region ?: ""
        repository.getProblemCheck("", filter.div, filter.timeStat, filter.startTime,
                filter.endTime, filter.block, filter.color, psm, region,
                object : GitsDataSource.GetProblemCheckCallback {
                    override fun onShowProgressDialog() {
                        showLoading.set(true)
                    }

                    override fun onHideProgressDialog() {
                        showLoading.set(false)
                    }

                    override fun onSuccess(data: ProblemCheckModel) {
                        problemItems.apply {
                            clear()
                            addAll(data.content.filter {
                                it.block.contains(searchData, true)
                            })
                        }
                        refreshList.value = problemItems
                    }

                    override fun onFinish() {

                    }

                    override fun onFailed(statusCode: Int, errorMessage: String?) {
                        showLoading.set(false)
                    }

                })
    }

    fun getAllData() {
        val data = repository.localDataSource.getUserDBDao().getAllData()?.filter {
            it.dateUpdated != null
        }
        val div = data?.get(data.lastIndex)?.userConfig?.division ?: ""
        val psm = data?.get(data.lastIndex)?.userConfig?.psm ?: ""
        val region = data?.get(data.lastIndex)?.userConfig?.region ?: ""
        repository.getAllData("", div, psm, region, false, object : GitsDataSource.GetAllData {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: AllDataModel) {
                createFilePath(data)
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {

            }

        })
    }

    fun createFilePath(data: AllDataModel) {
        val fetchConfiguration = FetchConfiguration.Builder(context)
                .setDownloadConcurrentLimit(50)
                .enableFileExistChecks(false)
                .enableHashCheck(false)
                .build()
        val fetch = Fetch.getInstance(fetchConfiguration)
        try {
            val dir = File(Environment.getExternalStorageDirectory().path + "/RedBlock")
            if (!dir.exists()) {
                dir.mkdir()
            } else {
                deleteRecursive(dir)
                dir.mkdir()
            }

            val requests = ArrayList<Request>()
            for ((index1, content) in data.problem_check.withIndex()) {
                val itemProblemModel = content.problem_target?.problem ?: arrayListOf()
                for ((index2, item) in itemProblemModel.withIndex()) {
                    val problemAction = item.problem_detail?.problem_action ?: arrayListOf()
                    for ((index3, action) in problemAction.withIndex()) {
                        if (action != null) {
                            for ((index4, file) in (action.files ?: arrayListOf()).withIndex()) {
                                val fileName = Environment.getExternalStorageDirectory().path + "/RedBlock/" +
                                        "${file?.created_at
                                                ?: SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(Date())}$index1$index2$index3$index4.jpeg"
                                val request = Request(GitsHelper.Const.BASE_IMAGE_URL + (file?.link
                                        ?: ""), fileName)
                                request.priority = Priority.HIGH
                                request.networkType = NetworkType.ALL
                                request.addHeader("INDEX", "$index1,$index2,$index3,$index4")
                                requests.add(request)
                            }
                        }
                    }
                }
            }

            fetch.cancelAll()
            fetch.removeAll()
            fetch.enqueue(requests)

            fetch.getDownloads(Func {
                if (it.isNotEmpty()) {
                    if (!fetch.hasActiveDownloads) {
                        for (d in it) {
                            val indexes = d.headers["INDEX"] ?: ""
                            if (indexes.contains(",")) {
                                val index1 = indexes.split(",")[0].toInt()
                                val index2 = indexes.split(",")[1].toInt()
                                val index3 = indexes.split(",")[2].toInt()
                                val index4 = indexes.split(",")[3].toInt()
                                data.problem_check[index1].problem_target?.problem?.get(index2)?.problem_detail?.problem_action?.get(index3)?.files?.get(index4)?.link = d.file
                            }
                        }
                        if (it.isNotEmpty()) {
                            for (problemCheck in data.problem_check) {
                                repository.localDataSource.getProblemListDBDao().insertData(
                                        ProblemListDBModel(
                                                problemCheck.problem_target?.id,
                                                DateHelper.getTimeStamp(),
                                                problemCheck.problem_target?.problem
                                        )
                                )
                            }
                        }
                        Handler().postDelayed({
                            fetch.close()
                        }, 10000)
                    }
                }
            })

        } catch (e: Throwable) {
            // Left empty
        }
    }

    private fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory) {
            for (child in fileOrDirectory.listFiles()) {
                deleteRecursive(child)
            }
        }

        fileOrDirectory.delete()
    }

}