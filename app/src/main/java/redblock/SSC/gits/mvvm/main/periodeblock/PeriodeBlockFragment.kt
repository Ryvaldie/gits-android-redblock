package redblock.SSC.gits.mvvm.main.periodeblock


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_periode_block.*
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.ProgressActionModel
import redblock.SSC.gits.databinding.FragmentPeriodeBlockBinding
import redblock.SSC.gits.mvvm.detailprogressaction.DetailProgressActionActivity
import redblock.SSC.gits.mvvm.filter.FilterActivity
import redblock.SSC.gits.mvvm.main.MainActivity
import redblock.SSC.gits.mvvm.main.allblock.AllBlockUserActionListener
import redblock.SSC.gits.util.putArgs


class PeriodeBlockFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentPeriodeBlockBinding
    lateinit var mViewModel: PeriodeBlockViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentPeriodeBlockBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as MainActivity).obtainPeriodeBlockViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : AllBlockUserActionListener {
            override fun onActionChartClick(progressActionModel: ProgressActionModel) {
                DetailProgressActionActivity.startActivity(requireContext(), progressActionModel)
            }

        }



        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.start()

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            (activity as MainActivity).obtainMainViewModel().clear()
            mViewModel.start()
        }
    }

    companion object {
        fun newInstance() = PeriodeBlockFragment().putArgs {

        }

    }

}
