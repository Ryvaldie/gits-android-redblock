package redblock.SSC.gits.mvvm.login


import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.telephony.TelephonyManager
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_login.*
import permissions.dispatcher.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.databinding.FragmentLoginBinding
import redblock.SSC.gits.mvvm.main.MainActivity


@RuntimePermissions
class LoginFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentLoginBinding
    lateinit var mViewModel: LoginViewModel
    private var deviceId = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mViewDataBinding = FragmentLoginBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as LoginActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel

        mViewDataBinding.mListener = object : LoginUserActionListener {

            override fun login() {
                if (isValid()) {
                    hideShowKeyboard(true)
                    LoginFragmentPermissionsDispatcher.loginWithCheck(this@LoginFragment)
                }
            }

            override fun onTampilkanPassword() {
                if (edt_password.transformationMethod is PasswordTransformationMethod) {
                    edt_password.transformationMethod = null
                    btn_red_eye.setImageResource(R.drawable.ic_red_eye_visible)
                    edt_password.setSelection(edt_password.text.toString().length)
                } else {
                    edt_password.transformationMethod = PasswordTransformationMethod()
                    btn_red_eye.setImageResource(R.drawable.ic_red_eye_not_visible)
                    edt_password.setSelection(edt_password.text.toString().length)
                }
            }

        }

        when (checkImei()) {
            LISTED_LOGGED_IN -> {
                MainActivity.startActivity(requireContext())
                requireActivity().finish()
            }
            LISTED_NOT_LOGGED_IN -> {
                //Stay in login page
            }
            else -> LoginFragmentPermissionsDispatcher.checkDeviceIdWithCheck(this@LoginFragment)
        }

        return mViewDataBinding.root
    }

    private fun checkImei(): Int {
        mViewModel.apply {
            return if (getToken().isNotEmpty()) {
                if (getUserConfig() != null) {
                    LISTED_LOGGED_IN
                } else {
                    LISTED_NOT_LOGGED_IN
                }
            } else {
                LISTED_NOT_LOGGED_IN
            }
        }
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun login() {
        when (checkImei()) {
            LISTED_LOGGED_IN -> {
                MainActivity.startActivity(requireContext())
                requireActivity().finish()
            }
            LISTED_NOT_LOGGED_IN -> {
                mViewModel.login()
            }
            else -> LoginFragmentPermissionsDispatcher.checkDeviceIdWithCheck(this@LoginFragment)
        }
    }

    @SuppressLint("HardwareIds")
    @Suppress("deprecation")
    @NeedsPermission(Manifest.permission.READ_PHONE_STATE)
    fun checkDeviceId() {
        try {
            val telephonyManager = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            deviceId = telephonyManager.deviceId
            mViewModel.getListImei()
        } catch (ex: SecurityException) {
            toast(getString(R.string.text_phone_state_require_permission))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edt_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btn_red_eye.visibility = View.VISIBLE
            }

        })

        mViewModel.apply {

            loginSuccessEvent.observe(this@LoginFragment, Observer {
                if (it?.isNotEmpty() == true) {
                    mViewModel.saveToken(it)
                    MainActivity.startActivity(requireContext())
                    requireActivity().finishAffinity()
                }
            })

            showToastEvent.observe(this@LoginFragment, Observer {
                if (!it.isNullOrEmpty()) {
                    toast(it.toString())
                }
            })

            showDialogNoConnection.observe(this@LoginFragment, Observer {
                dialogNoInternet()
            })

            imeiItems.observe(this@LoginFragment, Observer {
                if (it != null && deviceId.isNotEmpty()) {
                    var isDeviceListed = false
                    for (data in it) {
                        if (data.imei == deviceId) {
                            isDeviceListed = true
                            break
                        } else {
                            isDeviceListed = false
                        }
                    }
                    mViewModel.repository.setDeviceListedStatus(isDeviceListed)
                    if (isDeviceListed) {
                        if (mViewModel.getToken().isNotEmpty()) {
                            if (mViewModel.getUserConfig() != null) {
                                MainActivity.startActivity(requireContext())
                                requireActivity().finish()
                            }
                        }
                    } else {
                        imeiNotListed.call()
                    }
                }
            })

            imeiNotListed.observe(this@LoginFragment, Observer {
                repository.saveToken("")
                dialogPopupImei(deviceId)
            })

        }

    }

    private fun isValid(): Boolean {
        val userName = mViewModel.usernameField.get()
        val password = mViewModel.passwordField.get()
        return if (!userName.isNullOrEmpty() &&
                !password.isNullOrEmpty()) {
            true
        } else if (userName.isNullOrEmpty()) {
            edt_username.error = getString(R.string.text_nik_kosong)
            false
        } else if (password.isNullOrEmpty()) {
            btn_red_eye.visibility = View.GONE
            edt_password.error = getString(R.string.text_password_kosong)
            false
        } else {
            false
        }
    }

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showRationaleForWriteStorage(request: PermissionRequest) {
        showRationaleDialog(getString(R.string.text_storage_permission), request)
    }

    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onWriteStorageNeverAskAgain() {
        showNeverAskAgainDialog(getString(R.string.text_open_setting_storage_permission))
    }

    @OnShowRationale(Manifest.permission.READ_PHONE_STATE)
    fun showRationaleForPhoneState(request: PermissionRequest) {
        showRationaleDialog(getString(R.string.text_phone_state_require_permission), request)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        LoginFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    companion object {

        private const val LISTED_LOGGED_IN = 0
        private const val NOT_LISTED = 1
        private const val LISTED_NOT_LOGGED_IN = 2

        fun newInstance() = LoginFragment().apply {

        }

    }

}
