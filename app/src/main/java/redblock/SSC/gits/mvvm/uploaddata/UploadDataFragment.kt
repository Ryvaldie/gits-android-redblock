package redblock.SSC.gits.mvvm.uploaddata


import android.app.Dialog
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_upload_data.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.databinding.FragmentUploadDataBinding
import redblock.SSC.gits.mvvm.main.MainActivity
import redblock.SSC.gits.mvvm.updateproblem.UpdateProblemActivity
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.ProblemClassificationActivity
import redblock.SSC.gits.util.TYPE_UPDATE


class UploadDataFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentUploadDataBinding
    lateinit var mViewModel: UploadDataViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentUploadDataBinding.inflate(inflater, container, false).apply {
            mViewModel = (activity as MainActivity).obtainUploadDataViewModel()

            mListener = object : UploadDataUserActionListener {

                override fun onClickHapus() {
                    if (mViewModel?.validateCheckedData() == true) {
                        dialogHapusConfirm(mViewModel)
                    }
                }

                override fun onClickProblemTarget(content: Content) {
                    UpdateProblemActivity.startActivity(requireContext(), Gson().toJson(content, Content::class.java))
                }

                override fun onClickProblem(itemProblemModel: ItemProblemModel, content: Content, paramCreateProblem: ParamCreateProblem) {
                    ProblemClassificationActivity.startActivity(requireActivity(), TYPE_UPDATE, itemProblemModel.id
                            ?: 0, Gson().toJson(itemProblemModel), Gson().toJson(content), Gson().toJson(paramCreateProblem))
                }

                override fun onClickUpload() {
                    if (mViewModel?.validateCheckedData() == true) {
                        dialogUploadConfirm()
                    }
                }

                override fun onClickDownload() {
                    mViewModel?.getAllData()
                }

            }

            setLifecycleOwner(this@UploadDataFragment)
        }

        mViewModel = mViewDataBinding.mViewModel!!

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setObserver()

    }

    private fun setObserver() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            mViewModel.getProblemData()
        }

        mViewModel.apply {

            showToastEvent.observe(this@UploadDataFragment, Observer {
                if (!it.isNullOrEmpty()) {
                    toast(it.toString())
                }
            })

            successUnduhData.observe(this@UploadDataFragment, Observer {
                check_all.isChecked = false
                if (it == true) {
                    dialogUploadSuccess()
                } else {
                    dialogUploadError()
                }
            })

            successUploadData.observe(this@UploadDataFragment, Observer {
                check_all.isChecked = false
                if (it == true) {
                    mViewModel.getProblemData()
                    dialogUploadSuccess()
                } else {
                    dialogUploadError()
                }
            })

            succesLoadData.observe(this@UploadDataFragment, Observer {
                check_all.isChecked = false
            })

        }

        check_all.setOnCheckedChangeListener { _, isChecked ->
            mViewModel.checkAll(isChecked, dataRec)
        }

    }

    fun dialogUploadConfirm() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_back)
        dialog.setCanceledOnTouchOutside(false)

        val subject = dialog.findViewById<TextView>(R.id.subject)
        subject.text = getString(R.string.text_confirm_upload)

        val subTitle = dialog.findViewById<TextView>(R.id.subTitle)
        subTitle.text = getString(R.string.text_data_upload_terhapus)

        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            mViewModel.uploadData()
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun dialogHapusConfirm(mViewModel: UploadDataViewModel?) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_back)
        dialog.setCanceledOnTouchOutside(false)

        val subject = dialog.findViewById<TextView>(R.id.subject)
        subject.text = getString(R.string.text_hapus_data_confirm)

        val subTitle = dialog.findViewById<TextView>(R.id.subTitle)
        subTitle.visibility = View.GONE

        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            mViewModel?.deleteData(dataRec)
            check_all.isChecked = false
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        mViewModel.getProblemData()
    }

    companion object {

        fun newInstance() = UploadDataFragment().apply {

        }

    }

}
