package redblock.SSC.gits.mvvm.detailprogressaction;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail_progress_action.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.data.model.ProgressActionModel
import redblock.SSC.gits.util.obtainViewModel
import redblock.SSC.gits.util.replaceFragmentInActivity


class DetailProgressActionActivity : BaseActivity() {

    var progressActionModel = ProgressActionModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_progress_action)

        val jsonData = intent.getStringExtra("PROGRESS_ACTION")
        progressActionModel = Gson().fromJson(jsonData, ProgressActionModel::class.java)

        toolbar.findViewById<TextView>(R.id.title).text = "Progress - ${progressActionModel.block}"
        toolbar.findViewById<ImageView>(R.id.btn_back).setOnClickListener { super.onBackPressed() }

        setupFragment()
    }

    fun obtainViewModel(): DetailProgressActionViewModel = obtainViewModel(DetailProgressActionViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        replaceFragmentInActivity(DetailProgressActionFragment.newInstance(), R.id.frame_main_content)
    }

    companion object {

        fun startActivity(context: Context, progressActionModel: ProgressActionModel) {
            context.startActivity(Intent(context, DetailProgressActionActivity::class.java)
                    .putExtra("PROGRESS_ACTION", Gson().toJson(progressActionModel)))
        }

    }
}
