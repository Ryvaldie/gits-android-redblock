package redblock.SSC.gits.mvvm.camera;

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction.ProblemActionFragment.Companion.IMAGE_RESULT
import redblock.SSC.gits.util.obtainViewModel
import redblock.SSC.gits.util.replaceFragmentInActivity


class CustomCameraActivity : BaseActivity() {

    var type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_camera)

        type = intent.getStringExtra("TYPE")

        setupFragment()
    }

    fun obtainCameraViewModel(): CustomCameraViewModel = obtainViewModel(CustomCameraViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        replaceFragmentInActivity(CustomCameraFragment.newInstance(), R.id.frame_main_content)
    }

    companion object {

        fun startActivity(context: Activity, type: String) {
            val intent = Intent(context, CustomCameraActivity::class.java).putExtra("TYPE", type)
            context.startActivityForResult(intent, IMAGE_RESULT)
        }

    }
}
