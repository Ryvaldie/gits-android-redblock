package redblock.SSC.gits.mvvm.detailprogressaction;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import redblock.SSC.gits.data.model.ProgressActionModel
import redblock.SSC.gits.data.source.GitsRepository


class DetailProgressActionViewModel(context: Application, repository: GitsRepository) : AndroidViewModel(context) {

    val actionProgressItemList = ObservableArrayList<ProgressActionModel>()
    var block = ObservableField<String>()
    val isDetail = true

    fun setUpProgress(progressActionModel: ProgressActionModel) {
        block.set(progressActionModel.block)
        for (action in progressActionModel.action ?: arrayListOf()) {
            actionProgressItemList.add(
                    ProgressActionModel(
                            arrayListOf(),
                            action?.classification,
                            "",
                            (action?.finished ?: 0).toString()
                    )
            )
        }
    }

}