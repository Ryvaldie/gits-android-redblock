package redblock.SSC.gits.mvvm.uploaddata;

import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.data.param.ParamCreateProblem


interface UploadDataUserActionListener {

    fun onClickProblem(itemProblemModel: ItemProblemModel, content: Content, paramCreateProblem: ParamCreateProblem) {}

    fun onClickProblemTarget(content: Content) {}

    fun onClickUpload() {}

    fun onClickDownload() {}

    fun onClickHapus() {}

}