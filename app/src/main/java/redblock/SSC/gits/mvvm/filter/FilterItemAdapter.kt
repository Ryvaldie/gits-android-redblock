package redblock.SSC.gits.mvvm.filter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.data.model.FilterBlockModel
import redblock.SSC.gits.databinding.ItemFilterCheckboxBinding
import redblock.SSC.gits.databinding.ItemProblemCheckBinding
import redblock.SSC.gits.mvvm.problemcheck.ProblemCheckUserActionListener

class FilterItemAdapter (var item: List<FilterBlockModel>, val listener: FilterUserActionListener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<FilterBlockModel>, page: Int) {
        this.item = item
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item.size)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return BeritaViewHolder(ItemFilterCheckboxBinding.inflate(LayoutInflater.from(p0.context), p0, false))
    }

    override fun getItemCount(): Int = item.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        (p0 as BeritaViewHolder).bind(item[p1], listener)
    }

    class BeritaViewHolder(val mViewDataBinding: ItemFilterCheckboxBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: FilterBlockModel, listener: FilterUserActionListener) {

            mViewDataBinding.apply {

                name = obj

                mListener = listener

            }

        }

    }

}
