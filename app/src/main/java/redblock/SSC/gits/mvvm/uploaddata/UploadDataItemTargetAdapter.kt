package redblock.SSC.gits.mvvm.uploaddata

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.data.param.ParamUpdateProblemTarget
import redblock.SSC.gits.databinding.ItemUploadDataTargetBinding

class UploadDataItemTargetAdapter(var item: List<ParamUpdateProblemTarget>, val listener: UploadDataUserActionListener,
                                  private val uploadDataViewModel: UploadDataViewModel)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<ParamUpdateProblemTarget>, page: Int) {
        this.item = item
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item.size)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return DataViewHolder(ItemUploadDataTargetBinding.inflate(LayoutInflater.from(p0.context), p0, false), uploadDataViewModel)
    }

    override fun getItemCount(): Int = item.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        (p0 as DataViewHolder).bind(item[p1], listener)
    }

    class DataViewHolder(val mViewDataBinding: ItemUploadDataTargetBinding, private val uploadDataViewModel: UploadDataViewModel) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: ParamUpdateProblemTarget, listener: UploadDataUserActionListener) {

            mViewDataBinding.apply {
                mData = obj
                content = uploadDataViewModel.repository.getBlockContent(obj.problem_check_id ?: 0)
                achievement = obj.achievement + " %"

                blockName.setOnCheckedChangeListener { _, isChecked ->
                    val index = uploadDataViewModel.uploadProblemTargetItems.indexOf(obj)
                    if (index != -1) {
                        uploadDataViewModel.uploadProblemTargetItems[index].isChecked = isChecked
                    }
                }

                mListener = listener

            }
        }

    }

}
