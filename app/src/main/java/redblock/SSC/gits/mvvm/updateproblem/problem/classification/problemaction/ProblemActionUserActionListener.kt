package redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction;

import android.widget.EditText
import android.widget.TextView
import redblock.SSC.gits.data.model.DropDownModel
import redblock.SSC.gits.data.model.PicModel
import redblock.SSC.gits.data.model.UnitTypeModel


interface ProblemActionUserActionListener {

    fun onClickProblemUnit(editText: EditText, editTextId: EditText, array: ArrayList<UnitTypeModel>)

    fun onClickReloadUnit()

    fun onClickStartDate(editText: EditText)

    fun onClickCompletionDate(editText: EditText)

    fun onClickPIC(editText: EditText, array: ArrayList<PicModel>)

    fun onClickCameraAfter()

    fun onClickCameraBefore()

    fun onClickCekLocation()

    fun onClickProblemClassification(editText: EditText, editTextId: EditText, textView: TextView, array: ArrayList<DropDownModel>)

}