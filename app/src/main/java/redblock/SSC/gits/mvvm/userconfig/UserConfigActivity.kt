package redblock.SSC.gits.mvvm.userconfig;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.util.obtainViewModel
import redblock.SSC.gits.util.replaceFragmentInActivity


class UserConfigActivity : BaseActivity() {

    var editMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_config)

        editMode = intent.getBooleanExtra("EDIT", false)

        setupFragment()
    }

    fun obtainUserConfigViewModel(): UserConfigViewModel = obtainViewModel(UserConfigViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        replaceFragmentInActivity(UserConfigFragment.newInstance(), R.id.frame_main_content)
    }

    override fun onBackPressed() {
        if (editMode) {
            super.onBackPressed()
        }
    }

    companion object {

        fun startActivity(context: Context, editMode: Boolean) {
            context.startActivity(Intent(context, UserConfigActivity::class.java)
                    .putExtra("EDIT", editMode))
        }

    }
}
