package redblock.SSC.gits.mvvm.detailprogressaction


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import redblock.SSC.gits.data.model.ProgressActionModel
import redblock.SSC.gits.databinding.FragmentDetailProgressActionBinding
import redblock.SSC.gits.mvvm.main.allblock.AllBlockUserActionListener


class DetailProgressActionFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentDetailProgressActionBinding
    lateinit var mViewModel: DetailProgressActionViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentDetailProgressActionBinding.inflate(inflater, container, false)
        mViewModel = (activity as DetailProgressActionActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : AllBlockUserActionListener {
            override fun onActionChartClick(progressActionModel: ProgressActionModel) {

            }

        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.setUpProgress((activity as DetailProgressActionActivity).progressActionModel)

    }

    companion object {
        fun newInstance() = DetailProgressActionFragment().apply {

        }

    }

}
