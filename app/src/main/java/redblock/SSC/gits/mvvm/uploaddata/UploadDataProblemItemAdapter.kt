package redblock.SSC.gits.mvvm.uploaddata

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.R
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.databinding.ItemProblemUploadDataBinding
import redblock.SSC.gits.util.DateHelper

class UploadDataProblemItemAdapter(var item: MutableList<Content>, val listener: UploadDataUserActionListener,
                                   private val uploadDataViewModel: UploadDataViewModel)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<Content>, page: Int) {
        this.item.apply {
            clear()
            addAll(item)
        }
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item.size)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return DataViewHolder(ItemProblemUploadDataBinding.inflate(LayoutInflater.from(p0.context), p0, false))
    }

    override fun getItemCount(): Int = item.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        (p0 as DataViewHolder).bind(item[p1], listener, uploadDataViewModel)
    }

    class DataViewHolder(val mViewDataBinding: ItemProblemUploadDataBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: Content, listener: UploadDataUserActionListener, uploadDataViewModel: UploadDataViewModel) {

            mViewDataBinding.apply {
                content = obj
                val unit = content?.problem_target?.type_unit?.unit ?: ""
                totalArea = content?.problem_target?.area.toString() + " " + unit
                date = DateHelper.dateTimeMonth(content?.updated_at)
                val id = when ((content?.status_color_id ?: 0) - 1) {
                    -1 -> 0
                    else -> (content?.status_color_id ?: 0) - 1
                }
                val resId = root.context.resources
                        .getIdentifier(root.context.resources
                                .getStringArray(R.array.circleId)[id], "drawable",
                                root.context?.packageName)
                colorIndicator.setImageResource(resId)
                val colorId = root.context.resources.getStringArray(R.array.colorId)
                colorText.setTextColor(Color.parseColor(colorId[id]))
                colorName = if (content?.color?.name?.isNotEmpty() == true) {
                    when (content?.color?.name?.toLowerCase()) {
                        "red" -> {
                            "Merah"
                        }
                        "orange" -> {
                            "Oranye"
                        }
                        "green" -> {
                            "Hijau"
                        }
                        else -> {
                            "Hitam"
                        }
                    }
                } else {
                    "Merah"
                }

                blockName.setOnCheckedChangeListener { _, isChecked ->
                    uploadDataViewModel.checkData(obj, isChecked)
                }

                mListener = listener

                executePendingBindings()
            }

        }

    }

}
