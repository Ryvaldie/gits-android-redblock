package redblock.SSC.gits.mvvm.problemcheck


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_problem_check.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.data.model.FilterModel
import redblock.SSC.gits.databinding.FragmentProblemCheckBinding
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.FILTER_PROBLEM_CHECK
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.REFRESH_PROBLEM_CHECK
import redblock.SSC.gits.mvvm.main.MainActivity
import redblock.SSC.gits.mvvm.updateproblem.UpdateProblemActivity


class ProblemCheckFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentProblemCheckBinding
    lateinit var mViewModel: ProblemCheckViewModel
    var listener: ProblemCheckUserActionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentProblemCheckBinding.inflate(inflater, container, false).apply {
            mViewModel = (activity as MainActivity).obtainProblemCheckViewModel().apply {

                refreshList.observe(this@ProblemCheckFragment, Observer {
                    recProblemCheck.apply {
                        if (adapter != null) {
                            (adapter as ProblemCheckItemAdapter).replaceList(it ?: listOf(), 0)
                        }
                    }
                })

            }

            listener = object : ProblemCheckUserActionListener {

                override fun onClickWarna(textView: TextView) {
                    dropdownView(textView, arrayListOf("All", "Merah", "Oranye", "Hijau"))
                }

                override fun onClickItem(data: Content) {
                    UpdateProblemActivity.startActivity(requireContext(), Gson().toJson(data, Content::class.java))
                }

            }

            mListener = listener

            setLifecycleOwner(this@ProblemCheckFragment)
        }

        mViewModel = mViewDataBinding.mViewModel!!

        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recProblemCheck.apply {
            if (adapter == null) {
                adapter = ProblemCheckItemAdapter(ArrayList(0), listener!!)
                layoutManager = LinearLayoutManager(context)
            }
        }

        setupListener()
    }

    private fun setupListener() {
        swipeRefresh.setOnRefreshListener {
            edtSearch.setText("")
            swipeRefresh.isRefreshing = false
            mViewModel.start()
//            mViewModel.getAllData()
        }

        textColor.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val colorString: String
                when (textColor.text.toString()) {
                    "All" -> {
                        colorString = ""
                        textColor.setTextColor(resources.getColor(R.color.mainWhite))
                    }
                    "Hijau" -> {
                        colorString = "green"
                        textColor.setTextColor(resources.getColor(R.color.sinarmasGreen))
                    }
                    "Oranye" -> {
                        colorString = "orange"
                        textColor.setTextColor(resources.getColor(R.color.sinarmasOrange))
                    }
                    else -> {
                        colorString = "red"
                        textColor.setTextColor(resources.getColor(R.color.sinarmasRed))
                    }
                }
                val colorText = sharedPreferences.getString("COLOR", "") ?: ""
                sharedPreferences.edit()
                        .putString("COLOR", colorString).apply()

                val filterJson = sharedPreferences.getString(FILTER_PROBLEM_CHECK,
                        Gson().toJson(FilterModel("div1", "yearly", "", "", "", "")))
                val filter = Gson().fromJson(filterJson, FilterModel::class.java)
                val filterModel = FilterModel(
                        filter.div, filter.timeStat, filter.block
                        , sharedPreferences.getString("COLOR", "") ?: "",
                        filter.endTime, filter.startTime
                )
                sharedPreferences.edit().putString(FILTER_PROBLEM_CHECK, Gson().toJson(filterModel)).apply()
//                if (colorText.isNotEmpty()) {
                if (colorString != colorText) {
                    mViewModel.searchData = edtSearch.text.toString()
                    mViewModel.start()
//                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


        val filterJson = sharedPreferences.getString(FILTER_PROBLEM_CHECK,
                Gson().toJson(FilterModel()))
        val filter = Gson().fromJson(filterJson, FilterModel::class.java)
        block_filter.visibility = if (filter.block.isEmpty()) {
            View.GONE
        } else {
            View.GONE
        }
        textColor.text = if (filter.color.isNotEmpty()) {
            when (filter.color) {
                "red" -> {
                    "Merah"
                }
                "orange" -> {
                    "Oranye"
                }
                else -> {
                    "Hijau"
                }
            }
        } else {
            "All"
        }

        edtSearch.setOnTouchListener { _, event ->

            val drawableRight = 2

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= edtSearch.right - edtSearch.compoundDrawables[drawableRight].bounds.width()) {
                    edtSearch.clearFocus()
                    val key = edtSearch.text.toString()
                    mViewModel.search(key)
                    hideShowKeyboard(true)
                }
            }
            false
        }

        edtSearch.setOnEditorActionListener { _, actionId, _ ->
            textColor.text = "All"
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                edtSearch.clearFocus()
                mViewModel.searchData = edtSearch.text.toString()
                mViewModel.start()
                hideShowKeyboard(true)
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    override fun onResume() {
        super.onResume()
        if (sharedPreferences.getBoolean(REFRESH_PROBLEM_CHECK, false)) {
            if (mViewDataBinding.mViewModel != null) {
                val filterJson = sharedPreferences.getString(FILTER_PROBLEM_CHECK,
                        Gson().toJson(FilterModel()))
                val filter = Gson().fromJson(filterJson, FilterModel::class.java)
                block_filter.visibility = if (filter.block.isEmpty()) {
                    View.GONE
                } else {
                    View.GONE
                }
                mViewDataBinding.mViewModel?.start()
                sharedPreferences.edit().putBoolean(REFRESH_PROBLEM_CHECK, false).apply()
            }
        }
    }

    companion object {
        fun newInstance() = ProblemCheckFragment().apply {

        }

    }

}
