package redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import redblock.SSC.gits.data.model.ImageItemModel
import redblock.SSC.gits.databinding.AddImageFooterBinding
import redblock.SSC.gits.databinding.ItemImageBeforeAfterBinding
import java.io.File

class ImageAfterAdapter(var item: ArrayList<ImageItemModel>, val mViewModel: ProblemActionViewModel)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<ImageItemModel>) {
        this.item.clear()
        this.item.addAll(item)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            IMAGE_VIEW_HOLDER -> ImageViewHolder(ItemImageBeforeAfterBinding.inflate(LayoutInflater.from(p0.context), p0, false), this, mViewModel)
            else -> AddViewHolder(AddImageFooterBinding.inflate(LayoutInflater.from(p0.context), p0, false), mViewModel)
        }
    }

    override fun getItemCount(): Int = item.size + 1

    override fun getItemViewType(position: Int): Int {
        return if (position == item.size) {
            ADD_VIEW_HOLDER
        } else {
            IMAGE_VIEW_HOLDER
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ImageViewHolder -> item[position].let { holder.bind(it) }
            else -> (holder as AddViewHolder).bind()
        }
    }

    fun removeItem(imageItemModel: ImageItemModel) {
        val file = File(imageItemModel.link)
        if (file.exists()) {
            if (file.delete()) {
                //Left empty
            }
        }
        val position = item.indexOf(imageItemModel)
        item.remove(imageItemModel)
        mViewModel.imageAfterArray.remove(imageItemModel)
        mViewModel.reloadImageEvent.call()
        notifyItemRemoved(position)
    }

    class ImageViewHolder(val mViewDataBinding: ItemImageBeforeAfterBinding, val adapter: ImageAfterAdapter, val mViewModel: ProblemActionViewModel) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: ImageItemModel) {
            mViewDataBinding.apply {
                if (obj.created_at?.contains(" ") == true) {
                    obj.created_at = obj.created_at?.split(" ")?.get(0)
                }
                mData = obj

                closeImage.visibility = if (mViewModel.edittableFieldKemajuan.get()) {
                    View.GONE
                } else {
                    View.VISIBLE
                }

                closeImage.setOnClickListener {
                    adapter.removeItem(obj)
                }

                imgPreview.setOnClickListener {
                    mViewModel.imagePreviewEvent.value = obj.link
                }
            }
        }

    }

    class AddViewHolder(val mBinding: AddImageFooterBinding, val mViewModel: ProblemActionViewModel) : RecyclerView.ViewHolder(mBinding.root) {

        fun bind() {
            mBinding.apply {

                addImage.visibility = if (mViewModel.imageAfterArray.size < 3) {
                    if (mViewModel.edittableFieldKemajuan.get()) {
                        View.GONE
                    } else {
                        View.VISIBLE
                    }
                } else {
                    View.GONE
                }

                addImage.setOnClickListener {
                    mViewModel.imageAfterEvent.call()
                }

            }
        }

    }

    companion object {

        private const val IMAGE_VIEW_HOLDER = 1
        private const val ADD_VIEW_HOLDER = 0

    }

}
