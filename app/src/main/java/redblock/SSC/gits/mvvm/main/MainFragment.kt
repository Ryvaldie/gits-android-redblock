package redblock.SSC.gits.mvvm.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import kotlinx.android.synthetic.main.main_fragment.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.databinding.MainFragmentBinding
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.putArgs
import java.util.*
import android.arch.lifecycle.Observer


/**
 * Created by irfanirawansukirman on 26/01/18.
 */

class MainFragment : BaseFragment(), MainItemUserActionListener {

    private lateinit var viewBinding: MainFragmentBinding
    private lateinit var viewModel: MainViewModel
    var mAdapter: MainViewPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = MainFragmentBinding.inflate(inflater, container, false).apply {

            viewModel = (activity as MainActivity).obtainMainViewModel().apply {

                clearMessageEvent.observe(this@MainFragment, Observer {
                    edtSearch.setText("")
                    edtSearch.clearFocus()
                })

            }

            setLifecycleOwner(this@MainFragment)
        }

        viewModel = viewBinding.viewModel!!

        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.registerToken()
        setupViewPager()
        setupViewModel()
        setupListener()

    }

    private fun setupViewPager() {
        val titles = ArrayList<String>()
//        titles.add(DateHelper.getDatePeriode())
        titles.add(getString(R.string.text_all_blocks))
        mAdapter = MainViewPagerAdapter(context!!, titles, childFragmentManager)
        main_viewpager?.adapter = mAdapter
        main_viewpager?.offscreenPageLimit = mAdapter!!.count
        tabs?.setupWithViewPager(main_viewpager)
    }

    private fun setupViewModel() {
        viewModel = viewBinding.viewModel!!
    }

    private fun setupListener() {
        edtSearch.setOnTouchListener { _, event ->

            val drawableRight = 2

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= edtSearch.right - edtSearch.compoundDrawables[drawableRight].bounds.width()) {
                    edtSearch.clearFocus()
                    val key = edtSearch.text.toString()
                    (activity as MainActivity).obtainPeriodeBlockViewModel().search(key)
                    (activity as MainActivity).obtainAllBlockViewModel().search(key)
                    hideShowKeyboard(true)
                }
            }
            false
        }

        edtSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                edtSearch.clearFocus()
                val key = edtSearch.text.toString()
                (activity as MainActivity).obtainPeriodeBlockViewModel().search(key)
                (activity as MainActivity).obtainAllBlockViewModel().search(key)
                hideShowKeyboard(true)
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    companion object {
        fun newInstance() = MainFragment().putArgs { }
    }
}