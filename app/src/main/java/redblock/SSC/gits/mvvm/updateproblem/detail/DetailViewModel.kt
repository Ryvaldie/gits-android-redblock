package redblock.SSC.gits.mvvm.updateproblem.detail;

import android.app.Application
import android.databinding.ObservableField
import redblock.SSC.gits.data.model.PotensiModel
import redblock.SSC.gits.data.model.ProblemTarget
import redblock.SSC.gits.data.param.ParamUpdateProblemTarget
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.SingleLiveEvent
import redblock.co.gits.gitsbase.BaseViewModel


class DetailViewModel(context: Application, val repository: GitsRepository) : BaseViewModel(context) {

    val showLoading = ObservableField<Boolean>()
    val showProblemTarget = SingleLiveEvent<ProblemTarget>()
    val showUpdateLoading = SingleLiveEvent<Boolean>()
    var messageFailedUpdate = ""
    val successUpdateEvent = SingleLiveEvent<Boolean>()
    val edittableField = ObservableField(false)
    val saveDataEvent = SingleLiveEvent<Void>()
    val saveDataNextEvent = SingleLiveEvent<Void>()
    val updateOrangePotensiEvent = SingleLiveEvent<String>()
    val updateGreenPotensiEvent = SingleLiveEvent<String>()

    init {
        showLoading.set(true)
    }

    fun editMode(boolean: Boolean) {
        edittableField.set(boolean)
    }

    fun getEditMode(): Boolean {
        return edittableField.get() ?: false
    }

    fun getProblemTarget(id: Int) {
        repository.getProblemTarget("", id, object : GitsDataSource.GetProblemTargetCallback {
            override fun onShowProgressDialog() {
                showLoading.set(true)
            }

            override fun onHideProgressDialog() {
                showLoading.set(false)
            }

            override fun onSuccess(data: ProblemTarget) {
                showProblemTarget.value = data
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                messageFailedUpdate = errorMessage ?: ""
                successUpdateEvent.value = false
            }

        })
    }

    fun updateProblemTarget(id: Int, paramUpdateProblemTarget: ParamUpdateProblemTarget) {
        repository.updateProblemTarget("", id, paramUpdateProblemTarget, object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {
                showUpdateLoading.value = true
            }

            override fun onHideProgressDialog() {
                showUpdateLoading.value = false
            }

            override fun onSuccess(data: Any?) {
                successUpdateEvent.value = true
                getProblemTarget(id)
                //Continue the update
                saveDataNextEvent.call()
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                messageFailedUpdate = errorMessage ?: ""
                successUpdateEvent.value = false
            }

        })
    }

    fun getPotensi(id: Int, startTarget: String, endTarget: String, tahunTanam: Int, region: String, soil: String, seed: String, type: Int) {
        repository.getPotensi("", id, startTarget, endTarget, tahunTanam, region, soil, seed, object : GitsDataSource.GetPotensiCallback {
            override fun onShowProgressDialog() {
                showUpdateLoading.value = true
            }

            override fun onHideProgressDialog() {
                showUpdateLoading.value = false
            }

            override fun onSuccess(data: PotensiModel) {
                if (type == ORANGE_TYPE) {
                    updateOrangePotensiEvent.value = String.format("%.2f", data.value)
                } else {
                    updateGreenPotensiEvent.value = String.format("%.2f", data.value)
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                messageFailedUpdate = errorMessage ?: ""
                successUpdateEvent.value = false
            }

        })
    }

    companion object {

        const val ORANGE_TYPE = 0
        const val GREEN_TYPE = 1

    }

}