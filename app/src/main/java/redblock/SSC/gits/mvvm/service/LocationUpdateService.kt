package redblock.SSC.gits.mvvm.service

import android.Manifest
import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.ContextCompat

class LocationUpdateService : Service(), LocationListener {

    private lateinit var locationManager: LocationManager
    private var requestUpdate = false

    override fun onStart(intent: Intent?, startId: Int) {

        requestUpdate = intent?.getBooleanExtra(REQUEST_LOCATION, false) ?: false

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager

        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, this)

            isRunning = true
        }

        super.onStart(intent, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::locationManager.isInitialized) {
            locationManager.removeUpdates(this)
        }
        isRunning = false
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onLocationChanged(location: Location?) {
        if (requestUpdate) {
            val intent = Intent(LOCATION_UPDATE)
            val latLng = "${location?.latitude},${location?.longitude}"
            intent.putExtra(LOCATION_UPDATE, latLng)
            sendBroadcast(intent)
            requestUpdate = false
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

    companion object {

        var isRunning = false

        const val REQUEST_LOCATION = "REQUEST_LOCATION"

        //ms
        const val LOCATION_REFRESH_TIME = 1000.toLong()

        //meter
        const val LOCATION_REFRESH_DISTANCE = 1.toFloat()

        const val LOCATION_UPDATE = "LOCATION_UPDATE"

    }

}