package redblock.SSC.gits.mvvm.uploaddata;

import android.app.Application
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.graphics.BitmapFactory
import android.os.Environment
import android.os.Handler
import android.support.v7.widget.RecyclerView
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.Func
import id.zelory.compressor.Compressor
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import redblock.SSC.gits.R
import redblock.SSC.gits.data.model.AllDataModel
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.data.model.ImageItemModel
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.data.model.local.ProblemListDBModel
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.data.param.ParamUpdateProblem
import redblock.SSC.gits.data.param.ParamUpdateProblemTarget
import redblock.SSC.gits.data.param.ProblemActionParam
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.data.source.GitsRepository.Companion.isOnline
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.MutableListLiveData
import redblock.SSC.gits.util.SingleLiveEvent
import redblock.co.gits.gitsbase.BaseViewModel
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.io.File
import java.lang.NullPointerException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class UploadDataViewModel(val context: Application, val repository: GitsRepository) : BaseViewModel(context) {

    val uploadProblemItems = ObservableArrayList<ParamCreateProblem>()
    val uploadProblemTargetItems = ObservableArrayList<ParamUpdateProblemTarget>()
    val uploadProblemDataItems = ObservableArrayList<Content>()
    val reloadData = ObservableBoolean()
    val showToastEvent = SingleLiveEvent<String>()
    val successUnduhData = SingleLiveEvent<Boolean>()
    val successUploadData = SingleLiveEvent<Boolean>()
    var countProblemItems = 0
    var countProblemTargetItems = 0
    lateinit var data: AllDataModel
    private lateinit var fetchConfiguration: FetchConfiguration
    private lateinit var fetch: Fetch
    private var isChecked = false
    var count = 0
    val succesLoadData = SingleLiveEvent<Void>()

    fun validateCheckedData(): Boolean {
        countProblemItems = uploadProblemItems.count {
            it.isChecked
        }
        countProblemTargetItems = uploadProblemTargetItems.count {
            it.isChecked
        }

        return if ((countProblemItems + countProblemTargetItems) == 0) {
            reloadData.set(false)
            showToastEvent.value = context.getString(R.string.text_no_checked_data)
            false
        } else {
            true
        }
    }

    fun checkAll(isChecked: Boolean, dataRec: RecyclerView) {
        this.isChecked = isChecked

        val tempContentList = ArrayList<Content>()
        for (content in uploadProblemDataItems) {
            content.isChecked = isChecked
            tempContentList.add(content)
        }
        uploadProblemDataItems.apply {
            clear()
            addAll(tempContentList)
        }

        val problemItems = ArrayList<ParamCreateProblem>()
        for (problem in uploadProblemItems) {
            problem.isChecked = isChecked
            problemItems.add(problem)
        }
        uploadProblemItems.apply {
            clear()
            addAll(problemItems)
        }

        val problemTargetItems = ArrayList<ParamUpdateProblemTarget>()
        for (problemTarget in uploadProblemTargetItems) {
            problemTarget.isChecked = isChecked
            problemTargetItems.add(problemTarget)
        }
        uploadProblemTargetItems.apply {
            clear()
            addAll(problemTargetItems)
        }

        dataRec.adapter?.notifyDataSetChanged()
    }

    fun checkData(content: Content, isChecked: Boolean) {
        val targetId = content.problem_target_id
        var index: Int

        val tempDraftData = repository.localDataSource.getDraftProblemDBDao().getDataById(targetId)
        if (tempDraftData != null) {
            index = uploadProblemItems.indexOf(tempDraftData.problem)
            if (index != -1) {
                uploadProblemItems[index].isChecked = isChecked
            }
        }

        val tempData = repository.localDataSource.getDraftProblemTargetDao().getDataById(targetId)
        if (tempData != null) {
            index = uploadProblemTargetItems.indexOf(tempData.problem)
            if (index != -1) {
                uploadProblemTargetItems[index].isChecked = isChecked
            }
        }

        index = uploadProblemDataItems.indexOf(content)
        if (index != -1) {
            uploadProblemDataItems[index].isChecked = isChecked
        }
    }

    fun deleteData(dataRec: RecyclerView) {
        val problemItems = ArrayList<ParamCreateProblem>()
        problemItems.addAll(uploadProblemItems)
        for (paramCreateProblem in problemItems) {
            if (paramCreateProblem.isChecked) {
                val id = paramCreateProblem.problem_target_id?.toInt() ?: 0

                val tempProblemData = repository.localDataSource.getProblemListDBDao()
                        .getDataById(id) ?: ProblemListDBModel()
                repository.localDataSource.getProblemListDBDao().deleteData(tempProblemData)
                val tempDraftData = repository.localDataSource.getDraftProblemDBDao()
                        .getDataById(id)
                repository.localDataSource.getDraftProblemDBDao().deleteData(tempDraftData)

                //Reinsert local data
                val tempData = ProblemListDBModel()
                val tempList = tempProblemData.problemList
                tempData.let {
                    it.id = tempProblemData.id
                    it.dateUpdated = tempProblemData.dateUpdated
                    for (problemData in tempProblemData.problemList ?: listOf()) {
                        tempList?.get(tempList.indexOf(problemData))?.problem_detail?.stat = "Waiting for Approval"
                    }
                    it.problemList = tempList
                }
                repository.localDataSource.getProblemListDBDao().insertData(tempData)

                uploadProblemItems.remove(paramCreateProblem)
            }
        }

        val problemTargetItems = ArrayList<ParamUpdateProblemTarget>()
        problemTargetItems.addAll(uploadProblemTargetItems)
        for (paramUpdateProblemTarget in problemTargetItems) {
            if (paramUpdateProblemTarget.isChecked) {
                val id = repository.getBlockContent(paramUpdateProblemTarget.problem_check_id
                        ?: 0)?.problem_target?.id ?: 0
                val tempData = repository.localDataSource.getDraftProblemTargetDao().getDataById(id)
                repository.localDataSource.getDraftProblemTargetDao().deleteData(tempData)
                uploadProblemTargetItems.remove(paramUpdateProblemTarget)
            }
        }

        val tempContentList = ArrayList<Content>()
        tempContentList.addAll(uploadProblemDataItems)
        for (content in tempContentList) {
            if (content.isChecked) {
                uploadProblemDataItems.remove(content)
            }
        }

        dataRec.adapter?.notifyDataSetChanged()
    }

    fun uploadData() {
        reloadData.set(true)
        if (!isOnline) {
            reloadData.set(false)
            successUploadData.value = false
            return
        }

        val idList = ArrayList<Int>()
        uploadProblemDataItems.map {
            if (it.isChecked) {
                idList.add(it.problem_target_id)
            }
        }
        val problemOfflineItems = MutableListLiveData<ItemProblemModel>()
        repository.localDataSource.getProblemListDBDao().getAllData()?.map {
            problemOfflineItems.addAll(it.problemList ?: listOf())
        }
        for (offlineItem in problemOfflineItems) {
            if (offlineItem != null && offlineItem.problem_detail?.stat?.toLowerCase() == "draft") {
                /*if (idList.contains(offlineItem.problem_target_id)) {
                    //update
                    val paramUpdateProblem = ParamUpdateProblem()
                    val problemActionArray = ArrayList<ProblemActionParam>()
                    offlineItem.problem_detail?.let { problemDetail ->
                        paramUpdateProblem.let { update ->
                            update.total_area = problemDetail.total_area
                            update.problem_area = problemDetail.problem_area
                            update.progress_overall = problemDetail.progress_overall
                            for (action in problemDetail.problem_action) {
                                val problemAction = ProblemActionParam(
                                        PIC = action.PIC,
                                        action_plan = action.action_plan,
                                        cost = action.cost,
                                        end_date = action.end_date,
                                        finished = action.finished,
                                        problem_detail = action.problem_detail,
                                        problem_scale = action.problem_scale,
                                        progress = action.progress,
                                        start_date = action.start_date,
                                        stat_cost = action.stat_cost,
                                        type_unit_id = action.type_unit_id,
                                        files = action.files,
                                        problem_type_id = action.problem_type_id,
                                        pokok = action.pokok,
                                        kalibrasi = action.kalibrasi,
                                        is_primary = action.is_primary
                                )
                                problemActionArray.add(problemAction)
                            }
                            update.problem_action = problemActionArray
                        }
                    }
                    count += 1
                    updateUploadImages(offlineItem.id ?: 0, offlineItem.problem_target_id
                            ?: 0, paramUpdateProblem)
                } else {*/
                    //post
                    val paramCreateProblem = ParamCreateProblem()
                    val problemActionArray = ArrayList<ProblemActionParam>()
                    offlineItem.problem_detail?.let { problemDetail ->
                        paramCreateProblem.let { update ->
                            update.problem_target_id = "${offlineItem.problem_target_id ?: 0}"
                            update.total_area = problemDetail.total_area
                            update.problem_area = problemDetail.problem_area
                            update.progress_overall = problemDetail.progress_overall
                            for (action in problemDetail.problem_action) {
                                val problemAction = ProblemActionParam(
                                        PIC = action.PIC,
                                        action_plan = action.action_plan,
                                        cost = action.cost,
                                        end_date = action.end_date,
                                        finished = action.finished,
                                        problem_detail = action.problem_detail,
                                        problem_scale = action.problem_scale,
                                        progress = action.progress,
                                        start_date = action.start_date,
                                        stat_cost = action.stat_cost,
                                        type_unit_id = action.type_unit_id,
                                        files = action.files,
                                        problem_type_id = action.problem_type_id,
                                        pokok = action.pokok,
                                        kalibrasi = action.kalibrasi,
                                        is_primary = action.is_primary
                                )
                                problemActionArray.add(problemAction)
                            }
                            update.problem_action = problemActionArray
                        }
                    }
                    count += 1
                    postUploadImages(paramCreateProblem)
//                }
            }
        }
        if (uploadProblemTargetItems.isNotEmpty() && uploadProblemTargetItems.count { it.isChecked } > 0) {
            updateBulkProblemTarget()
        } else {
            reloadData.set(false)
            successUploadData.value = true
        }
    }

    private fun postUploadImages(paramCreateProblem: ParamCreateProblem) {
        val problemTemp = ArrayList<ProblemActionParam>()
        problemTemp.addAll(paramCreateProblem.problem_action ?: arrayListOf())
        val parts = ArrayList<MultipartBody.Part>()
        val problemIndex = ArrayList<Int>()
        val fileIndex = HashMap<Int, ArrayList<Int>>()
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = false
        for ((pIndex, problemAction) in problemTemp.withIndex()) {
            val indexes = ArrayList<Int>()
            var tempIndex = 0
            for ((index, file) in problemAction.files?.withIndex() ?: arrayListOf()) {
                if (file?.link?.contains("/") == true) {
                    if (!problemIndex.contains(pIndex)) {
                        problemIndex.add(pIndex)
                        tempIndex = pIndex
                    }
                    indexes.add(index)
                    val bitmapTemp = BitmapFactory.decodeFile(file.link, options)
                    if (bitmapTemp != null && bitmapTemp.width > 0 && bitmapTemp.height > 0) {
                        val filePath = Compressor.getDefault(context).compressToFile(File(file.link))
                        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), filePath)
                        parts.add(MultipartBody.Part.createFormData("upload", filePath.name, requestBody))
                    }
                }
            }
            if (!fileIndex.containsKey(tempIndex)) {
                fileIndex[tempIndex] = indexes
            }
        }

        if (parts.isEmpty()) {
            postProblem(paramCreateProblem)
        } else {
            repository.uploadImages(parts, object : GitsDataSource.GetUploadedImages {
                override fun onShowProgressDialog() {
                    reloadData.set(true)
                }

                override fun onHideProgressDialog() {
                    reloadData.set(false)
                }

                override fun onSuccess(data: List<String>) {
                    var count = 0
                    for (pIndex in problemIndex) {
                        val filesTemp = ArrayList<ImageItemModel?>()
                        filesTemp.addAll(problemTemp[pIndex].files ?: arrayListOf())
                        if (filesTemp.isNotEmpty()) {
                            for (index in fileIndex[pIndex] ?: arrayListOf()) {
                                if (count < data.size) {
                                    filesTemp[index]?.link = data[count]
                                    count++
                                }
                            }
                            paramCreateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        } else {
                            paramCreateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        }
                    }
                    postProblem(paramCreateProblem)
                }

                override fun onFinish() {

                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    if (statusCode == 500) {
                        showToastEvent.value = errorMessage
                    } else {
                        successUploadData.value = false
                    }
                }

            })
        }
    }

    fun postProblem(paramCreateProblem: ParamCreateProblem) {
        repository.postProblem("", paramCreateProblem, object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {
                reloadData.set(true)
            }

            override fun onHideProgressDialog() {
                reloadData.set(false)
            }

            override fun onSuccess(data: Any?) {
                deletePostData(paramCreateProblem)
                count -= 1
                if (count <= 0) {
                    //Finish
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                if (statusCode == 500) {
                    showToastEvent.value = errorMessage
                } else {
                    successUploadData.value = false
                }
            }

        })
    }

    private fun updateUploadImages(id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem) {
        val problemTemp = ArrayList<ProblemActionParam>()
        problemTemp.addAll(paramUpdateProblem.problem_action ?: arrayListOf())
        val parts = ArrayList<MultipartBody.Part>()
        val problemIndex = ArrayList<Int>()
        val fileIndex = HashMap<Int, ArrayList<Int>>()
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = false
        for ((pIndex, problemAction) in problemTemp.withIndex()) {
            val indexes = ArrayList<Int>()
            var tempIndex = 0
            for ((index, file) in problemAction.files?.withIndex() ?: arrayListOf()) {
                if (file?.link?.contains("/") == true) {
                    if (!problemIndex.contains(pIndex)) {
                        problemIndex.add(pIndex)
                        tempIndex = pIndex
                    }
                    indexes.add(index)
                    val bitmapTemp = BitmapFactory.decodeFile(file.link, options)
                    if (bitmapTemp != null && bitmapTemp.width > 0 && bitmapTemp.height > 0) {
                        val filePath = Compressor.getDefault(context).compressToFile(File(file.link))
                        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), filePath)
                        parts.add(MultipartBody.Part.createFormData("upload", filePath.name, requestBody))
                    }
                }
            }
            if (!fileIndex.containsKey(tempIndex)) {
                fileIndex[tempIndex] = indexes
            }
        }

        if (parts.isEmpty()) {
            updateProblem(id, targetId, paramUpdateProblem)
        } else {
            repository.uploadImages(parts, object : GitsDataSource.GetUploadedImages {
                override fun onShowProgressDialog() {
                    reloadData.set(true)
                }

                override fun onHideProgressDialog() {
                    reloadData.set(false)
                }

                override fun onSuccess(data: List<String>) {
                    var count = 0
                    for (pIndex in problemIndex) {
                        val filesTemp = ArrayList<ImageItemModel?>()
                        filesTemp.addAll(problemTemp[pIndex].files ?: arrayListOf())
                        if (filesTemp.isNotEmpty()) {
                            for (index in fileIndex[pIndex] ?: arrayListOf()) {
                                if (count < data.size) {
                                    filesTemp[index]?.link = data[count]
                                    count++
                                }
                            }
                            paramUpdateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        } else {
                            paramUpdateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        }
                    }
                    updateProblem(id, targetId, paramUpdateProblem)
                }

                override fun onFinish() {

                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    if (statusCode == 500) {
                        showToastEvent.value = errorMessage
                    } else {
                        successUploadData.value = false
                    }
                }

            })
        }
    }

    fun updateProblem(id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem) {
        repository.updateProblem("", id, targetId, paramUpdateProblem, object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {
                reloadData.set(true)
            }

            override fun onHideProgressDialog() {
                reloadData.set(false)
            }

            override fun onSuccess(data: Any?) {
                deleteUpdateData(targetId)
                count -= 1
                if (count <= 0) {
                    //Finish
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                if (statusCode == 500) {
                    showToastEvent.value = errorMessage
                } else {
                    successUploadData.value = false
                }
            }

        })
    }

    private fun updateBulkProblemTarget() {
        for ((index, paramUpdateProblemTarget) in uploadProblemTargetItems.withIndex()) {
            if (paramUpdateProblemTarget.isChecked) {
                val id = repository.getBlockContent(paramUpdateProblemTarget.problem_check_id
                        ?: 0)?.problem_target?.id ?: 0
                repository.updateProblemTarget("", id, paramUpdateProblemTarget, object : GitsDataSource.PostProblemCallback {
                    override fun onShowProgressDialog() {

                    }

                    override fun onHideProgressDialog() {

                    }

                    override fun onSuccess(data: Any?) {
                        val tempData = repository.localDataSource.getDraftProblemTargetDao().getDataById(id)
                        repository.localDataSource.getDraftProblemTargetDao().deleteData(tempData)
                        uploadProblemTargetItems.remove(paramUpdateProblemTarget)
                        successUploadData.value = true
                        if (index == countProblemTargetItems - 1) {
                            reloadData.set(false)
                        }
                    }

                    override fun onFinish() {

                    }

                    override fun onFailed(statusCode: Int, errorMessage: String?) {
                        reloadData.set(false)
                        if (statusCode == 500) {
                            showToastEvent.value = errorMessage
                        } else {
                            successUploadData.value = false
                        }
                        return
                    }

                })
            }
        }
    }

    private fun deletePostData(paramCreateProblem: ParamCreateProblem) {
        val id = paramCreateProblem.problem_target_id?.toInt() ?: 0
        val tempProblemData = repository.localDataSource.getProblemListDBDao()
                .getDataById(id) ?: ProblemListDBModel()
        repository.localDataSource.getProblemListDBDao().deleteData(tempProblemData)
        val tempDraftData = repository.localDataSource.getDraftProblemDBDao().getDataById(id)
        if (tempDraftData != null) {
            repository.localDataSource.getDraftProblemDBDao().deleteData(tempDraftData)
        }

        //Reinsert local data
        val tempData = ProblemListDBModel()
        val tempList: ArrayList<ItemProblemModel>? = arrayListOf()
        tempList?.addAll(tempProblemData.problemList ?: listOf())
        tempData.let {
            it.id = tempProblemData.id
            it.dateUpdated = tempProblemData.dateUpdated
            for (problemData in tempProblemData.problemList ?: listOf()) {
                try {
                    tempList?.get(tempList.indexOf(problemData))?.problem_detail?.stat = "Waiting for Approval"
                } catch (ex: NullPointerException) {
                    //Catch null exception
                }
            }
            it.problemList = tempList
        }
        repository.localDataSource.getProblemListDBDao().insertData(tempData)
    }

    private fun deleteUpdateData(id: Int) {
        val tempProblemData = repository.localDataSource.getProblemListDBDao()
                .getDataById(id) ?: ProblemListDBModel()
        repository.localDataSource.getProblemListDBDao().deleteData(tempProblemData)
        val tempDraftData = repository.localDataSource.getDraftProblemDBDao().getDataById(id)
        if (tempDraftData != null) {
            repository.localDataSource.getDraftProblemDBDao().deleteData(tempDraftData)
        }

        //Reinsert local data
        val tempData = ProblemListDBModel()
        val tempList = tempProblemData.problemList
        tempData.let {
            it.id = tempProblemData.id
            it.dateUpdated = tempProblemData.dateUpdated
            for (problemData in tempProblemData.problemList ?: listOf()) {
                try {
                    tempList?.get(tempList.indexOf(problemData))?.problem_detail?.stat = "Waiting for Approval"
                } catch (ex: NullPointerException) {
                    //Catch null exception
                }
            }
            it.problemList = tempList
        }
        repository.localDataSource.getProblemListDBDao().insertData(tempData)
    }

    fun getProblemData() {
        getProblemItems()
        succesLoadData.call()
    }

    private fun getProblemItems() {
        repository.getDraftProblem(object : GitsDataSource.GetDraftProblemCallback {
            override fun onShowProgressDialog() {
                reloadData.set(true)
            }

            override fun onHideProgressDialog() {
                reloadData.set(false)
            }

            override fun onSuccess(data: List<ParamCreateProblem?>?) {
                uploadProblemItems.clear()
                for (temp in data ?: arrayListOf()) {
                    if (temp != null) {
                        temp.isChecked = isChecked
                        uploadProblemItems.add(temp)
                    }
                }
                getProblemTargetItems()
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {

            }

        })
    }

    private fun getProblemTargetItems() {
        repository.getDraftProblemTarget(object : GitsDataSource.GetDraftProblemTargetCallback {
            override fun onShowProgressDialog() {
                reloadData.set(true)
            }

            override fun onHideProgressDialog() {
                reloadData.set(false)
            }

            override fun onSuccess(data: List<ParamUpdateProblemTarget?>?) {
                uploadProblemTargetItems.clear()
                for (temp in data ?: arrayListOf()) {
                    if (temp != null) {
                        temp.isChecked = isChecked
                        uploadProblemTargetItems.add(temp)
                    }
                }

                val dataSet: MutableSet<String> = mutableSetOf()
                for (idList in uploadProblemItems) {
                    dataSet.add(idList.problem_target_id ?: "")
                }
                for (idList in uploadProblemTargetItems) {
                    val targetId = repository.getBlockContent(idList.problem_check_id
                            ?: 0)?.problem_target?.id ?: 0
                    dataSet.add("$targetId")
                }
                uploadProblemDataItems.clear()
                for (targetId in dataSet) {
                    val content = repository.getBlockContent(targetId.toInt())
                    uploadProblemDataItems.add(content)
                }

            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {

            }

        })
    }

    fun getAllData() {
        val data = repository.localDataSource.getUserDBDao().getAllData()?.filter {
            it.dateUpdated != null
        }
        val div = data?.get(data.lastIndex)?.userConfig?.division ?: ""
        val psm = data?.get(data.lastIndex)?.userConfig?.psm ?: ""
        val region = data?.get(data.lastIndex)?.userConfig?.region ?: ""
        repository.getAllData("", div, psm, region, true, object : GitsDataSource.GetAllData {
            override fun onShowProgressDialog() {
                reloadData.set(true)
            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: AllDataModel) {
                createFilePath(data)
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                reloadData.set(false)
                successUnduhData.value = false
            }

        })
    }

    fun createFilePath(data: AllDataModel) {
        fetchConfiguration = FetchConfiguration.Builder(context)
                .setDownloadConcurrentLimit(50)
                .enableFileExistChecks(false)
                .enableHashCheck(false)
                .build()
        fetch = Fetch.getInstance(fetchConfiguration)
        this.data = data
        try {
            val dir = File(Environment.getExternalStorageDirectory().path + "/RedBlock")
            if (!dir.exists()) {
                dir.mkdir()
            } else {
                deleteRecursive(dir)
                dir.mkdir()
            }

            val requests = ArrayList<Request>()
            for ((index1, content) in data.problem_check.withIndex()) {
                val itemProblemModel = content.problem_target?.problem ?: arrayListOf()
                for ((index2, item) in itemProblemModel.withIndex()) {
                    val problemAction = item.problem_detail?.problem_action ?: arrayListOf()
                    for ((index3, action) in problemAction.withIndex()) {
                        if (action != null) {
                            for ((index4, file) in (action.files ?: arrayListOf()).withIndex()) {
                                val fileName = Environment.getExternalStorageDirectory().path + "/RedBlock/" +
                                        "${file?.created_at
                                                ?: SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(Date())}$index1$index2$index3$index4.jpeg"
                                val request = Request(GitsHelper.Const.BASE_IMAGE_URL + (file?.link
                                        ?: ""), fileName)
                                request.priority = Priority.HIGH
                                request.networkType = NetworkType.ALL
                                request.addHeader("INDEX", "$index1,$index2,$index3,$index4")
                                requests.add(request)
                            }
                        }
                    }
                }
            }

            fetch.cancelAll()
            fetch.removeAll()
            fetch.enqueue(requests)

            fetch.getDownloads(Func {
                if (it.isNotEmpty()) {
                    if (!fetch.hasActiveDownloads) {
                        showToastEvent.value = "Mengunduh asset terlebih dahulu..."
                        for (d in it) {
                            val indexes = d.headers["INDEX"] ?: ""
                            if (indexes.contains(",")) {
                                val index1 = indexes.split(",")[0].toInt()
                                val index2 = indexes.split(",")[1].toInt()
                                val index3 = indexes.split(",")[2].toInt()
                                val index4 = indexes.split(",")[3].toInt()
                                data.problem_check[index1].problem_target?.problem?.get(index2)?.problem_detail?.problem_action?.get(index3)?.files?.get(index4)?.link = d.file
                            }
                        }
                        if (it.isNotEmpty()) {
                            for (problemCheck in data.problem_check) {
                                repository.localDataSource.getProblemListDBDao().insertData(
                                        ProblemListDBModel(
                                                problemCheck.problem_target?.id,
                                                DateHelper.getTimeStamp(),
                                                problemCheck.problem_target?.problem
                                        )
                                )
                            }
                        }
                        Handler().postDelayed({
                            fetch.close()
                            reloadData.set(false)
                            successUnduhData.value = true
                        }, 10000)
                    }
                } else {
                    reloadData.set(false)
                    successUnduhData.value = true
                }
            })

        } catch (e: Throwable) {
            reloadData.set(false)
            successUnduhData.value = false
            showToastEvent.value = e.localizedMessage
        }
    }

    private fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory) {
            for (child in fileOrDirectory.listFiles()) {
                deleteRecursive(child)
            }
        }

        fileOrDirectory.delete()
    }

}