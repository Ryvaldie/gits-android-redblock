package redblock.SSC.gits.mvvm.problemcheck

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.R
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.databinding.ItemProblemCheckBinding
import redblock.SSC.gits.util.DateHelper

class ProblemCheckItemAdapter(var item: MutableList<Content>, val listener: ProblemCheckUserActionListener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<Content>, page: Int) {
        this.item.apply {
            clear()
            addAll(item)
        }
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item.size)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return BeritaViewHolder(ItemProblemCheckBinding.inflate(LayoutInflater.from(p0.context), p0, false))
    }

    override fun getItemCount(): Int = item.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        (p0 as BeritaViewHolder).bind(item[p1], listener)
    }

    class BeritaViewHolder(val mViewDataBinding: ItemProblemCheckBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: Content, listener: ProblemCheckUserActionListener) {

            mViewDataBinding.apply {
                data = obj
                val unit = data?.problem_target?.type_unit?.unit ?: ""
                area = data?.problem_target?.area.toString() + " " + unit
                date = DateHelper.dateTimeMonth(data?.created_at)
                val id = (data?.status_color_id ?: 0) - 1
                val resId = root.context.resources
                        .getIdentifier(root.context.resources
                                .getStringArray(R.array.circleId)[id], "drawable",
                                root.context?.packageName)
                colorIndicator.setImageResource(resId)
                val colorId = root.context.resources.getStringArray(R.array.colorId)
                colorText.setTextColor(Color.parseColor(colorId[id]))
                colorName = if (data?.color?.name?.isNotEmpty() == true) {
                    when (data?.color?.name?.toLowerCase()) {
                        "red" -> {
                            "Merah"
                        }
                        "orange" -> {
                            "Oranye"
                        }
                        "green" -> {
                            "Hijau"
                        }
                        else -> {
                            "Hitam"
                        }
                    }
                } else {
                    "Merah"
                }

                val status = data?.problem_target?.status ?: ""
                textStatus.text = status
                textStatus.setTextColor(when {
                    status.toLowerCase().contains("draft") -> root.context.resources.getColor(R.color.draftColor)
                    status.toLowerCase().contains("waiting") -> root.context.resources.getColor(R.color.waitingColor)
                    status.toLowerCase().contains("revision") -> root.context.resources.getColor(R.color.revisionColor)
                    status.toLowerCase().contains("approved") -> root.context.resources.getColor(R.color.approvedColor)
                    else -> root.context.resources.getColor(R.color.rejectedColor)
                })

                mListener = listener

                executePendingBindings()
            }

        }

    }

}
