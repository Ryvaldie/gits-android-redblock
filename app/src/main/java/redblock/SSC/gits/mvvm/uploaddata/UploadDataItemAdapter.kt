package redblock.SSC.gits.mvvm.uploaddata

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.data.model.ItemProblemTypeModel
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.databinding.ItemUploadDataBinding

class UploadDataItemAdapter(var item: List<ParamCreateProblem>, val listener: UploadDataUserActionListener,
                            private val uploadDataViewModel: UploadDataViewModel)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<ParamCreateProblem>, page: Int) {
        this.item = item
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item.size)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val problemTypeData = uploadDataViewModel.repository.localDataSource.getProblemTypeDBDao().getAllData()
        val problemType = problemTypeData?.get(problemTypeData.lastIndex)?.problemTypes
        val problemTypes = HashMap<Int, ItemProblemTypeModel>()
        for (type in problemType ?: arrayListOf()) {
            if (type != null) {
                problemTypes[type.id ?: 0] = type
            }
        }
        return DataViewHolder(ItemUploadDataBinding.inflate(LayoutInflater.from(p0.context), p0, false), problemTypes, uploadDataViewModel)
    }

    override fun getItemCount(): Int = item.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        (p0 as DataViewHolder).bind(item[p1], listener)
    }

    class DataViewHolder(val mViewDataBinding: ItemUploadDataBinding, private val problemTypes: HashMap<Int, ItemProblemTypeModel>, private val uploadDataViewModel: UploadDataViewModel) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: ParamCreateProblem, listener: UploadDataUserActionListener) {

            mViewDataBinding.apply {
                mData = obj
                val targetId = obj.problem_target_id?.toInt() ?: 0
                itemProblem = uploadDataViewModel.repository.getItemProblem(targetId)
                content = uploadDataViewModel.repository.getBlockContent(targetId)
                totalArea = (obj.total_area ?: "") + " Ha"
                problemArea = (obj.problem_area ?: "") + " Ha"

                blockName.setOnCheckedChangeListener { _, isChecked ->
                    val index = uploadDataViewModel.uploadProblemItems.indexOf(obj)
                    if (index != -1) {
                        uploadDataViewModel.uploadProblemItems[index].isChecked = isChecked
                    }
                }

                mListener = listener

            }

        }

    }

}
