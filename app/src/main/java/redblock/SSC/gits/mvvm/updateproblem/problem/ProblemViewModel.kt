package redblock.SSC.gits.mvvm.updateproblem.problem;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import android.graphics.BitmapFactory
import id.zelory.compressor.Compressor
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import redblock.SSC.gits.data.model.ImageItemModel
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.data.model.local.ProblemListDBModel
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.data.param.ParamUpdateProblem
import redblock.SSC.gits.data.param.ProblemActionParam
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.MutableListLiveData
import redblock.SSC.gits.util.SingleLiveEvent
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ProblemViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val problemItems = MutableListLiveData<ItemProblemModel>()
    var showProgress = SingleLiveEvent<Boolean>()
    var showErrorButton = SingleLiveEvent<Boolean>()
    var showErrorText = SingleLiveEvent<Boolean>()
    val message = SingleLiveEvent<String>()
    var id = 1
    var targetId = 1
    val paging: Int? = 0
    val checkPercentage = SingleLiveEvent<Boolean>()
    val saveDataEvent = SingleLiveEvent<Void>()
    val problemOfflineItems = MutableListLiveData<ItemProblemModel>()
    val showOfflineData = ObservableField(false)
    val showOnlineData = ObservableField(false)
    val successUpdateEvent = SingleLiveEvent<Boolean>()
    var count = 0
    val problemHistoryItems = MutableListLiveData<ItemProblemModel>()

    init {
        showProgress.value = true
    }

    fun showError(button: Boolean, text: Boolean) {
        showErrorButton.value = button
        showErrorText.value = text
    }

    private fun loadLocalDataProblem() {
        repository.localDataSource.getProblem("", targetId, object : GitsDataSource.GetProblemCallback {
            override fun onShowProgressDialog() {
                showProgress.value = true
            }

            override fun onHideProgressDialog() {
                showProgress.value = false
            }

            override fun onSuccess(data: List<ItemProblemModel>) {
                problemOfflineItems.clear()
                var targetId: Int? = 0
                for (problem in data) {
                    if (problem != null) {
                        if (problem.problem_detail?.stat?.toLowerCase() == "draft")
                            if (targetId != problem.problem_target_id) {
                                problemOfflineItems.add(problem)
                                targetId = problem.problem_target_id
                            }
                    }
                }
                val isDraftDataExists = problemOfflineItems.isNotEmpty()
                showOfflineData.set(isDraftDataExists)
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                message.value = errorMessage
                if (statusCode == 404) {
                    showError(button = false, text = true)
                } else {
                    showError(button = true, text = true)
                }
            }

        })
    }

    fun loadDataProblem() {
        repository.getProblem("", targetId, object : GitsDataSource.GetProblemCallback {
            override fun onShowProgressDialog() {
                showProgress.value = true
            }

            override fun onHideProgressDialog() {
                showProgress.value = false
            }

            override fun onSuccess(data: List<ItemProblemModel>) {
                loadDataProblemHistory()
                problemItems.apply {
                    clear()
                    if (data.isNotEmpty()) {
                        add(data[data.lastIndex])
                    }
                }

                var isAllCompleted = problemItems.isEmpty()
                for (problem in problemItems) {
                    if (problem != null) {
                        if (problem.problem_detail?.stat?.toLowerCase(Locale.getDefault()) == "new") {
                            isAllCompleted = true
                        } else {
                            isAllCompleted = false
                            break
                        }
                    }
                }
                checkPercentage.value = isAllCompleted

//                checkPercentage.value = problemItems.isEmpty()

                for (problem in data) {
                    if (problem != null) {
                        if (problem.problem_detail?.stat?.toLowerCase(Locale.getDefault()) == "draft") {
                            problemItems.remove(problem)
                        }
                    }
                }

                loadLocalDataProblem()

                showOnlineData.set(data.isNotEmpty())
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                message.value = errorMessage
                if (statusCode == 404) {
                    showError(button = false, text = true)
                } else {
                    showError(button = true, text = true)
                }
            }

        })
    }

    private fun loadDataProblemHistory() {
        repository.getProblemHistory("", targetId, object : GitsDataSource.GetProblemCallback {
            override fun onShowProgressDialog() {
                showProgress.value = true
            }

            override fun onHideProgressDialog() {
                showProgress.value = false
            }

            override fun onSuccess(data: List<ItemProblemModel>) {
                problemHistoryItems.apply {
                    clear()
                    addAll(data)
                    reverse()
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                message.value = errorMessage
                if (statusCode == 404) {
                    showError(button = false, text = true)
                } else {
                    showError(button = true, text = true)
                }
            }

        })
    }

    fun saveAllData() {
        if (showOfflineData.get() == false) {
            successUpdateEvent.value = false
            return
        }
        val idList = ArrayList<Int>()
        problemItems.map {
            idList.add(it.id ?: 0)
        }
        for (offlineItem in problemOfflineItems) {
            if (idList.contains(offlineItem.id)) {
                //update
                val paramUpdateProblem = ParamUpdateProblem()
                val problemActionArray = ArrayList<ProblemActionParam>()
                offlineItem.problem_detail?.let { problemDetail ->
                    paramUpdateProblem.let { update ->
                        update.total_area = problemDetail.total_area
                        update.problem_area = problemDetail.problem_area
                        update.progress_overall = problemDetail.progress_overall
                        for (action in problemDetail.problem_action) {
                            val problemAction = ProblemActionParam(
                                    PIC = action.PIC,
                                    action_plan = action.action_plan,
                                    cost = action.cost,
                                    end_date = action.end_date,
                                    finished = action.finished,
                                    problem_detail = action.problem_detail,
                                    problem_scale = action.problem_scale,
                                    progress = action.progress,
                                    start_date = action.start_date,
                                    stat_cost = action.stat_cost,
                                    type_unit_id = action.type_unit_id,
                                    files = action.files,
                                    problem_type_id = action.problem_type_id,
                                    pokok = action.pokok,
                                    kalibrasi = action.kalibrasi,
                                    is_primary = action.is_primary,
                                    is_locked = action.is_locked,
                                    pre_progress = action.pre_progress
                            )
                            problemActionArray.add(problemAction)
                        }
                        update.problem_action = problemActionArray
                    }
                }
                count += 1
                updateUploadImages(offlineItem.id ?: 0, offlineItem.problem_target_id
                        ?: 0, paramUpdateProblem)
            } else {
                //post
                val paramCreateProblem = ParamCreateProblem()
                val problemActionArray = ArrayList<ProblemActionParam>()
                offlineItem.problem_detail?.let { problemDetail ->
                    paramCreateProblem.let { update ->
                        update.problem_target_id = "$targetId"
                        update.total_area = problemDetail.total_area
                        update.problem_area = problemDetail.problem_area
                        update.progress_overall = problemDetail.progress_overall
                        for (action in problemDetail.problem_action) {
                            val problemAction = ProblemActionParam(
                                    PIC = action.PIC,
                                    action_plan = action.action_plan,
                                    cost = action.cost,
                                    end_date = action.end_date,
                                    finished = action.finished,
                                    problem_detail = action.problem_detail,
                                    problem_scale = action.problem_scale,
                                    progress = action.progress,
                                    start_date = action.start_date,
                                    stat_cost = action.stat_cost,
                                    type_unit_id = action.type_unit_id,
                                    files = action.files,
                                    problem_type_id = action.problem_type_id,
                                    pokok = action.pokok,
                                    kalibrasi = action.kalibrasi,
                                    is_primary = action.is_primary,
                                    is_locked = action.is_locked,
                                    pre_progress = action.pre_progress
                            )
                            problemActionArray.add(problemAction)
                        }
                        update.problem_action = problemActionArray
                    }
                }
                count += 1
                postUploadImages(paramCreateProblem)
            }
        }
    }

    private fun postUploadImages(paramCreateProblem: ParamCreateProblem) {
        val problemTemp = ArrayList<ProblemActionParam>()
        problemTemp.addAll(paramCreateProblem.problem_action ?: arrayListOf())
        val parts = ArrayList<MultipartBody.Part>()
        val problemIndex = ArrayList<Int>()
        val fileIndex = HashMap<Int, ArrayList<Int>>()
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = false
        for ((pIndex, problemAction) in problemTemp.withIndex()) {
            val indexes = ArrayList<Int>()
            var tempIndex = 0
            for ((index, file) in problemAction.files?.withIndex() ?: arrayListOf()) {
                if (file?.link?.contains("/") == true) {
                    if (!problemIndex.contains(pIndex)) {
                        problemIndex.add(pIndex)
                        tempIndex = pIndex
                    }
                    indexes.add(index)
                    val bitmapTemp = BitmapFactory.decodeFile(file.link, options)
                    if (bitmapTemp != null && bitmapTemp.width > 0 && bitmapTemp.height > 0) {
                        val filePath = Compressor.getDefault(context).compressToFile(File(file.link))
                        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), filePath)
                        parts.add(MultipartBody.Part.createFormData("upload", filePath.name, requestBody))
                    }
                }
            }
            if (!fileIndex.containsKey(tempIndex)) {
                fileIndex[tempIndex] = indexes
            }
        }

        if (parts.isEmpty()) {
            postProblem(paramCreateProblem)
        } else {
            repository.uploadImages(parts, object : GitsDataSource.GetUploadedImages {
                override fun onShowProgressDialog() {

                }

                override fun onHideProgressDialog() {

                }

                override fun onSuccess(data: List<String>) {
                    var count = 0
                    for (pIndex in problemIndex) {
                        val filesTemp = ArrayList<ImageItemModel?>()
                        filesTemp.addAll(problemTemp[pIndex].files ?: arrayListOf())
                        if (filesTemp.isNotEmpty()) {
                            for (index in fileIndex[pIndex] ?: arrayListOf()) {
                                if (count < data.size) {
                                    filesTemp[index]?.link = data[count]
                                    count++
                                }
                            }
                            paramCreateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        } else {
                            paramCreateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        }
                    }
                    postProblem(paramCreateProblem)
                }

                override fun onFinish() {

                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    deleteUpdateData(targetId)
                    successUpdateEvent.value = false
                }

            })
        }
    }

    fun postProblem(paramCreateProblem: ParamCreateProblem) {
        repository.postProblem("", paramCreateProblem, object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: Any?) {
                deletePostData(paramCreateProblem)
                count -= 1
                if (count <= 0) {
                    successUpdateEvent.value = true
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                count -= 1
                if (count <= 0) {
                    successUpdateEvent.value = false
                }
            }

        })
    }

    private fun updateUploadImages(id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem) {
        val problemTemp = ArrayList<ProblemActionParam>()
        problemTemp.addAll(paramUpdateProblem.problem_action ?: arrayListOf())
        val parts = ArrayList<MultipartBody.Part>()
        val problemIndex = ArrayList<Int>()
        val fileIndex = HashMap<Int, ArrayList<Int>>()
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = false
        for ((pIndex, problemAction) in problemTemp.withIndex()) {
            val indexes = ArrayList<Int>()
            var tempIndex = 0
            for ((index, file) in problemAction.files?.withIndex() ?: arrayListOf()) {
                if (file?.link?.contains("/") == true) {
                    if (!problemIndex.contains(pIndex)) {
                        problemIndex.add(pIndex)
                        tempIndex = pIndex
                    }
                    indexes.add(index)
                    val bitmapTemp = BitmapFactory.decodeFile(file.link, options)
                    if (bitmapTemp != null && bitmapTemp.width > 0 && bitmapTemp.height > 0) {
                        val filePath = Compressor.getDefault(context).compressToFile(File(file.link))
                        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), filePath)
                        parts.add(MultipartBody.Part.createFormData("upload", filePath.name, requestBody))
                    }
                }
            }
            if (!fileIndex.containsKey(tempIndex)) {
                fileIndex[tempIndex] = indexes
            }
        }

        if (parts.isEmpty()) {
            updateProblem(id, targetId, paramUpdateProblem)
        } else {
            repository.uploadImages(parts, object : GitsDataSource.GetUploadedImages {
                override fun onShowProgressDialog() {

                }

                override fun onHideProgressDialog() {

                }

                override fun onSuccess(data: List<String>) {
                    var count = 0
                    for (pIndex in problemIndex) {
                        val filesTemp = ArrayList<ImageItemModel?>()
                        filesTemp.addAll(problemTemp[pIndex].files ?: arrayListOf())
                        if (filesTemp.isNotEmpty()) {
                            for (index in fileIndex[pIndex] ?: arrayListOf()) {
                                if (count < data.size) {
                                    filesTemp[index]?.link = data[count]
                                    count++
                                }
                            }
                            paramUpdateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        } else {
                            paramUpdateProblem.problem_action?.get(pIndex)?.files = filesTemp
                        }
                    }
                    updateProblem(id, targetId, paramUpdateProblem)
                }

                override fun onFinish() {

                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    deleteUpdateData(targetId)
                    successUpdateEvent.value = false
                }

            })
        }
    }

    fun updateProblem(id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem) {
        repository.updateProblem("", id, targetId, paramUpdateProblem, object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: Any?) {
                deleteUpdateData(targetId)

                count -= 1
                if (count <= 0) {
                    successUpdateEvent.value = true
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                count -= 1
                if (count <= 0) {
                    successUpdateEvent.value = false
                }
            }

        })
    }

    private fun deletePostData(paramCreateProblem: ParamCreateProblem) {
        try {
            val id = paramCreateProblem.problem_target_id?.toInt() ?: 0
            val tempProblemData = repository.localDataSource.getProblemListDBDao()
                    .getDataById(id) ?: ProblemListDBModel()
            if (GitsRepository.isOnline) {
                repository.localDataSource.getProblemListDBDao().deleteData(tempProblemData)
                val tempDraftData = repository.localDataSource.getDraftProblemDBDao().getDataById(id)
                repository.localDataSource.getDraftProblemDBDao().deleteData(tempDraftData)
            }

            //Reinsert local data
            val tempData = ProblemListDBModel()
            val tempList: ArrayList<ItemProblemModel>? = arrayListOf()
            tempList?.addAll(tempProblemData.problemList ?: listOf())
            tempData.let {
                it.id = tempProblemData.id
                it.dateUpdated = tempProblemData.dateUpdated
                if (GitsRepository.isOnline) {
                    for (problemData in tempProblemData.problemList ?: listOf()) {
                        tempList?.get(tempList.indexOf(problemData))?.problem_detail?.stat = "Waiting for Approval"
                    }
                } else {

                }
                it.problemList = tempList
            }
            repository.localDataSource.getProblemListDBDao().insertData(tempData)
        } catch (ex: NullPointerException) {
            //Catch null exception
        }
    }

    private fun deleteUpdateData(id: Int) {
        try {
            val tempProblemData = repository.localDataSource.getProblemListDBDao()
                    .getDataById(id) ?: ProblemListDBModel()
            if (GitsRepository.isOnline) {
                repository.localDataSource.getProblemListDBDao().deleteData(tempProblemData)
                val tempDraftData = repository.localDataSource.getDraftProblemDBDao().getDataById(id)
                repository.localDataSource.getDraftProblemDBDao().deleteData(tempDraftData)
            }

            //Reinsert local data
            val tempData = ProblemListDBModel()
            val tempList = tempProblemData.problemList
            tempData.let {
                it.id = tempProblemData.id
                it.dateUpdated = tempProblemData.dateUpdated
                if (GitsRepository.isOnline) {
                    for (problemData in tempProblemData.problemList ?: listOf()) {
                        tempList?.get(tempList.indexOf(problemData))?.problem_detail?.stat = "Waiting for Approval"
                    }
                }
                it.problemList = tempList
            }
            repository.localDataSource.getProblemListDBDao().insertData(tempData)
        } catch (ex: NullPointerException) {
            //Catch null exception
        }
    }

}