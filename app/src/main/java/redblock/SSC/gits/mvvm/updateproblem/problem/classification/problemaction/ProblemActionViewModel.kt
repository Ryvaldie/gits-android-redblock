package redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction;

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.model.local.HideDropDownModel
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.SingleLiveEvent
import java.text.NumberFormat


class ProblemActionViewModel(val repository: GitsRepository) : ViewModel() {

    val problemUnitArray = ObservableArrayList<UnitTypeModel>()
    val picArray = ObservableArrayList<PicModel>()
    val loadingProblemPic = SingleLiveEvent<Boolean>()
    val errorProblemPic = SingleLiveEvent<Boolean>()
    val loadingProblemUnit = SingleLiveEvent<Boolean>()
    val errorProblemUnit = SingleLiveEvent<Boolean>()
    val onBudgetField = ObservableField<Boolean>(false)
    val setProblemUnit = SingleLiveEvent<List<UnitTypeModel>>()
    val imageAfterArray = ObservableArrayList<ImageItemModel>()
    val imageAfterEvent = SingleLiveEvent<Void>()
    val imageBeforeArray = ObservableArrayList<ImageItemModel>()
    val imageBeforeEvent = SingleLiveEvent<Void>()
    val reloadImageEvent = SingleLiveEvent<Void>()
    val documentFirstView = SingleLiveEvent<Boolean>()
    var problemAction = ItemProblemModel.ProblemAction()
    var stringHideDropDownModel = ""
    val imagePreviewEvent = SingleLiveEvent<String>()
    val edittableField = ObservableBoolean(false)
    val edittableFieldKemajuan = ObservableBoolean(false)
    val edittableFieldSatuanMeter = ObservableBoolean(false)
    val showUpdateLocation = ObservableField<Boolean>()
    val updateLocation = SingleLiveEvent<Void>()
    val latLangValue = ObservableField<String>()
    val problemClassificationArray = ObservableArrayList<DropDownModel>()
    val setProblemType = SingleLiveEvent<List<DropDownModel>>()
    val titleSatuan = ObservableField<String>()
    val jumlahSatuan = ObservableField<String>()
    val calibrationMode = ObservableBoolean(false)
    val satuanMode = ObservableBoolean(false)
    val progressOverall = SingleLiveEvent<String>()
    val areaMasalahUpdate = SingleLiveEvent<String>()
    val kalibrasiValue = ObservableField<String>()
    val addAble = ObservableBoolean(true)
    val editSatuan = ObservableBoolean(false)
    val mKemajuan = ObservableField<String>()
    val fileImage = ObservableBoolean(false)
    val isLocked = ObservableBoolean(false)

    fun countProgressOverall(unitType: String, areaMasalah: String, jumlah: String, kemajuan: String, selesai: String) {
        progressOverall.value = selesai
        if (areaMasalah.isNotEmpty() && jumlah.isNotEmpty()) {
            when (unitType) {
                "kg", "pokok" -> {
                    if (!kalibrasiValue.get().isNullOrEmpty()) {
                        try {
                            val tempArea = if (jumlah.contains(",")) {
                                jumlah.replace(",", ".")
                            } else {
                                jumlah
                            }
                            val area = tempArea.toDouble()
                            val kalibrasi = kalibrasiValue.get()?.toDouble() ?: 0.0
                            val hasilJumlah = area * kalibrasi
                            jumlahSatuan.set(hasilJumlah.toString())
                        } catch (ex: Exception) {
                            // Do nothing
                        }
                    } else {
                        clearJumlahSatuan()
                    }
                }
            }
        } else {
            clearJumlahSatuan()
        }
    }

    fun clearJumlahSatuan() {
        jumlahSatuan.set("")
    }

    fun getProblemType() {
        repository.getProblemType("", object : GitsDataSource.GetProblemTypeCallback {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: List<ItemProblemTypeModel>?) {
                if (data != null) {
                    problemClassificationArray.addAll(data.map {
                        if (it != null) {
                            DropDownModel(it.id ?: 1, it.name ?: "-", it.description ?: "")
                        } else {
                            DropDownModel(0, "", "")
                        }
                    })
                    setProblemType.value = problemClassificationArray
                }

            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {

            }

        })
    }

    fun waitForLocationUpdate() {
        updateLocation.call()
        showUpdateLocation.set(false)
    }

    fun editMode(boolean: Boolean) {
        edittableField.set(boolean)
    }

    fun getEditMode(): Boolean {
        return edittableField.get()
    }

    fun editModeKemajuan(boolean: Boolean) {
        edittableFieldKemajuan.set(boolean)
    }

    fun getEditModeKemajuan(): Boolean {
        return edittableFieldKemajuan.get()
    }

    fun setFirstView(boolean: Boolean) {
        documentFirstView.value = boolean
    }

    fun editImage(boolean: Boolean) {
        fileImage.set(boolean)
    }

    fun getEditImage(): Boolean {
        return fileImage.get()
    }

    fun getUnitType() {
        repository.getUnit("", object : GitsDataSource.GetUnit {
            override fun onShowProgressDialog() {
                loadingProblemUnit.value = true
            }

            override fun onHideProgressDialog() {
                loadingProblemUnit.value = false
            }

            override fun onSuccess(data: List<UnitTypeModel>) {
                problemUnitArray.apply {
                    clear()
                    addAll(data)
                }
                setProblemUnit.value = problemUnitArray

            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                errorProblemUnit.value = true
            }

        })
    }

    fun getPic() {
        repository.getAllPic("", object : GitsDataSource.GetAllPic {
            override fun onShowProgressDialog() {
                loadingProblemPic.value = true
            }

            override fun onHideProgressDialog() {
                loadingProblemPic.value = false
            }

            override fun onSuccess(data: List<PicModel>) {
                picArray.apply {
                    clear()
                    addAll(data)
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                errorProblemPic.value = true
            }

        })
    }

}