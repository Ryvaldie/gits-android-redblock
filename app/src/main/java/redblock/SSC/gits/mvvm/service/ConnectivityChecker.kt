package redblock.SSC.gits.mvvm.service

import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager.CONNECTIVITY_ACTION
import android.os.Build
import androidx.annotation.RequiresApi
import redblock.SSC.gits.base.BaseActivity.Companion.CONNECTIVITY_CHANGE
import redblock.SSC.gits.data.source.GitsRepository


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class ConnectivityChecker : JobService(), ConnectivityChangeReceiver.ConnectivityReceiverListener {

    private lateinit var mConnectivityReceiver: ConnectivityChangeReceiver

    override fun onCreate() {
        super.onCreate()
        mConnectivityReceiver = ConnectivityChangeReceiver(this)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return START_NOT_STICKY
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        try {
            unregisterReceiver(mConnectivityReceiver)
        } catch (ex: IllegalArgumentException) {
            //Uncomment for debugging purpose
//            ex.printStackTrace()
        }
        return true
    }

    @Suppress("DEPRECATION")
    override fun onStartJob(params: JobParameters?): Boolean {
        registerReceiver(mConnectivityReceiver, IntentFilter(CONNECTIVITY_ACTION))
        return true
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        GitsRepository.setConnectivityStatus(isConnected)
        this.sendBroadcast(Intent(CONNECTIVITY_CHANGE).putExtra("STATUS", isConnected))
    }

}
