package redblock.SSC.gits.mvvm.userguide


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_user_guide.*
import kotlinx.android.synthetic.main.fragment_user_guide_content.*
import redblock.SSC.gits.R


private const val ARG_PARAM1 = "param1"

class UserGuideContent : Fragment() {

    private var param1: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_guide_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        content.setImageResource(param1 ?: 0)

    }

    companion object {

        @JvmStatic
        fun newInstance(param1: Int) =
                UserGuideContent().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_PARAM1, param1)
                    }
                }

    }
}
