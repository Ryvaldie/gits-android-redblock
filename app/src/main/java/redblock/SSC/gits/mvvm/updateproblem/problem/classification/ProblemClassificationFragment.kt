package redblock.SSC.gits.mvvm.updateproblem.problem.classification


import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_problem_action.*
import kotlinx.android.synthetic.main.fragment_problem_classification.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.data.model.local.HideDropDownModel
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.data.param.ParamUpdateProblem
import redblock.SSC.gits.data.param.ProblemActionParam
import redblock.SSC.gits.databinding.FragmentProblemClassificationBinding
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction.ProblemActionFragment
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction.ProblemActionViewPagerAdapter
import redblock.SSC.gits.util.*
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.util.*
import kotlin.collections.ArrayList


class ProblemClassificationFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentProblemClassificationBinding
    lateinit var mViewModel: ProblemClassificationViewModel
//    var fragmentAction = ArrayList<ProblemActionFragment>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentProblemClassificationBinding.inflate(inflater, container, false)
        mViewModel = (activity as ProblemClassificationActivity).obtainProblemClassificationViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : ProblemClassificationUserActionListener {

            override fun onClickSave() {
                countProgressOverall()
                mViewModel.showPostProgressEvent.set(true)
                if (validateField()) {
                    hideShowKeyboard(true)
                    postDataProblem()
                } else {
                    mViewModel.showPostProgressEvent.set(false)
                }
            }

        }

        mViewDataBinding.let {
            it.mViewModel = mViewDataBinding.mViewModel
            it.mListener = mViewDataBinding.mListener
            it.setLifecycleOwner(this@ProblemClassificationFragment)
        }

        return mViewDataBinding.root
    }

    fun postDataProblem() {
        val problemActionArray = ArrayList<ProblemActionParam>()
        for (fragment in (activity as ProblemClassificationActivity).fragmentAction) {
            val latLng = if (fragment.location.text.toString().isEmpty()) {
                arrayListOf("0", "0")
            } else {
                fragment.location.text.toString().split(",")
            }
            problemActionArray.add(ProblemActionParam(
                    PIC = fragment.mViewModel.problemAction.PIC,
                    action_plan = fragment.mViewModel.problemAction.action_plan,
                    cost = fragment.mViewModel.problemAction.cost,
                    end_date = fragment.mViewModel.problemAction.end_date,
                    finished = fragment.mViewModel.problemAction.finished,
                    problem_detail = fragment.mViewModel.problemAction.problem_detail,
                    problem_scale = fragment.mViewModel.problemAction.problem_scale,
                    progress = fragment.mViewModel.problemAction.progress,
                    start_date = fragment.mViewModel.problemAction.start_date,
                    stat_cost = fragment.mViewModel.problemAction.stat_cost,
                    type_unit_id = fragment.mViewModel.problemAction.type_unit_id,
                    files = fragment.mViewModel.problemAction.files,
                    problem_type_id = fragment.mViewModel.problemAction.problem_type_id,
                    lat = latLng[0],
                    lang = latLng[1],
                    pokok = fragment.mViewModel.problemAction.pokok,
                    kalibrasi = fragment.mViewModel.problemAction.kalibrasi,
                    is_primary = fragment.mViewModel.problemAction.is_primary,
                    is_locked = fragment.mViewModel.problemAction.is_locked,
                    pre_progress = fragment.mViewModel.problemAction.pre_progress
            ))

        }
        if ((activity as ProblemClassificationActivity).typeAction == TYPE_POST || (activity as ProblemClassificationActivity).typeAction == TYPE_DRAFT) {
            mViewModel.postUploadImages(
                    ParamCreateProblem(
                            (activity as ProblemClassificationActivity).id.toString(),
                            et_luas_total.text.toString(),
                            et_masalah_area.text.toString(),
                            etProgressOverall.text.toString(),
                            problemActionArray
                    )
            )
        } else {
            mViewModel.updateUploadImages((activity as ProblemClassificationActivity).data.id ?: 0,
                    (activity as ProblemClassificationActivity).data.problem_target_id ?: 0,
                    ParamUpdateProblem(
                            et_luas_total.text.toString(),
                            et_masalah_area.text.toString(),
                            etProgressOverall.text.toString(),
                            problemActionArray
                    )
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTypeAction()

        tabs?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
                if ((activity as ProblemClassificationActivity).mAdapter != null) {
                    (activity as ProblemClassificationActivity).mAdapter?.position = p0?.position
                            ?: 0
                }
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                if ((activity as ProblemClassificationActivity).mAdapter != null) {
                    (activity as ProblemClassificationActivity).mAdapter?.position = p0?.position
                            ?: 0
                }
            }

        })

        setEdit(false)

    }

    @Suppress("DEPRECATION")
    private fun setTypeAction() {
        mViewModel.editMode(
                when ((activity as ProblemClassificationActivity).data.problem_detail?.stat?.toLowerCase(Locale.getDefault())) {
                    "approved", "waiting", "revision", "close", "waiting for approval", "new", "" -> true
                    else -> false
                }
        )
        if (!mViewModel.getEditMode()) {
            setObserver()
        }
        et_luas_total.setText((activity as ProblemClassificationActivity).dataPost.problem_target?.area)
//        mViewModel.areaMasalahUpdate.set((activity as ProblemClassificationActivity).data.problem_detail?.problem_area)
        if ((activity as ProblemClassificationActivity).typeAction == TYPE_UPDATE) {
            btn_save.text = getString(R.string.sunting)
            btn_save.setTextColor(resources.getColor(R.color.sinarmasRed))
            btn_save.setBackgroundResource(R.drawable.button_background_border)
            initViewPager()
        } else {
            btn_save.text = getString(R.string.text_save)
            btn_save.setTextColor(resources.getColor(R.color.mainWhite))
            btn_save.setBackgroundResource(R.drawable.button_background_red)
            initEmptyViewPager()
        }

        buttonAdd.setOnClickListener {
            if ((activity as ProblemClassificationActivity).mAdapter != null) {
                main_viewpager?.offscreenPageLimit = (activity as ProblemClassificationActivity).mAdapter?.count
                        ?: 0
                (activity as ProblemClassificationActivity).mAdapter?.addView()
                setTabLongClick()
            }
        }

    }

    private fun initViewPager() {
        val data = (activity as ProblemClassificationActivity).data
        val problemActions = data.problem_detail?.problem_action ?: arrayListOf()
        if (problemActions.isEmpty()) {
            initEmptyViewPager()
        } else {
            for (problemAction in problemActions) {
                val dataTemp = Gson().toJson(problemAction, ItemProblemModel.ProblemAction::class.java)
                (activity as ProblemClassificationActivity).fragmentAction.apply {
                    add(ProblemActionFragment.newInstance(dataTemp))
                }
            }
            setupViewPager((activity as ProblemClassificationActivity).fragmentAction)
        }
    }

    private fun initEmptyViewPager() {
        (activity as ProblemClassificationActivity).fragmentAction.apply {
            add(ProblemActionFragment.newInstance())
        }
        setupViewPager((activity as ProblemClassificationActivity).fragmentAction)
    }

    private fun setupViewPager(fragmentAction: ArrayList<ProblemActionFragment>) {
        main_viewpager?.apply {
            (activity as ProblemClassificationActivity).mAdapter = ProblemActionViewPagerAdapter(fragmentAction, childFragmentManager)
            adapter = (activity as ProblemClassificationActivity).mAdapter
            (adapter as ProblemActionViewPagerAdapter).replaceItem(fragmentAction)
            offscreenPageLimit = fragmentAction.size
        }
        tabs?.setupWithViewPager(main_viewpager)
        setTabLongClick()
    }

    @SuppressLint("InflateParams")
    private fun setTabLongClick() {
        val tabStrip = tabs?.getChildAt(0) as LinearLayout
        if (tabStrip.childCount > 1) {
            for (i in 1 until tabStrip.childCount) {
                val customTab = LayoutInflater.from(requireContext()).inflate(R.layout.custom_tablayout, null)
                val title = customTab.findViewById<TextView>(R.id.tabTitle)
                val close = customTab.findViewById<ImageView>(R.id.closeTab)
                if ((activity as ProblemClassificationActivity).mAdapter != null) {
                    title.text = (activity as ProblemClassificationActivity).mAdapter?.getPageTitle(i)
                    close.setOnClickListener {
                        dialogDeleteAction(i, (activity as ProblemClassificationActivity).mAdapter!!)
                    }

                    Handler().postDelayed({
                        if (activity != null) {
                            val isLocked = (activity as ProblemClassificationActivity).mAdapter?.getIsLocked(i) ?: false
                            if(isLocked) {
                                close.visibility = View.GONE
                            } else close.visibility = View.VISIBLE
                        }
                    }, 50)
                }
                tabs.getTabAt(i)?.customView = customTab
            }
        }
    }

    private fun setObserver() {
        mViewModel.successPost.observe(this@ProblemClassificationFragment, Observer {
            (activity as ProblemClassificationActivity).setResult(RESULT_CODE_PROBLEM_CLASSIFICATION)
            (activity as ProblemClassificationActivity).finish()
        })

        mViewModel.failPost.observe(this@ProblemClassificationFragment, Observer {
            val snackBar = Snackbar.make(mViewDataBinding.root, mViewModel.messageFail, Snackbar.LENGTH_SHORT)
            snackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text).maxLines = 3
            snackBar.show()
        })

        et_luas_total.addTextChangedListener(finishedWatcher())

        et_masalah_area.addTextChangedListener(finishedWatcher())

        mViewModel.updatePrimaryProblem.observe(this, Observer {
            if (it != null) {
                val fragmentAction = ArrayList<ProblemActionFragment>()
                for ((index, data) in (activity as ProblemClassificationActivity).mAdapter?.views?.withIndex()
                        ?: arrayListOf()) {
                    if (index != it) {
                        data.mViewModel.problemAction.is_primary = 0
                    }
                    val dataTemp = Gson().toJson(data.mViewModel.problemAction, ItemProblemModel.ProblemAction::class.java)
                    fragmentAction.add(ProblemActionFragment.newInstance(dataTemp))
                }
                (activity as ProblemClassificationActivity).fragmentAction = fragmentAction
                setupViewPager(fragmentAction)
            }
        })
    }

    private fun finishedWatcher() = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            val luasTotal = et_luas_total.text.toString()
            (activity as ProblemClassificationActivity).problemArea.value = et_masalah_area.text.toString()
            if (luasTotal.trim().isNotEmpty() && ((activity as ProblemClassificationActivity).problemArea.value
                            ?: "").trim().isNotEmpty()) {
//                try {
//                    if (((activity as ProblemClassificationActivity).problemArea.value
//                                    ?: "0").toFloat() > luasTotal.toFloat()) {
//                        et_masalah_area.error = getString(R.string.text_masalah_area_lebih_besar_total)
//                        et_masalah_area.removeTextChangedListener(this)
//                        et_masalah_area.setText("")
//                        et_masalah_area.addTextChangedListener(this)
//                    }
//                } catch (ex: NumberFormatException) {
//
//                }
            }
            setEdit(true)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }

    }

    private fun setEdit(boolean: Boolean) {
        sharedPreferences.edit().putBoolean("ISEDIT", boolean).apply()
    }

    fun validateField(): Boolean {
        var isValid = true

        if (et_luas_total.text.trim().isEmpty()) {
            et_luas_total.error = getString(R.string.text_must_be_filled)
            isValid = false
        }
        if (et_masalah_area.text.trim().isEmpty()) {
            et_masalah_area.error = getString(R.string.text_must_be_filled)
            isValid = false
        }

        val problemActionArray = ArrayList<ItemProblemModel.ProblemAction>()
        for (fragment in (activity as ProblemClassificationActivity).fragmentAction) {
            problemActionArray.add(fragment.mViewModel.problemAction)
        }

        var isMajorProblemSelected = true

        for (itemProblem in problemActionArray) {
            if ((itemProblem.problem_type_id
                            ?: "").trim().isEmpty() || itemProblem.problem_type_id == "0") {
                isValid = false
            }
//            if (itemProblem.is_primary == 1) {
//                isMajorProblemSelected = true
//            }
            if ((itemProblem.progress ?: "").trim().isEmpty()) {
                isValid = false
            }
            if ((itemProblem.stat_cost ?: "").trim().isEmpty()) {
                isValid = false
            } else {
                if ((itemProblem.stat_cost ?: "") == getString(R.string.tidak_sesuai_anggaran)) {
                    if ((itemProblem.cost ?: "").trim().isEmpty()) {
                        isValid = false
                    }
                }
            }
            if ((itemProblem.problem_scale ?: "").trim().isEmpty()) {
                isValid = false
            }
            if ((itemProblem.problem_detail ?: "").trim().isEmpty()) {
                isValid = false
            }
            if ((itemProblem.action_plan ?: "").trim().isEmpty()) {
                isValid = false
            }
            if ((itemProblem.start_date
                            ?: "").trim().isEmpty() || itemProblem.start_date == getString(R.string.text_select_month)) {
                isValid = false
            }
            if ((itemProblem.end_date
                            ?: "").trim().isEmpty() || itemProblem.end_date == getString(R.string.text_select_month)) {
                isValid = false
            }
            if ((itemProblem.type_unit?.unit ?: "").trim().isEmpty()) {
                isValid = false
            }
            if ((itemProblem.PIC ?: "").trim().isEmpty()) {
                isValid = false
            }
            if (itemProblem.files?.isEmpty() == true) {
                isValid = false
            }
        }
        if (!isMajorProblemSelected) {
            mViewDataBinding.root.showSnackbarDefault(mViewDataBinding.root, getString(R.string.text_validasi_pilih_masalah_utama),
                    GitsHelper.Const.SNACKBAR_TIMER_SHOWING_DEFAULT)
            isValid = false
        } else {
            if (!isValid) {
                mViewDataBinding.root.showSnackbarDefault(mViewDataBinding.root, getString(R.string.message_isi_field),
                        GitsHelper.Const.SNACKBAR_TIMER_SHOWING_DEFAULT)
            }
        }

        return isValid
    }

    private fun dialogDeleteAction(position: Int, mAdapter: ProblemActionViewPagerAdapter) {
        val dialog = Dialog(context!!)
        dialog.setContentView(R.layout.dialog_back)
        dialog.setCanceledOnTouchOutside(false)

        val subject = dialog.findViewById<TextView>(R.id.subject)
        val problemActionTitle = mAdapter.getPageTitle(position)
        subject.text = getString(R.string.text_hapus_action_placeholder, problemActionTitle)

        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.text = getString(R.string.text_hapus)
        buttonOk.setOnClickListener {
            mAdapter.removeView(position)
            val fragmentAction = ArrayList<ProblemActionFragment>()
            for (data in mAdapter.views) {
                val dataTemp = Gson().toJson(data.mViewModel.problemAction, ItemProblemModel.ProblemAction::class.java)
                fragmentAction.add(ProblemActionFragment.newInstance(dataTemp))
            }
            setupViewPager(fragmentAction)
            (activity as ProblemClassificationActivity).fragmentAction = fragmentAction
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if ((activity as ProblemClassificationActivity).mAdapter != null) {
            (activity as ProblemClassificationActivity).mAdapter?.getCurrentItem()?.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun countProgressOverall() {
        Handler().postDelayed({
            var progress = 0f
            var count = 0
            var countArea = 0
            var resultMasalahArea = 0f
            val fragmentAction = (activity as? ProblemClassificationActivity)?.fragmentAction
            if (fragmentAction != null && fragmentAction.size > 0) {
                for (fragment in fragmentAction) {
                    val value = fragment.mViewModel.progressOverall.value
                    val masalahAreaValue = fragment.mViewModel.areaMasalahUpdate.value
                    if (value?.isNotEmpty() == true) {
                        count++
                        progress += value.toFloat()
                    }

                    if (masalahAreaValue?.isNotEmpty() == true) {
                        countArea++
                        resultMasalahArea += masalahAreaValue.toFloat()
                    }
                }
            }

            progress = (progress / count).roundDecimal(2)
            resultMasalahArea = (resultMasalahArea).roundDecimal(2)
            if (isResumed) {
                mViewModel.progressOverall.set(progress.toString())
                mViewModel.areaMasalahUpdate.set(resultMasalahArea.toString())
                countProgressOverall()
            }
        }, 500)
    }

    override fun onResume() {
        super.onResume()
        countProgressOverall()
    }

    companion object {

        fun newInstance() = ProblemClassificationFragment().apply {

        }

    }

}
