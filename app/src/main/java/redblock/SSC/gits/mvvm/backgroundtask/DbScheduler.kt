package redblock.SSC.gits.mvvm.backgroundtask

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Environment
import android.os.Handler
import android.preference.PreferenceManager
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.Func
import redblock.SSC.gits.data.model.AllDataModel
import redblock.SSC.gits.data.model.local.DraftProblemDBModel
import redblock.SSC.gits.data.model.local.DraftProblemTargetDBModel
import redblock.SSC.gits.data.model.local.ProblemDetailDBModel
import redblock.SSC.gits.data.model.local.ProblemListDBModel
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.data.param.ParamUpdateProblemTarget
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.Injection
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class DbScheduler : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.hasExtra("UPDATE") == true) {
            val repository = obtainRepository(context)
            val currentDate = Calendar.getInstance().get(Calendar.DATE)
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            if (currentDate == 1 && sharedPreferences.getBoolean(GitsHelper.Const.IS_FIRST_UPDATE, false)) {
                deleteOldData(repository)
                getAllData(context, repository)
            } else if (currentDate != 1) {
                sharedPreferences.edit().putBoolean(GitsHelper.Const.IS_FIRST_UPDATE, true).commit()
            }
        }
    }

    private fun obtainRepository(context: Context?): GitsRepository? {
        return if (context != null) {
            Injection.provideGitsRepository(context)
        } else {
            null
        }
    }


    private fun deleteOldData(repository: GitsRepository?) {
        if (repository != null) {
            repository.localDataSource.appExecutors.diskIO.execute {
                val dir = File(Environment.getExternalStorageDirectory().path + "/RedBlock")
                if (dir.exists()) {
                    dir.delete()
                }
            }
            val deleteDataList = ArrayList<ProblemListDBModel>()
            val deleteDataDraft = ArrayList<DraftProblemDBModel>()
            val deleteDataListTarget = ArrayList<ProblemDetailDBModel>()
            val deleteDataDraftTarget = ArrayList<DraftProblemTargetDBModel>()
            val currentMonth = DateHelper.getTimeStamp().split(" ")[0].split("-")[1]

            repository.getDraftProblem(object : GitsDataSource.GetDraftProblemCallback {
                override fun onShowProgressDialog() {

                }

                override fun onHideProgressDialog() {

                }

                override fun onSuccess(data: List<ParamCreateProblem?>?) {
                    if (data != null) {
                        for (paramCreateProblem in data) {
                            val id = paramCreateProblem?.problem_target_id?.toInt() ?: 0
                            val tempProblemData = repository.localDataSource.getProblemListDBDao()
                                    .getDataById(id) ?: ProblemListDBModel()
                            if (tempProblemData.dateUpdated?.split(" ")?.get(0)?.split("-")?.get(1) != currentMonth) {
                                deleteDataList.add(tempProblemData)
                            }
                            val tempDraftData = repository.localDataSource.getDraftProblemDBDao()
                                    .getDataById(id)
                            if (tempDraftData.dateUpdated?.split(" ")?.get(0)?.split("-")?.get(1) != currentMonth) {
                                deleteDataDraft.add(tempDraftData)
                            }
                        }
                        for (tempProblemData in deleteDataList) {
                            repository.localDataSource.getProblemListDBDao().deleteData(tempProblemData)
                        }
                        for (tempDraftData in deleteDataDraft) {
                            repository.localDataSource.getDraftProblemDBDao().deleteData(tempDraftData)
                        }
                    }
                }

                override fun onFinish() {

                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {

                }

            })
            repository.getDraftProblemTarget(object : GitsDataSource.GetDraftProblemTargetCallback {
                override fun onShowProgressDialog() {

                }

                override fun onHideProgressDialog() {

                }

                override fun onSuccess(data: List<ParamUpdateProblemTarget?>?) {
                    if (data != null) {
                        for (paramUpdateProblemTarget in data) {
                            val id = repository.getBlockContent(paramUpdateProblemTarget?.problem_check_id
                                    ?: 0)?.problem_target?.id ?: 0
                            val tempProblemTargetData = repository.localDataSource.getProblemTargetDBDao()
                                    .getDataById(id) ?: ProblemDetailDBModel()
                            if (tempProblemTargetData.dateUpdated?.split(" ")?.get(0)?.split("-")?.get(1) != currentMonth) {
                                deleteDataListTarget.add(tempProblemTargetData)
                            }
                            val tempData = repository.localDataSource.getDraftProblemTargetDao().getDataById(id)
                            if (tempData.dateUpdated?.split(" ")?.get(0)?.split("-")?.get(1) != currentMonth) {
                                deleteDataDraftTarget.add(tempData)
                            }
                        }
                        for (tempProblemTargetData in deleteDataListTarget) {
                            repository.localDataSource.getProblemTargetDBDao().deleteData(tempProblemTargetData)
                        }
                        for (tempData in deleteDataDraftTarget) {
                            repository.localDataSource.getDraftProblemTargetDao().deleteData(tempData)
                        }
                    }
                }

                override fun onFinish() {

                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {

                }

            })
        }
    }

    fun getAllData(context: Context?, repository: GitsRepository?) {
        if (repository == null) {
            return
        }
        val data = repository.localDataSource.getUserDBDao().getAllData()?.filter {
            it.dateUpdated != null
        }
        val div = data?.get(data.lastIndex)?.userConfig?.division ?: ""
        val psm = data?.get(data.lastIndex)?.userConfig?.psm ?: ""
        val region = data?.get(data.lastIndex)?.userConfig?.region ?: ""
        repository.getAllData("", div, psm, region, true, object : GitsDataSource.GetAllData {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: AllDataModel) {
                createFilePath(context, repository, data)
                val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                sharedPreferences.edit().putBoolean(GitsHelper.Const.IS_FIRST_UPDATE, false).commit()
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {

            }

        })
    }

    fun createFilePath(context: Context?, repository: GitsRepository?, data: AllDataModel) {
        if (context == null || repository == null) {
            return
        }
        val fetchConfiguration = FetchConfiguration.Builder(context)
                .setDownloadConcurrentLimit(50)
                .enableFileExistChecks(false)
                .enableHashCheck(false)
                .build()
        val fetch = Fetch.getInstance(fetchConfiguration)
        try {
            val dir = File(Environment.getExternalStorageDirectory().path + "/RedBlock")
            if (!dir.exists()) {
                dir.mkdir()
            } else {
                deleteRecursive(dir)
                dir.mkdir()
            }

            val requests = ArrayList<Request>()
            for ((index1, content) in data.problem_check.withIndex()) {
                val itemProblemModel = content.problem_target?.problem ?: arrayListOf()
                for ((index2, item) in itemProblemModel.withIndex()) {
                    val problemAction = item.problem_detail?.problem_action ?: arrayListOf()
                    for ((index3, action) in problemAction.withIndex()) {
                        if (action != null) {
                            for ((index4, file) in (action.files ?: arrayListOf()).withIndex()) {
                                val fileName = Environment.getExternalStorageDirectory().path + "/RedBlock/" +
                                        "${file?.created_at
                                                ?: SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(Date())}$index1$index2$index3$index4.jpeg"
                                val request = Request(GitsHelper.Const.BASE_IMAGE_URL + (file?.link
                                        ?: ""), fileName)
                                request.priority = Priority.HIGH
                                request.networkType = NetworkType.ALL
                                request.addHeader("INDEX", "$index1,$index2,$index3,$index4")
                                requests.add(request)
                            }
                        }
                    }
                }
            }

            fetch.cancelAll()
            fetch.removeAll()
            fetch.enqueue(requests)

            fetch.getDownloads(Func {
                if (it.isNotEmpty()) {
                    if (!fetch.hasActiveDownloads) {
                        for (d in it) {
                            val indexes = d.headers["INDEX"] ?: ""
                            if (indexes.contains(",")) {
                                val index1 = indexes.split(",")[0].toInt()
                                val index2 = indexes.split(",")[1].toInt()
                                val index3 = indexes.split(",")[2].toInt()
                                val index4 = indexes.split(",")[3].toInt()
                                data.problem_check[index1].problem_target?.problem?.get(index2)?.problem_detail?.problem_action?.get(index3)?.files?.get(index4)?.link = d.file
                            }
                        }
                        if (it.isNotEmpty()) {
                            for (problemCheck in data.problem_check) {
                                repository.localDataSource.getProblemListDBDao().insertData(
                                        ProblemListDBModel(
                                                problemCheck.problem_target?.id,
                                                DateHelper.getTimeStamp(),
                                                problemCheck.problem_target?.problem
                                        )
                                )
                            }
                        }
                        Handler().postDelayed({
                            fetch.close()
                        }, 10000)
                    }
                }
            })

        } catch (e: Throwable) {
            //Left empty
        }
    }

    private fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory) {
            for (child in fileOrDirectory.listFiles()) {
                deleteRecursive(child)
            }
        }

        fileOrDirectory.delete()
    }

}