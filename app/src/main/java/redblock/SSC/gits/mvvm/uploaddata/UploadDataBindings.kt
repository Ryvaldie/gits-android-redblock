package redblock.SSC.gits.mvvm.uploaddata;

import android.databinding.BindingAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.data.param.ParamUpdateProblemTarget

/**
 * @author radhikayusuf.
 */

object UploadDataBindings {

    @BindingAdapter("app:uploadDataItems", "app:uploadListener", "app:pagingUpload", "app:uploadViewModel")
    @JvmStatic
    fun setupListProblem(recyclerView: RecyclerView, list: List<ParamCreateProblem>, listener: UploadDataUserActionListener, page: Int, uploadDataViewModel: UploadDataViewModel) {
        recyclerView.apply {
            if (adapter == null) {
                adapter = UploadDataItemAdapter(ArrayList(0), listener, uploadDataViewModel)
                layoutManager = LinearLayoutManager(context)
            }
            (adapter as UploadDataItemAdapter).replaceList(list, page)
        }
    }

    @BindingAdapter("app:uploadTargetDataItems", "app:uploadTargetListener", "app:pagingTargetUpload", "app:uploadTargetViewModel")
    @JvmStatic
    fun setupListProblemTarget(recyclerView: RecyclerView, list: List<ParamUpdateProblemTarget>, listener: UploadDataUserActionListener, page: Int, uploadDataViewModel: UploadDataViewModel) {
        recyclerView.apply {
            if (adapter == null) {
                adapter = UploadDataItemTargetAdapter(ArrayList(0), listener, uploadDataViewModel)
                layoutManager = LinearLayoutManager(context)
            }
            (adapter as UploadDataItemTargetAdapter).replaceList(list, page)
        }
    }


    @BindingAdapter("app:uploadDataProblem", "app:UploadDataUserActionListener", "app:uploadDataViewModel")
    @JvmStatic
    fun setupListProblemData(recyclerView: RecyclerView, list: List<Content>, listener: UploadDataUserActionListener, uploadDataViewModel: UploadDataViewModel) {
        recyclerView.apply {
            if (adapter == null) {
                adapter = UploadDataProblemItemAdapter(ArrayList(0), listener, uploadDataViewModel)
                layoutManager = LinearLayoutManager(context)
            }
            (adapter as UploadDataProblemItemAdapter).replaceList(list, 0)
        }
    }

}