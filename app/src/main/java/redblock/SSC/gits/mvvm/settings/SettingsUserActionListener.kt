package redblock.SSC.gits.mvvm.settings;


interface SettingsUserActionListener {

    fun onClickBantuan()

    fun onClickUserConfig()

    fun onClickBahasa()

    fun onClickAbout()

    fun onClickLogout()

    fun onDeleteOldData()

}