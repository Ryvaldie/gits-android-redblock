package redblock.SSC.gits.mvvm.problemcheck;

import android.widget.TextView
import redblock.SSC.gits.data.model.Content


interface ProblemCheckUserActionListener {

    fun onClickItem(data: Content)

    fun onClickWarna(textView: TextView)

}