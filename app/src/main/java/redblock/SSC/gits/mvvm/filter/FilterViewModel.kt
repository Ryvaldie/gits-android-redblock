package redblock.SSC.gits.mvvm.filter;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import redblock.SSC.gits.data.model.FilterBlockModel
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.SingleLiveEvent


class FilterViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val weeklyField = ObservableField<Boolean>(false)
    val monthlyField = ObservableField<Boolean>(false)
    val yearlyField = ObservableField<Boolean>(false)
    val customField = ObservableField<Boolean>(false)
    var endTime = ""
    var startTime = ""
    val blockList = ObservableArrayList<FilterBlockModel>()
    var blockCheckList = ""
    private val divisiList = ObservableArrayList<String>()
    val pagingBlock: Int? = 0
    val loadEvent = ObservableField<Boolean>(false)
    val succesGetBlockEvent = SingleLiveEvent<Void>()

    fun start() {
        getAllBlock()
        divisiList.addAll(arrayListOf("MNHE", "MNHE", "MNHE"))
    }

    private fun getAllBlock() {
        repository.getAllBlock("", repository.localDataSource.getUserDBDao()
                .getDataById(0)?.userConfig?.division
                ?: "", "Kalimantan Utara", "Gumas", object : GitsDataSource.GetAllBlock {
            override fun onShowProgressDialog() {
                loadEvent.set(false)
            }

            override fun onHideProgressDialog() {
                loadEvent.set(true)
            }

            override fun onSuccess(data: List<String>) {
                blockList.apply {
                    clear()
                    for (block in data) {
                        add(FilterBlockModel(block, false))
                    }
                }
                loadEvent.set(true)
                succesGetBlockEvent.call()
            }

            override fun onFinish() {
                loadEvent.set(true)
            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                loadEvent.set(true)
            }

        })
    }


    fun clearCheck() {
        val tempBlock = ArrayList<FilterBlockModel>()
        for (block in blockList) {
            tempBlock.add(FilterBlockModel(block.block, false))
        }
        blockList.apply {
            clear()
            addAll(tempBlock)
        }
    }

}