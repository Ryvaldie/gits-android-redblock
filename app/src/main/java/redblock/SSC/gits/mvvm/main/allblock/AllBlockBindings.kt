package redblock.SSC.gits.mvvm.main.allblock;

import android.databinding.BindingAdapter
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TableLayout
import android.widget.TextView
import redblock.SSC.gits.data.model.Item
import redblock.SSC.gits.data.model.ProgressActionModel
import redblock.SSC.gits.util.ActionProgressAdapter
import redblock.SSC.gits.util.ItemChartAdapter


/**
 * @author radhikayusuf.
 */

object AllBlockBindings {

    @BindingAdapter("app:actionProgressItems", "app:actionProgressListener", "app:pagingActionProgress", "app:isDetail")
    @JvmStatic
    fun setupListActionProgress(recyclerView: RecyclerView, listAction: List<ProgressActionModel>, listener: AllBlockUserActionListener, page: Int, isDetail: Boolean) {
        recyclerView.apply {
            if (adapter == null) {
                adapter = ActionProgressAdapter(ArrayList(0), listener, isDetail)
                layoutManager = LinearLayoutManager(context)
            }
            (adapter as ActionProgressAdapter).replaceList(listAction, page)
        }
    }

    @BindingAdapter("layout_percent_progress")
    @JvmStatic
    fun setLayoutConstraintWidthPercent(view: View, percent: Float) {
        val constraintLayout = view.parent as ConstraintLayout
        val constraintSet = ConstraintSet()
        constraintSet.clone(constraintLayout)
        constraintSet.constrainHeight(view.id, 15)
        constraintSet.constrainPercentWidth(view.id, percent)
        constraintSet.applyTo(constraintLayout)
    }

    @BindingAdapter("app:chartItems", "app:chartListener", "app:pagingChart")
    @JvmStatic
    fun setupListChart(recyclerView: RecyclerView, listItems: List<Item>, listener: AllBlockUserActionListener, page: Int) {
        recyclerView.apply {
            if (adapter == null) {
                adapter = ItemChartAdapter(ArrayList(0))
                layoutManager = LinearLayoutManager(context)
            }
            (adapter as ItemChartAdapter).replaceList(listItems, 0)
        }
    }

    @BindingAdapter("app:addDateChart")
    @JvmStatic
    fun addDateTextChart(linearLayout: LinearLayout, text: List<String>) {
        linearLayout.weightSum = (text.size).toFloat()
        for (t in text) {
            val child = TextView(linearLayout.context)
            child.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT, 1f)
            child.gravity = Gravity.CENTER
            child.textSize = (28 / text.size).toFloat()
            child.text = t
            linearLayout.addView(child)
        }
    }

    @BindingAdapter("app:percentText")
    @JvmStatic
    fun percentText(text : TextView, data : String){
        text.text = (data.toDouble() * 100).toInt().toString() + "%"
    }

}