package redblock.SSC.gits.mvvm.camera


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.*
import android.hardware.Camera
import android.media.ExifInterface
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_custom_camera.*
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.RuntimePermissions
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.databinding.FragmentCustomCameraBinding
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.ProblemClassificationActivity
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


@Suppress("deprecation")
@RuntimePermissions
class CustomCameraFragment : BaseFragment(), SurfaceHolder.Callback, Camera.ShutterCallback
        , Camera.PictureCallback, Camera.PreviewCallback {

    lateinit var mViewDataBinding: FragmentCustomCameraBinding
    lateinit var mViewModel: CustomCameraViewModel
    var camera: Camera? = null
    private var inPreview = false
    private var data: ByteArray? = null
    private var cameraParameters: Camera.Parameters? = null
    private var onAskPermissionState = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentCustomCameraBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as CustomCameraActivity).obtainCameraViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : CustomCameraUserActionListener {
            override fun onClickDone() {
                saveImage()
            }

            override fun onClickCancel() {
                if (inPreview && camera != null) {
                    mViewModel.viewField.set(false)
                    camera?.startPreview()
                }
            }

            override fun onClickBack() {
                requireActivity().onBackPressed()
            }

            override fun onClickCapture() {
                if (camera != null) {
                    camera?.setOneShotPreviewCallback(this@CustomCameraFragment)
                }
            }

        }

        return mViewDataBinding.root
    }

    override fun onShutter() {

    }

    private fun saveImage() {
        if (data != null) {
            Thread {
                try {
                    val dir = File(Environment.getExternalStorageDirectory().path + "/RedBlock")
                    if (!dir.exists()) {
                        dir.mkdir()
                    }

                    val fileName = SimpleDateFormat("ddMMyyyyHHmmss", Locale.getDefault()).format(Date())
                    val file = File(Environment.getExternalStorageDirectory().path + "/RedBlock/" + "$fileName.jpg")
                    //Compress image
                    val yuv = YuvImage(data, ImageFormat.NV21, cameraParameters?.previewSize?.width
                            ?: 800
                            , cameraParameters?.previewSize?.height ?: 800, null)
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    yuv.compressToJpeg(Rect(0, 0, cameraParameters?.previewSize?.width ?: 800
                            , cameraParameters?.previewSize?.height
                            ?: 800), 100, byteArrayOutputStream)
                    val imageBytes = byteArrayOutputStream.toByteArray()
                    val bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                            , 800, 800, false)
                    val out = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, out)
                    if (!file.exists()) {
                        file.createNewFile()
                        val outputStream = FileOutputStream(file)
                        outputStream.write(out.toByteArray())
                        outputStream.flush()
                        outputStream.close()
                    }
                    //Change rotation
                    val exifInterface = ExifInterface(file.path)
                    exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, "${ExifInterface.ORIENTATION_ROTATE_90}")
                    exifInterface.saveAttributes()

                    MediaScannerConnection.scanFile(requireContext(), arrayOf(file.absolutePath), null)
                    { _, _ ->
                        requireActivity().runOnUiThread {
                            val intent = Intent(requireContext(), ProblemClassificationActivity::class.java)
                            intent.putExtra("TYPE", (activity as CustomCameraActivity).type)
                            intent.putExtra("LINK", file.absolutePath)
                            (activity as CustomCameraActivity).setResult(Activity.RESULT_OK, intent)
                            requireActivity().onBackPressed()
                        }
                    }
                } catch (e: IOException) {
                    requireActivity().runOnUiThread {
                        toast(e.localizedMessage)
                        requireActivity().onBackPressed()
                    }
                }
            }.start()
        }
    }

    override fun onPictureTaken(p0: ByteArray?, p1: Camera?) {
        if (p0 != null) {
            data = p0
            mViewModel.viewField.set(true)
            p1?.stopPreview()
        }
    }

    override fun onPreviewFrame(data: ByteArray?, camera: Camera?) {
        if (data != null) {
            cameraParameters = camera?.parameters
            this.data = data
            mViewModel.viewField.set(true)
            camera?.stopPreview()
        }
    }

    @NeedsPermission(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun startCamera() {
        try {
            val surfaceHolder = camera_view.holder
            surfaceHolder.addCallback(this)
            camera = Camera.open()
            camera?.setDisplayOrientation(90)

            camera?.setPreviewDisplay(surfaceHolder)
            camera?.startPreview()
            inPreview = true
        } catch (ex: RuntimeException) {
            toast(ex.localizedMessage)
            return
        }

    }

    @OnNeverAskAgain(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onCameraNeverAskAgain() {
        if(onAskPermissionState == 0) {
            showNeverAskAgainDialog(getString(R.string.text_open_setting_camera_permission))
            onAskPermissionState = 1
        }
    }

    override fun onResume() {
        super.onResume()
        if (activity != null && isAdded && mViewModel.viewField.get() == false) {
            Handler().postDelayed({
                try {
                    CustomCameraFragmentPermissionsDispatcher.startCameraWithCheck(this)
                } catch (ex: IllegalStateException) {

                }
            }, 2000)
        }
    }

    override fun onPause() {
        if (inPreview && camera != null && mViewModel.viewField.get() == false) {
            camera?.stopPreview()
            camera?.release()
            camera = null
        }
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (camera != null) {
            camera?.release()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        CustomCameraFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {

    }

    override fun surfaceDestroyed(p0: SurfaceHolder?) {

    }

    override fun surfaceCreated(p0: SurfaceHolder?) {

    }

    companion object {
        fun newInstance() = CustomCameraFragment().apply {

        }

    }

}
