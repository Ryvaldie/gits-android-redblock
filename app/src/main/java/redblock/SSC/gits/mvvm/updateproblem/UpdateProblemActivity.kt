package redblock.SSC.gits.mvvm.updateproblem;

import android.arch.lifecycle.Observer
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.toolbar.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.mvvm.filter.FilterActivity
import redblock.SSC.gits.mvvm.updateproblem.detail.DetailViewModel
import redblock.SSC.gits.mvvm.updateproblem.problem.ProblemViewModel
import redblock.SSC.gits.util.CODE_PROBLEM_CLASSIFICATION
import redblock.SSC.gits.util.RESULT_CODE_PROBLEM_CLASSIFICATION
import redblock.SSC.gits.util.obtainViewModel
import redblock.SSC.gits.util.replaceFragmentInActivity


class UpdateProblemActivity : BaseActivity() {

    private lateinit var back: ImageView
    private lateinit var title: TextView
    private lateinit var mainTitle: RelativeLayout
    private lateinit var notification: ImageView
    private lateinit var filter: ImageView
    private lateinit var save: TextView
    private lateinit var indicator: ImageView
    var dataPost: Content? = Content()
    var targetId = 0
    var id = 0
    var block = ""
    var estate = ""
    var divisi = ""
    var colorId = 0
    var rejectReason = ""
    var tahunTanam = 0
    var region = ""
    var soil = ""
    var seed = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_problem)

        registerReceiver(connectionReceiver, IntentFilter(CONNECTIVITY_CHANGE))

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        back = toolbar.findViewById(R.id.btn_back)
        title = toolbar.findViewById(R.id.txt_toolbar_title)
        mainTitle = toolbar.findViewById(R.id.main_title)
        notification = toolbar.findViewById(R.id.notification)
        filter = toolbar.findViewById(R.id.filter)
        save = toolbar.findViewById(R.id.btn_save)
        indicator = toolbar.findViewById(R.id.indicator)
        setupMainToolbar()

        dataPost = Gson().fromJson(intent.getStringExtra("DATA"), Content::class.java)
        targetId = dataPost?.problem_target?.id ?: 0
        id = dataPost?.id ?: 0
        block = dataPost?.block ?: ""
        estate = dataPost?.estate ?: ""
        divisi = dataPost?.divisi ?: ""
        colorId = (dataPost?.color?.id ?: 0) - 1
        region = dataPost?.region ?: ""
        soil = dataPost?.soil ?: ""
        seed = dataPost?.seed ?: ""

        sendBroadcast(Intent(NOTIFICATION)
                .putExtra("STATUS", true)
                .putExtra("NOTIF", false))

        if (intent.hasExtra("REJECT_REASON")) {
            rejectReason = intent.getStringExtra("REJECT_REASON")
        }

        setupFragment()
    }

    private fun setupMainToolbar() {
        toolbar.findViewById<ImageView>(R.id.indicator).visibility = View.GONE
        back.visibility = View.VISIBLE
        back.setOnClickListener {
            onBackPressed()
        }
        title.visibility = View.VISIBLE
        title.text = getString(R.string.text_update_problem)
        mainTitle.visibility = View.GONE
        notification.visibility = View.GONE
        filter.visibility = View.GONE
        save.visibility = View.GONE
        indicator.visibility = View.VISIBLE
    }

    fun obtainUpdateProblemViewModel(): UpdateProblemViewModel = obtainViewModel(UpdateProblemViewModel::class.java)
    fun obtainDetailViewModel(): DetailViewModel = obtainViewModel(DetailViewModel::class.java)
    fun obtainProblemViewModel(): ProblemViewModel = obtainViewModel(ProblemViewModel::class.java)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CODE_PROBLEM_CLASSIFICATION && resultCode == RESULT_CODE_PROBLEM_CLASSIFICATION) {
            obtainProblemViewModel().loadDataProblem()
        }

    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        replaceFragmentInActivity(UpdateProblemFragment.newInstance(), R.id.frame_main_content)
    }

    fun changeViewFragment(page: Int) {
        (supportFragmentManager.findFragmentById(R.id.frame_main_content) as UpdateProblemFragment).changeFragment(page)
    }

    private val connectionReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getBooleanExtra("STATUS", false) == true) {
                indicator.setImageResource(R.drawable.ic_online_indicator)
            } else {
                indicator.setImageResource(R.drawable.ic_offline_indicator)
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(connectionReceiver)
    }

    override fun onResume() {
        super.onResume()
        obtainDetailViewModel().getProblemTarget(targetId)
        obtainProblemViewModel().id = id
        obtainProblemViewModel().targetId = targetId
        obtainProblemViewModel().loadDataProblem()
    }

    fun showLoadingEvent(show: Boolean) {
        obtainUpdateProblemViewModel().showUpdateLoading.set(show)
    }

    fun hideSaveButton(show: Boolean) {
        obtainUpdateProblemViewModel().hideButtonEvent.set(show)
    }

    fun saveAllProblem() {
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(FilterActivity.REFRESH_PROBLEM_CHECK, true).apply()
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(FilterActivity.REFRESH_DASHBOARD, true).apply()
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(FilterActivity.REFRESH_UPLOAD_DATA, true).apply()
        obtainDetailViewModel().saveDataEvent.call()
        obtainDetailViewModel().saveDataNextEvent.observe(this, Observer {
            obtainProblemViewModel().saveDataEvent.call()
        })
    }

    companion object {

        fun startActivity(context: Context, data: String) {
            context.startActivity(Intent(context, UpdateProblemActivity::class.java)
                    .putExtra("DATA", data))
        }

    }
}
