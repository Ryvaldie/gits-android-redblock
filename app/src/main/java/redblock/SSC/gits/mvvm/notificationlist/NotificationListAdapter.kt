package redblock.SSC.gits.mvvm.notificationlist


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.data.model.ContentNotification
import redblock.SSC.gits.data.model.NotificationListModel
import redblock.SSC.gits.databinding.ItemNotificationListBinding
import redblock.SSC.gits.util.DateHelper


class NotificationListAdapter(data: List<ContentNotification>,
                              viewModel: NotificationListViewModel,
                              private val mListener: NotificationListUserActionListener) : RecyclerView.Adapter<NotificationListAdapter.NotificationListItem>() {

    var mData = data
    val mViewModel = viewModel

    override fun onBindViewHolder(holder: NotificationListItem, position: Int) {
        holder.bind(mData[position], mViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationListItem {
        val binding = ItemNotificationListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotificationListItem(binding, mListener)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun replaceData(data: List<ContentNotification>) {
        mData = data
        notifyDataSetChanged()
    }

    class NotificationListItem(binding: ItemNotificationListBinding, private val mListener: NotificationListUserActionListener) : RecyclerView.ViewHolder(binding.root) {
        private val mBinding = binding

        fun bind(data: ContentNotification, viewModel: NotificationListViewModel) {
            if (data.body?.contains("null") == true) {
                data.body = data.body?.replace("null", "(tanpa alasan)")
            }
            mBinding.mData = data
            mBinding.timestamp = DateHelper.getTimeAgo(data.createdAt ?: "")
            mBinding.mListener = mListener
        }

    }

}
