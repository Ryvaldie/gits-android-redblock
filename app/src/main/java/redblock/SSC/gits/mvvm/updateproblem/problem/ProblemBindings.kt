package redblock.SSC.gits.mvvm.updateproblem.problem;

import android.arch.lifecycle.MutableLiveData
import android.databinding.BindingAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import redblock.SSC.gits.data.model.ItemProblemModel

/**
 * @author radhikayusuf.
 */

object ProblemBindings {

    @BindingAdapter("app:problemListItems", "app:problemListListener", "app:pagingProblemList")
    @JvmStatic
    fun setupListProblem(recyclerView: RecyclerView, problemListItems: MutableLiveData<List<ItemProblemModel>>, problemListListener: ProblemUserActionListener, page: Int) {
        recyclerView.apply {
            try {
                if (adapter == null) {
                    adapter = ProblemItemAdapter(problemListItems.value, problemListListener)
                    layoutManager = LinearLayoutManager(context)
                    (layoutManager as LinearLayoutManager).stackFromEnd = false
                }
                (adapter as ProblemItemAdapter).replaceList(problemListItems.value!!, page)
            } catch (e: Exception) {
                //Left empty
            }
        }
    }

}