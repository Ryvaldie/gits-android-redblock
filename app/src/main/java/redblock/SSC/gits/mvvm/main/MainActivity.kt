package redblock.SSC.gits.mvvm.main

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import kotlinx.android.synthetic.main.badge_counter.*
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.toolbar.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.databinding.MainActivityBinding
import redblock.SSC.gits.mvvm.backgroundtask.AlarmReceiver
import redblock.SSC.gits.mvvm.backgroundtask.DbScheduler
import redblock.SSC.gits.mvvm.filter.FilterActivity
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.FILTER_DASHBOARD
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.FILTER_PROBLEM_CHECK
import redblock.SSC.gits.mvvm.filter.FilterActivity.Companion.FILTER_UPLOAD_DATA
import redblock.SSC.gits.mvvm.main.allblock.AllBlockViewModel
import redblock.SSC.gits.mvvm.main.periodeblock.PeriodeBlockFragment
import redblock.SSC.gits.mvvm.main.periodeblock.PeriodeBlockViewModel
import redblock.SSC.gits.mvvm.notificationlist.NotificationListActivity
import redblock.SSC.gits.mvvm.problemcheck.ProblemCheckFragment
import redblock.SSC.gits.mvvm.problemcheck.ProblemCheckViewModel
import redblock.SSC.gits.mvvm.settings.SettingsFragment
import redblock.SSC.gits.mvvm.settings.SettingsViewModel
import redblock.SSC.gits.mvvm.uploaddata.UploadDataFragment
import redblock.SSC.gits.mvvm.uploaddata.UploadDataViewModel
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.gone
import redblock.SSC.gits.util.obtainViewModel
import redblock.SSC.gits.util.visible
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.util.*


class MainActivity : BaseActivity() {

    private lateinit var viewMainBinding: MainActivityBinding
    private val mFragmentList = ArrayList<Fragment>()
    private lateinit var back: ImageView
    private lateinit var indicator: ImageView
    private lateinit var title: TextView
    private lateinit var mainTitle: RelativeLayout
    private lateinit var notification: ImageView
    private lateinit var filter: ImageView
    private lateinit var save: TextView
    var fragment: PeriodeBlockFragment? = null
    private lateinit var showCase: TapTargetSequence
    private var isAlarmNotif = true
    val isFirstUpdate = "isFirstUpdate"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewMainBinding = DataBindingUtil.setContentView(this, R.layout.main_activity)

        registerReceiver(connectionReceiver, IntentFilter(CONNECTIVITY_CHANGE))
        registerReceiver(notificationReceiver, IntentFilter(NOTIFICATION))

        mFragmentList.apply {
            add(supportFragmentManager.findFragmentById(R.id.fragmentMain) as MainFragment)
            add(supportFragmentManager.findFragmentById(R.id.fragmentProblemCheck) as ProblemCheckFragment)
            add(supportFragmentManager.findFragmentById(R.id.fragmentUploadData) as UploadDataFragment)
            add(supportFragmentManager.findFragmentById(R.id.fragmentSettings) as SettingsFragment)
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        back = toolbar.findViewById(R.id.btn_back)
        indicator = toolbar.findViewById(R.id.indicator)
        title = toolbar.findViewById(R.id.txt_toolbar_title)
        mainTitle = toolbar.findViewById(R.id.main_title)
        notification = toolbar.findViewById(R.id.notification)
        filter = toolbar.findViewById(R.id.filter)
        save = toolbar.findViewById(R.id.btn_save)
        setupMainToolbar()
        setupMainFragment()
        setupNavigationBar()

        if (intent.getBooleanExtra("PROBLEM_CHECK", false)) {
            setupProblemCheckToolbar()
            changeFragment(mFragmentList[1])
            bottom_navigation.menu.getItem(1).isChecked = true
            bottom_navigation.isSelected = true
        }

    }

    fun setupShowCaseDashboard() {
        showCase = TapTargetSequence(this@MainActivity)
                .targets(
                        TapTarget
                                .forView(
                                        toolbar.findViewById(R.id.indicator),
                                        "Indikator Online",
                                        "Indikator hijau ketika anda online dan merah ketika offline")
                                .transparentTarget(false)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .cancelable(true)
                                .id(-2),
                        TapTarget
                                .forView(
                                        toolbar.findViewById(R.id.notification),
                                        "Counter Notifikasi",
                                        "Sebagai penanda ketika masih ada data yang belum di upload ketika jatuh tempo")
                                .transparentTarget(false)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .cancelable(true)
                                .id(-1),
                        TapTarget
                                .forView(
                                        toolbar.findViewById(R.id.filter),
                                        "Filter",
                                        "Untuk memilih data yang ingin ditampilkan")
                                .transparentTarget(false)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .cancelable(true)
                                .id(0),
                        TapTarget
                                .forView(
                                        bottom_navigation.findViewById(R.id.action_dashboard),
                                        "Dashboard",
                                        "Di dalam menu ini, anda bisa lihat 3 report utama")
                                .transparentTarget(false)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .cancelable(true)
                                .id(1),
                        /*TapTarget.forView(mFragmentList[0].view?.findViewById(R.id.lin_allBlock_container), "Report status per block",
                                "Dengan melihat report ini anda akan mudah mentrack blok mana saja " +
                                        "yang masih merah dan blok mana saja yang sudah orange\n" +
                                        "atau hijau")
                                .transparentTarget(true)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .targetRadius(200)
                                .cancelable(true)
                                .id(2),*/
                        TapTarget
                                .forView(
                                        bottom_navigation.findViewById(R.id.action_problem_check),
                                        "Cek Masalah",
                                        "Di dalam menu ini, anda bisa melihat list masalah yang ada")
                                .transparentTarget(false)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .cancelable(true)
                                .id(3),
                        TapTarget
                                .forView(
                                        mFragmentList[1].view?.findViewById(R.id.textColor),
                                        "Untuk sorting masalah berdasarkan warna")
                                .transparentTarget(false)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .cancelable(true)
                                .id(4),
                        TapTarget
                                .forView(
                                        bottom_navigation.findViewById(R.id.action_upload_data),
                                        "Upload Data",
                                        "Di dalam menu ini, anda bisa melihat masalah yang sudah anda input ketika anda offline")
                                .transparentTarget(false)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .cancelable(true)
                                .id(5),
                        TapTarget
                                .forView(
                                        bottom_navigation.findViewById(R.id.action_settings),
                                        "Pengaturan",
                                        "Menu pengaturan untuk menampilkan petunjuk penggunaan dan tombol keluar")
                                .transparentTarget(false)
                                .targetCircleColor(R.color.mainWhite)
                                .outerCircleColor(R.color.sinarmasRed)
                                .tintTarget(true)
                                .cancelable(true)
                                .id(6)
                )
                .continueOnCancel(false)
                .considerOuterCircleCanceled(true)
                .listener(sequenceListener)

        if (obtainPeriodeBlockViewModel().getStateShowCase()) {
            showCase.cancel()
        } else {
            showCase.start()
        }
    }

    private val sequenceListener = object : TapTargetSequence.Listener {

        override fun onSequenceCanceled(lastTarget: TapTarget?) {
            showCase.cancel()
            obtainPeriodeBlockViewModel().setStateShowCase(true)
        }

        override fun onSequenceFinish() {
            showCase.cancel()
            obtainPeriodeBlockViewModel().setStateShowCase(true)
        }

        override fun onSequenceStep(lastTarget: TapTarget?, targetClicked: Boolean) {
            if (lastTarget?.id() == 3) {
                setupProblemCheckToolbar()
                changeFragment(mFragmentList[1])
                bottom_navigation.menu.getItem(1).isChecked = true
                bottom_navigation.isSelected = true
            }
            if (lastTarget?.id() == 5) {
                setupUploadDataToolbar()
                changeFragment(mFragmentList[2])
                bottom_navigation.menu.getItem(2).isChecked = true
                bottom_navigation.isSelected = true
            }
            if (lastTarget?.id() == 6) {
                setupSettingsToolbar()
                changeFragment(mFragmentList[3])
                bottom_navigation.menu.getItem(3).isChecked = true
                bottom_navigation.isSelected = true
            }
        }
    }

    private fun setupMainToolbar() {
        indicator.visible()
        back.gone()
        title.gone()
        mainTitle.visible()
        notification.visible()
        notification.setOnClickListener {
            setUpNotification()
        }
        filter.visible()
        filter.setOnClickListener {
            FilterActivity.startActivity(this, FILTER_DASHBOARD)
        }
        save.gone()
    }

    private fun setupProblemCheckToolbar() {
        back.gone()
        title.visible()
        title.text = getString(R.string.text_problem_check)
        mainTitle.gone()
        notification.visible()
        notification.setOnClickListener {
            setUpNotification()
        }
        filter.visible()
        filter.setOnClickListener {
            FilterActivity.startActivity(this, FILTER_PROBLEM_CHECK)
        }
        save.gone()
    }

    private fun setupUploadDataToolbar() {
        back.gone()
        title.visible()
        title.text = getString(R.string.text_sync_data)
        mainTitle.gone()
        notification.visible()
        notification.setOnClickListener {
            setUpNotification()
        }
        filter.gone()
        filter.setOnClickListener {
            FilterActivity.startActivity(this, FILTER_UPLOAD_DATA)
        }
        save.gone()
    }

    private fun setupSettingsToolbar() {
        back.gone()
        title.visible()
        title.text = getString(R.string.text_settings)
        mainTitle.gone()
        notification.visible()
        notification.setOnClickListener {
            setUpNotification()
        }
        filter.gone()
        save.gone()
    }

    private fun setUpNotification() {
        NotificationListActivity.startActivity(this)
//        ImageViewCompat.setImageTintList(notification, ColorStateList.valueOf(resources.getColor(R.color.mainWhite)))
//        if (isAlarmNotif) {
//            setupUploadDataToolbar()
//            changeFragment(mFragmentList[2])
//            bottom_navigation.menu.getItem(2).isChecked = true
//            bottom_navigation.isSelected = true
//        } else {
//            setupProblemCheckToolbar()
//            changeFragment(mFragmentList[1])
//            bottom_navigation.menu.getItem(1).isChecked = true
//            bottom_navigation.isSelected = true
//        }
    }

    private fun setupMainFragment() {
        setupMainToolbar()
        changeFragment(mFragmentList[0])
        bottom_navigation.menu.getItem(0).isChecked = true
        bottom_navigation.isSelected = true
    }

    fun obtainMainViewModel(): MainViewModel = obtainViewModel(MainViewModel::class.java)
    fun obtainPeriodeBlockViewModel(): PeriodeBlockViewModel = obtainViewModel(PeriodeBlockViewModel::class.java)
    fun obtainAllBlockViewModel(): AllBlockViewModel = obtainViewModel(AllBlockViewModel::class.java)
    fun obtainProblemCheckViewModel(): ProblemCheckViewModel = obtainViewModel(ProblemCheckViewModel::class.java)
    fun obtainUploadDataViewModel(): UploadDataViewModel = obtainViewModel(UploadDataViewModel::class.java)
    fun obtainSettingsViewModel(): SettingsViewModel = obtainViewModel(SettingsViewModel::class.java)

    private fun changeFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        for (f in mFragmentList) {
            transaction.hide(f)
        }
        transaction.show(fragment)
        transaction.commit()
    }

    private fun setupNavigationBar() {
        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_dashboard -> {
                    setupMainFragment()
                }
                R.id.action_problem_check -> {
                    setupProblemCheckToolbar()
                    changeFragment(mFragmentList[1])
                    bottom_navigation.menu.getItem(1).isChecked = true
                    bottom_navigation.isSelected = true
                    obtainProblemCheckViewModel().start()
//                    obtainProblemCheckViewModel().getAllData()
                }
                R.id.action_upload_data -> {
                    setupUploadDataToolbar()
                    changeFragment(mFragmentList[2])
                    bottom_navigation.menu.getItem(2).isChecked = true
                    bottom_navigation.isSelected = true
                }
                R.id.action_settings -> {
                    setupSettingsToolbar()
                    changeFragment(mFragmentList[3])
                    bottom_navigation.menu.getItem(3).isChecked = true
                    bottom_navigation.isSelected = true
                }
            }
            false
        }
    }

    private val connectionReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getBooleanExtra("STATUS", false) == true) {
                indicator.setImageResource(R.drawable.ic_online_indicator)
            } else {
                indicator.setImageResource(R.drawable.ic_offline_indicator)
            }
        }

    }

    private val notificationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getBooleanExtra("NOTIF", false) == true) {
                if (intent.getBooleanExtra("STATUS", false)) {

                }
                val count = obtainMainViewModel().getNotifCount() + 1
                notifCount.text = "$count"
                notifCount.visibility = View.VISIBLE
                obtainMainViewModel().saveNotifCount(count)
            } else {
                notifCount.visibility = View.GONE
                obtainMainViewModel().saveNotifCount(0)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(connectionReceiver)
        unregisterReceiver(notificationReceiver)
    }

    override fun onResume() {
        super.onResume()
        setUpBadgeCount()
        /*
        * TODO remove alarm
        * */
        createAlarm()
        createDbSchedule()
    }

    private fun setUpBadgeCount() {
        val count = obtainMainViewModel().getNotifCount()
        if (count > 0) {
            notifCount.text = "$count"
            notifCount.visibility = View.VISIBLE
        } else {
            notifCount.visibility = View.GONE
        }
    }

    private fun createAlarm() {
        val alarmManager = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val receiverIntent = Intent(this, AlarmReceiver::class.java)
        val local = obtainMainViewModel().repository.localDataSource
        val problemDetail = local.getDraftProblemTargetDao().getAllData()
        val problemList = local.getDraftProblemDBDao().getAllData()
        val notify = (problemDetail != null && problemDetail.isNotEmpty()) || (problemList != null && problemList.isNotEmpty())
        receiverIntent.putExtra("NOTIFY", notify)
        val alarmIntent = receiverIntent.let { intent ->
            PendingIntent.getBroadcast(this, 123, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val nowDate = DateHelper.getDate()
        val nowHour = DateHelper.getHour()
        val dayOfMonth = when {
            nowDate in 9..13 -> 14
            nowDate > 14 -> 8
            nowDate == 8 && nowHour > 11 -> 14
            else -> 8
        }
        val nowMonth = DateHelper.getMonth()
        val month = when {
            nowDate in 9..13 -> nowMonth
            nowDate > 14 -> nowMonth + 1
            else -> nowMonth
        }

        val calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(DateHelper.getYear(), month, dayOfMonth, 11, 59, 59)
        }

        alarmManager.cancel(alarmIntent)

        alarmManager.set(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                alarmIntent
        )

    }

    private fun createDbSchedule() {
        val alarmManager = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val receiverIntent = Intent(this, DbScheduler::class.java)
        val update = "UPDATE"
        receiverIntent.putExtra(update, true)
        val alarmIntent = receiverIntent.let { intent ->
            PendingIntent.getBroadcast(this, 321, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val cal = Calendar.getInstance().apply {
            add(Calendar.MONTH, 1)
        }
        val calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(DateHelper.getYear(), cal.get(Calendar.MONTH), 1, 0, 0, 0)
        }

        alarmManager.cancel(alarmIntent)

        alarmManager.set(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                alarmIntent
        )

    }

    companion object {

        fun startActivity(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }

    }

}
