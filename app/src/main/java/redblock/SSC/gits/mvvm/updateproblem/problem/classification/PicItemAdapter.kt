package redblock.SSC.gits.mvvm.updateproblem.problem.classification

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.data.model.PicModel
import redblock.SSC.gits.databinding.ItemPicBinding
import redblock.SSC.gits.databinding.ItemProblemListBinding
import redblock.co.gits.gitsdriver.utils.GitsBindableAdapter

class PicItemAdapter (var item: ArrayList<PicModel>?, val listener: PicClickListener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<PicModel>) {
        this.item?.clear()
        this.item?.addAll(item)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return BeritaViewHolder(ItemPicBinding.inflate(LayoutInflater.from(p0.context), p0, false))
    }

    override fun getItemCount(): Int = item?.size?:0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        item?.get(holder.adapterPosition)?.let { (holder as BeritaViewHolder).bind(it, listener) }
    }

    class BeritaViewHolder(val mViewDataBinding: ItemPicBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        private val mBinding = mViewDataBinding

        fun bind(obj: PicModel, listener: PicClickListener) {

            mBinding.mData = obj
            mBinding.mListener = listener

        }

    }

    fun setStatusSelected(isSelected: Boolean, picModel: PicModel) {
        val index = this.item?.indexOf(picModel)?:0
        this.item?.set(index, PicModel(
                picModel.id,
                picModel.name,
                picModel.created_at,
                picModel.updated_at,
                isSelected
        ))
        notifyItemChanged(index)
    }

    interface PicClickListener {

        fun onItemClick(picModel: PicModel)

    }

}
