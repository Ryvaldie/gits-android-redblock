package redblock.SSC.gits.mvvm.userguide;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_user_guide.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.util.obtainViewModel
import redblock.SSC.gits.util.replaceFragmentInActivity


class UserGuideActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_guide)
        toolbar.setNavigationOnClickListener { super.onBackPressed() }
        setupFragment()
    }

    fun obtainUserGuideViewModel(): UserGuideViewModel = obtainViewModel(UserGuideViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        replaceFragmentInActivity(UserGuideFragment.newInstance(), R.id.frame_main_content)
    }

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, UserGuideActivity::class.java))
        }
    }
}
