package redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import redblock.SSC.gits.util.DynamicHeightViewPager


class ProblemActionViewPagerAdapter(var views: ArrayList<ProblemActionFragment>, fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    var position = 0
    var currentPosition = 0

    override fun getItem(position: Int): Fragment {
        currentPosition = position
        return views[position]
    }

    override fun getCount(): Int {
        return views.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Masalah ${position + 1}"
    }

    override fun getItemPosition(`object`: Any): Int {
        return if (views.contains(`object`)) {
            views.indexOf(`object`)
        } else {
            PagerAdapter.POSITION_NONE
        }
    }

    override fun destroyItem(container: View, position: Int, `object`: Any) {
        if (position >= count) {
            val fragmentTransaction = (`object` as Fragment).fragmentManager?.beginTransaction()
            fragmentTransaction?.remove(`object`)
            fragmentTransaction?.commit()
        }
    }

    fun getIsLocked(position: Int) : Boolean {
       return views[position].mViewModel.isLocked.get()
    }

    fun getCurrentItem(): Fragment {
        return views[position]
    }

    fun addView() {
        views.add(ProblemActionFragment.newInstance())
        notifyDataSetChanged()
    }

    fun removeView(position: Int) {
        if (position < views.size) {
            views.removeAt(position)
            notifyDataSetChanged()
        }
        getItem(0)
    }

    fun replaceItem(views: ArrayList<ProblemActionFragment>) {
        this.views = views
        notifyDataSetChanged()
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        super.setPrimaryItem(container, position, `object`)
        val fragment = `object` as Fragment
        val pager = container as DynamicHeightViewPager
        if (fragment.view != null) {
            this.position = position
            pager.measureCurrentView(fragment.view)
        }
    }

}