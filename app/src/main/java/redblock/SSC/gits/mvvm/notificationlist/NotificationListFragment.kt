package redblock.SSC.gits.mvvm.notificationlist


import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_notification_list.*
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.databinding.FragmentNotificationListBinding
import redblock.SSC.gits.mvvm.updateproblem.UpdateProblemActivity
import java.util.*


class NotificationListFragment : BaseFragment(), NotificationListUserActionListener {

    lateinit var mViewDataBinding: FragmentNotificationListBinding
    lateinit var mViewModel: NotificationListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentNotificationListBinding.inflate(inflater, container, false)
        mViewModel = (activity as NotificationListActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = this

        return mViewDataBinding.root
    }

    override fun onItemClick(data: String, body: String) {
        if (data.isNotEmpty() && data != "{}") {
            val updateProblemIntent = Intent(context, UpdateProblemActivity::class.java)
            updateProblemIntent.putExtra("DATA", data)
            if (body.toLowerCase(Locale.getDefault()).contains("tolak")) {
                val reason = body.split("tolak ")[1]
                updateProblemIntent.putExtra("REJECT_REASON", reason.substring(0, 1).toUpperCase(Locale.getDefault()) + reason.substring(1))
            }
            startActivity(updateProblemIntent)
        }
    }

    override fun onResume() {
        super.onResume()
        mViewDataBinding.mViewModel?.start()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupListData()
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            mViewModel.start()
        }

    }

    private fun setupListData() {
        mViewDataBinding.recyclerNotificationList.adapter = NotificationListAdapter(mViewModel.notificationListData, mViewModel, this)
        mViewDataBinding.recyclerNotificationList.layoutManager = LinearLayoutManager(context)
    }

    companion object {
        fun newInstance() = NotificationListFragment().apply {

        }

    }

}
