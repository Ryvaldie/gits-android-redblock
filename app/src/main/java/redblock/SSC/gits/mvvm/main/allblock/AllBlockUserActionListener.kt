package redblock.SSC.gits.mvvm.main.allblock;

import redblock.SSC.gits.data.model.ProgressActionModel


interface AllBlockUserActionListener {

    fun onActionChartClick(progressActionModel: ProgressActionModel)

}