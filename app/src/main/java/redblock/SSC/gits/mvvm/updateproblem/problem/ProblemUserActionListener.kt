package redblock.SSC.gits.mvvm.updateproblem.problem;

import redblock.SSC.gits.data.model.ItemProblemModel


interface ProblemUserActionListener {

    fun onClickProblem(data: ItemProblemModel)

}