package redblock.SSC.gits.mvvm.updateproblem

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import redblock.SSC.gits.mvvm.updateproblem.detail.DetailFragment
import redblock.SSC.gits.mvvm.updateproblem.problem.ProblemFragment

class UpdateProblemViewPagerAdapter (val context: Context, private var mFragmentTitleList: List<String>, fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            DetailFragment.newInstance()
        } else {
            ProblemFragment.newInstance()
        }
    }

    override fun getCount(): Int {
        return mFragmentTitleList.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return mFragmentTitleList[position]
    }

}
