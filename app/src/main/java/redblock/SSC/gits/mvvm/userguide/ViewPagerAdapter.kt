package redblock.SSC.gits.mvvm.userguide

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter

/**
 * Created by Ryvaldie I.H on 24/10/18.
 * Void Group.
 */
class ViewPagerAdapter(fragmentManager: FragmentManager, val content: List<Int>)
    : FragmentPagerAdapter(fragmentManager) {

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getItem(position: Int): Fragment {
        return UserGuideContent.newInstance(content[position])
    }

    override fun getCount() = content.size

}
