package redblock.SSC.gits.mvvm.settings


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.databinding.FragmentSettingsBinding
import redblock.SSC.gits.mvvm.main.MainActivity
import redblock.SSC.gits.mvvm.userconfig.UserConfigActivity
import redblock.SSC.gits.mvvm.userguide.UserGuideActivity


class SettingsFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentSettingsBinding
    lateinit var mViewModel: SettingsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentSettingsBinding.inflate(inflater, container, false).apply {

            mViewModel = (activity as MainActivity).obtainSettingsViewModel()

            mListener = object : SettingsUserActionListener {
                override fun onDeleteOldData() {
                    deleteOldData(mViewModel)
                }

                override fun onClickBantuan() {
                    UserGuideActivity.startActivity(requireContext())
                }

                override fun onClickUserConfig() {
                    UserConfigActivity.startActivity(requireContext(), true)
                }

                override fun onClickBahasa() {

                }

                override fun onClickAbout() {

                }

                override fun onClickLogout() {
                    (activity as MainActivity).obtainPeriodeBlockViewModel().setStateShowCase(false)
                    dialogLogout(mViewModel)
                }

            }

            setLifecycleOwner(this@SettingsFragment)
        }

        mViewModel = mViewDataBinding.mViewModel!!


        return mViewDataBinding.root

    }


    companion object {
        fun newInstance() = SettingsFragment().apply {

        }

    }

}
