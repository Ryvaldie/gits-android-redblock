package redblock.SSC.gits.mvvm.updateproblem.detail;

import android.widget.EditText


interface DetailUserActionListener {

    fun onClickOrangeMonth(editText: EditText)

    fun onClickOrangeTon(editText: EditText, array: ArrayList<String>)

    fun onClickGreenMonth(editText: EditText)

    fun onClickGreenTon(editText: EditText, array: ArrayList<String>)

    fun onClickCamera()

    fun onClickSave()

}
