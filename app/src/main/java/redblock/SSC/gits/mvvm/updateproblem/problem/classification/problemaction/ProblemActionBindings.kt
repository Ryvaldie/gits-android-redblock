package redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction;

import android.databinding.BindingAdapter
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import redblock.SSC.gits.R
import redblock.SSC.gits.data.model.ImageItemModel
import redblock.SSC.gits.util.ImageHelper
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.io.File

/**
 * @author radhikayusuf.
 */

object ProblemActionBindings {

    @BindingAdapter("app:imageFromPath")
    @JvmStatic
    fun loadImageFromPath(imageView: ImageView, path: String) {
        // Start tambahan untuk loading glide
        val circularProgressDrawable = CircularProgressDrawable(imageView.context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.setColorFilter(ContextCompat.getColor(imageView.context, android.R.color.white), PorterDuff.Mode.SRC_IN)
        circularProgressDrawable.start()
        // End tambahan untuk loading glide
        val requestOptions = RequestOptions()
                .override(ImageHelper.dpToPx(80, imageView.context), ImageHelper.dpToPx(80, imageView.context))
                .centerCrop()
                .timeout(500 * 60 * 1000)
//                .transform(RotateTransformation(imageView.context, 90f))
                .error(R.color.greyBackgroundDefault)
                .placeholder(circularProgressDrawable)
                .diskCacheStrategy(DiskCacheStrategy.NONE)


        val link = if (path.contains("/")) {
            Uri.fromFile(File(path))
        } else {
            GitsHelper.Const.BASE_IMAGE_URL + path
        }
        Glide.with(imageView.context)
                .load(link)
                .apply(requestOptions)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                })
                .into(imageView)
    }

    @BindingAdapter("app:imageBefore", "app:problemViewModel")
    @JvmStatic
    fun setupImageBeforeAdapter(recyclerView: RecyclerView, list: List<ImageItemModel>, mViewModel: ProblemActionViewModel) {
        recyclerView.apply {
            if (adapter == null) {
                adapter = ImageBeforeAdapter(ArrayList(0), mViewModel)
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
            (adapter as ImageBeforeAdapter).replaceList(list)
        }
    }

    @BindingAdapter("app:imageAfter", "app:problemViewModel")
    @JvmStatic
    fun setupImageAfterAdapter(recyclerView: RecyclerView, list: List<ImageItemModel>, mViewModel: ProblemActionViewModel) {
        recyclerView.apply {
            if (adapter == null) {
                adapter = ImageAfterAdapter(ArrayList(0), mViewModel)
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
            (adapter as ImageAfterAdapter).replaceList(list)
        }
    }

}