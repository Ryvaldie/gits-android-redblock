package redblock.SSC.gits.mvvm.updateproblem.problem

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import redblock.SSC.gits.R
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.databinding.ItemProblemListBinding
import redblock.SSC.gits.util.DateHelper
import java.lang.Double.parseDouble
import java.util.*

class ProblemItemAdapter (var item: List<ItemProblemModel>?, val listener: ProblemUserActionListener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<ItemProblemModel>, page: Int) {
        this.item = item
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            this.item?.size?.let { notifyItemInserted(it) }
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return BeritaViewHolder(ItemProblemListBinding.inflate(LayoutInflater.from(p0.context), p0, false))
    }

    override fun getItemCount(): Int = item?.size?:0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        item?.get(holder.adapterPosition)?.let { (holder as BeritaViewHolder).bind(it, listener) }
    }

    class BeritaViewHolder(val mViewDataBinding: ItemProblemListBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        private val mBinding = mViewDataBinding

        fun bind(obj: ItemProblemModel, listener: ProblemUserActionListener) {
            mBinding.apply {
                root.visibility = if (obj.problemTarget != null && obj.problemTarget != ItemProblemModel.ProblemTarget()) {
                    if (obj.problemTarget.problemCheck?.statusColorId == 1) {
                        View.GONE
                    } else {
                        View.VISIBLE
                    }
                } else {
                    View.VISIBLE
                }
                val tempTotalArea = obj.problem_detail?.total_area ?: ""
                obj.problem_detail?.total_area = try {
                    root.context.getString(R.string.text_2_decimal_places, tempTotalArea?.toFloat()
                            ?: 0f)
                } catch (ex: Exception) {
                    tempTotalArea
                }
                val tempProblemArea = obj.problem_detail?.problem_area ?: ""
                obj.problem_detail?.problem_area = try {
                    root.context.getString(R.string.text_2_decimal_places, tempProblemArea?.toFloat()
                            ?: 0f)
                } catch (ex: Exception) {
                    tempProblemArea
                }
                mData = obj
                if(!mData?.problem_detail?.stat.equals("draft"))
                problemArea = (obj.problem_detail?.progress_overall?:"0").toFloat() * 100
                else problemArea = (obj.problem_detail?.progress_overall?:"0").toFloat()

                var creatAt = ""
                var numeric = true
                try {
                    if(obj.created_at?.length != 0){
                        creatAt = obj.created_at?.get(1).toString()
                    }
                    val num = parseDouble(creatAt)
                } catch (e: NumberFormatException) {
                    numeric = false
                }

                if(numeric) date = DateHelper.dateTimeMonth(obj.created_at)
                else date = obj.created_at

                val status = mData?.problem_detail?.stat ?: ""
                textStatus.text = status
                textStatus.setTextColor(when {
                    status.toLowerCase(Locale.getDefault()).contains("draft") -> mViewDataBinding.root.context.resources.getColor(R.color.draftColor)
                    status.toLowerCase(Locale.getDefault()).contains("waiting") -> mViewDataBinding.root.context.resources.getColor(R.color.waitingColor)
                    status.toLowerCase(Locale.getDefault()).contains("revision") -> mViewDataBinding.root.context.resources.getColor(R.color.revisionColor)
                    status.toLowerCase(Locale.getDefault()).contains("approved") -> mViewDataBinding.root.context.resources.getColor(R.color.approvedColor)
                    else -> mViewDataBinding.root.context.resources.getColor(R.color.rejectedColor)
                })

                mListener = listener
            }

        }
    }

}
