package redblock.SSC.gits.mvvm.updateproblem.problem


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_problem.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.model.ItemProblemModel
import redblock.SSC.gits.databinding.FragmentProblemBinding
import redblock.SSC.gits.mvvm.updateproblem.UpdateProblemActivity
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.ProblemClassificationActivity
import redblock.SSC.gits.util.*


class ProblemFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentProblemBinding
    lateinit var mViewModel: ProblemViewModel

    @Suppress("DEPRECATION")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentProblemBinding.inflate(inflater, container, false)
        mViewModel = (activity as UpdateProblemActivity).obtainProblemViewModel()

        mViewDataBinding.apply {
            block = (activity as UpdateProblemActivity).block
            estate = (activity as UpdateProblemActivity).estate
            divisi = "Divisi ${(activity as UpdateProblemActivity).divisi}"
        }

        (activity as UpdateProblemActivity).dataPost?.problem_target?.status
        mViewDataBinding.mViewModel = mViewModel.apply {

            checkPercentage.observe(this@ProblemFragment, Observer {
                // Set button visibility
                btn_add_problem.apply {
                    isClickable = if (it == true && (activity as UpdateProblemActivity).colorId == 1) {
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_add_white_24dp), null)
                        setBackgroundResource(R.drawable.button_background_active)
                        true
                    } else {
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_add_white_24dp_inactive), null)
                        setBackgroundResource(R.drawable.button_background_inactive)
                        false
                    }
                }
                onlineRec.adapter?.notifyDataSetChanged()
                offlineRec.adapter?.notifyDataSetChanged()
            })

            successUpdateEvent.observe(this@ProblemFragment, Observer {
                (activity as UpdateProblemActivity).showLoadingEvent(false)
                loadDataProblem()
            })

            saveDataEvent.observe(this@ProblemFragment, Observer {
                (activity as UpdateProblemActivity).showLoadingEvent(true)
                saveAllData()
            })

        }

        mViewDataBinding.mListener = object : ProblemUserActionListener {

            override fun onClickProblem(data: ItemProblemModel) {
                ProblemClassificationActivity.startActivity(requireActivity(), TYPE_UPDATE, data.id
                        ?: 0,
                        Gson().toJson(data), Gson().toJson((activity as UpdateProblemActivity).dataPost))
            }

        }

        mViewDataBinding.let {
            it.mViewModel = mViewDataBinding.mViewModel
            it.mListener = mViewDataBinding.mListener
            it.setLifecycleOwner(this@ProblemFragment)

        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val colorId = when ((activity as UpdateProblemActivity).colorId) {
            -1 -> 0
            else -> (activity as UpdateProblemActivity).colorId
        }
        val resId = resources.getIdentifier(resources.getStringArray(R.array.iconId)[colorId], "drawable", context?.packageName)
        icon_location.setImageResource(resId)

        mViewModel.id = (activity as UpdateProblemActivity).id
        mViewModel.targetId = (activity as UpdateProblemActivity).targetId

        setupListener()
    }

    private fun setupListener() {
        btn_add_problem.setOnClickListener {
            btn_add_problem.isClickable = false
            ProblemClassificationActivity.startActivity(requireActivity(), TYPE_POST, mViewModel.id,
                    Gson().toJson((activity as UpdateProblemActivity).dataPost))
        }

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            (activity as UpdateProblemActivity).obtainDetailViewModel().getProblemTarget((activity as UpdateProblemActivity).targetId)
            mViewModel.loadDataProblem()
        }
    }

    companion object {
        fun newInstance() = ProblemFragment().apply {

        }

    }

}
