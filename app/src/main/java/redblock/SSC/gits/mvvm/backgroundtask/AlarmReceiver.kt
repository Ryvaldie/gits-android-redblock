package redblock.SSC.gits.mvvm.backgroundtask

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.mvvm.login.LoginActivity
import redblock.SSC.gits.util.DateHelper
import java.util.*

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent != null && intent.hasExtra("NOTIFY")) {
            if (intent.getBooleanExtra("NOTIFY", false)) {
                createNotification(context, context?.getString(R.string.text_anda_belum_submit)
                        ?: "")
            }
        }
        if (intent != null && intent.hasExtra("REMINDER")) {
            if (intent.getBooleanExtra("REMINDER", false)) {
                createNotification(context, context?.getString(R.string.text_check_data_jatuh_tempo)
                        ?: "")
            }
        }
    }

    private fun createNotification(context: Context?, message: String) {
        val randomId = Random().nextInt(1000)
        val intent = Intent(context, LoginActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(context, 456, intent, 0)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(context.getString(R.string.app_name), message, NotificationManager.IMPORTANCE_DEFAULT)
            val att = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()
            channel.description = message
            channel.enableVibration(true)
            channel.setShowBadge(false)
            channel.setSound(defaultSoundUri, att)
            notificationManager.createNotificationChannel(channel)
            val builder = Notification.Builder(context, context.getString(R.string.app_name))
                    .setContentTitle(context.getString(R.string.app_name))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentText(message)
                    .setChannelId(context.getString(R.string.app_name))
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)

            notificationManager.notify(randomId, builder.build())
        } else {
            val notificationBuilder = NotificationCompat.Builder(context)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
            notificationManager.notify(randomId, notificationBuilder.build())
        }
        context.sendBroadcast(Intent(BaseActivity.NOTIFICATION)
                .putExtra("STATUS", true)
                .putExtra("NOTIF", true)
                .putExtra("MESSAGE", message)
                .putExtra("TIMESTAMP", DateHelper.getTimeStamp()))
        createAlarm(context)
    }

    private fun createAlarm(context: Context?) {
        val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val receiverIntent = Intent(context, AlarmReceiver::class.java)
        receiverIntent.putExtra("REMINDER", false)
        val alarmIntent = receiverIntent.let { intent ->
            PendingIntent.getBroadcast(context, 123, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }
        val nowDate = DateHelper.getDate()
        val dayOfMonth = when {
            nowDate in 9..13 -> 14
            nowDate > 14 -> 8
            else -> 14
        }
        val nowMonth = DateHelper.getMonth()
        val month = when {
            nowDate in 9..13 -> nowMonth
            nowDate > 14 -> nowMonth + 1
            else -> nowMonth
        }

        val calendar = Calendar.getInstance().apply {
            set(DateHelper.getYear(), month, dayOfMonth, 11, 59, 59)
        }

        alarmManager.cancel(alarmIntent)

        alarmManager.set(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                alarmIntent
        )

    }

}
