package redblock.SSC.gits.mvvm.settings;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import android.os.Build
import android.os.Environment
import android.widget.Toast
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.util.DateHelper
import redblock.co.gits.gitsbase.BaseViewModel
import java.io.File
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.Month
import java.util.*
import kotlin.collections.ArrayList




class SettingsViewModel(context: Application, val repository: GitsRepository) : BaseViewModel(context) {

    fun logout() {
        repository.unregisterToken(repository.getToken(), object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: Any?) {

            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {

            }

        })
        repository.localDataSource.saveToken("")
//        clearDB()
    }

    fun resetShowCase() {
        repository.saveStateShowCase(false)
    }

    /*delete image kurang dari 31 hari*/
    fun deleteOldImage(context: Context){
        val dir = File(Environment.getExternalStorageDirectory().path + "/RedBlock")
        val allFile = dir.listFiles()
        val currentDate = Calendar.getInstance()
        val fileDate = Calendar.getInstance()

        if(allFile != null && allFile.size > 0){
            var fileName : String = ""
            for(i in allFile){
                val dateModif = Date(i.lastModified())
                fileName = i.name
                val sdfYear = SimpleDateFormat("yyyy")
                val sdfMonth = SimpleDateFormat("MM")
                val sdfDay = SimpleDateFormat("dd")

                currentDate.set(DateHelper.getYear(), DateHelper.getMonth(), DateHelper.getDate())
                fileDate.set(sdfYear.format(dateModif).toInt(), sdfMonth.format(dateModif).toInt(), sdfDay.format(dateModif).toInt())
                val dayBaru = (currentDate.timeInMillis + 1 - fileDate.timeInMillis) / (24 * 60 * 60 * 1000)
                if(dayBaru > 31){
                    val dirFull = File(Environment.getExternalStorageDirectory().path + "/RedBlock/" + fileName)
                    dirFull.delete()
                }
            }
        }

    }
}