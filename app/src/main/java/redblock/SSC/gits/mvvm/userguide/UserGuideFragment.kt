package redblock.SSC.gits.mvvm.userguide


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_user_guide.*
import redblock.SSC.gits.R
import redblock.SSC.gits.databinding.FragmentUserGuideBinding


class UserGuideFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentUserGuideBinding
    lateinit var mViewModel: UserGuideViewModel
    private val content = intArrayOf(R.drawable.page_problem_check, R.drawable.page_problem_detail
            , R.drawable.page_problem_list, R.drawable.page_klasifikasi_masalah1, R.drawable.page_klasifikasi_masalah2
            , R.drawable.page_klasifikasi_masalah3, R.drawable.page_upload_data)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentUserGuideBinding.inflate(inflater, container, false)
        mViewModel = (activity as UserGuideActivity).obtainUserGuideViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : UserGuideUserActionListener {

        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViewPager()

    }

    private fun setViewPager() {
        viewpager.apply {
            if (adapter == null) {
                adapter = ViewPagerAdapter(childFragmentManager, content.asList())
            }
            addOnPageChangeListener(onPageListener)
        }

        lin_vp_indicator.apply {
            if (content.size > 1) {
                val dotsImages = kotlin.collections.mutableListOf<ImageView>()

                (0..content.lastIndex).mapTo(dotsImages) { ImageView(context) }

                dotsImages.forEach {
                    it.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.inactive_circle_indicator))
                    val params = android.widget.LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
                        setMargins(8, 0, 8, 0)
                    }
                    addView(it, params)
                }
            }

            gravity = Gravity.CENTER_VERTICAL

            if (childCount > 0) {
                (getChildAt(0 % content.size) as ImageView).apply {
                    setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.active_circle_indicator))
                }
            }
        }
    }

    private val onPageListener = object : ViewPager.OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            lin_vp_indicator.apply {
                if (childCount > 0) {
                    for (x in 0..content.lastIndex) {
                        (getChildAt(x) as ImageView).apply {
                            setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.inactive_circle_indicator))
                        }
                    }

                    (getChildAt(position % content.size) as ImageView).apply {
                        setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.active_circle_indicator))
                    }
                }
            }
        }

        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }
    }

    companion object {
        fun newInstance() = UserGuideFragment().apply {

        }

    }

}
