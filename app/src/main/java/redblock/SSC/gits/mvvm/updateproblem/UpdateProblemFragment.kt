package redblock.SSC.gits.mvvm.updateproblem


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import kotlinx.android.synthetic.main.main_fragment.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.databinding.FragmentUpdateProblemBinding
import redblock.SSC.gits.mvvm.main.MainViewPagerAdapter
import java.util.ArrayList


class UpdateProblemFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentUpdateProblemBinding
    lateinit var mViewModel: UpdateProblemViewModel
    var mAdapter: UpdateProblemViewPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentUpdateProblemBinding.inflate(inflater, container, false)
        mViewModel = (activity as UpdateProblemActivity).obtainUpdateProblemViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : UpdateProblemUserActionListener {

            override fun onClickSave() {
                /*
                * TODO Tambah validasi problem harus ditambah atau edit
                * */
                (activity as UpdateProblemActivity).saveAllProblem()
            }

        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Check alasan reject
        if ((activity as UpdateProblemActivity).rejectReason.isNotEmpty()) {
            dialogPopupReason((activity as UpdateProblemActivity).rejectReason)
        }

        val titles = ArrayList<String>()
        titles.add(getString(R.string.text_detail))
        titles.add(getString(R.string.text_masalah))
        mAdapter = UpdateProblemViewPagerAdapter(context!!, titles, childFragmentManager)
        main_viewpager?.adapter = mAdapter
        main_viewpager?.offscreenPageLimit = mAdapter!!.count
        tabs?.setupWithViewPager(main_viewpager)
    }

    fun changeFragment(page: Int) {
        main_viewpager?.setCurrentItem(page, true)
    }

    companion object {
        fun newInstance() = UpdateProblemFragment().apply {

        }

    }

}
