package redblock.SSC.gits.mvvm.updateproblem.detail


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import kotlinx.android.synthetic.main.fragment_detail.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseFragment
import redblock.SSC.gits.data.param.ParamUpdateProblemTarget
import redblock.SSC.gits.databinding.FragmentDetailBinding
import redblock.SSC.gits.mvvm.updateproblem.UpdateProblemActivity
import redblock.SSC.gits.mvvm.updateproblem.detail.DetailViewModel.Companion.GREEN_TYPE
import redblock.SSC.gits.mvvm.updateproblem.detail.DetailViewModel.Companion.ORANGE_TYPE
import redblock.SSC.gits.util.*
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.util.*
import kotlin.collections.ArrayList


class DetailFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentDetailBinding
    lateinit var mViewModel: DetailViewModel
    var problemCheckId = 0
    var problemUnitId = 0
    var firstValidation = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentDetailBinding.inflate(inflater, container, false)
        mViewModel = (activity as UpdateProblemActivity).obtainDetailViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

            showProblemTarget.observe(this@DetailFragment, Observer {

                if (it != null) {
                    if((activity as UpdateProblemActivity).colorId == 1){
                        editMode(
                                when (it.status.toLowerCase(Locale.getDefault())) {
                                    "approved", "waiting", "revision" -> true
                                    else -> false
                                }
                        )
                    } else editMode(true)
                    problemCheckId = it.problem_check_id
                    mViewDataBinding.createdDate = DateHelper.dateTimeSet(it.created_at)
                    val unit = it.type_unit?.unit ?: ""
                    mViewDataBinding.area = it.area + " " + unit
                    mViewDataBinding.plantYear = it.plant_year
                    mViewDataBinding.achievement = it.achievement + " %"
                    mViewDataBinding.orangeDate = DateHelper.dateTimeSet(it.target_orange)
                    mViewDataBinding.greenDate = DateHelper.dateTimeSet(it.target_green)
                    val greenTon = if (it.target_ton_green.isNullOrEmpty()) {
                        "0"
                    } else {
                        it.target_ton_green
                    }
                    mViewDataBinding.greenTon = greenTon
                    val orangeTon = if (it.target_ton_orange.isNullOrEmpty()) {
                        "0"
                    } else {
                        it.target_ton_orange
                    }
                    mViewDataBinding.orangeTon = orangeTon
                    mViewDataBinding.unit = unit
                    problemUnitId = it.type_unit?.id ?: 0

                    (activity as UpdateProblemActivity).hideSaveButton(getEditMode())

                    (activity as UpdateProblemActivity).tahunTanam = it.plant_year.toInt()

                }

                updateOrangePotensiEvent.observe(this@DetailFragment, Observer {
                    val ton = if (it.isNullOrEmpty()) {
                        "0"
                    } else {
                        it
                    }
                    mViewDataBinding.orangeTon = ton
                })

                updateGreenPotensiEvent.observe(this@DetailFragment, Observer {
                    val ton = if (it.isNullOrEmpty()) {
                        "0"
                    } else {
                        it
                    }
                    mViewDataBinding.greenTon = ton
                })


                setTextWatcher()

            })

            successUpdateEvent.observe(this@DetailFragment, Observer {
                (activity as UpdateProblemActivity).showLoadingEvent(false)
                val message = if (it == true) {
                    getString(R.string.text_data_tersimpan)
                } else {
                    messageFailedUpdate
                }

                if (message.isNotEmpty()) {
                    mViewDataBinding.root.showSnackbarDefault(mViewDataBinding.root, message,
                            GitsHelper.Const.SNACKBAR_TIMER_SHOWING_DEFAULT)
                    messageFailedUpdate = ""
                }
            })

            saveDataEvent.observe(this@DetailFragment, Observer {
                saveProblemDetail()
            })
        }

        mViewDataBinding.apply {
            block = (activity as UpdateProblemActivity).block
            estate = (activity as UpdateProblemActivity).estate
            divisi = "Divisi ${(activity as UpdateProblemActivity).divisi}"
        }

        mViewDataBinding.mListener = object : DetailUserActionListener {
            override fun onClickOrangeMonth(editText: EditText) {
                dialogDatePicker(editText, setOrangeDate.text.toString())
            }

            override fun onClickOrangeTon(editText: EditText, array: ArrayList<String>) {
                dropdownView(editText, array)
            }

            override fun onClickGreenMonth(editText: EditText) {
                dialogDatePicker(editText, setGreenDate.text.toString())
            }

            override fun onClickGreenTon(editText: EditText, array: ArrayList<String>) {
                dropdownView(editText, array)
            }

            override fun onClickCamera() {
                //Left empty
            }

            override fun onClickSave() {
                saveProblemDetail()
            }

        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val colorId = when ((activity as UpdateProblemActivity).colorId) {
            -1 -> 0
            else -> (activity as UpdateProblemActivity).colorId
        }
        val resId = resources.getIdentifier(resources.getStringArray(R.array.iconId)[colorId], "drawable", context?.packageName)
        icon_location.setImageResource(resId)

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            mViewModel.getProblemTarget((activity as UpdateProblemActivity).targetId)
            (activity as UpdateProblemActivity).obtainProblemViewModel().loadDataProblem()
        }

        setGreenSelection(false)
        setOrangeDate.disableCopyPaste()
        setGreenDate.disableCopyPaste()
        setGreenTon.disableCopyPaste()
        setOrangeTon.disableCopyPaste()
    }

    private fun setTextWatcher() {
        if (!mViewModel.getEditMode()) {
            setOrangeDate.addTextChangedListener(orangeValidation)
            setGreenDate.addTextChangedListener(greenValidation)
        }
    }

    private val orangeValidation = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (firstValidation == 0) firstValidation = 1
            else{
                if (dateDetailValidation(setOrangeDate, setOrangeDate.text.toString())) {
                    val startTarget = "${DateHelper.getYear()}-${DateHelper.getMonth()}"
                    val endDate = setOrangeDate.text.toString()
                    val endTarget = "${endDate.split("/")[1]}-${endDate.split("/")[0]}"
                    mViewModel.getPotensi((activity as UpdateProblemActivity).id, startTarget, endTarget,
                            (activity as UpdateProblemActivity).tahunTanam,
                            (activity as UpdateProblemActivity).region,
                            (activity as UpdateProblemActivity).soil,
                            (activity as UpdateProblemActivity).seed,
                            ORANGE_TYPE)
                    if(!setOrangeDate.text.toString().equals(R.string.text_select_month)){
                        setGreenTonData()
                    }
                    setGreenSelection(true)
                } else {
                    setGreenSelection(false)
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }

    }

    private val greenValidation = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            setGreenTonData()
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }

    }

    private fun setGreenTonData(){
        if (dateDetailValidation(setGreenDate, setGreenDate.text.toString())) {
            val fromDate = setOrangeDate.text.toString()
            val tillDate = setGreenDate.text.toString()
            if (fromDate.contains("/") && tillDate.contains("/")) {
                val fdate = fromDate.split("/")
                val fmonth = fdate[0].toInt()
                val fyear = fdate[1].toInt()
                val tdate = tillDate.split("/")
                val tmonth = tdate[0].toInt()
                val tyear = tdate[1].toInt()
                if ((((tmonth - fmonth) >= 1 && fyear <= tyear) || fyear < tyear)) {
                    val startTarget = "${DateHelper.getYear()}-${DateHelper.getMonth()}"
                    val endDate = setGreenDate.text.toString()
                    val endTarget = "${endDate.split("/")[1]}-${endDate.split("/")[0]}"
                    mViewModel.getPotensi((activity as UpdateProblemActivity).id, startTarget, endTarget,
                            (activity as UpdateProblemActivity).tahunTanam,
                            (activity as UpdateProblemActivity).region,
                            (activity as UpdateProblemActivity).soil,
                            (activity as UpdateProblemActivity).seed,
                            GREEN_TYPE)
                } else {
                    toast("Bulan hijau harus lebih besar dari bulan oranye")
                    setGreenDate.setText(getString(R.string.text_select_month))
                }
            }
        }

    }

    private fun setGreenSelection(isEnable: Boolean) {
        setGreenDate.isEnabled = isEnable
        setGreenTon.isEnabled = isEnable
    }

    private fun isValid(): Boolean {
        var valid = true
        if (setOrangeDate.text.trim().toString().isEmpty() ||
                setOrangeDate.text.trim().toString() == getString(R.string.text_select_month)) {
            setOrangeDate.error = getString(R.string.text_must_be_filled)
            valid = false
        }
        if (setGreenDate.text.trim().toString().isEmpty() ||
                setGreenDate.text.trim().toString() == getString(R.string.text_select_month)) {
            setGreenDate.error = getString(R.string.text_must_be_filled)
            valid = false
        }
        if (setOrangeTon.text.trim().toString().isEmpty()) {
            setOrangeTon.error = getString(R.string.text_must_be_filled)
            valid = false
        }
        if (setGreenTon.text.trim().toString().isEmpty()) {
            setGreenTon.error = getString(R.string.text_must_be_filled)
            valid = false
        }

        if (!valid) {
            view?.apply {
                showSnackbarDefault(this, getString(R.string.text_ada_data_yang_belum), Snackbar.LENGTH_SHORT)
            }
        }

        return valid
    }

    fun saveProblemDetail() {
        if (isValid()) {
            (activity as UpdateProblemActivity).showLoadingEvent(true)
            val achievement = mViewDataBinding.achievement ?: ""
            val area = mViewDataBinding.area ?: ""
            mViewModel.updateProblemTarget((activity as UpdateProblemActivity).targetId,
                    ParamUpdateProblemTarget(
                            problemCheckId,
                            mViewDataBinding.plantYear,
                            achievement.split(" ")[0],
                            area.split(" ")[0],
                            setOrangeDate.text.toString(),
                            setOrangeTon.text.toString(),
                            setGreenDate.text.toString(),
                            setGreenTon.text.toString()
//                            problemUnitId
                    ))
        } else {
            (activity as UpdateProblemActivity).changeViewFragment(0)
        }
    }

    companion object {
        fun newInstance() = DetailFragment().apply {

        }

    }

}
