package redblock.SSC.gits.mvvm.notificationlist

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import redblock.SSC.gits.data.model.ContentNotification


object NotificationListBindings {

    @BindingAdapter("app:listDataNotificationList")
    @JvmStatic
    fun setListDataNotificationList(recyclerView: RecyclerView, data: List<ContentNotification>) {
        with(recyclerView.adapter as NotificationListAdapter) {
            replaceData(data)
        }
    }

}
