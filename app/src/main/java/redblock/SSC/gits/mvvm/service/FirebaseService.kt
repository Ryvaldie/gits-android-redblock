package redblock.SSC.gits.mvvm.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.data.model.Content
import redblock.SSC.gits.data.model.TokenModel
import redblock.SSC.gits.data.model.User
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.mvvm.main.MainActivity
import redblock.SSC.gits.mvvm.updateproblem.UpdateProblemActivity
import redblock.SSC.gits.util.Injection
import java.lang.Exception
import java.util.*


open class FirebaseService : FirebaseMessagingService() {

    @Suppress("deprecation")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.isNotEmpty()) {
            val data = remoteMessage.data["data"]
            val message = remoteMessage.data?.get("body")
            val title = remoteMessage.data?.get("title")
            val timestamp = remoteMessage.data?.get("timestamps")

            val intent = if (data == null || data.isEmpty()) {
                Intent(this, MainActivity::class.java)
                        .putExtra("PROBLEM_CHECK", true)
            } else {
                val updateProblemIntent = Intent(this, UpdateProblemActivity::class.java)
                updateProblemIntent.putExtra("DATA", remoteMessage.data["data"])
                if (message?.toLowerCase()?.contains("tolak") == true) {
                    updateProblemIntent.putExtra("REJECT_REASON", message.split("tolak ")[1])
                }
                updateProblemIntent
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val requestCode = 0
            val pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT)
            val sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            val randomId = Random().nextInt(1000)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationManager = getSystemService(NotificationManager::class.java)
                val channel = NotificationChannel(getString(R.string.app_name), message, NotificationManager.IMPORTANCE_HIGH)
                val att = AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build()
                channel.apply {
                    name = getString(R.string.app_name)
                    importance = NotificationManager.IMPORTANCE_DEFAULT
                    description = message
                    enableVibration(true)
                    setSound(sound, att)
                }
                notificationManager.createNotificationChannel(channel)
                val builder = Notification.Builder(applicationContext, getString(R.string.app_name))
                builder.apply {
                    setSmallIcon(R.drawable.ic_location_red)
                    setColor(this@FirebaseService.resources.getColor(R.color.sinarmasRed))
                    setContentText(message)
                    setContentTitle(title)
                    setAutoCancel(true)
                    setContentIntent(pendingIntent)
                }
                builder.style = Notification.BigTextStyle()
                        .bigText(message)
                notificationManager.notify(randomId, builder.build()) //0 = ID of notification
            } else {
                val noBuilder = NotificationCompat.Builder(this).apply {
                    setSmallIcon(R.drawable.ic_location_red)
                    setContentText(message)
                    setSound(sound)
                    setStyle(NotificationCompat.BigTextStyle()
                            .bigText(message))
                    setContentTitle(title)
                    setAutoCancel(true)
                    setContentIntent(pendingIntent)
                }

                val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(randomId, noBuilder.build()) //0 = ID of notification
            }
            val broadcastIntent = Intent(BaseActivity.NOTIFICATION)
                    .putExtra("STATUS", false)
                    .putExtra("NOTIF", true)
                    .putExtra("MESSAGE", message)
                    .putExtra("TIMESTAMP", timestamp)
            sendBroadcast(broadcastIntent)
        }

    }

    override fun onNewToken(token: String?) {
        val gitsRepository = Injection.provideGitsRepository(this)

        gitsRepository.registerToken(gitsRepository.getToken(), TokenModel(token), object : GitsDataSource.PostProblemCallback {
            override fun onShowProgressDialog() {
                //Left empty
            }

            override fun onHideProgressDialog() {
                //Left empty
            }

            override fun onSuccess(data: Any?) {
                //Left empty
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                //Left empty
            }

        })

    }

}
