package redblock.SSC.gits.mvvm.filter;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.toolbar.*
import redblock.SSC.gits.R
import redblock.SSC.gits.base.BaseActivity
import redblock.SSC.gits.data.model.FilterModel
import redblock.SSC.gits.util.gone
import redblock.SSC.gits.util.obtainViewModel
import redblock.SSC.gits.util.replaceFragmentInActivity
import redblock.SSC.gits.util.visible


class FilterActivity : BaseActivity() {

    var type = ""
    var refresh = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)

        type = intent.getStringExtra("TYPE")

        setupToolbar()
        setupFragment()
    }

    private fun setupToolbar() {

        refresh = when (type) {
            FILTER_DASHBOARD -> REFRESH_DASHBOARD
            FILTER_PROBLEM_CHECK -> REFRESH_PROBLEM_CHECK
            else -> REFRESH_UPLOAD_DATA
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.findViewById<ImageView>(R.id.indicator).gone()
        val back = toolbar.findViewById<ImageView>(R.id.btn_back)
        back.visible()
        back.setOnClickListener {
            onBackPressed()
        }
        val title = toolbar.findViewById<TextView>(R.id.txt_toolbar_title)
        title.visible()
        title.text = getString(R.string.text_filter)
        val mainTitle = toolbar.findViewById<RelativeLayout>(R.id.main_title)
        mainTitle.gone()
        val notification = toolbar.findViewById<ImageView>(R.id.notification)
        notification.gone()
        val filter = toolbar.findViewById<ImageView>(R.id.filter)
        filter.gone()
        val save = toolbar.findViewById<TextView>(R.id.btn_save)
        save.visible()
        save.setOnClickListener {
            val timeStat = when {
                obtainFilterViewModel().monthlyField.get() == true -> "monthly"
                obtainFilterViewModel().customField.get() == true -> "custom"
                else -> ""
            }
            val blockCheckList = if (obtainFilterViewModel().blockCheckList.isNotEmpty() && obtainFilterViewModel().blockCheckList[0] == ',') {
                obtainFilterViewModel().blockCheckList.substring(1)
            } else {
                obtainFilterViewModel().blockCheckList
            }
            val filterModel = FilterModel(
                    obtainFilterViewModel().repository.localDataSource.getUserDBDao()
                            .getDataById(0)?.userConfig?.division ?: "",
                    timeStat, blockCheckList
                    , PreferenceManager.getDefaultSharedPreferences(this)
                    .getString("COLOR", "") ?: "",
                    obtainFilterViewModel().endTime, obtainFilterViewModel().startTime
            )
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                    .putString(type, Gson().toJson(filterModel)).apply()
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                    .putBoolean(refresh, true).apply()
            onBackPressed()
        }
    }

    fun obtainFilterViewModel(): FilterViewModel = obtainViewModel(FilterViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        replaceFragmentInActivity(FilterFragment.newInstance(), R.id.frame_main_content)
    }

    companion object {

        const val FILTER_DASHBOARD = "FILTER_DASHBOARD"
        const val REFRESH_DASHBOARD = "REFRESH_DASHBOARD"
        const val FILTER_PROBLEM_CHECK = "FILTER_PROBLEM_CHECK"
        const val REFRESH_PROBLEM_CHECK = "REFRESH_PROBLEM_CHECK"
        const val FILTER_UPLOAD_DATA = "FILTER_UPLOAD_DATA"
        const val REFRESH_UPLOAD_DATA = "REFRESH_UPLOAD_DATA"

        fun startActivity(context: Context, type: String) {
            context.startActivity(Intent(context, FilterActivity::class.java).putExtra("TYPE", type))
        }

    }
}
