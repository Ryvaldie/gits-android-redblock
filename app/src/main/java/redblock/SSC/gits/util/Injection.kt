package redblock.SSC.gits.util

import android.content.Context
import android.preference.PreferenceManager
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.data.source.local.GitsAppDatabase
import redblock.SSC.gits.data.source.local.GitsLocalDataSource
import redblock.SSC.gits.data.source.remote.GitsRemoteDataSource
import redblock.SSC.gits.util.dbhelper.AppExecutors

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

object Injection {

    fun provideGitsRepository(context: Context): GitsRepository {
        val localDatabase = GitsAppDatabase.getInstance(context)

        return GitsRepository.getInstance(GitsRemoteDataSource,
                GitsLocalDataSource.getInstance(PreferenceManager.getDefaultSharedPreferences(context),
                        AppExecutors(), localDatabase.userDBDao(),
                        localDatabase.problemCheckDBDao(),
                        localDatabase.problemTargetDBDao(),
                        localDatabase.problemListDBDao(),
                        localDatabase.problemTypeDBDao(),
                        localDatabase.allBlockDBDao(),
                        localDatabase.draftProblemDBDao(),
                        localDatabase.unitTypeDBDao(),
                        localDatabase.draftProblemTargetDao(),
                        localDatabase.allPicDBDao(),
                        localDatabase.progressBlockDBDao(),
                        localDatabase.progrssActionDBDao(),
                        localDatabase.zPotensiDao()))
    }
}