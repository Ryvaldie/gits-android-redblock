package redblock.SSC.gits.util

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.DecimalFormat


class CustomValueFormatter(private val suffix: String) : IAxisValueFormatter {

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return mFormat.format(value) + suffix
    }

    private val mFormat: DecimalFormat = DecimalFormat("###,###,###,##0.0")

    fun getAxisLabel(value: Float, axis: AxisBase): String {
        return if (axis is XAxis) {
            mFormat.format(value)
        } else if (value > 0) {
            mFormat.format(value) + suffix
        } else {
            mFormat.format(value)
        }
    }
}
