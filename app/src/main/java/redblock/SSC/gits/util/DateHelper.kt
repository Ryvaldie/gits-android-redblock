package redblock.SSC.gits.util

import android.text.format.DateFormat
import java.lang.Exception
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class DateHelper {

    companion object {

        fun getDatePeriode(): String {
            val calendar = Calendar.getInstance()
            val df = SimpleDateFormat("MMM yy", Locale.getDefault())
            return df.format(Calendar.getInstance().time) + "-" + df.format(calendar.time)
        }

        fun getStartDate(): String {
            val calendar = Calendar.getInstance()
            val df = SimpleDateFormat("yyyy-MM", Locale.getDefault())
            return df.format(calendar.time)
        }

        fun getEndDate(): String {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.MONTH, -12)
            val df = SimpleDateFormat("yyyy-MM", Locale.getDefault())
            return df.format(calendar.time)
        }

        fun getTimeStamp(): String {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault())
            return simpleDateFormat.format(Date())
        }

        fun getYear(): Int {
            val simpleDateFormat = SimpleDateFormat("yyyy", Locale.getDefault())
            return simpleDateFormat.format(Date()).toInt()
        }

        fun getMonth(): Int {
            val simpleDateFormat = SimpleDateFormat("MM", Locale.getDefault())
            return simpleDateFormat.format(Date()).toInt()
        }

        fun getDate(): Int {
            val simpleDateFormat = SimpleDateFormat("dd", Locale.getDefault())
            return simpleDateFormat.format(Date()).toInt()
        }

        fun getHour(): Int {
            val simpleDateFormat = SimpleDateFormat("HH", Locale.getDefault())
            return simpleDateFormat.format(Date()).toInt()
        }

        fun getMinute(): Int {
            val simpleDateFormat = SimpleDateFormat("mm", Locale.getDefault())
            return simpleDateFormat.format(Date()).toInt()
        }

        fun getPhotoTimeStamp(): String {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            return simpleDateFormat.format(Date())
        }

        fun dateTimeMonth(date: String?): String {
            val tz = TimeZone.getTimeZone("UTC")
            val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            df.timeZone = tz

            return try {
                DateFormat.format("MMM yyyy", df.parse(date)).toString()
            } catch (ex: Exception) {
                ""
            }
        }

        fun dateTimeSet(date: String?): String {
            return if (date != null) {
                try {
                    val tz = TimeZone.getTimeZone("UTC")
                    val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                    df.timeZone = tz

                    DateFormat.format("MM/yyyy", df.parse(date)).toString()
                } catch (ex: ParseException) {
                    ""
                }
            } else {
                ""
            }
        }

        fun dateTimeProblem(date: String?): String? {
            return if (date != null) {
                try {
                    val tz = TimeZone.getTimeZone("UTC")
                    val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    df.timeZone = tz

                    DateFormat.format("MM/yyyy", df.parse(date)).toString()
                } catch (ex: ParseException) {
                    date
                }
            } else {
                date
            }
        }

        fun getCharMonth(date: String?): String? {
            return if (date != null) {
                try {
                    val tz = TimeZone.getTimeZone("UTC")
                    val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    df.timeZone = tz

                    DateFormat.format("MMM", df.parse(date)).toString()
                } catch (ex: ParseException) {
                    null
                }
            } else {
                null
            }
        }

        fun toFormattedDate(date: String?): String {
            return if (date != null) {
                try {
                    val dates = date.split("/")
                    "${dates[1]}-${dates[0]}-01"
                } catch (ex: ParseException) {
                    date ?: ""
                } catch (ex: IndexOutOfBoundsException) {
                    date ?: ""
                }
            } else {
                date ?: ""
            }
        }

        fun revertFormattedDate(date: String?): String {
            return if (date != null) {
                try {
                    val dates = date.split("-")
                    "${dates[1]}/${dates[0]}"
                } catch (ex: ParseException) {
                    date ?: ""
                } catch (ex: IndexOutOfBoundsException) {
                    date ?: ""
                }
            } else {
                date ?: ""
            }
        }

        fun convertTargetDate(string: String?): String {
            return if (string != null) {
                if (string.contains("/")) {
                    val temp = string.split("/")
                    temp[1] + "-" + temp[0] + "-01 00:00:00"
                } else {
                    ""
                }
            } else {
                ""
            }
        }

        private fun timeStampToLong(date: String?): Long {
            var timestamp: Long = 0
            val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

            if (date != null) {
                try {
                    timestamp = df.parse(date).time / 1000
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            } else {
                timestamp = Date().time / 1000
            }
            return timestamp
        }

        fun getTimeAgo(timestamp: String): String? {
            val secondMillis = 1000
            val minuteMillis = 60 * secondMillis
            val hourMillis = 60 * minuteMillis

            var time = timeStampToLong(timestamp)
            if (time < 1000000000000L) {
                time *= 1000
            }

            val now = System.currentTimeMillis()
            if (time <= 0) {
                return null
            }

            val diff = now - time
            return if (diff < minuteMillis || diff < 2 * minuteMillis ||
                    diff < 50 * minuteMillis || diff < 90 * minuteMillis ||
                    diff < 24 * hourMillis) {
                DateFormat.format("HH:mm aa", time).toString()
            } else {
                DateFormat.format("dd MMM yy", time).toString()
            }
        }

        fun getMonth(date: String?): String? {
            return if (date != null) {
                try {
                    val tz = TimeZone.getTimeZone("UTC")
                    val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    df.timeZone = tz

                    DateFormat.format("M", df.parse(date)).toString()
                } catch (ex: ParseException) {
                    null
                }
            } else {
                null
            }
        }


    }
}