package redblock.SSC.gits.util

import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object SSLConnection {
    @Throws(Exception::class)
    fun createSslSocketFactory(): SSLSocketFactory {
        val byPassTrustManagers = arrayOf<TrustManager>(object : X509TrustManager {
            override fun getAcceptedIssuers(): Array<X509Certificate?> {
                return arrayOfNulls(0)
            }

            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                // belum digunakan
            }

            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                // belum digunakan
            }
        })
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, byPassTrustManagers, SecureRandom())
        return sslContext.socketFactory
    }
}