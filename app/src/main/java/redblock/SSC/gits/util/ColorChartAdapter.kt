package redblock.SSC.gits.util

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.R
import redblock.SSC.gits.databinding.ItemColorChartBinding

class ColorChartAdapter(var item: List<Int?>?)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<Int?>?, page: Int) {
        this.item = item
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item?.size?:0)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return ItemColorChartViewHolder(ItemColorChartBinding.inflate(LayoutInflater.from(p0.context), p0, false))
    }

    override fun getItemCount(): Int = item?.size?:0

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (item != null) {
            (p0 as ItemColorChartViewHolder).bind(item!![p1])
        }
    }

    class ItemColorChartViewHolder(val mViewDataBinding: ItemColorChartBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        private val colors = arrayListOf(R.color.sinarmasRed, R.color.sinarmasOrange, R.color.sinarmasGreen, R.color.sinarmasWhite)

        fun bind(obj: Int?) {
            mViewDataBinding.apply {
                color.setBackgroundColor(root.context.resources.getColor(colors[obj ?: 0]))
            }
        }

    }

}