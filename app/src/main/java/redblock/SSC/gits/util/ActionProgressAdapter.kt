package redblock.SSC.gits.util

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.data.model.ProgressActionModel
import redblock.SSC.gits.databinding.ItemActionProgressBinding
import redblock.SSC.gits.mvvm.main.allblock.AllBlockUserActionListener
import redblock.SSC.gits.databinding.DetailItemActionProgressBinding

class ActionProgressAdapter(var item: List<ProgressActionModel>, val listener: AllBlockUserActionListener, val isDetail: Boolean)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<ProgressActionModel>, page: Int) {
        this.item = item
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item.size)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return if (isDetail) {
            DetailItemActionViewHolder(DetailItemActionProgressBinding.inflate(LayoutInflater.from(p0.context), p0, false))
        } else {
            ItemActionViewHolder(ItemActionProgressBinding.inflate(LayoutInflater.from(p0.context), p0, false))
        }
    }

    override fun getItemCount(): Int = item.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (isDetail) {
            (p0 as DetailItemActionViewHolder).bind(item[p1], listener)
        } else {
            (p0 as ItemActionViewHolder).bind(item[p1], listener)
        }
    }

    class ItemActionViewHolder(val mViewDataBinding: ItemActionProgressBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: ProgressActionModel?, listener: AllBlockUserActionListener) {
            mViewDataBinding.apply {
                item = obj
                val result = try {
                    (obj?.progress?.toFloat() ?: 0f)
                } catch (ex: Exception) {
                    0f
                }
                progress = if (result == 1.0f) {
                    0.999f
                } else {
                    result
                }
                mListener = listener
            }
        }

    }

    class DetailItemActionViewHolder(val mViewDataBinding: DetailItemActionProgressBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: ProgressActionModel?, listener: AllBlockUserActionListener) {
            mViewDataBinding.apply {
                item = obj
                val result = (obj?.progress?.toFloat() ?: 0.0f)
                progress = if (result == 1.0f) {
                    0.999f
                } else {
                    result
                }
                mListener = listener
            }
        }

    }

}