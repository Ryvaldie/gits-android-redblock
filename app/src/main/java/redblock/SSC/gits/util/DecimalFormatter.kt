package redblock.SSC.gits.util

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class DecimalFormatter {

    companion object {

        fun thousandSeperator(value: Int): String {
            val numberFormat = DecimalFormat.getInstance(Locale.US)
            val customSymbol = DecimalFormatSymbols()
            customSymbol.decimalSeparator = '.'
            customSymbol.groupingSeparator = '.'
            (numberFormat as DecimalFormat).decimalFormatSymbols = customSymbol
            numberFormat.setGroupingUsed(true)
            return numberFormat.format(value)
        }

        fun rupiahString(value: String): String {
            val numberFormat = DecimalFormat.getInstance(Locale.US)
            val customSymbol = DecimalFormatSymbols()
            customSymbol.decimalSeparator = '.'
            customSymbol.groupingSeparator = '.'
            (numberFormat as DecimalFormat).decimalFormatSymbols = customSymbol
            numberFormat.setGroupingUsed(true)
            return try {
                "Rp. " + numberFormat.format(value.replace("Rp. ", "").replace(".", "").toBigInteger())
            } catch (e: NumberFormatException) {
                ""
            }
        }


    }

}