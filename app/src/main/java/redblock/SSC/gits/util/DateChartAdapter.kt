package redblock.SSC.gits.util

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.databinding.ItemListDateChartBinding

class DateChartAdapter(var item: List<String?>?)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<String?>?, page: Int) {
        this.item = item
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item?.size?:0)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return ItemDateViewHolder(ItemListDateChartBinding.inflate(LayoutInflater.from(p0.context), p0, false))
    }

    override fun getItemCount(): Int = item?.size?:0

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (item != null) {
            (p0 as ItemDateViewHolder).bind(item!![p1])
        }
    }

    class ItemDateViewHolder(val mViewDataBinding: ItemListDateChartBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: String?) {
            mViewDataBinding.apply {
                dateChart.text = obj ?: ""
            }
        }

    }

}