package redblock.SSC.gits.util

import android.content.Context
import android.util.DisplayMetrics

class ImageHelper {

    companion object {

        fun dpToPx(dp: Int, context: Context): Int {
            val displayMetrics = context.resources.displayMetrics
            return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        }

    }

}