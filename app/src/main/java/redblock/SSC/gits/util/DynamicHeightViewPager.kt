package redblock.SSC.gits.util

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.View


class DynamicHeightViewPager @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ViewPager(context, attrs) {

    private var mCurrentView: View? = null

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (mCurrentView == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
            return
        }
        var height = 0
        mCurrentView?.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED))
        val h = mCurrentView?.measuredHeight ?: 0
        if (h > height) height = h
        val mHeightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)

        super.onMeasure(widthMeasureSpec, mHeightMeasureSpec)
    }

//    private fun measureFragment(view: View?): Int {
//        if (view == null)
//            return 0
//
//        view.measure(0, 0)
//        return view.measuredHeight
//    }

    fun measureCurrentView(currentView: View?) {
        mCurrentView = currentView
        requestLayout()
    }

}