package redblock.SSC.gits.util

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.param.ParamCreateProblem
import redblock.SSC.gits.data.param.ParamUpdateProblemTarget

class Converters {

    companion object {

        @TypeConverter
        @JvmStatic
        fun fromUserConfig(value: UserConfigModel?): String {
            return if (value == null) {
                ""
            } else {
                Gson().toJson(value)
            }
        }

        @TypeConverter
        @JvmStatic
        fun toUserConfig(value: String?): UserConfigModel {
            return if (value.isNullOrEmpty()) {
                UserConfigModel()
            } else {
                Gson().fromJson(value, UserConfigModel::class.java)
            }
        }

        @TypeConverter
        @JvmStatic
        fun fromProblemCheck(value: ProblemCheckModel?): String {
            return if (value == null) {
                ""
            } else {
                Gson().toJson(value)
            }
        }

        @TypeConverter
        @JvmStatic
        fun toProblemCheck(value: String): ProblemCheckModel {
            return Gson().fromJson(value, ProblemCheckModel::class.java)
        }

        @TypeConverter
        @JvmStatic
        fun fromProblemTarget(value: ProblemTarget?): String {
            return if (value == null) {
                ""
            } else {
                Gson().toJson(value)
            }
        }

        @TypeConverter
        @JvmStatic
        fun toProblemTarget(value: String): ProblemTarget {
            return Gson().fromJson(value, ProblemTarget::class.java)
        }

        @TypeConverter
        @JvmStatic
        fun fromProblemList(value: List<ItemProblemModel>?): String {
            return if (value == null) {
                ""
            } else {
                var output = ""
                for (data in value) {
                    output += Gson().toJson(data).toString() + "~#@!"
                }
                return output
            }
        }

        @TypeConverter
        @JvmStatic
        fun toProblemList(value: String): List<ItemProblemModel> {
            val arrayList = arrayListOf<ItemProblemModel>()
            for (data in value.split("~#@!")) {
                arrayList.add(Gson().fromJson(data, ItemProblemModel::class.java))
            }
            return arrayList
        }

        @TypeConverter
        @JvmStatic
        fun fromProblemType(value: List<ItemProblemTypeModel>?): String {
            return if (value == null) {
                ""
            } else {
                var output = ""
                for (data in value) {
                    output += Gson().toJson(data).toString() + "~#@!"
                }
                return output
            }
        }

        @TypeConverter
        @JvmStatic
        fun toProblemType(value: String): List<ItemProblemTypeModel> {
            val arrayList = arrayListOf<ItemProblemTypeModel>()
            for (data in value.split("~#@!")) {
                arrayList.add(Gson().fromJson(data, ItemProblemTypeModel::class.java))
            }
            return arrayList
        }

        @TypeConverter
        @JvmStatic
        fun fromAllBlock(value: List<String>?): String {
            return if (value == null) {
                ""
            } else {
                var output = ""
                for (data in value) {
                    output += Gson().toJson(data).toString() + "~#@!"
                }
                return output
            }
        }

        @TypeConverter
        @JvmStatic
        fun toAllBlock(value: String): List<String> {
            val arrayList = arrayListOf<String>()
            for (data in value.split("~#@!")) {
                arrayList.add(Gson().fromJson(data, String::class.java))
            }
            return arrayList
        }

        @TypeConverter
        @JvmStatic
        fun fromDraftProblem(value: ParamCreateProblem?): String {
            return if (value == null) {
                ""
            } else {
                Gson().toJson(value)
            }
        }

        @TypeConverter
        @JvmStatic
        fun toDraftProblem(value: String): ParamCreateProblem {
            return Gson().fromJson(value, ParamCreateProblem::class.java)
        }

        @TypeConverter
        @JvmStatic
        fun fromUnitType(value: List<UnitTypeModel>?): String {
            return if (value == null) {
                ""
            } else {
                var output = ""
                for (data in value) {
                    output += Gson().toJson(data).toString() + "~#@!"
                }
                return output
            }
        }

        @TypeConverter
        @JvmStatic
        fun toUnitType(value: String): List<UnitTypeModel> {
            val arrayList = arrayListOf<UnitTypeModel>()
            for (data in value.split("~#@!")) {
                arrayList.add(Gson().fromJson(data, UnitTypeModel::class.java))
            }
            return arrayList
        }

        @TypeConverter
        @JvmStatic
        fun fromDraftProblemTarget(value: ParamUpdateProblemTarget?): String {
            return if (value == null) {
                ""
            } else {
                Gson().toJson(value)
            }
        }

        @TypeConverter
        @JvmStatic
        fun toDraftProblemTarget(value: String): ParamUpdateProblemTarget {
            return Gson().fromJson(value, ParamUpdateProblemTarget::class.java)
        }

        @TypeConverter
        @JvmStatic
        fun fromPicModel(value: List<PicModel>?): String {
            return if (value == null) {
                ""
            } else {
                var output = ""
                for (data in value) {
                    output += Gson().toJson(data).toString() + "~#@!"
                }
                return output
            }
        }

        @TypeConverter
        @JvmStatic
        fun toPicModel(value: String): List<PicModel> {
            val arrayList = arrayListOf<PicModel>()
            for (data in value.split("~#@!")) {
                arrayList.add(Gson().fromJson(data, PicModel::class.java))
            }
            return arrayList
        }

        @TypeConverter
        @JvmStatic
        fun fromProgressBlockModel(value: List<ProgressBlockModel>?): String {
            return if (value == null) {
                ""
            } else {
                var output = ""
                for (data in value) {
                    output += Gson().toJson(data).toString() + "~#@!"
                }
                return output
            }
        }

        @TypeConverter
        @JvmStatic
        fun toProgressBlockModel(value: String): List<ProgressBlockModel> {
            val arrayList = arrayListOf<ProgressBlockModel>()
            for (data in value.split("~#@!")) {
                arrayList.add(Gson().fromJson(data, ProgressBlockModel::class.java))
            }
            return arrayList
        }

        @TypeConverter
        @JvmStatic
        fun fromProgressActionModel(value: List<ProgressActionModel>?): String {
            return if (value == null) {
                ""
            } else {
                var output = ""
                for (data in value) {
                    output += Gson().toJson(data).toString() + "~#@!"
                }
                return output
            }
        }

        @TypeConverter
        @JvmStatic
        fun toProgressActionModel(value: String): List<ProgressActionModel> {
            val arrayList = arrayListOf<ProgressActionModel>()
            for (data in value.split("~#@!")) {
                arrayList.add(Gson().fromJson(data, ProgressActionModel::class.java))
            }
            return arrayList
        }

    }

}