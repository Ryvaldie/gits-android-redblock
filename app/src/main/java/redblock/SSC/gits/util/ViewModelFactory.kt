package redblock.SSC.gits.util

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import redblock.SSC.gits.data.model.UserConfigModel
import redblock.SSC.gits.data.source.GitsRepository
import redblock.SSC.gits.mvvm.camera.CustomCameraViewModel
import redblock.SSC.gits.mvvm.detailprogressaction.DetailProgressActionViewModel
import redblock.SSC.gits.mvvm.filter.FilterViewModel
import redblock.SSC.gits.mvvm.login.LoginViewModel
import redblock.SSC.gits.mvvm.main.MainViewModel
import redblock.SSC.gits.mvvm.main.allblock.AllBlockViewModel
import redblock.SSC.gits.mvvm.main.periodeblock.PeriodeBlockViewModel
import redblock.SSC.gits.mvvm.notificationlist.NotificationListViewModel
import redblock.SSC.gits.mvvm.problemcheck.ProblemCheckViewModel
import redblock.SSC.gits.mvvm.settings.SettingsViewModel
import redblock.SSC.gits.mvvm.updateproblem.UpdateProblemViewModel
import redblock.SSC.gits.mvvm.updateproblem.detail.DetailViewModel
import redblock.SSC.gits.mvvm.updateproblem.problem.ProblemViewModel
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.ProblemClassificationViewModel
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.problemaction.ProblemActionViewModel
import redblock.SSC.gits.mvvm.uploaddata.UploadDataViewModel
import redblock.SSC.gits.mvvm.userconfig.UserConfigViewModel
import redblock.SSC.gits.mvvm.userguide.UserGuideViewModel

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

class ViewModelFactory private constructor(
        private val mApplication: Application,
        private val gitsRepository: GitsRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(MainViewModel::class.java) ->
                        MainViewModel(mApplication, gitsRepository)
                    isAssignableFrom(LoginViewModel::class.java) ->
                        LoginViewModel(mApplication, gitsRepository)
                    isAssignableFrom(PeriodeBlockViewModel::class.java) ->
                        PeriodeBlockViewModel(mApplication, gitsRepository)
                    isAssignableFrom(AllBlockViewModel::class.java) ->
                        AllBlockViewModel(mApplication, gitsRepository)
                    isAssignableFrom(FilterViewModel::class.java) ->
                        FilterViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ProblemCheckViewModel::class.java) ->
                        ProblemCheckViewModel(mApplication, gitsRepository)
                    isAssignableFrom(UploadDataViewModel::class.java) ->
                        UploadDataViewModel(mApplication, gitsRepository)
                    isAssignableFrom(SettingsViewModel::class.java) ->
                        SettingsViewModel(mApplication, gitsRepository)
                    isAssignableFrom(UpdateProblemViewModel::class.java) ->
                        UpdateProblemViewModel(mApplication, gitsRepository)
                    isAssignableFrom(DetailViewModel::class.java) ->
                        DetailViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ProblemViewModel::class.java) ->
                        ProblemViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ProblemClassificationViewModel::class.java) ->
                        ProblemClassificationViewModel(mApplication, gitsRepository)
                    isAssignableFrom(CustomCameraViewModel::class.java) ->
                        CustomCameraViewModel(mApplication, gitsRepository)
                    isAssignableFrom(UserConfigViewModel::class.java) ->
                        UserConfigViewModel(mApplication, gitsRepository)
//                    isAssignableFrom(ProblemActionViewModel::class.java) ->
//                        ProblemActionViewModel(mApplication, repository)
                    isAssignableFrom(DetailProgressActionViewModel::class.java) ->
                        DetailProgressActionViewModel(mApplication, gitsRepository)
                    isAssignableFrom(UserGuideViewModel::class.java) ->
                        UserGuideViewModel(mApplication, gitsRepository)
                    isAssignableFrom(NotificationListViewModel::class.java) ->
                        NotificationListViewModel(mApplication, gitsRepository)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(mApplication: Application) =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ViewModelFactory(mApplication,
                            Injection.provideGitsRepository(mApplication.applicationContext))
                            .also { INSTANCE = it }
                }
    }
}