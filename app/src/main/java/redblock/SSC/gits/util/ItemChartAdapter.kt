package redblock.SSC.gits.util

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import redblock.SSC.gits.data.model.Item
import redblock.SSC.gits.databinding.ItemListBarChartBinding

class ItemChartAdapter(var item: List<Item?>?)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceList(item: List<Item?>?, page: Int) {
        this.item = item
        if (page == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(this.item?.size ?: 0)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return ItemChartViewHolder(ItemListBarChartBinding.inflate(LayoutInflater.from(p0.context), p0, false))
    }

    override fun getItemCount(): Int = item?.size ?: 0

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (item != null) {
            (p0 as ItemChartViewHolder).bind(item!![p1])
        }
    }

    class ItemChartViewHolder(val mViewDataBinding: ItemListBarChartBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: Item?) {
            mViewDataBinding.apply {
                chartItem = obj ?: Item()

                itemChart.apply {
                    adapter = ColorChartAdapter(ArrayList(0))
                    layoutManager = GridLayoutManager(context, 12)
                    (adapter as ColorChartAdapter).replaceList(chartItem?.colorChart, 0)
                }

                recDateItem.apply {
                    adapter = DateChartAdapter(ArrayList(0))
                    layoutManager = GridLayoutManager(context,  12)
                    (adapter as DateChartAdapter).replaceList(obj?.date, 0)
                }
            }
        }

    }

}