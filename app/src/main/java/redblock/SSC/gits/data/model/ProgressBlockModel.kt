package redblock.SSC.gits.data.model

import com.google.gson.annotations.SerializedName

data class ProgressBlockModel(
        val block: String? = "",
        val colors: List<ProgressColor?>? = listOf()
)

data class ProgressColor(
        val color: String? = "",
        @SerializedName("month_block")
        val month: String? = ""
)
