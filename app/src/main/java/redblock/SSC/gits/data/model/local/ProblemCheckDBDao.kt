package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*

@Dao
interface ProblemCheckDBDao {

    @Query("SELECT * FROM problemCheck")
    fun getAllData(): List<ProblemCheckDBModel>?

    @Query("SELECT * FROM problemCheck WHERE id = :id")
    fun getDataById(id: Int): ProblemCheckDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: ProblemCheckDBModel)

    @Delete
    fun deleteData(data: ProblemCheckDBModel)

    @Query("DELETE FROM problemCheck")
    fun deleteTable()

}