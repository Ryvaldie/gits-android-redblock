package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import redblock.SSC.gits.data.model.ProblemCheckModel
import java.io.Serializable

@Entity(tableName = "problemCheck")
class ProblemCheckDBModel (

    @PrimaryKey()
    var id: Int? = null,

    @ColumnInfo(name = "date_updated")
    var dateUpdated: String? = null,

    @ColumnInfo(name = "problem_check")
    var problemCheck: ProblemCheckModel? = null


): Serializable