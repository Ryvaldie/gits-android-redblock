package redblock.SSC.gits.data.model


data class ItemProblemTypeModel(
        val id: Int? = 0, // 4
        val name: String? = "", // Aspek Sosial
        val description: String? = "",
        val created_at: String? = "", // null
        val updated_at: String? = "" // null
)