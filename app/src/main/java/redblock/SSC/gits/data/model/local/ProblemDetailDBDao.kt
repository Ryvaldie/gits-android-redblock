package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*
import redblock.SSC.gits.data.model.ProblemTarget

@Dao
interface ProblemDetailDBDao {

    @Query("SELECT * FROM problemDetail")
    fun getAllData(): List<ProblemDetailDBModel>?

    @Query("SELECT * FROM problemDetail WHERE id = :id")
    fun getDataById(id: Int): ProblemDetailDBModel?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: ProblemDetailDBModel)

    @Query("UPDATE problemDetail set problem_target = :data  WHERE id = :id")
    fun updateData(id: Int, data: ProblemTarget)

    @Delete
    fun deleteData(data: ProblemDetailDBModel)

    @Query("DELETE FROM problemDetail")
    fun deleteTable()

}