package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import redblock.SSC.gits.data.param.ParamCreateProblem
import java.io.Serializable

@Entity(tableName = "draftProblem")
class DraftProblemDBModel (

        @PrimaryKey(autoGenerate = true)
        var id: Int? = null,

        @ColumnInfo(name = "date_updated")
        var dateUpdated: String? = null,

        @ColumnInfo(name = "status")
        var status: String? = null,

        @ColumnInfo(name = "problem")
        var problem: ParamCreateProblem? = null

): Serializable