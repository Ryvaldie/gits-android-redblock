package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*
import redblock.SSC.gits.data.model.ItemProblemModel

@Dao
interface ProblemListDBDao {

    @Query("SELECT * FROM problemList")
    fun getAllData(): List<ProblemListDBModel>?

    @Query("SELECT * FROM problemList WHERE id = :id")
    fun getDataById(id: Int): ProblemListDBModel?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: ProblemListDBModel)

    @Query("UPDATE problemList set problem_list = :data  WHERE id = :id")
    fun updateData(id: Int, data: List<ItemProblemModel>)

    @Delete
    fun deleteData(data: ProblemListDBModel)

    @Query("DELETE FROM problemList")
    fun deleteTable()

}