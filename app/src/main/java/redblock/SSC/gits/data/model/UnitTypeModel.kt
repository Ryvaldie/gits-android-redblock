package redblock.SSC.gits.data.model

data class UnitTypeModel(
        var created_at: String? = "",
        var id: Int? = 0,
        var unit: String? = "",
        var updated_at: String? = ""
)