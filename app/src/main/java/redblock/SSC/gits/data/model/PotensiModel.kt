package redblock.SSC.gits.data.model


import com.google.gson.annotations.SerializedName

data class PotensiModel(
        @SerializedName("value")
        val value: Double? = 0.0
)