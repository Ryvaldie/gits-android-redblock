package redblock.SSC.gits.data.source

import okhttp3.MultipartBody
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.model.local.*
import redblock.SSC.gits.data.param.*
import redblock.SSC.gits.data.source.local.GitsLocalDataSource
import redblock.SSC.gits.data.source.remote.GitsRemoteDataSource
import redblock.SSC.gits.util.DateHelper

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

open class GitsRepository(private val remoteDataSource: GitsRemoteDataSource,
                          val localDataSource: GitsLocalDataSource) : GitsDataSource {

    override fun getZPotensi(token: String, callback: GitsDataSource.GetZPotensiCallback) {
        if (isOnline) {
            remoteDataSource.getZPotensi(token, object : GitsDataSource.GetZPotensiCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: List<ZPotensiModel>) {
                    localDataSource.appExecutors.diskIO.execute {
                        localDataSource.getZPotensiDao().deleteTable()
                        for (zpotensi in data) {
                            localDataSource.getZPotensiDao().insertData(zpotensi)
                        }
                    }

                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        }
    }

    override fun getProblemHistory(token: String, id: Int, callback: GitsDataSource.GetProblemCallback) {
        if (isOnline) {
            remoteDataSource.getProblemHistory(getToken(), id, callback)
        } else {
            //TODO implement offline function
        }
    }

    override fun unregisterToken(token: String, callback: GitsDataSource.PostProblemCallback) {
        if (isOnline) {
            remoteDataSource.unregisterToken(getToken(), object : GitsDataSource.PostProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: Any?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        }
    }

    override fun updateAll(token: String, idProblemTarget: Int, paramUpdateAll: ParamUpdateAll, callback: GitsDataSource.PostProblemCallback) {
        if (isOnline) {
            remoteDataSource.updateAll(getToken(), idProblemTarget, paramUpdateAll,
                    object : GitsDataSource.PostProblemCallback {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: Any?) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        }
    }

    override fun getPotensi(token: String, idProblemCheck: Int, startTarget: String, endTarget: String, tahunTanam: Int, region: String, soil: String, seed: String, callback: GitsDataSource.GetPotensiCallback) {
        if (isOnline) {
            remoteDataSource.getPotensi(getToken(), idProblemCheck, startTarget, endTarget, tahunTanam, region, soil, seed,
                    object : GitsDataSource.GetPotensiCallback {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: PotensiModel) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        } else {
            localDataSource.getPotensi(getToken(), idProblemCheck, startTarget, endTarget, tahunTanam, region, soil, seed,
                    object : GitsDataSource.GetPotensiCallback {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: PotensiModel) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        }
    }

    override fun getNotificationList(token: String, callback: GitsDataSource.GetNotificationListCallback) {
        if (isOnline) {
            remoteDataSource.getNotificationList(getToken(), object : GitsDataSource.GetNotificationListCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: NotificationListModel) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        }
    }

    override fun getNotifCount(): Int {
        return localDataSource.getNotifCount()
    }

    override fun saveNotifCount(count: Int) {
        localDataSource.saveNotifCount(count)
    }

    override fun registerToken(token: String, tokenModel: TokenModel, callback: GitsDataSource.PostProblemCallback) {
        if (isOnline) {
            remoteDataSource.registerToken(token, tokenModel, object : GitsDataSource.PostProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: Any?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFinish()
                }

            })
        }
    }

    override fun isDeviceListed(): Boolean {
        return localDataSource.isDeviceListed()
    }

    override fun setDeviceListedStatus(status: Boolean) {
        localDataSource.setDeviceListedStatus(status)
    }

    override fun getListImei(token: String, callback: GitsDataSource.GetListImeiCallback) {
        if (isOnline) {
            remoteDataSource.getListImei(getToken(), object : GitsDataSource.GetListImeiCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: List<ImeiModel>?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        } else {
            if (isDeviceListed()) {
                callback.onSuccess(arrayListOf())
            } else {
                callback.onFailed(404, "Device Imei is not found. Please contact your administrator.")
            }
        }
    }

    override fun getStateShowCase(): Boolean {
        return localDataSource.getStateShowCase()
    }

    override fun saveStateShowCase(state: Boolean) {
        localDataSource.saveStateShowCase(state)
    }

    override fun getProgressAction(token: String, div: String, estate: String, block: String?, startTime: String?, endTime: String?, callback: GitsDataSource.GetProgressAction) {
        if (isOnline) {
            remoteDataSource.getProgressAction(getToken(), div, estate, block, startTime, endTime,
                    object : GitsDataSource.GetProgressAction {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: List<ProgressActionModel>?) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        } else {
            localDataSource.getProgressAction(getToken(), div, estate, block, startTime, endTime,
                    object : GitsDataSource.GetProgressAction {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: List<ProgressActionModel>?) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        }
    }

    override fun getProgressBlock(token: String, div: String, estate: String, startTime: String?, endTime: String?, callback: GitsDataSource.GetProgressBlock) {
        if (isOnline) {
            remoteDataSource.getProgressBlock(getToken(), div, estate, startTime, endTime,
                    object : GitsDataSource.GetProgressBlock {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: List<ProgressBlockModel?>?) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            setConnectivityStatus(statusCode != 504)
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        } else {
            localDataSource.getProgressBlock(getToken(), div, estate, startTime, endTime,
                    object : GitsDataSource.GetProgressBlock {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: List<ProgressBlockModel?>?) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            setConnectivityStatus(statusCode != 504)
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        }
    }

    override fun uploadImages(parts: ArrayList<MultipartBody.Part>, callback: GitsDataSource.GetUploadedImages) {
        if (isOnline) {
            remoteDataSource.uploadImages(parts, callback)
        } else {
            callback.onHideProgressDialog()
            callback.onSuccess(arrayListOf())
        }
    }

    override fun getDraftProblem(callback: GitsDataSource.GetDraftProblemCallback) {
        localDataSource.getDraftProblem(object : GitsDataSource.GetDraftProblemCallback {
            override fun onShowProgressDialog() {
                callback.onShowProgressDialog()
            }

            override fun onHideProgressDialog() {
                callback.onHideProgressDialog()
            }

            override fun onSuccess(data: List<ParamCreateProblem?>?) {
                callback.onSuccess(data)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                callback.onFailed(statusCode, errorMessage)
            }

        })
    }

    override fun getDraftProblemTarget(callback: GitsDataSource.GetDraftProblemTargetCallback) {
        localDataSource.getDraftProblemTarget(object : GitsDataSource.GetDraftProblemTargetCallback {
            override fun onShowProgressDialog() {
                callback.onShowProgressDialog()
            }

            override fun onHideProgressDialog() {
                callback.onHideProgressDialog()
            }

            override fun onSuccess(data: List<ParamUpdateProblemTarget?>?) {
                callback.onSuccess(data)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                callback.onFailed(statusCode, errorMessage)
            }

        })
    }

    override fun getAllPic(token: String, callback: GitsDataSource.GetAllPic) {
        localDataSource.getAllPic(getToken(), object : GitsDataSource.GetAllPic {
            override fun onShowProgressDialog() {
                callback.onShowProgressDialog()
            }

            override fun onHideProgressDialog() {
                callback.onHideProgressDialog()
            }

            override fun onSuccess(data: List<PicModel>) {
                callback.onSuccess(data)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                callback.onFailed(statusCode, errorMessage)
            }

        })
    }

    override fun getAllData(token: String, div: String, psm: String, region: String, withPotensi: Boolean, callback: GitsDataSource.GetAllData) {
        if (isOnline) {
            val tokenTemp = if (token.isEmpty()) {
                getToken()
            } else {
                token
            }
            if (withPotensi) {
                getZPotensi(tokenTemp, object : GitsDataSource.GetZPotensiCallback {})
            }
            remoteDataSource.getAllData(tokenTemp, div, psm, region, withPotensi, object : GitsDataSource.GetAllData {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {

                }

                override fun onSuccess(data: AllDataModel) {
                    localDataSource.appExecutors.diskIO.execute {
                        localDataSource.getAllBlockDBDao().insertData(
                                AllBlockDBModel(0, DateHelper.getTimeStamp(), data.block)
                        )

                        localDataSource.getProblemCheckDBDao().insertData(
                                ProblemCheckDBModel(
                                        0,
                                        DateHelper.getTimeStamp(),
                                        ProblemCheckModel(
                                                data.problem_check
                                        )
                                )
                        )

                        localDataSource.getProgressBlockDBDao().insertData(
                                ProgressBlockDBModel(
                                        0,
                                        DateHelper.getTimeStamp(),
                                        data.progress_problem
                                )
                        )

                        localDataSource.getProgressActionDBDao().insertData(
                                ProgressActionDBModel(
                                        0,
                                        DateHelper.getTimeStamp(),
                                        data.progress_action
                                )
                        )

                        for (problemCheck in data.problem_check) {
                            if (problemCheck.problem_target != null) {
                                localDataSource.getProblemTargetDBDao().insertData(
                                        ProblemDetailDBModel(
                                                problemCheck.id,
                                                DateHelper.getTimeStamp(),
                                                problemCheck.problem_target
                                        )
                                )
                                if (problemCheck.problem_target.problem != null) {
                                    localDataSource.getProblemListDBDao().insertData(
                                            ProblemListDBModel(
                                                    problemCheck.problem_target_id,
                                                    DateHelper.getTimeStamp(),
                                                    problemCheck.problem_target.problem
                                            )
                                    )
                                }
                            }
                        }

                        localDataSource.getProblemTypeDBDao().insertData(
                                ProblemTypeDBModel(
                                        0,
                                        DateHelper.getTimeStamp(),
                                        data.problem_type
                                )
                        )

                        localDataSource.getUnitTypeDBDao().insertData(
                                UnitTypeDBModel(
                                        0,
                                        DateHelper.getTimeStamp(),
                                        data.type_unit
                                )
                        )

                        localDataSource.getAllPicDBDao().insertData(
                                AllPicDBModel(
                                        0,
                                        DateHelper.getTimeStamp(),
                                        data.pic
                                )
                        )
                    }

                    callback.onHideProgressDialog()
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        } else {
            localDataSource.getAllData(if (token.isEmpty()) {
                getToken()
            } else {
                token
            }, div, psm, region, withPotensi, object : GitsDataSource.GetAllData {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: AllDataModel) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        }
    }

    override fun updateProblemTarget(token: String, id: Int, paramUpdateProblemTarget: ParamUpdateProblemTarget, callback: GitsDataSource.PostProblemCallback) {
        if (isOnline) {
            remoteDataSource.updateProblemTarget(getToken(), id, paramUpdateProblemTarget, object : GitsDataSource.PostProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: Any?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    setConnectivityStatus(statusCode != 504)
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        } else {
            localDataSource.updateProblemTarget(getToken(), id, paramUpdateProblemTarget, object : GitsDataSource.PostProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: Any?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        }

    }

    override fun login(paramLogin: ParamLogin, callback: GitsDataSource.GetLogin) {
        if (isOnline) {
            remoteDataSource.login(paramLogin, object : GitsDataSource.GetLogin {

                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {

                }

                override fun onSuccess(data: LoginModel) {
                    localDataSource.appExecutors.diskIO.execute {
                        localDataSource.getUserDBDao().insertData(
                                UserDBModel(id = 0, dateUpdated = DateHelper.getTimeStamp(),
                                        userConfig = UserConfigModel(
                                                paramLogin.nik, paramLogin.password,
                                                data.user?.division ?: "", "1",
                                                data.user?.estate ?: "",
                                                data.user?.region ?: "",
                                                data.user?.psm ?: ""
                                        )
                                )
                        )
                    }

                    callback.onHideProgressDialog()
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        } else {
            callback.onHideProgressDialog()
            callback.onFailed(501, "Offline")
        }
    }

    override fun getToken(): String {
        return localDataSource.getToken()
    }

    override fun saveToken(token: String) {
        localDataSource.saveToken(token)
    }

    override fun getUnit(token: String, callback: GitsDataSource.GetUnit) {
        localDataSource.getUnit(getToken(), object : GitsDataSource.GetUnit {

            override fun onShowProgressDialog() {
                callback.onShowProgressDialog()
            }

            override fun onHideProgressDialog() {
                callback.onHideProgressDialog()
            }

            override fun onSuccess(data: List<UnitTypeModel>) {
                callback.onSuccess(data)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                callback.onFailed(statusCode, errorMessage)
            }

        })
    }

    override fun getAllBlock(token: String, div: String, psm: String, region: String, callback: GitsDataSource.GetAllBlock) {
        localDataSource.getAllBlock(getToken(), div, psm, region,
                object : GitsDataSource.GetAllBlock {

                    override fun onShowProgressDialog() {
                        callback.onShowProgressDialog()
                    }

                    override fun onHideProgressDialog() {
                        callback.onHideProgressDialog()
                    }

                    override fun onSuccess(data: List<String>) {
                        callback.onSuccess(data)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                    override fun onFailed(statusCode: Int, errorMessage: String?) {
                        callback.onFailed(statusCode, errorMessage)
                    }

                })
    }

    override fun getProblemTarget(token: String, id: Int, callback: GitsDataSource.GetProblemTargetCallback) {
        if (isOnline) {
            remoteDataSource.getProblemTarget(getToken(), id,
                    object : GitsDataSource.GetProblemTargetCallback {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: ProblemTarget) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            setConnectivityStatus(statusCode != 504)
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        } else {
            localDataSource.getProblemTarget(getToken(), id,
                    object : GitsDataSource.GetProblemTargetCallback {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: ProblemTarget) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        }
    }

    override fun getProblemCheck(token: String, div: String, timeStat: String, startTime: String, endTime: String, block: String, color: String, psm: String, region: String, callback: GitsDataSource.GetProblemCheckCallback) {
        if (isOnline) {
            remoteDataSource.getProblemCheck(getToken(), div, timeStat, startTime, endTime, block, color, psm, region,
                    object : GitsDataSource.GetProblemCheckCallback {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: ProblemCheckModel) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            setConnectivityStatus(statusCode != 504)
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        } else {
            localDataSource.getProblemCheck(getToken(), div, timeStat, startTime, endTime, block, color, psm, region,
                    object : GitsDataSource.GetProblemCheckCallback {
                        override fun onShowProgressDialog() {
                            callback.onShowProgressDialog()
                        }

                        override fun onHideProgressDialog() {
                            callback.onHideProgressDialog()
                        }

                        override fun onSuccess(data: ProblemCheckModel) {
                            callback.onSuccess(data)
                        }

                        override fun onFinish() {
                            callback.onFinish()
                        }

                        override fun onFailed(statusCode: Int, errorMessage: String?) {
                            callback.onFailed(statusCode, errorMessage)
                        }

                    })
        }
    }


    override fun getProblemType(token: String, callback: GitsDataSource.GetProblemTypeCallback) {
        localDataSource.getProblemType(getToken(), object : GitsDataSource.GetProblemTypeCallback {
            override fun onShowProgressDialog() {
                callback.onShowProgressDialog()
            }

            override fun onHideProgressDialog() {
                callback.onHideProgressDialog()
            }

            override fun onSuccess(data: List<ItemProblemTypeModel>?) {
                callback.onSuccess(data)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                callback.onFailed(statusCode, errorMessage)
            }

        })
    }

    override fun postProblem(token: String, paramCreateProblem: ParamCreateProblem, callback: GitsDataSource.PostProblemCallback) {
        if (isOnline) {
            remoteDataSource.postProblem(getToken(), paramCreateProblem, object : GitsDataSource.PostProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: Any?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    setConnectivityStatus(statusCode != 504)
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        } else {
            localDataSource.postProblem(getToken(), paramCreateProblem, object : GitsDataSource.PostProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: Any?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        }
    }

    override fun updateProblem(token: String, id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem, callback: GitsDataSource.PostProblemCallback) {
        if (isOnline) {
            remoteDataSource.updateProblem(getToken(), id, targetId, paramUpdateProblem, object : GitsDataSource.PostProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: Any?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    setConnectivityStatus(statusCode != 504)
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        } else {
            localDataSource.updateProblem(getToken(), id, targetId, paramUpdateProblem, object : GitsDataSource.PostProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: Any?) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        }
    }

    override fun getProblem(token: String, id: Int, callback: GitsDataSource.GetProblemCallback) {
        if (isOnline) {
            remoteDataSource.getProblem(getToken(), id, object : GitsDataSource.GetProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: List<ItemProblemModel>) {
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    setConnectivityStatus(statusCode != 504)
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        } else {
            localDataSource.getProblem(getToken(), id, object : GitsDataSource.GetProblemCallback {
                override fun onShowProgressDialog() {
                    callback.onShowProgressDialog()
                }

                override fun onHideProgressDialog() {
                    callback.onHideProgressDialog()
                }

                override fun onSuccess(data: List<ItemProblemModel>) {
                    for (i in 0 until data.size) {
                        data[i].created_at = DateHelper.dateTimeMonth(data[i].created_at)
                    }
                    callback.onSuccess(data)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

                override fun onFailed(statusCode: Int, errorMessage: String?) {
                    callback.onFailed(statusCode, errorMessage)
                }

            })
        }
    }

    fun getBlockContent(id: Int): Content? {
        val data = localDataSource.getProblemCheckDBDao().getAllData()
        for (content in data?.get(data.lastIndex)?.problemCheck?.content ?: arrayListOf()) {
            if (content.problem_target_id == id || content.id == id) {
                return content
            }
        }
        return Content()
    }

    fun getItemProblem(targetId: Int): ItemProblemModel? {
        val data = localDataSource.getProblemListDBDao().getDataById(targetId)
        for (itemProblem in data?.problemList ?: arrayListOf()) {
            if (itemProblem != null && itemProblem.problem_target_id == targetId) {
                return itemProblem
            }
        }
        return ItemProblemModel()
    }

    companion object {

        private var INSTANCE: GitsRepository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.

         * @param gitsRemoteDataSource backend data source
         * *
         * @return the [GitsRepository] instance
         */
        @JvmStatic
        fun getInstance(gitsRemoteDataSource: GitsRemoteDataSource, gitsLocalDataSource: GitsLocalDataSource) =
                INSTANCE ?: synchronized(GitsRepository::class.java) {
                    INSTANCE ?: GitsRepository(gitsRemoteDataSource, gitsLocalDataSource)
                            .also { INSTANCE = it }
                }

        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }

        var isOnline = true

        fun setConnectivityStatus(isOnline: Boolean) {
            this.isOnline = isOnline
        }

    }
}