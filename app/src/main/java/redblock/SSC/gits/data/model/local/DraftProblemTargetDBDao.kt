package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*
import redblock.SSC.gits.data.param.ParamUpdateProblemTarget

@Dao
interface DraftProblemTargetDBDao {

    @Query("SELECT * FROM draftProblemTarget")
    fun getAllData(): List<DraftProblemTargetDBModel>?

    @Query("SELECT * FROM draftProblemTarget WHERE id = :id")
    fun getDataById(id: Int): DraftProblemTargetDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: DraftProblemTargetDBModel)

    @Query("UPDATE draftProblemTarget set problem = :data, status = :status, date_updated = :dateUpdated WHERE id = :id")
    fun updateData(id: Int, data: ParamUpdateProblemTarget, status: String, dateUpdated: String)

    @Delete
    fun deleteData(data: DraftProblemTargetDBModel)

    @Query("DELETE FROM draftProblemTarget")
    fun deleteTable()

}