package redblock.SSC.gits.data.model

data class AllDataModel(
        val problem_check: List<Content>,
        val progress_problem: List<ProgressBlockModel>,
        val progress_action: List<ProgressActionModel>,
        val color: List<Color>,
        val problem_type: List<ItemProblemTypeModel>,
        val type_unit: List<UnitTypeModel>,
        val block: List<String>,
        val pic: List<PicModel>
)
