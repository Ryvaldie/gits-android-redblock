package redblock.SSC.gits.data.model

class PicModel (
        val id: Int? = 0,
        val name: String? = "",
        val created_at: String? = "",
        val updated_at: String? = "",
        val selected: Boolean? = false
)
