package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*

@Dao
interface UserDBDao {

    @Query("SELECT * FROM user")
    fun getAllData(): List<UserDBModel>?

    @Query("SELECT * FROM user WHERE id = :id")
    fun getDataById(id: Int): UserDBModel?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: UserDBModel)

    @Delete
    fun deleteData(data: UserDBModel)

    @Query("DELETE FROM user")
    fun deleteTable()

}