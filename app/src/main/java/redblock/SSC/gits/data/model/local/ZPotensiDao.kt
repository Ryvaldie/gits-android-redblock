package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*
import redblock.SSC.gits.data.model.ZPotensiModel

@Dao
interface ZPotensiDao {

    @Query("SELECT * FROM zPotensi")
    fun getAllData(): List<ZPotensiModel>?

    @Query("SELECT zvalue FROM zPotensi WHERE zregion = :region AND zsoil = :soil AND zptn = :potensi AND zseed = :seed")
    fun getDataByProblem(region: String, soil: String, potensi: Int, seed: String): Int?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: ZPotensiModel)

    @Delete
    fun deleteData(data: ZPotensiModel)

    @Query("DELETE FROM zPotensi")
    fun deleteTable()

}