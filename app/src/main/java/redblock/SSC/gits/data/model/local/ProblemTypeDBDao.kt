package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*

@Dao
interface ProblemTypeDBDao {

    @Query("SELECT * FROM problemType")
    fun getAllData(): List<ProblemTypeDBModel>?

    @Query("SELECT * FROM user WHERE id = :id")
    fun getDataById(id: Int): ProblemTypeDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: ProblemTypeDBModel)

    @Delete
    fun deleteData(data: ProblemTypeDBModel)

    @Query("DELETE FROM problemType")
    fun deleteTable()

}