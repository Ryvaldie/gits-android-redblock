package redblock.SSC.gits.data.model

data class ItemChartModel(
        val title: String?,
        val item: ArrayList<Item>
)