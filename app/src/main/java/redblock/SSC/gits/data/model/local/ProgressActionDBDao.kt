package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*

@Dao
interface ProgressActionDBDao {

    @Query("SELECT * FROM progressAction")
    fun getAllData(): List<ProgressActionDBModel>?

    @Query("SELECT * FROM progressAction WHERE id = :id")
    fun getDataById(id: Int): ProgressActionDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: ProgressActionDBModel)

    @Delete
    fun deleteData(data: ProgressActionDBModel)

    @Query("DELETE FROM progressAction")
    fun deleteTable()

}