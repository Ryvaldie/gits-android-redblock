package redblock.SSC.gits.data.model

import com.google.gson.annotations.SerializedName


data class NotificationListModel(
        @SerializedName("content")
        val content: List<ContentNotification?>? = listOf(),
        @SerializedName("notifications_total")
        val notificationsTotal: Int? = 0,
        @SerializedName("pagginate")
        val pagginate: Pagginate? = Pagginate()
)

data class ContentNotification(
        @SerializedName("data")
        val `data`: String? = "",
        @SerializedName("body")
        var body: String? = "",
        @SerializedName("created_at")
        val createdAt: String? = "",
        @SerializedName("id")
        val id: Int? = 0,
        @SerializedName("read")
        val read: Int? = 0,
        @SerializedName("receiver_id")
        val receiverId: Int? = 0,
        @SerializedName("sender_id")
        val senderId: Int? = 0,
        @SerializedName("title")
        val title: String? = "",
        @SerializedName("updated_at")
        val updatedAt: String? = "",
        @SerializedName("user_receiver")
        val userReceiver: UserReceiver? = UserReceiver(),
        @SerializedName("user_sender")
        val userSender: Any? = Any()
)

data class UserReceiver(
        @SerializedName("created_at")
        val createdAt: String? = "",
        @SerializedName("division")
        val division: String? = "",
        @SerializedName("email")
        val email: Any? = Any(),
        @SerializedName("estate")
        val estate: String? = "",
        @SerializedName("id")
        val id: Int? = 0,
        @SerializedName("id_atasan")
        val idAtasan: Int? = 0,
        @SerializedName("nama")
        val nama: String? = "",
        @SerializedName("nik")
        val nik: String? = "",
        @SerializedName("psm")
        val psm: String? = "",
        @SerializedName("region")
        val region: String? = "",
        @SerializedName("role")
        val role: Role? = Role(),
        @SerializedName("roles_id")
        val rolesId: Int? = 0,
        @SerializedName("status")
        val status: Int? = 0,
        @SerializedName("updated_at")
        val updatedAt: String? = ""
)
