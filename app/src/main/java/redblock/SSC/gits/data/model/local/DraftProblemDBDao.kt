package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*
import redblock.SSC.gits.data.param.ParamCreateProblem

@Dao
interface DraftProblemDBDao {

    @Query("SELECT * FROM draftProblem")
    fun getAllData(): List<DraftProblemDBModel>?

    @Query("SELECT * FROM draftProblem WHERE id = :id")
    fun getDataById(id: Int): DraftProblemDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: DraftProblemDBModel)

    @Query("UPDATE draftProblem set problem = :data, status = :status, date_updated = :dateUpdated WHERE id = :id")
    fun updateData(id: Int, data: ParamCreateProblem, status: String, dateUpdated: String)

    @Delete
    fun deleteData(data: DraftProblemDBModel)

    @Query("DELETE FROM draftProblem")
    fun deleteTable()

}