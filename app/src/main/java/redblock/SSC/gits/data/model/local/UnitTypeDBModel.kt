package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import redblock.SSC.gits.data.model.UnitTypeModel
import java.io.Serializable

@Entity(tableName = "unitType")
class UnitTypeDBModel (

        @PrimaryKey()
        var id: Int? = null,

        @ColumnInfo(name = "date_updated")
        var dateUpdated: String? = null,

        @ColumnInfo(name = "unit")
        var unit: List<UnitTypeModel>? = null

): Serializable