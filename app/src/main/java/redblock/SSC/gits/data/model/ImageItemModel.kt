package redblock.SSC.gits.data.model

data class ImageItemModel(
    var created_at: String? = "",
    val id: Int? = 0,
    var link: String? = "",
    val problem_id: Int? = 0,
    var section: String? = "",
    val updated_at: String? = ""
)