package redblock.SSC.gits.data.param

data class ParamLogin(
        val nik: String,
        val password: String
)