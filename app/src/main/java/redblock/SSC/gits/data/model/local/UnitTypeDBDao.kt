package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*

@Dao
interface UnitTypeDBDao {

    @Query("SELECT * FROM unitType")
    fun getAllData(): List<UnitTypeDBModel>?

    @Query("SELECT * FROM unitType WHERE id = :id")
    fun getDataById(id: Int): UnitTypeDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: UnitTypeDBModel)

    @Delete
    fun deleteData(data: UnitTypeDBModel)

    @Query("DELETE FROM unitType")
    fun deleteTable()

}