package redblock.SSC.gits.data.param

data class ParamUpdateProblem(
        var total_area: String? = "", // 1000
        var problem_area: String? = "", // 20
        var progress_overall: String? = "",
        var problem_action: List<ProblemActionParam>? = listOf()
)
