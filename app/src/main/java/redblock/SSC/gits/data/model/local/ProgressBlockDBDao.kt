package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*

@Dao
interface ProgressBlockDBDao {

    @Query("SELECT * FROM progressBlock")
    fun getAllData(): List<ProgressBlockDBModel>?

    @Query("SELECT * FROM progressBlock WHERE id = :id")
    fun getDataById(id: Int): ProgressBlockDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: ProgressBlockDBModel)

    @Delete
    fun deleteData(data: ProgressBlockDBModel)

    @Query("DELETE FROM progressBlock")
    fun deleteTable()

}