package redblock.SSC.gits.data.model


import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "zPotensi")
data class ZPotensiModel(

        @PrimaryKey
        @SerializedName("id")
        var id: Int? = 0,

        @ColumnInfo(name = "createdAt")
        @SerializedName("created_at")
        var createdAt: String? = "",

        @ColumnInfo(name = "updatedAt")
        @SerializedName("updated_at")
        var updatedAt: String? = "",

        @ColumnInfo(name = "zptn")
        @SerializedName("zptn")
        var zptn: Int? = 0,

        @ColumnInfo(name = "zregion")
        @SerializedName("zregion")
        var zregion: String? = "",

        @ColumnInfo(name = "zseed")
        @SerializedName("zseed")
        var zseed: String? = "",

        @ColumnInfo(name = "zsoil")
        @SerializedName("zsoil")
        var zsoil: String? = "",

        @ColumnInfo(name = "zvalue")
        @SerializedName("zvalue")
        var zvalue: Int? = 0

) : Serializable
