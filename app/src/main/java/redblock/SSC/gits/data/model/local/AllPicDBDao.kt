package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*

@Dao
interface AllPicDBDao {

    @Query("SELECT * FROM allPic")
    fun getAllData(): List<AllPicDBModel>?

    @Query("SELECT * FROM allPic WHERE id = :id")
    fun getDataById(id: Int): AllPicDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: AllPicDBModel)

    @Delete
    fun deleteData(data: AllPicDBModel)

    @Query("DELETE FROM allPic")
    fun deleteTable()

}