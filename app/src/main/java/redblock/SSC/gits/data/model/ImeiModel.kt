package redblock.SSC.gits.data.model

data class ImeiModel(
    val created_at: String? = "",
    val id: Int? = 0,
    val imei: String? = "",
    val updated_at: String? = ""
)