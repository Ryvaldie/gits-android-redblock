package redblock.SSC.gits.data.model

data class FilterModel(
        val div: String = "",
        val timeStat: String = "yearly",
        val block: String = "",
        val color: String = "",
        val endTime: String = "",
        val startTime: String = ""
)