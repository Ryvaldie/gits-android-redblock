package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import redblock.SSC.gits.data.model.ProgressActionModel
import redblock.SSC.gits.data.model.ProgressBlockModel
import java.io.Serializable

@Entity(tableName = "progressAction")
class ProgressActionDBModel (

        @PrimaryKey()
        var id: Int? = null,

        @ColumnInfo(name = "date_updated")
        var dateUpdated: String? = null,

        @ColumnInfo(name = "action_progress")
        var progressAction: List<ProgressActionModel>? = null

): Serializable
