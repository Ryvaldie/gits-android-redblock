package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.*

@Dao
interface AllBlockDBDao {

    @Query("SELECT * FROM allBlock")
    fun getAllData(): List<AllBlockDBModel>?

    @Query("SELECT * FROM allBlock WHERE id = :id")
    fun getDataById(id: Int): AllBlockDBModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: AllBlockDBModel)

    @Delete
    fun deleteData(data: AllBlockDBModel)

    @Query("DELETE FROM allBlock")
    fun deleteTable()

}