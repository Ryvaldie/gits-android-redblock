package redblock.SSC.gits.data.model

import com.google.gson.annotations.SerializedName


data class ItemProblemModel(
        val id: Int? = 0, // 3
        val problem_target_id: Int? = 0, // 1
        val problem_detail_id: Int? = 0, // 4
        var created_at: String? = "", // null
        var updated_at: String? = "", // null
        var problem_detail: ProblemDetail? = ProblemDetail(),
        // Field lokal
        var status: Boolean = true,
        // Field history
        val parents_id: Int? = 0,
        val history_date: String? = "",
        @SerializedName("problem_target")
        val problemTarget: ProblemTarget? = ProblemTarget()
) {

    data class ProblemTarget(
            @SerializedName("achievement")
            val achievement: Double? = 0.0,
            @SerializedName("alasan")
            val alasan: String? = "",
            @SerializedName("area")
            val area: Float? = 0F,
            @SerializedName("created_at")
            val createdAt: String? = "",
            @SerializedName("current_approval")
            val currentApproval: Int? = 0,
            @SerializedName("description")
            val description: String? = "",
            @SerializedName("hierarcy_approval")
            val hierarcyApproval: String? = "",
            @SerializedName("id")
            val id: Int? = 0,
            @SerializedName("info")
            val info: String? = "",
            @SerializedName("plant_year")
            val plantYear: Int? = 0,
            @SerializedName("problem_check")
            val problemCheck: ProblemCheck? = ProblemCheck(),
            @SerializedName("problem_check_id")
            val problemCheckId: Int? = 0,
            @SerializedName("status")
            val status: String? = "",
            @SerializedName("target_green")
            val targetGreen: String? = "",
            @SerializedName("target_orange")
            val targetOrange: String? = "",
            @SerializedName("target_ton_green")
            val targetTonGreen: Double? = 0.0,
            @SerializedName("target_ton_orange")
            val targetTonOrange: Int? = 0,
            @SerializedName("unit_type_id")
            val unitTypeId: Int? = 0,
            @SerializedName("updated_at")
            val updatedAt: String? = "",
            @SerializedName("updated_by")
            val updatedBy: Int? = 0,
            @SerializedName("version")
            val version: Int? = 0
    )

    data class ProblemCheck(
            @SerializedName("id")
            val id: Int? = 0,
            @SerializedName("status_color_id")
            val statusColorId: Int? = 0
    )

    data class ProblemType(
            val id: Int? = 0, // 3
            val name: String? = "", // Harvesting
            val description: String? = "",
            val created_at: String? = "", // null
            val updated_at: String? = "" // null
    )

    data class ProblemDetail(
            val id: Int? = 0, // 4
            val problem_id: Int? = 0, // 3
            var total_area: String = "", // 1000
            var problem_area: String = "", // 20
            val version: String = "",
            val unit_type: UnitTypeModel? = UnitTypeModel(
                    unit = "Ha"
            ),
            val finished: String? = "0",
            var stat: String? = null, // approved
            val created_at: String? = "", // null
            val updated_at: String? = "",
            var progress_overall: String? = "0",
            var problem_action: List<ProblemAction> = listOf()
    )

    data class ProblemAction(
            var PIC: String? = "",
            var action_plan: String? = "",
            var cost: String? = "",
            var created_at: String? = "",
            var end_date: String? = "",
            var finished: String? = "",
            var id: Int? = 0,
            var problem_detail: String? = "",
            var problem_detail_id: Int? = 0,
            var problem_scale: String? = "",
            var progress: String? = "0",
            var start_date: String? = "",
            var stat_cost: String? = "Sesuai Anggaran",
            var type_unit: UnitTypeModel? = UnitTypeModel(),
            var type_unit_id: Int? = 0,
            var updated_at: String? = "",
            var files: List<ImageItemModel?>? = listOf(),
            var problem_type_id: String? = "0",
            var pokok: String? = "",
            var kalibrasi: String? = "",
            var is_primary: Int? = 0,
            var is_locked: Int? = 0,
            val pre_progress: Float? = 0F,
            val comment_action: List<CommentAction?>? = listOf(),
            val comment_problem: List<CommentProblem?>? = listOf()
    )

    data class CommentAction(
            val comment: String? = "",
            val created_at: String? = "",
            val id: Int? = 0,
            val problem_id: Int? = 0,
            val time: String? = "",
            val updated_at: Any? = Any(),
            val user: Any? = Any(),
            val user_id: Int? = 0
    )

    data class CommentProblem(
            val comment: String? = "",
            val created_at: String? = "",
            val id: Int? = 0,
            val problem_id: Int? = 0,
            val time: String? = "",
            val updated_at: Any? = Any(),
            val user: User? = User(),
            val user_id: Int? = 0
    )

    data class User(
            val created_at: String? = "",
            val division: String? = "",
            val id: Int? = 0,
            val nik: String? = "",
            val password: String? = "",
            val roles_id: Int? = 0,
            val updated_at: String? = ""
    )

}