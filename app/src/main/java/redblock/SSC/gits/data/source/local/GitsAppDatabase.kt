package redblock.SSC.gits.data.source.local

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.arch.persistence.room.migration.Migration
import android.content.Context
import redblock.SSC.gits.data.model.ZPotensiModel
import redblock.SSC.gits.data.model.local.*
import redblock.SSC.gits.util.Converters


@Database(entities = [(UserDBModel::class), (ProblemCheckDBModel::class),
    (ProblemDetailDBModel::class), (ProblemListDBModel::class), (ProblemTypeDBModel::class),
    (AllBlockDBModel::class), (DraftProblemDBModel::class), (UnitTypeDBModel::class),
    (DraftProblemTargetDBModel::class), (AllPicDBModel::class), (ProgressBlockDBModel::class),
    (ProgressActionDBModel::class), (ZPotensiModel::class)], version = 5)
@TypeConverters(Converters::class)
abstract class GitsAppDatabase : RoomDatabase() {

    abstract fun userDBDao(): UserDBDao
    abstract fun problemCheckDBDao(): ProblemCheckDBDao
    abstract fun problemTargetDBDao(): ProblemDetailDBDao
    abstract fun problemListDBDao(): ProblemListDBDao
    abstract fun problemTypeDBDao(): ProblemTypeDBDao
    abstract fun allBlockDBDao(): AllBlockDBDao
    abstract fun draftProblemDBDao(): DraftProblemDBDao
    abstract fun unitTypeDBDao(): UnitTypeDBDao
    abstract fun draftProblemTargetDao(): DraftProblemTargetDBDao
    abstract fun allPicDBDao(): AllPicDBDao
    abstract fun progressBlockDBDao(): ProgressBlockDBDao
    abstract fun progrssActionDBDao(): ProgressActionDBDao
    abstract fun zPotensiDao(): ZPotensiDao

    companion object {

        @Volatile
        private var INSTANCE: GitsAppDatabase? = null

        fun getInstance(context: Context): GitsAppDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also {
                        INSTANCE = it
                    }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        GitsAppDatabase::class.java, "redblock.db")
                        .allowMainThreadQueries()
//                        .addMigrations(MIGRATION)
                        .fallbackToDestructiveMigration()
                        .build()

        private val MIGRATION: Migration = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE user ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE problemCheck ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE problemDetail ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE problemList ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE problemType ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE allBlock ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE draftProblem ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE unitType ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE draftProblemTarget ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE allPic ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE progressBlock ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE progressAction ADD COLUMN last_update INTEGER")
                database.execSQL("ALTER TABLE zPotensi ADD COLUMN last_update INTEGER")
            }
        }
    }
}