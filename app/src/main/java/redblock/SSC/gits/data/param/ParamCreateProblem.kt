package redblock.SSC.gits.data.param

import redblock.SSC.gits.data.model.ImageItemModel
import redblock.SSC.gits.data.model.ItemProblemModel

data class ParamCreateProblem(
        var problem_target_id: String? = "", // 1
        var total_area: String? = "", // 1000
        var problem_area: String? = "", // 20
        var progress_overall: String? = "",
        var problem_action: List<ProblemActionParam>? = listOf(),
        //local variable
        var isChecked: Boolean = false
)

data class ProblemActionParam(
        var id: Int? = null,
        var PIC: String? = "",
        var action_plan: String? = "",
        var cost: String? = "",
        var end_date: String? = "",
        var finished: String? = "",
        var problem_detail: String? = "",
        var problem_scale: String? = "",
        var progress: String? = "",
        var start_date: String? = "",
        var stat_cost: String? = "",
        var type_unit_id: Int? = 0,
        var files: List<ImageItemModel?>? = listOf(),
        var problem_type_id: String? = "1",
        var lat: String? = "",
        var lang: String? = "",
        var pokok: String? = "",
        var kalibrasi: String? = "",
        var is_primary: Int? = 0,
        var is_locked: Int? = 0,
        val pre_progress: Float? = 0F
)
