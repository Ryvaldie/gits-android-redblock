package redblock.SSC.gits.data.source.remote

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import redblock.SSC.gits.base.BaseApiModel
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.param.*
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.util.DateHelper
import redblock.co.gits.gitsdriver.utils.GitsHelper

/**
 * Created by irfanirawansukirman on 26/01/18.
 */
object GitsRemoteDataSource : GitsDataSource {

    override fun getZPotensi(token: String, callback: GitsDataSource.GetZPotensiCallback) {
        GitsApiService.getApiService
                .getZPotensi(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<ZPotensiModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<ZPotensiModel>>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }

                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun getProblemHistory(token: String, id: Int, callback: GitsDataSource.GetProblemCallback) {
        GitsApiService.getApiService
                .getProblemHistory(token, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<ItemProblemModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<ItemProblemModel>>) {
                        if (model.data != null) {
                            val list: MutableList<ItemProblemModel> = ArrayList()
                            for(i in model.data.indices){
                                model.data[i].problem_detail?.stat = ""
                                if (model.data[i].problemTarget != null && model.data[i].problemTarget != ItemProblemModel.ProblemTarget()) {
                                    if (model.data[i].problemTarget?.problemCheck?.statusColorId == 2) {
                                       list.add(model.data[i])
                                    }
                                }
                            }

                            var tempDate = ""
                            var dataResult : Int? = 0
                            var create1 = ""
                            var create2 = ""

                            val listResult: MutableList<ItemProblemModel> = ArrayList()
                            for (i in list.indices){
                                val month = DateHelper.getMonth(list[i].created_at)
                                if(!"".equals(tempDate)) {
                                    dataResult = month?.toInt()?.minus(tempDate.toInt())
                                }

                                if("".equals(tempDate) || (!"".equals(tempDate)) && dataResult != 1 && dataResult != -11){
                                    if ("" == create1) {
                                        create1 = list[i].created_at?:""
                                    } else {
                                        create1 = list[i].created_at?:""
                                    }
                                    listResult.add(list[i])
                                } else {
                                    listResult.set(listResult.size - 1, list[i])
                                    create2 = list[i].created_at?:""
                                    if (listResult.size != 0) {
                                        listResult.get(listResult.size - 1).created_at = "${DateHelper.dateTimeMonth(create1)} - ${DateHelper.dateTimeMonth(create2)} "
                                    }
                                }
                                tempDate = month.toString()
                            }

                            callback.onSuccess(listResult)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }

                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun unregisterToken(token: String, callback: GitsDataSource.PostProblemCallback) {
        GitsApiService.getApiService
                .unregisterToken(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<Any?>>() {
                    override fun onSuccess(model: BaseApiModel<Any?>) {
                        callback.onSuccess(model.data)
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun updateAll(token: String, idProblemTarget: Int, paramUpdateAll: ParamUpdateAll, callback: GitsDataSource.PostProblemCallback) {
        GitsApiService.getApiService
                .updateAll(token, idProblemTarget, paramUpdateAll)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<Any?>>() {
                    override fun onSuccess(model: BaseApiModel<Any?>) {
                        callback.onSuccess(model.data)
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getPotensi(token: String, idProblemCheck: Int, startTarget: String, endTarget: String, tahunTanam: Int, region: String, soil: String, seed: String, callback: GitsDataSource.GetPotensiCallback) {
        GitsApiService.getApiService
                .getPotensi(token, idProblemCheck, startTarget, endTarget)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<PotensiModel>>() {
                    override fun onSuccess(model: BaseApiModel<PotensiModel>) {
                        callback.onSuccess(model.data ?: PotensiModel())
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getNotificationList(token: String, callback: GitsDataSource.GetNotificationListCallback) {
        GitsApiService.getApiService
                .getNotificationList(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<NotificationListModel>>() {
                    override fun onSuccess(model: BaseApiModel<NotificationListModel>) {
                        callback.onSuccess(model.data ?: NotificationListModel())
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getNotifCount(): Int {
        return 0
    }

    override fun saveNotifCount(count: Int) {
        //TODO("not implemented")
    }

    override fun registerToken(token: String, tokenModel: TokenModel, callback: GitsDataSource.PostProblemCallback) {
        GitsApiService.getApiService
                .registerToken(token, tokenModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<Any>>() {
                    override fun onSuccess(model: BaseApiModel<Any>) {
                        callback.onSuccess(model.data)
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun isDeviceListed(): Boolean {
        return false
    }

    override fun setDeviceListedStatus(status: Boolean) {
        //TODO("not implemented")
    }

    override fun getListImei(token: String, callback: GitsDataSource.GetListImeiCallback) {
        GitsApiService.getApiService
                .getListImei(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<ImeiModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<ImeiModel>>) {
                        callback.onSuccess(model.data)
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getStateShowCase(): Boolean {
        return false
    }

    override fun saveStateShowCase(state: Boolean) {
        //TODO("not implemented")
    }

    override fun getProgressAction(token: String, div: String, estate: String, block: String?, startTime: String?, endTime: String?, callback: GitsDataSource.GetProgressAction) {
        GitsApiService.getApiService
                .getProgressAction(token, div, estate, block, startTime, endTime)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<ProgressActionModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<ProgressActionModel>>) {
                        callback.onSuccess(model.data)
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getProgressBlock(token: String, div: String, estate: String, startTime: String?, endTime: String?, callback: GitsDataSource.GetProgressBlock) {
        GitsApiService.getApiService
                .getProgressBlock(token, div, estate, startTime, endTime)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<ProgressBlockModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<ProgressBlockModel>>) {
                        callback.onSuccess(model.data)
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun uploadImages(parts: ArrayList<MultipartBody.Part>, callback: GitsDataSource.GetUploadedImages) {
        GitsApiService.getImageService
                .uploadImages(null, parts)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<String>>>() {
                    override fun onSuccess(model: BaseApiModel<List<String>>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getDraftProblem(callback: GitsDataSource.GetDraftProblemCallback) {
        //TODO("not implemented")
    }

    override fun getDraftProblemTarget(callback: GitsDataSource.GetDraftProblemTargetCallback) {
        //TODO("not implemented")
    }

    override fun getAllPic(token: String, callback: GitsDataSource.GetAllPic) {
        GitsApiService.getApiService
                .getAllPic(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<PicModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<PicModel>>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun getAllData(token: String, div: String, psm: String, region: String, withPotensi: Boolean, callback: GitsDataSource.GetAllData) {
        GitsApiService.getApiService
                .getAllData(token, div, psm, region)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<AllDataModel>>() {
                    override fun onSuccess(model: BaseApiModel<AllDataModel>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun updateProblemTarget(token: String, id: Int, paramUpdateProblemTarget: ParamUpdateProblemTarget, callback: GitsDataSource.PostProblemCallback) {
        GitsApiService.getApiService
                .updateProblemTarget(token = token, paramUpdateProblemTarget = paramUpdateProblemTarget, id = id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<Any>>() {
                    override fun onSuccess(model: BaseApiModel<Any>) {
                        callback.onSuccess("")
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun login(paramLogin: ParamLogin, callback: GitsDataSource.GetLogin) {
        GitsApiService.getApiService
                .login(paramLogin)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<LoginModel>>() {
                    override fun onSuccess(model: BaseApiModel<LoginModel>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getToken(): String {
        return ""
    }

    override fun saveToken(token: String) {
        //TODO("not implemented")
    }

    override fun getUnit(token: String, callback: GitsDataSource.GetUnit) {
        GitsApiService.getApiService
                .getUnit(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<UnitTypeModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<UnitTypeModel>>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getAllBlock(token: String, div: String, psm: String, region: String, callback: GitsDataSource.GetAllBlock) {
        GitsApiService.getApiService
                .getAllBlock(token, div, psm, region)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<String>>>() {
                    override fun onSuccess(model: BaseApiModel<List<String>>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getProblemTarget(token: String, id: Int, callback: GitsDataSource.GetProblemTargetCallback) {
        GitsApiService.getApiService
                .getProblemTarget(token, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<ProblemTarget>>() {
                    override fun onSuccess(model: BaseApiModel<ProblemTarget>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getProblemCheck(token: String, div: String, timeStat: String, startTime: String, endTime: String, block: String, color: String, psm: String, region: String, callback: GitsDataSource.GetProblemCheckCallback) {
        GitsApiService.getApiService
                .getProblemCheck(token, div, timeStat, startTime, endTime, block, color, psm, region)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<ProblemCheckModel>>() {
                    override fun onSuccess(model: BaseApiModel<ProblemCheckModel>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

    override fun getProblemType(token: String, callback: GitsDataSource.GetProblemTypeCallback) {
        GitsApiService.getApiService
                .getProblemType(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<ItemProblemTypeModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<ItemProblemTypeModel>>) {
                        if (model.data != null) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }

                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun postProblem(token: String, paramCreateProblem: ParamCreateProblem, callback: GitsDataSource.PostProblemCallback) {
        GitsApiService.getApiService
                .postProblem(token = token, paramCreateProblem = paramCreateProblem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<Any>>() {
                    override fun onSuccess(model: BaseApiModel<Any>) {
                        callback.onSuccess("")

                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun updateProblem(token: String, id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem, callback: GitsDataSource.PostProblemCallback) {
        GitsApiService.getApiService
                .updateProblem(token = token, paramUpdateProblem = paramUpdateProblem, id = id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<Any>>() {
                    override fun onSuccess(model: BaseApiModel<Any>) {
                        callback.onSuccess("")
                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun getProblem(token: String, id: Int, callback: GitsDataSource.GetProblemCallback) {
        GitsApiService.getApiService
                .getProblem(token, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { callback.onShowProgressDialog() }
                .doOnTerminate { callback.onHideProgressDialog() }
                .subscribe(object : ApiCallback<BaseApiModel<List<ItemProblemModel>>>() {
                    override fun onSuccess(model: BaseApiModel<List<ItemProblemModel>>) {
                        if (model.data != null) {
                            for(i in 0 until model.data.size){
                                model.data[i].created_at = DateHelper.dateTimeMonth(model.data[i].created_at)
                            }
                            callback.onSuccess(model.data)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }

                    }

                    override fun onFailure(code: Int, errorMessage: String) {
                        callback.onFailed(code, errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

}