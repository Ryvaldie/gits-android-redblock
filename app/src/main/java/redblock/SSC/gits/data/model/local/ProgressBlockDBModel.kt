package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import redblock.SSC.gits.data.model.ProgressBlockModel
import java.io.Serializable

@Entity(tableName = "progressBlock")
class ProgressBlockDBModel (

        @PrimaryKey()
        var id: Int? = null,

        @ColumnInfo(name = "date_updated")
        var dateUpdated: String? = null,

        @ColumnInfo(name = "block_progress")
        var progressBlock: List<ProgressBlockModel>? = null

): Serializable
