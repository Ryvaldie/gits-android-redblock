package redblock.SSC.gits.data.source.local

import android.content.SharedPreferences
import android.support.annotation.VisibleForTesting
import okhttp3.MultipartBody
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.model.local.*
import redblock.SSC.gits.data.param.*
import redblock.SSC.gits.data.source.GitsDataSource
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.dbhelper.AppExecutors
import redblock.co.gits.gitsdriver.utils.GitsHelper

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

class GitsLocalDataSource private constructor(private val sharedPreferences: SharedPreferences,
                                              val appExecutors: AppExecutors,
                                              private val userDBDao: UserDBDao,
                                              private val problemCheckDBDao: ProblemCheckDBDao,
                                              private val problemDetailDBDao: ProblemDetailDBDao,
                                              private val problemListDBDao: ProblemListDBDao,
                                              private val problemTypeDBDao: ProblemTypeDBDao,
                                              private val allBlockDBDao: AllBlockDBDao,
                                              private val draftProblemDBDao: DraftProblemDBDao,
                                              private val unitTypeDBDao: UnitTypeDBDao,
                                              private val draftProblemTargetDBDao: DraftProblemTargetDBDao,
                                              private val allPicDBDao: AllPicDBDao,
                                              private val progressBlockDBDao: ProgressBlockDBDao,
                                              private val progressActionDBDao: ProgressActionDBDao,
                                              private val zPotensiDao: ZPotensiDao) : GitsDataSource {

    override fun getProblemHistory(token: String, id: Int, callback: GitsDataSource.GetProblemCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun unregisterToken(token: String, callback: GitsDataSource.PostProblemCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateAll(token: String, idProblemTarget: Int, paramUpdateAll: ParamUpdateAll, callback: GitsDataSource.PostProblemCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPotensi(token: String, idProblemCheck: Int, startTarget: String, endTarget: String, tahunTanam: Int, region: String, soil: String, seed: String, callback: GitsDataSource.GetPotensiCallback) {
        appExecutors.diskIO.execute {

            appExecutors.mainThread.execute {

                val startYear = startTarget.split("-")[0].toIntOrNull() ?: 0
                val yop1 = (startYear - tahunTanam) + 1
                val startZValue = getZPotensiDao().getDataByProblem(region, soil, yop1, seed) ?: 0
                val endYear = endTarget.split("-")[0].toIntOrNull() ?: 0
                val endDate = endTarget.split("-")[1].toIntOrNull() ?: 0
                val startDate = 12 - endDate
                val yop2 = (endYear - tahunTanam)
                val endZValue = getZPotensiDao().getDataByProblem(region, soil, yop2, seed) ?: 0
                val yopM1 = startDate.div(12.0).times(startZValue)
                val yopM2 = endDate.div(12.0).times(endZValue)
                val potensi = yopM1 + yopM2

                callback.onSuccess(PotensiModel(potensi))
            }

        }
    }

    override fun getNotificationList(token: String, callback: GitsDataSource.GetNotificationListCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getNotifCount(): Int {
        return sharedPreferences.getInt("NOTIF_COUNT", 0)
    }

    override fun saveNotifCount(count: Int) {
        sharedPreferences.edit().putInt("NOTIF_COUNT", count).apply()
    }

    override fun registerToken(token: String, tokenModel: TokenModel, callback: GitsDataSource.PostProblemCallback) {
        //TODO("not implemented")
    }

    override fun setDeviceListedStatus(status: Boolean) {
        sharedPreferences.edit().putBoolean(GitsHelper.Const.IS_DEVICE_LISTED, status).apply()
    }

    override fun isDeviceListed(): Boolean {
        return sharedPreferences.getBoolean(GitsHelper.Const.IS_DEVICE_LISTED, false)
    }

    override fun getListImei(token: String, callback: GitsDataSource.GetListImeiCallback) {
        //TODO("not implemented")
    }

    override fun getStateShowCase(): Boolean {
        return sharedPreferences.getBoolean(GitsHelper.Const.STATE_SHOWCASE, false)
    }

    override fun saveStateShowCase(state: Boolean) {
        sharedPreferences.edit().putBoolean(GitsHelper.Const.STATE_SHOWCASE, state).apply()
    }

    override fun getProgressAction(token: String, div: String, estate: String, block: String?, startTime: String?, endTime: String?, callback: GitsDataSource.GetProgressAction) {
        appExecutors.diskIO.execute {
            val data = getProgressActionDBDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    if (data != null) {
                        if (data.isNotEmpty()) {
                            val temp = data[data.lastIndex].progressAction
                            callback.onSuccess(temp)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    } else {
                        callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                    }
                }
            }
        }
    }

    override fun getProgressBlock(token: String, div: String, estate: String, startTime: String?, endTime: String?, callback: GitsDataSource.GetProgressBlock) {
        appExecutors.diskIO.execute {
            val data = getProgressBlockDBDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    if (data != null) {
                        if (data.isNotEmpty()) {
                            val temp = data[data.lastIndex].progressBlock
                            callback.onSuccess(temp)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    } else {
                        callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                    }
                }
            }
        }
    }

    override fun uploadImages(parts: ArrayList<MultipartBody.Part>, callback: GitsDataSource.GetUploadedImages) {
        appExecutors.mainThread.execute {
            callback.onHideProgressDialog()
            callback.onSuccess(listOf())
        }
    }

    override fun getDraftProblem(callback: GitsDataSource.GetDraftProblemCallback) {
        appExecutors.diskIO.execute {
            val data = getDraftProblemDBDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    val temp = arrayListOf<ParamCreateProblem?>()
                    if (data.isNotEmpty()) {
                        for (d in data) {
                            if (d != null) {
                                temp.add(d.problem)
                            }
                        }
                    }
                    callback.onSuccess(temp)
                }
            }
        }
    }

    override fun getDraftProblemTarget(callback: GitsDataSource.GetDraftProblemTargetCallback) {
        appExecutors.diskIO.execute {
            val data = getDraftProblemTargetDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    val temp = arrayListOf<ParamUpdateProblemTarget?>()
                    if (data.isNotEmpty()) {
                        for (d in data) {
                            if (d != null) {
                                temp.add(d.problem)
                            }
                        }
                    }
                    callback.onSuccess(temp)
                }
            }
        }
    }

    override fun getAllPic(token: String, callback: GitsDataSource.GetAllPic) {
        appExecutors.diskIO.execute {
            val data = getAllPicDBDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    val temp = arrayListOf<PicModel>()
                    if (data.isNotEmpty()) {
                        for (d in data[data.lastIndex].pic!!) {
                            if (d != null) {
                                temp.add(d)
                            }
                        }
                    }
                    callback.onSuccess(temp)
                }
            }
        }
    }

    override fun getAllData(token: String, div: String, psm: String, region: String, withPotensi: Boolean, callback: GitsDataSource.GetAllData) {
        appExecutors.mainThread.execute {
            callback.onShowProgressDialog()
            callback.onFailed(504, "")
        }
    }

    override fun updateProblemTarget(token: String, id: Int, paramUpdateProblemTarget: ParamUpdateProblemTarget, callback: GitsDataSource.PostProblemCallback) {
        appExecutors.diskIO.execute {
            val data = getProblemTargetDBDao().getAllData()
            if (data != null) {
                for (problem in data) {
                    if (problem.problemTarget?.id == id) {
                        getProblemTargetDBDao().updateData(
                                problem.id ?: 0,
                                ProblemTarget(
                                        problem.problemTarget?.id ?: 0,
                                        paramUpdateProblemTarget.problem_check_id ?: 0,
                                        paramUpdateProblemTarget.unit_type_id,
                                        paramUpdateProblemTarget.plant_year ?: "",
                                        paramUpdateProblemTarget.achievement ?: "",
                                        paramUpdateProblemTarget.area ?: "",
                                        DateHelper.convertTargetDate(paramUpdateProblemTarget.target_orange),
                                        paramUpdateProblemTarget.target_ton_orange ?: "",
                                        DateHelper.convertTargetDate(paramUpdateProblemTarget.target_green),
                                        paramUpdateProblemTarget.target_ton_green ?: "",
                                        0,
                                        "draft",
                                        problem.problemTarget?.created_at ?: "",
                                        problem.problemTarget?.updated_at ?: "",
                                        problem.problemTarget?.type_unit,
                                        getProblemListDBDao().getDataById(problem.id
                                                ?: 0)?.problemList
                                )
                        )
                    }
                }
            }

            getDraftProblemTargetDao().insertData(
                    DraftProblemTargetDBModel(
                            id,
                            DateHelper.getTimeStamp(),
                            "draft",
                            paramUpdateProblemTarget
                    )
            )

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                callback.onSuccess("Success!")
            }

        }
    }

    override fun login(paramLogin: ParamLogin, callback: GitsDataSource.GetLogin) {
        //TODO("not implemented")
    }

    override fun getToken(): String {
        return sharedPreferences.getString(GitsHelper.Const.TOKEN, "") ?: ""
    }

    override fun saveToken(token: String) {
        sharedPreferences.edit().putString(GitsHelper.Const.TOKEN, token).apply()
    }

    override fun getUnit(token: String, callback: GitsDataSource.GetUnit) {
        appExecutors.diskIO.execute {
            val data = getUnitTypeDBDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    val temp = arrayListOf<UnitTypeModel>()
                    if (data.isNotEmpty()) {
                        for (d in data[data.lastIndex].unit!!) {
                            if (d != null) {
                                temp.add(d)
                            }
                        }
                    }
                    callback.onSuccess(temp)
                }
            }
        }
    }

    override fun getAllBlock(token: String, div: String, psm: String, region: String, callback: GitsDataSource.GetAllBlock) {
        appExecutors.diskIO.execute {
            val data = getAllBlockDBDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    val temp = arrayListOf<String>()
                    if (data.isNotEmpty()) {
                        for (d in data[data.lastIndex].blocks!!) {
                            if (d != null) {
                                temp.add(d)
                            }
                        }
                    }
                    callback.onSuccess(temp)
                }
            }
        }
    }

    override fun getProblemType(token: String, callback: GitsDataSource.GetProblemTypeCallback) {
        appExecutors.diskIO.execute {
            val data = getProblemTypeDBDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    val temp = arrayListOf<ItemProblemTypeModel>()
                    if (data.isNotEmpty()) {
                        for (d in data[data.lastIndex].problemTypes!!) {
                            if (d != null) {
                                temp.add(d)
                            }
                        }
                    }
                    callback.onSuccess(temp)
                }
            }
        }
    }

    override fun postProblem(token: String, paramCreateProblem: ParamCreateProblem, callback: GitsDataSource.PostProblemCallback) {
        appExecutors.diskIO.execute {

            val targetId = paramCreateProblem.problem_target_id ?: "0"
            val tempProblemActions = ArrayList<ProblemActionParam>()
            for (problemAction in paramCreateProblem.problem_action ?: arrayListOf()) {
                tempProblemActions.add(problemAction)
            }
            paramCreateProblem.problem_action = tempProblemActions
            getDraftProblemDBDao().insertData(
                    DraftProblemDBModel(
                            targetId.toInt(),
                            DateHelper.getTimeStamp(),
                            "draft",
                            paramCreateProblem
                    )
            )

            val data = getProblemListDBDao().getDataById(targetId.toInt())
            val temp = arrayListOf<ItemProblemModel>()
            temp.addAll(data?.problemList ?: arrayListOf())

            val problemTypeData = getProblemTypeDBDao().getAllData()
            val problemType = problemTypeData?.get(problemTypeData.lastIndex)?.problemTypes
            val problemTypes = HashMap<Int, ItemProblemModel.ProblemType>()
            for (type in problemType ?: arrayListOf()) {
                if (type != null) {
                    problemTypes[type.id ?: 0] = ItemProblemModel.ProblemType(
                            type.id,
                            type.name,
                            type.description,
                            type.created_at,
                            type.updated_at
                    )
                }
            }

            /**
             * Fungsi Validasi Hanya bisa update problem ketika progress sudah 100%
             * To use delete all commented line in this function
             * */
            /*var update = true
            for (check in temp) {
                if (check != null) {
                    if (check.problem_detail?.finished != "100") {
                        update = false
                        break
                    } else {
                        update = true
                    }
                }
            }*/
//            if (update) {
            val problemActionArray = ArrayList<ItemProblemModel.ProblemAction>()
            for (problemAction in paramCreateProblem.problem_action ?: listOf()) {
                var unitType: UnitTypeModel? = UnitTypeModel()
                val units = getUnitTypeDBDao().getDataById(0).unit
                if (units != null) {
                    for (unit in units) {
                        if (unit != null) {
                            if (unit.id == problemAction.type_unit_id) {
                                unitType = unit
                            }
                        }
                    }
                }
                problemActionArray.add(
                        ItemProblemModel.ProblemAction(
                                problemAction.PIC,
                                problemAction.action_plan,
                                problemAction.cost,
                                "",
                                problemAction.end_date,
                                problemAction.finished,
                                0,
                                problemAction.problem_detail,
                                0,
                                problemAction.problem_scale,
                                problemAction.progress,
                                problemAction.start_date,
                                problemAction.stat_cost,
                                unitType,
                                problemAction.type_unit_id,
                                "",
                                problemAction.files,
                                problemAction.problem_type_id,
                                problemAction.pokok,
                                problemAction.kalibrasi,
                                problemAction.is_primary,
                                problemAction.is_locked,
                                problemAction.pre_progress
                        )
                )
            }
            temp.add(
                    ItemProblemModel(
                            targetId.toInt(),
                            paramCreateProblem.problem_target_id?.toInt(),
                            paramCreateProblem.problem_target_id?.toInt(),
                            "",
                            "",
                            ItemProblemModel.ProblemDetail(
                                    temp.size,
                                    temp.size,
                                    paramCreateProblem.total_area ?: "",
                                    paramCreateProblem.problem_area ?: "",
                                    "0",
                                    UnitTypeModel(
                                            unit = "Ha"
                                    ),
                                    "0",
                                    "draft",
                                    DateHelper.getTimeStamp(),
                                    DateHelper.getTimeStamp(),
                                    paramCreateProblem.progress_overall ?: "",
                                    problemActionArray
                            ),
                            status = false
                    )
            )

            getProblemListDBDao().insertData(
                    ProblemListDBModel(
                            targetId.toInt(),
                            DateHelper.getTimeStamp(),
                            temp
                    )
            )
//            }

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
//                if (update) {
                callback.onSuccess("Success!")
                /*} else {
                    callback.onFailed(500, "Masalah dengan klasifikasi yang sama belum selesai!")
                }*/
            }

        }
    }

    override fun updateProblem(token: String, id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem, callback: GitsDataSource.PostProblemCallback) {
        appExecutors.diskIO.execute {
            val data = getProblemListDBDao().getDataById(targetId)
            val temp = arrayListOf<ItemProblemModel>()
            if (data?.problemList != null) {
                val problemTypeData = getProblemTypeDBDao().getAllData()
                val problemType = problemTypeData?.get(problemTypeData.lastIndex)?.problemTypes
                val problemTypes = HashMap<Int, ItemProblemModel.ProblemType>()
                for (type in problemType ?: arrayListOf()) {
                    if (type != null) {
                        problemTypes[type.id ?: 0] = ItemProblemModel.ProblemType(
                                type.id,
                                type.name,
                                type.description,
                                type.created_at,
                                type.updated_at
                        )
                    }
                }

                temp.addAll(data.problemList!!)
                val problemActionArray = ArrayList<ItemProblemModel.ProblemAction>()
                for (problemAction in paramUpdateProblem.problem_action ?: listOf()) {
                    var unitType: UnitTypeModel? = UnitTypeModel()
                    val units = getUnitTypeDBDao().getDataById(0).unit
                    if (units != null) {
                        for (unit in units) {
                            if (unit != null) {
                                if (unit.id == problemAction.type_unit_id) {
                                    unitType = unit
                                }
                            }
                        }
                    }
                    problemActionArray.add(
                            ItemProblemModel.ProblemAction(
                                    problemAction.PIC,
                                    problemAction.action_plan,
                                    problemAction.cost,
                                    "",
                                    problemAction.end_date,
                                    problemAction.finished,
                                    0,
                                    problemAction.problem_detail,
                                    0,
                                    problemAction.problem_scale,
                                    problemAction.progress,
                                    problemAction.start_date,
                                    problemAction.stat_cost,
                                    unitType,
                                    problemAction.type_unit_id,
                                    "",
                                    problemAction.files,
                                    problemAction.problem_type_id,
                                    problemAction.pokok,
                                    problemAction.kalibrasi,
                                    problemAction.is_primary,
                                    problemAction.is_locked,
                                    problemAction.pre_progress
                            )
                    )
                }

                for ((index, d) in data.problemList?.withIndex() ?: listOf()) {
                    if (d != null) {
                        if (d.id == id) {
                            temp[index].status = false
                            temp[index].problem_detail = ItemProblemModel.ProblemDetail(
                                    d.id,
                                    d.problem_detail_id,
                                    paramUpdateProblem.total_area ?: "",
                                    paramUpdateProblem.problem_area ?: "",
                                    "0",
                                    d.problem_detail?.unit_type,
                                    d.problem_detail?.finished,
                                    "draft",
                                    d.problem_detail?.created_at,
                                    d.problem_detail?.updated_at,
                                    paramUpdateProblem.progress_overall ?: "",
                                    problemActionArray
                            )
                        }
                    }
                }
            }

            val update = data?.problemList?.count {
                if (it != null) {
                    it.id == id
                } else {
                    false
                }
            } ?: 0 > 0
            if (update) {
                getProblemListDBDao().updateData(
                        targetId,
                        temp
                )

                val tempProblemActions = ArrayList<ProblemActionParam>()
                for (problemAction in paramUpdateProblem.problem_action ?: arrayListOf()) {
                    tempProblemActions.add(problemAction)
                }
                paramUpdateProblem.problem_action = tempProblemActions
                getDraftProblemDBDao().insertData(
                        DraftProblemDBModel(
                                targetId,
                                DateHelper.getTimeStamp(),
                                "draft",
                                ParamCreateProblem(
                                        "$targetId",
                                        paramUpdateProblem.total_area,
                                        paramUpdateProblem.problem_area,
                                        paramUpdateProblem.progress_overall,
                                        paramUpdateProblem.problem_action
                                )
                        )
                )
            }

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (update) {
                    callback.onSuccess("Success!")
                } else {
                    callback.onFailed(504, "Data Offline belum update! Silahkan Download Terlebih dahulu")
                }
            }
        }
    }

    override fun getProblemTarget(token: String, id: Int, callback: GitsDataSource.GetProblemTargetCallback) {
        appExecutors.diskIO.execute {
            val data = problemDetailDBDao.getDataById(id)

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    if (data.problemTarget != null) {
                        callback.onSuccess(data.problemTarget!!)
                    } else {
                        callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                    }
                }
            }
        }
    }

    override fun getProblemCheck(token: String, div: String, timeStat: String, startTime: String, endTime: String, block: String, color: String, psm: String, region: String, callback: GitsDataSource.GetProblemCheckCallback) {
        appExecutors.diskIO.execute {
            val data = getProblemCheckDBDao().getAllData()

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    if (data.isNotEmpty()) {
                        if (data[data.lastIndex].problemCheck != null) {
                            val filtered = data[data.lastIndex].problemCheck!!
                            val content = arrayListOf<Content>()
                            content.addAll(filtered.content)
                            if (div.isNotEmpty()) {
                            }
                            if (timeStat.isNotEmpty()) {
                                val contentTemp = arrayListOf<Content>()
                                contentTemp.addAll(content)
                                for (temp in contentTemp) {
                                    if (temp != null) {
                                        when (timeStat) {
                                            "custom" -> {
                                                val createdAt = DateHelper.dateTimeSet(temp.created_at)
                                                if (startTime.isNotEmpty() && endTime.isNotEmpty()) {
                                                    if (createdAt.contains("/")) {
                                                        val dateInit = createdAt.split("/")[0].toInt()
                                                        val yearInit = createdAt.split("/")[1].toInt()
                                                        val dateStart = startTime.split("/")[0].toInt()
                                                        val yearStart = startTime.split("/")[1].toInt()
                                                        val dateEnd = endTime.split("/")[0].toInt()
                                                        val yearEnd = endTime.split("/")[1].toInt()
                                                        if ((yearInit < yearStart && dateInit < dateStart) ||
                                                                (yearInit > yearEnd && dateInit < dateEnd)) {
                                                            content.remove(temp)
                                                        }
                                                    }
                                                }
                                            }
                                            "monthly" -> {

                                            }
                                            else -> {

                                            }
                                        }
                                    }
                                }
                            }
                            if (block.isNotEmpty()) {
                                val contentTemp = arrayListOf<Content>()
                                contentTemp.addAll(content)
                                for (temp in contentTemp) {
                                    if (temp != null && !block.contains(temp.block, true)) {
                                        content.remove(temp)
                                    }
                                }
                            }
                            if (color.isNotEmpty()) {
                                val black = arrayListOf<Content>()
                                val red = arrayListOf<Content>()
                                val orange = arrayListOf<Content>()
                                val green = arrayListOf<Content>()
                                val contentTemp = arrayListOf<Content>()
                                contentTemp.addAll(content)
                                for (temp in contentTemp) {
                                    if (temp != null) {
                                        if (temp.color.name?.contains("black", true) == true) {
                                            content.remove(temp)
                                            black.add(temp)
                                        }
                                    }
                                }
                                for (temp in contentTemp) {
                                    if (temp != null) {
                                        if (temp.color.name?.contains("red", true) == true) {
                                            content.remove(temp)
                                            red.add(temp)
                                        }
                                    }
                                }
                                for (temp in contentTemp) {
                                    if (temp != null) {
                                        if (temp.color.name?.contains("orange", true) == true) {
                                            content.remove(temp)
                                            orange.add(temp)
                                        }
                                    }
                                }
                                for (temp in contentTemp) {
                                    if (temp != null) {
                                        if (temp.color.name?.contains("green", true) == true) {
                                            content.remove(temp)
                                            green.add(temp)
                                        }
                                    }
                                }
                                content.apply {
                                    clear()
                                    addAll(black)
                                    when {
                                        color.contains("red", true) -> {
                                            addAll(red)
                                        }
                                        color.contains("orange", true) -> {
                                            addAll(orange)
                                        }
                                        color.contains("green", true) -> {
                                            addAll(green)
                                        }
                                    }
                                }
                            }
                            filtered.content = content
                            callback.onSuccess(filtered)
                        } else {
                            callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                    GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                        }
                    } else {
                        callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                    }
                }
            }
        }
    }

    override fun getProblem(token: String, id: Int, callback: GitsDataSource.GetProblemCallback) {
        appExecutors.diskIO.execute {
            val data = getProblemListDBDao().getDataById(id)

            appExecutors.mainThread.execute {
                callback.onHideProgressDialog()
                if (data == null) {
                    callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                            GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                } else {
                    if (data.problemList != null) {
                        val temp = arrayListOf<ItemProblemModel>()
                        for (d in data.problemList!!) {
                            if (d != null) {
                                temp.add(d.apply {
                                    created_at = DateHelper.dateTimeMonth(created_at)
                                    updated_at = DateHelper.dateTimeMonth(updated_at)
                                })
                            }
                        }
                        callback.onSuccess(temp)
                    } else {
                        callback.onFailed(GitsHelper.Const.SERVER_CODE_404,
                                GitsHelper.Const.SERVER_ERROR_MESSAGE_DEFAULT)
                    }
                }
            }
        }
    }

    fun getUserDBDao(): UserDBDao {
        return userDBDao
    }

    fun getProblemCheckDBDao(): ProblemCheckDBDao {
        return problemCheckDBDao
    }

    fun getProblemTargetDBDao(): ProblemDetailDBDao {
        return problemDetailDBDao
    }

    fun getProblemListDBDao(): ProblemListDBDao {
        return problemListDBDao
    }

    fun getProblemTypeDBDao(): ProblemTypeDBDao {
        return problemTypeDBDao
    }

    fun getAllBlockDBDao(): AllBlockDBDao {
        return allBlockDBDao
    }

    fun getDraftProblemDBDao(): DraftProblemDBDao {
        return draftProblemDBDao
    }

    fun getUnitTypeDBDao(): UnitTypeDBDao {
        return unitTypeDBDao
    }

    fun getDraftProblemTargetDao(): DraftProblemTargetDBDao {
        return draftProblemTargetDBDao
    }

    fun getAllPicDBDao(): AllPicDBDao {
        return allPicDBDao
    }

    fun getProgressBlockDBDao(): ProgressBlockDBDao {
        return progressBlockDBDao
    }

    fun getProgressActionDBDao(): ProgressActionDBDao {
        return progressActionDBDao
    }

    fun getZPotensiDao(): ZPotensiDao {
        return zPotensiDao
    }

    companion object {

        private var INSTANCE: GitsLocalDataSource? = null

        @JvmStatic
        fun getInstance(sharedPreferences: SharedPreferences,
                        appExecutors: AppExecutors, userDBDao: UserDBDao,
                        problemCheckDBDao: ProblemCheckDBDao,
                        problemDetailDBDao: ProblemDetailDBDao,
                        problemListDBDao: ProblemListDBDao,
                        problemTypeDBDao: ProblemTypeDBDao,
                        allBlockDBDao: AllBlockDBDao,
                        draftProblemDBDao: DraftProblemDBDao,
                        unitTypeDBDao: UnitTypeDBDao,
                        draftProblemTargetDBDao: DraftProblemTargetDBDao,
                        allPicDBDao: AllPicDBDao,
                        progressBlockDBDao: ProgressBlockDBDao,
                        progressActionDBDao: ProgressActionDBDao,
                        zPotensiDao: ZPotensiDao): GitsLocalDataSource {
            if (INSTANCE == null) {
                synchronized(GitsLocalDataSource::javaClass) {
                    INSTANCE = GitsLocalDataSource(sharedPreferences, appExecutors, userDBDao,
                            problemCheckDBDao, problemDetailDBDao, problemListDBDao, problemTypeDBDao,
                            allBlockDBDao, draftProblemDBDao, unitTypeDBDao, draftProblemTargetDBDao,
                            allPicDBDao, progressBlockDBDao, progressActionDBDao, zPotensiDao)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}