package redblock.SSC.gits.data.model

data class Item(
        val chartTitle: String? = "",
        val colorChart: ArrayList<Int> = arrayListOf(),
        val date: ArrayList<String> = arrayListOf()
)