package redblock.SSC.gits.data.model

data class DropDownModel(
        var id:Int,
        var title:String,
        var description: String = ""
)