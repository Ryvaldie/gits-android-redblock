package redblock.SSC.gits.data.model

data class ProgressActionModel(
        val action: List<Action?>? = listOf(),
        val block: String? = "",
        val classification: String? = "",
        var progress: String? = ""
)

data class Action(
        val finished: Float? = 0f,
        val name: String? = "",
        val classification: String? = ""
)
