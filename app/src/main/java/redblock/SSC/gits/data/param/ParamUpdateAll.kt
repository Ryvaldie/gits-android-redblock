package redblock.SSC.gits.data.param


import com.google.gson.annotations.SerializedName

data class ParamUpdateAll(
        //local
        var isChecked: Boolean? = false,
        @SerializedName("problems")
        val problems: List<Problem?>? = listOf(),
        @SerializedName("target_green")
        val targetGreen: String? = "",
        @SerializedName("target_orange")
        val targetOrange: String? = "",
        @SerializedName("target_ton_green")
        val targetTonGreen: String? = "",
        @SerializedName("target_ton_orange")
        val targetTonOrange: String? = ""
)

data class Problem(
        @SerializedName("id")
        val id: Int? = 0,
        @SerializedName("problem_action")
        val problemAction: List<ProblemActionParam?>? = listOf(),
        @SerializedName("problem_area")
        val problemArea: String? = "",
        @SerializedName("progress_overall")
        val progressOverall: Int? = 0,
        @SerializedName("total_area")
        val totalArea: String? = ""
)
