package redblock.SSC.gits.data.model.local

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import redblock.SSC.gits.data.model.UserConfigModel
import java.io.Serializable

@Entity(tableName = "user")
class UserDBModel(

        @PrimaryKey()
        var id: Int? = null,

        @ColumnInfo(name = "date_updated")
        var dateUpdated: String? = null,

        @ColumnInfo(name = "user_config")
        var userConfig: UserConfigModel? = null

) : Serializable
