package redblock.SSC.gits.data.model

data class UserConfigModel(
        var nik: String = "",
        var password: String = "",
        var division: String = "div1",
        var reminder: String = "1",
        var estate: String? = "",
        var region: String? = "",
        var psm: String? = ""
)
