package redblock.SSC.gits.data.param

data class ParamUpdateProblemTarget(
        val problem_check_id: Int? = 0,
        val plant_year: String? = "",
        val achievement: String? = "",
        val area: String? = "",
        val target_orange: String? = "",
        val target_ton_orange: String? = "",
        val target_green: String? = "",
        val target_ton_green: String? = "",
        val unit_type_id: Int = 0,
        //local variable
        var isChecked: Boolean = false
)