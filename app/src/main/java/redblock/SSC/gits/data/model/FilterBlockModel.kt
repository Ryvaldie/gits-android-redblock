package redblock.SSC.gits.data.model

data class FilterBlockModel(
        val block: String? = "",
        val isChecked: Boolean
)