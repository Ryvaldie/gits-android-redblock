package redblock.SSC.gits.data.source.remote

import android.annotation.SuppressLint
import com.readystatesoftware.chuck.ChuckInterceptor
import io.reactivex.Observable
import okhttp3.Cache
import okhttp3.ConnectionSpec
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import redblock.SSC.gits.BuildConfig
import redblock.SSC.gits.GitsApplication
import redblock.SSC.gits.base.BaseApiModel
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.param.*
import redblock.SSC.gits.util.SSLConnection
import redblock.co.gits.gitsdriver.utils.GitsHelper
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

interface GitsApiService {

    @GET("api/v1/data/zpotensi")
    fun getZPotensi(
            @Header("accessToken") token: String
    ): Observable<BaseApiModel<List<ZPotensiModel>>>

    @GET("api/v1/problem/target/history_problem/{idProblemTarget}")
    fun getProblemHistory(
            @Header("accessToken") token: String,
            @Path("idProblemTarget") idProblemTarget: Int
    ): Observable<BaseApiModel<List<ItemProblemModel>>>

    @DELETE("api/v1/notifications/unregister")
    fun unregisterToken(
            @Header("accessToken") token: String
    ): Observable<BaseApiModel<Any?>>

    @POST("api/v1/update_all/{idProblemTarget}")
    fun updateAll(
            @Header("accessToken") token: String,
            @Path("idProblemTarget") idProblemTarget: Int,
            @Body paramUpdateAll: ParamUpdateAll
    ): Observable<BaseApiModel<Any?>>

    @POST("api/v1/problem/potensi")
    @FormUrlEncoded
    fun getPotensi(
            @Header("accessToken") token: String,
            @Field("id_problem_check") idProblemCheck: Int,
            @Field("start_target") startTarget: String,
            @Field("end_target") endTarget: String
    ): Observable<BaseApiModel<PotensiModel>>

    @GET("api/v1/notifications?limit=5000&page=1")
    fun getNotificationList(
            @Header("accessToken") token: String
    ): Observable<BaseApiModel<NotificationListModel>>

    @POST("api/v1/notifications/register")
    fun registerToken(
            @Header("accessToken") token: String,
            @Body tokenModel: TokenModel
    ): Observable<BaseApiModel<Any>>

    @GET("api/v1/data/imei")
    fun getListImei(
            @Header("accessToken") token: String
    ): Observable<BaseApiModel<List<ImeiModel>>>

    @GET("api/v1/dashboard/action")
    fun getProgressAction(
            @Header("accessToken") token: String,
            @Query("divisi") div: String,
            @Query("estate") estate: String,
            @Query("block") block: String?,
            @Query("start_time") startTime: String?, //YYYY-MM
            @Query("end_time") endTime: String? //YYYY-MM
    ): Observable<BaseApiModel<List<ProgressActionModel>>>

    @GET("api/v1/dashboard/progress")
    fun getProgressBlock(
            @Header("accessToken") token: String,
            @Query("divisi") div: String,
            @Query("estate") estate: String,
            @Query("start_time") startTime: String?, //YYYY-MM
            @Query("end_time") endTime: String? //YYYY-MM
    ): Observable<BaseApiModel<List<ProgressBlockModel>>>

    @Multipart
    @POST("upload")
    fun uploadImages(
            @Header("Content-Type") contentType: String? = "application/x-www-form-urlencoded",
            @Part upload: List<MultipartBody.Part>
    ): Observable<BaseApiModel<List<String>>>

    @GET("api/v1/data/pic")
    fun getAllPic(
            @Header("accessToken") token: String
    ): Observable<BaseApiModel<List<PicModel>>>

    @GET("api/v1/data/sync")
    fun getAllData(
            @Header("accessToken") token: String,
            @Query("division") div: String,
            @Query("psm") psm: String = "Kalimantan Utara",
            @Query("region") region: String = "Gumas"
    ): Observable<BaseApiModel<AllDataModel>>

    @PUT("api/v1/problem/target/{id}")
    fun updateProblemTarget(
            @Header("Content-Type") contentType: String? = "application/json",
            @Header("accessToken") token: String,
            @Body paramUpdateProblemTarget: ParamUpdateProblemTarget,
            @Path("id") id: Int
    ): Observable<BaseApiModel<Any>>

    @PUT("api/v1/problem/{id}?novalidation=true")
    fun updateProblem(
            @Header("Content-Type") contentType: String? = "application/json",
            @Header("accessToken") token: String,
            @Body paramUpdateProblem: ParamUpdateProblem,
            @Path("id") id: Int
    ): Observable<BaseApiModel<Any>>

    @GET("api/v1/problem/type")
    fun getProblemType(
            @Header("accessToken") token: String
    ): Observable<BaseApiModel<List<ItemProblemTypeModel>>>

    @POST("api/v1/problem")
    fun postProblem(
            @Header("Content-Type") contentType: String? = "application/json",
            @Header("accessToken") token: String,
            @Body paramCreateProblem: ParamCreateProblem
    ): Observable<BaseApiModel<Any>>

    @GET("api/v1/problem/{id}")
    fun getProblem(
            @Header("accessToken") token: String,
            @Path("id") id: Int
    ): Observable<BaseApiModel<List<ItemProblemModel>>>

    @GET("api/v1/apps/problem/check")
    fun getProblemCheck(
            @Header("accessToken") token: String,
            @Query("division") div: String,
            @Query("timestat") timeStat: String,
            @Query("startTime") startTime: String,
            @Query("endTime") endTime: String,
            @Query("block") block: String,
            @Query("color") color: String,
            @Query("psm") psm: String = "Kalimantan Utara",
            @Query("region") region: String = "Gumas"
    ): Observable<BaseApiModel<ProblemCheckModel>>

    @GET("api/v1/problem/target/{id}")
    fun getProblemTarget(
            @Header("accessToken") token: String,
            @Path("id") id: Int
    ): Observable<BaseApiModel<ProblemTarget>>

    @GET("api/v1/data/block")
    fun getAllBlock(
            @Header("accessToken") token: String,
            @Query("division") div: String,
            @Query("psm") psm: String = "Kalimantan Utara",
            @Query("region") region: String = "Gumas"
    ): Observable<BaseApiModel<List<String>>>

    @GET("api/v1/data/type_unit")
    fun getUnit(
            @Header("accessToken") token: String
    ): Observable<BaseApiModel<List<UnitTypeModel>>>

    @POST("api/v1/apps/user/login")
    fun login(@Body paramLogin: ParamLogin): Observable<BaseApiModel<LoginModel>>

    companion object Factory {

        // Create an ssl socket factory with our all-trusting manager
        private val sslSocketFactory = object : X509TrustManager {

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {

            }

            @SuppressLint("TrustAllX509TrustManager")
            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {

            }

        }

        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {

            }

            @SuppressLint("TrustAllX509TrustManager")
            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {

            }

        })

        val getApiService: GitsApiService by lazy {
            val mLoggingInterceptor = HttpLoggingInterceptor()
            mLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            // Install the all-trusting trust manager
            val sslContext: SSLContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            val cacheSize = (5 * 1024 * 1024).toLong()
            val appCache = Cache(GitsApplication.getContext().cacheDir, cacheSize)
            val mClient = if (BuildConfig.DEBUG) {
                OkHttpClient.Builder()
                        .sslSocketFactory(sslContext.socketFactory, sslSocketFactory)
                        .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                        .cache(appCache)
                        .addInterceptor(ChuckInterceptor(GitsApplication.getContext()))
                        .addInterceptor { chain ->
                            // Get the request from the chain.
                            var request = chain.request()

                            /*
                        *  Leveraging the advantage of using Kotlin,
                        *  we initialize the request and change its header depending on whether
                        *  the device is connected to Internet or not.
                        */
                            request = if (GitsHelper.Func.isNetworkAvailable(GitsApplication.getContext())!!)
                            /*
                        *  If there is Internet, get the cache that was stored 5 seconds ago.
                        *  If the cache is older than 5 seconds, then discard it,
                        *  and indicate an error in fetching the response.
                        *  The 'max-age' attribute is responsible for this behavior.
                        */
                                request.newBuilder().header("Cache-Control",
                                        "public, max-age=" + 5).build()
                            else
                            /*
                        *  If there is no Internet, get the cache that was stored 7 days ago.
                        *  If the cache is older than 7 days, then discard it,
                        *  and indicate an error in fetching the response.
                        *  The 'max-stale' attribute is responsible for this behavior.
                        *  The 'only-if-cached' attribute indicates to not retrieve new data; fetch the cache only instead.
                        */
                                request.newBuilder().header("Cache-Control",
                                        "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                            // End of if-else statement

                            // Add the modified request to the chain.
                            chain.proceed(request)
                        }
                        .addInterceptor(mLoggingInterceptor)
//                        .addInterceptor(ChuckInterceptor(GitsApplication.getContext()))
                        .connectionSpecs(listOf(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
                        .readTimeout(30, TimeUnit.SECONDS)
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .build()
            } else {
                OkHttpClient.Builder()
                        .sslSocketFactory(sslContext.socketFactory, sslSocketFactory)
                        .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                        .cache(appCache)
                        .addInterceptor { chain ->
                            var request = chain.request()
                            request = if (GitsHelper.Func.isNetworkAvailable(GitsApplication.getContext())!!)
                                request.newBuilder().header("Cache-Control",
                                        "public, max-age=" + 5).build()
                            else
                                request.newBuilder().header("Cache-Control",
                                        "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                            chain.proceed(request)
                        }
                        .connectionSpecs(listOf(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
                        .readTimeout(30, TimeUnit.SECONDS)
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .build()
            }

            val mRetrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(mClient)
                    .build()

            mRetrofit.create(GitsApiService::class.java)
        }

        val getImageService: GitsApiService by lazy {
            val mLoggingInterceptor = HttpLoggingInterceptor()
            mLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            // Install the all-trusting trust manager
            val sslContext: SSLContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            val cacheSize = (5 * 1024 * 1024).toLong()
            val appCache = Cache(GitsApplication.getContext().cacheDir, cacheSize)
            val mClient = if (BuildConfig.DEBUG) {
                OkHttpClient.Builder()
                        .sslSocketFactory(sslContext.socketFactory, sslSocketFactory)
                        .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                        .cache(appCache)
                        .addInterceptor(ChuckInterceptor(GitsApplication.getContext()))
                        .addInterceptor { chain ->
                            // Get the request from the chain.
                            var request = chain.request()

                            /*
                        *  Leveraging the advantage of using Kotlin,
                        *  we initialize the request and change its header depending on whether
                        *  the device is connected to Internet or not.
                        */
                            request = if (GitsHelper.Func.isNetworkAvailable(GitsApplication.getContext())!!)
                            /*
                        *  If there is Internet, get the cache that was stored 5 seconds ago.
                        *  If the cache is older than 5 seconds, then discard it,
                        *  and indicate an error in fetching the response.
                        *  The 'max-age' attribute is responsible for this behavior.
                        */
                                request.newBuilder().header("Cache-Control",
                                        "public, max-age=" + 5).build()
                            else
                            /*
                        *  If there is no Internet, get the cache that was stored 7 days ago.
                        *  If the cache is older than 7 days, then discard it,
                        *  and indicate an error in fetching the response.
                        *  The 'max-stale' attribute is responsible for this behavior.
                        *  The 'only-if-cached' attribute indicates to not retrieve new data; fetch the cache only instead.
                        */
                                request.newBuilder().header("Cache-Control",
                                        "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                            // End of if-else statement

                            // Add the modified request to the chain.
                            chain.proceed(request)
                        }
                        .addInterceptor(mLoggingInterceptor)
                        .addInterceptor(ChuckInterceptor(GitsApplication.getContext()))
                        .connectionSpecs(listOf(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
                        .readTimeout(30, TimeUnit.SECONDS)
                        .sslSocketFactory(SSLConnection.createSslSocketFactory())
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .build()
            } else {
                OkHttpClient.Builder()
                        .sslSocketFactory(sslContext.socketFactory, sslSocketFactory)
                        .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                        .cache(appCache)
                        .addInterceptor { chain ->
                            var request = chain.request()
                            request = if (GitsHelper.Func.isNetworkAvailable(GitsApplication.getContext())!!)
                                request.newBuilder().header("Cache-Control",
                                        "public, max-age=" + 5).build()
                            else
                                request.newBuilder().header("Cache-Control",
                                        "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                            chain.proceed(request)
                        }
                        .connectionSpecs(listOf(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
                        .readTimeout(30, TimeUnit.SECONDS)
                        .sslSocketFactory(SSLConnection.createSslSocketFactory())
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .build()
            }

            val mRetrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.UPLOAD_SERVICE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(mClient)
                    .build()

            mRetrofit.create(GitsApiService::class.java)
        }

    }

}