package redblock.SSC.gits.data.model

import com.google.gson.annotations.SerializedName

data class LoginModel(
    val token: String = "",
    val user: User? = User()
)

data class User(
        val created_at: String? = "",
        val division: String? = "",
        val estate: String? = "",
        val id: Int? = 0,
        val id_atasan: Int? = 0,
        val nama: String? = "",
        val nik: String? = "",
        val region: String? = "",
        val role: Role? = Role(),
        val roles_id: Int? = 0,
        val status: Int? = 0,
        val updated_at: String? = "",
        val psm: String? = ""
)

data class Role(
        @SerializedName("created_at")
        val createdAt: String? = "",
        @SerializedName("description")
        val description: String? = "",
        @SerializedName("group_role_id")
        val groupRoleId: Int? = 0,
        @SerializedName("id")
        val id: Int? = 0,
        @SerializedName("name")
        val name: String? = "",
        @SerializedName("updated_at")
        val updatedAt: String? = ""
)
