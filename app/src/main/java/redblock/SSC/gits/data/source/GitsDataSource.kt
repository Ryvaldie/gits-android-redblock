package redblock.SSC.gits.data.source

import okhttp3.MultipartBody
import redblock.SSC.gits.base.BaseDataSource
import redblock.SSC.gits.base.BaseDataSource.GitsResponseCallback
import redblock.SSC.gits.data.model.*
import redblock.SSC.gits.data.param.*

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

interface GitsDataSource : BaseDataSource {

    fun getZPotensi(token: String, callback: GetZPotensiCallback) {}

    fun getProblem(token: String, id: Int, callback: GetProblemCallback)

    fun getProblemType(token: String, callback: GetProblemTypeCallback)

    fun postProblem(token: String, paramCreateProblem: ParamCreateProblem, callback: PostProblemCallback)

    fun updateProblem(token: String, id: Int, targetId: Int, paramUpdateProblem: ParamUpdateProblem, callback: PostProblemCallback)

    fun getProblemCheck(token: String,
                        div: String,
                        timeStat: String,
                        startTime: String,
                        endTime: String,
                        block: String,
                        color: String,
                        psm: String,
                        region: String,
                        callback: GetProblemCheckCallback)

    fun getProblemTarget(token: String, id: Int, callback: GetProblemTargetCallback)

    fun getAllBlock(token: String, div: String,
                    psm: String,
                    region: String, callback: GetAllBlock)

    fun getUnit(token: String, callback: GetUnit)

    fun login(paramLogin: ParamLogin, callback: GetLogin)

    fun getToken(): String

    fun saveToken(token: String)

    fun getStateShowCase(): Boolean

    fun saveStateShowCase(state: Boolean)

    fun updateProblemTarget(token: String, id: Int, paramUpdateProblemTarget: ParamUpdateProblemTarget, callback: PostProblemCallback)

    fun getAllData(token: String, div: String,
                   psm: String,
                   region: String, withPotensi: Boolean, callback: GetAllData)

    fun getAllPic(token: String, callback: GetAllPic)

    fun getDraftProblem(callback: GetDraftProblemCallback)

    fun getDraftProblemTarget(callback: GetDraftProblemTargetCallback)

    fun uploadImages(parts: ArrayList<MultipartBody.Part>, callback: GetUploadedImages)

    fun getProgressBlock(token: String,
                         div: String,
                         estate: String,
                         startTime: String?,
                         endTime: String?,
                         callback: GetProgressBlock)

    fun getProgressAction(token: String,
                          div: String,
                          estate: String,
                          block: String?,
                          startTime: String?,
                          endTime: String?,
                          callback: GetProgressAction)

    fun getListImei(token: String, callback: GetListImeiCallback)

    fun isDeviceListed(): Boolean

    fun setDeviceListedStatus(status: Boolean)

    fun registerToken(token: String, tokenModel: TokenModel, callback: PostProblemCallback)

    fun getNotifCount(): Int

    fun saveNotifCount(count: Int)

    fun getNotificationList(token: String, callback: GetNotificationListCallback)

    fun getPotensi(token: String, idProblemCheck: Int, startTarget: String, endTarget: String,
                   tahunTanam: Int, region: String, soil: String, seed: String, callback: GetPotensiCallback)

    fun updateAll(token: String, idProblemTarget: Int, paramUpdateAll: ParamUpdateAll, callback: PostProblemCallback)

    fun unregisterToken(token: String, callback: PostProblemCallback)

    fun getProblemHistory(token: String, id: Int, callback: GetProblemCallback)

    interface GetZPotensiCallback : GitsResponseCallback<List<ZPotensiModel>>

    interface GetPotensiCallback : GitsResponseCallback<PotensiModel>

    interface GetNotificationListCallback : GitsResponseCallback<NotificationListModel>

    interface GetListImeiCallback : GitsResponseCallback<List<ImeiModel>?>

    interface GetProgressAction : GitsResponseCallback<List<ProgressActionModel>?>

    interface GetProgressBlock : GitsResponseCallback<List<ProgressBlockModel?>?>

    interface GetUploadedImages : GitsResponseCallback<List<String>>

    interface GetAllPic : GitsResponseCallback<List<PicModel>>

    interface GetAllData : GitsResponseCallback<AllDataModel>

    interface GetLogin : GitsResponseCallback<LoginModel>

    interface GetUnit : GitsResponseCallback<List<UnitTypeModel>>

    interface GetAllBlock : GitsResponseCallback<List<String>>

    interface PostProblemCallback : GitsResponseCallback<Any?>

    interface GetProblemTypeCallback : GitsResponseCallback<List<ItemProblemTypeModel>?>

    interface GetProblemCallback : GitsResponseCallback<List<ItemProblemModel>>

    interface GetProblemCheckCallback : GitsResponseCallback<ProblemCheckModel>

    interface GetProblemTargetCallback : GitsResponseCallback<ProblemTarget>

    interface GetDraftProblemCallback : GitsResponseCallback<List<ParamCreateProblem?>?>

    interface GetDraftProblemTargetCallback : GitsResponseCallback<List<ParamUpdateProblemTarget?>?>

}