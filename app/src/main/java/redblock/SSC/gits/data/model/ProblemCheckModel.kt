package redblock.SSC.gits.data.model

import com.google.gson.annotations.SerializedName

data class ProblemCheckModel(
        var content: List<Content>,
        val pagginate: Pagginate = Pagginate()
)

data class Pagginate(
        @SerializedName("current_page")
        val currentPage: Int? = 0,
        @SerializedName("last_page")
        val lastPage: Int? = 0,
        @SerializedName("per_page")
        val perPage: Int? = 0,
        @SerializedName("total")
        val total: Int? = 0
)

data class Content(
        //local
        var isChecked: Boolean = false,

        val id: Int = 0,
        val estate: String = "",
        val region: String = "",
        val resident: String = "",
        val divisi: String = "",
        val block: String = "",
        val status_color_id: Int = 0,
        val created_by: Int = 0,
        val problem_target_id: Int = 0,
        val created_at: String = "",
        val updated_at: String = "",
        val color: Color = Color(),
        val problem_target: ProblemTarget? = ProblemTarget(),
        @SerializedName("period")
        val period: String? = "",
        @SerializedName("seed")
        val seed: String? = "",
        @SerializedName("soil")
        val soil: String? = "",
        @SerializedName("psm")
        val psm: String? = "",
        @SerializedName("potency")
        val potency: String? = "",
        @SerializedName("production")
        val production: String? = "",
        @SerializedName("BM")
        val BM: String? = ""
)

data class Color(
        val id: Int? = 0,
        val name: String? = "",
        val created_at: String? = "",
        val updated_at: String? = ""
)

data class ProblemTarget(
        val id: Int = 0,
        val problem_check_id: Int = 0,
        val unit_type_id: Int = 0,
        val plant_year: String = "",
        val achievement: String = "",
        val area: String = "",
        val target_orange: String = "",
        val target_ton_orange: String? = "",
        val target_green: String = "",
        val target_ton_green: String? = "",
        val version: Int = 0,
        val status: String = "",
        val created_at: String = "",
        val updated_at: String = "",
        val type_unit: UnitTypeModel? = UnitTypeModel(),
        val problem: List<ItemProblemModel>? = listOf()
)