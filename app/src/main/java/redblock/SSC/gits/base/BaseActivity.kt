package redblock.SSC.gits.base

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.content.Context.JOB_SCHEDULER_SERVICE
import android.app.job.JobScheduler
import android.app.job.JobInfo
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.widget.Button
import android.widget.Toast
import permissions.dispatcher.PermissionRequest
import redblock.SSC.gits.R
import redblock.SSC.gits.mvvm.service.ConnectivityChecker


/**
 * Created by irfanirawansukirman on 26/01/18.
 */

open class BaseActivity : AppCompatActivity() {

    lateinit var mActivity: AppCompatActivity
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActivity = this

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scheduleConnectivityCheckerJob()
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun scheduleConnectivityCheckerJob() {
        val myJob = JobInfo.Builder(0, ComponentName(this, ConnectivityChecker::class.java))
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build()

        val jobScheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jobScheduler.schedule(myJob)
    }

    fun dialogBack() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_back)
        dialog.setCanceledOnTouchOutside(false)
        val button_ok = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        button_ok.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        val button_cancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        button_cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun showRationaleDialog(messageResId: String, request: PermissionRequest) {
        AlertDialog.Builder(this, R.style.MyDialogTheme)
                .setPositiveButton("Allow") { _, _ -> request.proceed() }
                .setCancelable(false)
                .setMessage(messageResId)
                .show()
    }

    fun showNeverAskAgainDialog(messageResId: String){
        AlertDialog.Builder(this, R.style.MyDialogTheme)
                .setPositiveButton("Ok") { _, _ ->
                    val intent = Intent()
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.setData(uri)
                    startActivity(intent) }
                .setCancelable(false)
                .setMessage(messageResId)
                .show()
    }


    fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    companion object {

        const val CONNECTIVITY_CHANGE = "connectivity_change"

        const val NOTIFICATION = "notification"

    }

}