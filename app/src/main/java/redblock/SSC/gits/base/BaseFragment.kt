package redblock.SSC.gits.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.databinding.ObservableArrayList
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import permissions.dispatcher.PermissionRequest
import redblock.SSC.gits.R
import redblock.SSC.gits.data.model.DropDownModel
import redblock.SSC.gits.data.model.PicModel
import redblock.SSC.gits.data.model.UnitTypeModel
import redblock.SSC.gits.data.model.local.HideDropDownModel
import redblock.SSC.gits.mvvm.login.LoginActivity
import redblock.SSC.gits.mvvm.settings.SettingsViewModel
import redblock.SSC.gits.mvvm.updateproblem.problem.classification.PicItemAdapter
import redblock.SSC.gits.util.DateHelper
import redblock.SSC.gits.util.ImageHelper
import redblock.co.gits.gitsdriver.utils.GitsHelper
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by irfanirawansukirman on 26/01/18.
 */

open class BaseFragment : Fragment() {

    lateinit var mActivity: BaseActivity
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActivity = (activity as BaseActivity)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity)

        val strictPolice = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(strictPolice)

    }

    fun showRationaleDialog(messageResId: String, request: PermissionRequest) {
        AlertDialog.Builder(requireContext(), R.style.MyDialogTheme)
                .setPositiveButton("Ok") { _, _ -> request.proceed() }
                .setCancelable(false)
                .setMessage(messageResId)
                .show()
    }

    fun showNeverAskAgainDialog(messageResId: String){
        AlertDialog.Builder(requireContext(), R.style.MyDialogTheme)
                .setPositiveButton("Ok") { _, _ ->
                    val intent = Intent()
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", activity?.packageName, null)
                    intent.setData(uri)
                    startActivity(intent) }
                .setCancelable(false)
                .setMessage(messageResId)
                .show()
    }

    fun toast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    fun hideShowKeyboard(hide: Boolean) {
        if (view != null) {
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (hide) {
                imm.hideSoftInputFromWindow(view?.windowToken, 0)
            } else {
                imm.showSoftInput(view!!, 0)
            }
        }
    }

    fun dialogNoInternet() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_no_internet_connection)
        dialog.setCanceledOnTouchOutside(false)
        val button = dialog.findViewById<Button>(R.id.btn_dialog)
        button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun dialogPreviewImage(path: String) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_preview_image)
        dialog.setCancelable(true)
        val imagePreview = dialog.findViewById<ImageView>(R.id.img_preview)

        // Start tambahan untuk loading glide
        val circularProgressDrawable = CircularProgressDrawable(requireContext())
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.setColorFilter(ContextCompat.getColor(requireContext(), android.R.color.white), PorterDuff.Mode.SRC_IN)
        circularProgressDrawable.start()
        // End tambahan untuk loading glide

        val requestOptions = RequestOptions()
                .override(ImageHelper.dpToPx(360, imagePreview.context), ImageHelper.dpToPx(360, imagePreview.context))
                .fitCenter()
//                .transform(RotateTransformation(requireContext(), 90f))
                .timeout(30000)
                .error(R.color.greyBackgroundDefault)
                .placeholder(circularProgressDrawable)
                .diskCacheStrategy(DiskCacheStrategy.NONE)

        val link = if (path.contains("/")) {
            Uri.fromFile(File(path))
        } else {
            GitsHelper.Const.BASE_IMAGE_URL + path
        }
        Glide.with(requireContext())
                .load(link)
                .apply(requestOptions)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        imagePreview.layoutParams.height = resource?.intrinsicHeight ?: 0
                        return false
                    }
                })
                .into(imagePreview)

        dialog.show()
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    fun dialogLogout(mViewModel: SettingsViewModel?) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_back)
        dialog.setCanceledOnTouchOutside(false)

        val subject = dialog.findViewById<TextView>(R.id.subject)
        subject.text = getString(R.string.text_validation_logout)
        dialog.findViewById<TextView>(R.id.subTitle).visibility = View.GONE

        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            mViewModel?.logout()
            dialog.dismiss()
            requireActivity().finishAffinity()
            LoginActivity.startActivity(requireContext())
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun deleteOldData(mViewModel: SettingsViewModel?) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_back)
        dialog.setCanceledOnTouchOutside(false)

        val subject = dialog.findViewById<TextView>(R.id.subject)
        subject.text = getString(R.string.text_validation_delete_old_data)
        dialog.findViewById<TextView>(R.id.subTitle).visibility = View.GONE

        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        val llSubject = dialog.findViewById<LinearLayout>(R.id.llDialog)
        val progressBar = dialog.findViewById<ProgressBar>(R.id.progress_bar)
        val statusDelete = false

        buttonOk.setOnClickListener {
            if(statusDelete == false) {
                statusDelete == true
                llSubject.visibility = View.INVISIBLE
                progressBar.visibility = View.VISIBLE
                buttonOk.visibility = View.GONE
                buttonCancel.visibility = View.GONE
                buttonCancel.isClickable = false
                mViewModel?.deleteOldImage(requireContext())

                Handler().postDelayed({
                    dialog.dismiss()
                    toast("Data lama berhasil dihapus")
                }, 2000)
            } else {
                dialog.dismiss()
            }
        }
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    fun dialogPopupReason(reason: String) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_upload_error)
        dialog.setCanceledOnTouchOutside(false)
        val txtTitle = dialog.findViewById<TextView>(R.id.txtTitle)
        txtTitle.text = getString(R.string.text_masalah_ditolak)
        val txtContent = dialog.findViewById<TextView>(R.id.txtContent)
        txtContent.text = reason
        txtContent.visibility = if (reason.contains("null")) {
            View.GONE
        } else {
            View.VISIBLE
        }
        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.visibility = View.GONE
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun dialogUploadError() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_upload_error)
        dialog.setCanceledOnTouchOutside(false)
        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun dialogUploadSuccess() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_upload_success)
        dialog.setCanceledOnTouchOutside(true)

        dialog.show()
    }

    fun dialogDatePicker(editText: EditText, date: String) {

        var tmonth: Int = 0
        var tyear: Int = 0
        if(!date.equals("")&&!date.equals("Pilih bulan")) {
            val tdate = date.split("/")
            tmonth = tdate[0].toInt()
            tyear = tdate[1].toInt()
        }
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_month_picker)
        dialog.setCanceledOnTouchOutside(false)

        val date = Date()
        val calendar = GregorianCalendar()
        calendar.time = date
        calendar.add(Calendar.MONTH, 1)
        val nowMonth = calendar.get(Calendar.MONTH)
        val nowYear = calendar.get(Calendar.YEAR)

        val monthPicker = dialog.findViewById<NumberPicker>(R.id.month)
        monthPicker.maxValue = 12
        monthPicker.minValue = 1
        if(tmonth != 0) monthPicker.value = tmonth
        else monthPicker.value = nowMonth + 1

        val yearPicker = dialog.findViewById<NumberPicker>(R.id.year)
        yearPicker.maxValue = nowYear + 10
        yearPicker.minValue = nowYear - 10
        if(tyear != 0) yearPicker.value = tyear
        else yearPicker.value = nowYear

        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            val setDate = """${String.format("%02d", monthPicker.value)}/${yearPicker.value}"""
            editText.setText(setDate)
            editText.error = null
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()

    }

    fun dropdownView(view: View, array: List<String>) {
        val wrapper = ContextThemeWrapper(requireContext(), R.style.DropdownTheme)
        val popup = PopupMenu(wrapper, view, Gravity.END)
        for (e in array) {
            popup.menu.add(e).title = e
        }
        popup.setOnMenuItemClickListener {
            if (it != null) {
                if (view is TextView) {
                    view.text = it.title
                }
                if (view is EditText) {
                    view.error = null
                    view.setText(it.title)
                }
            }
            true
        }

        popup.show()
    }

    fun dropdownViewClassification(arrayHideData: ObservableArrayList<HideDropDownModel>, editText: EditText, editTextId: EditText, textView: TextView, array: List<DropDownModel>) {
        val wrapper = ContextThemeWrapper(requireContext(), R.style.DropdownTheme)
        var popup = PopupMenu(wrapper, editText, Gravity.END)
        val description = hashMapOf<String, String>()
        if(arrayHideData.size != 0) {
            val arrayData = ArrayList<DropDownModel>()
            for (i in array){
                arrayData.add(DropDownModel(i.id, i.title, i.description))
                for(s in arrayHideData){
                    if(s.title.equals(i.title, true))
                        arrayData.remove(DropDownModel(i.id, i.title, i.description))
                }
            }

            for (e in arrayData) {
                popup.menu.add(1, e.id, e.id, e.title).title = e.title
                description[e.title] = e.description
            }
        }else {
            for (e in array) {
                popup.menu.add(1, e.id, e.id, e.title).title = e.title
                description[e.title] = e.description
            }

        }

        popup.setOnMenuItemClickListener {
            if (it != null) {
                editText.setText(it.title)
                editTextId.setText(it.itemId.toString())
                editText.error = null
                textView.text = description[it.title]
            }
            true
        }

        popup.show()
    }

    fun dropdownViewUnit(editText: EditText, editTextId: EditText, array: List<UnitTypeModel>) {
        val wrapper = ContextThemeWrapper(requireContext(), R.style.DropdownTheme)
        val popup = PopupMenu(wrapper, editText, Gravity.END)
        for (e in array) {
            popup.menu.add(1, e.id ?: 0, e.id ?: 0, e.unit).title = e.unit
        }

        popup.setOnMenuItemClickListener {
            if (it != null) {
                editText.setText(it.title)
                editText.error = null
                editTextId.setText(it.itemId.toString())
            }
            true
        }

        popup.show()
    }

    @SuppressLint("InflateParams")
    fun dropdownViewPic(editText: EditText, array: List<PicModel>) {
        val dialog = Dialog(requireContext())
        val inflater = requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_pic, null)

        dialog.setContentView(view)
        dialog.setCanceledOnTouchOutside(false)

        val arrayPic = arrayListOf<String>()
        if (editText.text.toString().trim().isNotEmpty()) {
            for (pic in editText.text.toString().split(",")) {
                arrayPic.add(pic.trim())
            }
        }

        val arrayTemp = arrayListOf<PicModel>()
        for (pic in array) {
            if (pic != null) {
                arrayTemp.add(
                        PicModel(
                                pic.id,
                                pic.name,
                                pic.created_at,
                                pic.updated_at,
                                arrayPic.contains(pic.name ?: "")
                        )
                )
            }
        }

        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.apply {
            itemAnimator?.changeDuration = 0
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            adapter = PicItemAdapter(ArrayList(0), object : PicItemAdapter.PicClickListener {
                override fun onItemClick(picModel: PicModel) {
                    val isSelected = if (arrayPic.contains(picModel.name)) {
                        arrayPic.removeAt(arrayPic.indexOf(picModel.name))
                        false
                    } else {
                        arrayPic.add(picModel.name ?: "")
                        true
                    }
                    (recyclerView.adapter as PicItemAdapter).setStatusSelected(isSelected, picModel)
                }

            })
            layoutManager = LinearLayoutManager(context)
            (adapter as PicItemAdapter).replaceList(arrayTemp)
        }

        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            editText.setText("")
            var picText = ""
            for ((index, pic) in arrayPic.withIndex()) {
                val text = if (index != arrayPic.lastIndex) {
                    "$pic,"
                } else {
                    pic
                }
                picText += text
            }
            editText.setText(picText)
            editText.error = null
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }
        val buttonKosongkan = dialog.findViewById<Button>(R.id.btn_dialog_kosongkan)
        buttonKosongkan.setOnClickListener{
            editText.setText("")
            dialog.dismiss()
        }

        dialog.show()
    }

    fun validateMonthFilter(view: View, fromDate: String, tillDate: String): Boolean {
        return if (fromDate.contains("/") && tillDate.contains("/")) {
            val fdate = fromDate.split("/")
            val fmonth = fdate[0].toInt()
            val fyear = fdate[1].toInt()
            val tdate = tillDate.split("/")
            val tmonth = tdate[0].toInt()
            val tyear = tdate[1].toInt()
            if ((((fmonth <= tmonth) && (fyear <= tyear)) || (fmonth >= tmonth) && (fyear <= tyear))) {
                true
            } else {
                toast(getString(R.string.text_date_validation_filter))
                (view as EditText).setText(getString(R.string.text_select_month))
                false
            }
        } else {
            false
        }
    }

    fun dateValidation(startDate: EditText, endDate: EditText): Boolean {
        val fromDate = startDate.text.toString()
        val tillDate = endDate.text.toString()
        val month = DateHelper.getMonth()
        val year = DateHelper.getYear()
        return if (fromDate.contains("/") && tillDate.contains("/")) {
            val tdate = tillDate.split("/")
            val tmonth = tdate[0].toInt()
            val tyear = tdate[1].toInt()
            val fdate = fromDate.split("/")
            val fmonth = fdate[0].toInt()
            val fyear = fdate[1].toInt()
            if (fyear < year || (fmonth < month && year == fyear)) {
                toast(getString(R.string.text_harus_lebih_dari_sekarang))
                startDate.setText(getString(R.string.text_select_month))
//                endDate.setText(getString(R.string.text_select_month))
                false
            } else {
                if (tyear < fyear || (tmonth < fmonth && tyear == fyear)) {
                    toast(getString(R.string.text_date_validation))
                    endDate.setText(getString(R.string.text_select_month))
                    false
                } else {
                    true
                }
            }
        } else if (fromDate.contains("/")) {
            val fdate = fromDate.split("/")
            val fmonth = fdate[0].toInt()
            val fyear = fdate[1].toInt()
            if (fyear < year || (fmonth < month && year == fyear)) {
                toast(getString(R.string.text_harus_lebih_dari_sekarang))
                startDate.setText(getString(R.string.text_select_month))
                false
            } else {
                true
            }
        } else {
            false
        }
    }

    fun dateDetailValidation(view: View, tillDate: String): Boolean {
        return if (tillDate.contains("/")) {
            val fmonth = DateHelper.getMonth()
            val fyear = DateHelper.getYear()
            val tdate = tillDate.split("/")
            val tmonth = tdate[0].toInt()
            val tyear = tdate[1].toInt()
            if (tyear < fyear || (tmonth < fmonth && tyear == fyear)) {
                toast(getString(R.string.text_harus_lebih_dari_sekarang))
                (view as EditText).setText(getString(R.string.text_select_month))
                false
            } else {
                true
            }
        } else {
            false
        }
    }

    fun dialogPopupImei(imei: String) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_upload_error)
        dialog.setCanceledOnTouchOutside(false)
        val txtTitle = dialog.findViewById<TextView>(R.id.txtTitle)
        txtTitle.text = getString(R.string.text_imei_not_found)
        val txtContent = dialog.findViewById<TextView>(R.id.txtContent)
        txtContent.setTextIsSelectable(true)
        txtContent.text = getString(R.string.text_device_imei_not_found, imei)
        val buttonOk = dialog.findViewById<Button>(R.id.btn_dialog_ok)
        buttonOk.setOnClickListener {
            dialog.dismiss()
        }
        val buttonCancel = dialog.findViewById<Button>(R.id.btn_dialog_cancel)
        buttonCancel.visibility = View.GONE
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

}