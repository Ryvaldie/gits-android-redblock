package redblock.SSC.gits.base

import com.google.gson.annotations.SerializedName

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

data class BaseApiModel<T>(
        @SerializedName("code") val code: Int,
        @SerializedName("message") val message: String,
        @SerializedName("data") val data: T? = null
)
